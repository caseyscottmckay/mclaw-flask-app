from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, g, \
    jsonify, current_app
from flask_login import current_user, login_required,  login_user
from flask_babel import _, get_locale
import os
from os import path
import json
from app import db
from app.main.forms import product_forms
from app.main.forms.general_forms import EditProfileForm, EmptyForm, PostForm, SearchForm, \
    MessageForm,   ContactForm, OrderForm
from app.models import Document, User, Post, Message, Notification, Product, Order, UserProductFormData, ContactMessage, Professional, Analytics, Practice, Industry, Service
from app.translate import translate
from app.main import bp
from app.email import send_email
import stripe
from app.mcdoc.run_docx import make_users_docx_and_pdf
from pathlib import Path
import requests
import markdown
import random
from sqlalchemy.sql import func

@bp.before_app_request
def before_request():
    run_request_analytics(request,
                          current_user)  # TODO move analytics to after request to increase user speed
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()
    g.locale = str(get_locale())


def run_request_analytics(request, current_user):
    current_path = request.path
    if current_path.startswith('/static/') or current_path.startswith('/favicon'):
        return
    headers = dict(request.headers)
    request_ip = request.headers.get('X-Forwarded-For', request.remote_addr)

    domain = None
    header_cookie = None
    if 'Host' in headers:
        domain = headers['Host']
    if 'Cookie' in headers:
        header_cookie = headers['Cookie']
    cookies = {}
    if header_cookie:
        cookie_parts = header_cookie.split(';')
        for cookie in cookie_parts:
            cookie = cookie.strip()
            field_and_value = cookie.split('=')
            if len(field_and_value) == 2:
                field = field_and_value[0]
                value = field_and_value[1]
                cookies[field]=value
    #ip_exists = Analytics.query.filter_by(ip=request_ip).first() is not None
    ga_cookie = None
    if cookies and '_ga' in cookies:
        ga_cookie = cookies['_ga']

    #

    #TODO find better candidate for unique_identifier (cannot use ga_cookie without google)
    analytics = Analytics.query.filter_by(ip=request_ip).first()
    user_email = None
    ip_info = None
    if current_user.is_authenticated:
        user_email = current_user.email

    if analytics is None or analytics.ip_info is None:
        ip_info =requests.get(url='https://ipinfo.io/'+request_ip).json()
    analytics = Analytics(domain=domain, ip=request_ip,
                          url=current_path, user_email=user_email,
                          ga_cookie=ga_cookie,
                          headers=headers or '', ip_info=ip_info or '')
    db.session.add(analytics)
    db.session.commit()


@bp.route('/', methods=['GET', 'POST'])
def index():
    featured_products = Product.query.filter_by(featured=True)
    if request.method == "POST":
        send_contact_message(request=request)
        return redirect(url_for('main.index'))
    page = request.args.get('page', 1, type=int)
    articles = Document.query.order_by(func.random()).paginate(
        page=page, per_page=current_app.config['POSTS_PER_PAGE'], error_out=False)
    
    next_url = url_for('main.index', page=articles.next_num) \
        if articles.has_next else None
    prev_url = url_for('main.index', page=articles.prev_num) \
        if articles.has_prev else None

    return render_template('index.html', page_title=_('Home'),
                         next_url=next_url,
                           prev_url=prev_url, articles=articles.items, featured_products=featured_products)


def send_contact_message(request):

    form = request.form
    message=form.get('message')
    if not message or len(message) <= 15:
        print('INFO:: routes.send_contact_message(): suspected bot, not saving message. request: ' +str(request.form))
        return

    contact_message = ContactMessage(name=form.get('name'), email=form.get('email'), phone=form.get('phone'), category=form.get('category'),message=message)
    db.session.add(contact_message)
    db.session.commit()
    message_text ='<p>Dear '+ form.get('name') + '</p><p>Thank you for contacting MC Law, PLLC about the following legal issues: ' + message +'</p><p>An MC Law representative will contact you soon to further discuss you legal issues.</br></br>MC Law</br>'+ current_app.config['ADMINS'][0] +' </p>'

    message_html= message_text
    send_email(_('MC Law Contact Inquiry'),
               sender=current_app.config['ADMINS'][0],
               recipients=[form.get('email')],
               text_body=message_text,
               html_body=message_html,
               attachments=None)
    print('INFO:: routes.send_contact_message(): message committed and sent. request: '+ str(request.form))
    flash(_('Your message was sent!'))



@bp.route('/contact', methods=['GET', 'POST'])
def contact():
    form = ContactForm()
    if form.validate_on_submit():
        contact_message = ContactMessage(name=form.name.data, email=form.email.data, phone=form.phone.data, category=form.category.data,message=form.message.data)
        db.session.add(contact_message)
        db.session.commit()

        message_body = '<p>Client Email: ' + form.email.data +"</p><p>"+form.message.data+"</p>"
        send_email(_('MC Law Client Contact Message'),
               sender=current_app.config['ADMINS'][0],
               recipients=['info@mclaw.io'],
               text_body=message_body,
               html_body=message_body,
               attachments=None)

        flash(_('Your message was sent and one of our legal experts will contact you soon.'))
        return redirect(url_for('main.contact'))
    return render_template('contact.html', page_title=_('Contact'), page_subtitle=_('We Love Hearing from our Clients'), form = form)

@bp.route('/industries', defaults={'slug': None})
@bp.route('/industries/<slug>')
@bp.route('/practices', defaults={'slug': None})
@bp.route('/practices/<slug>')
@bp.route('/services', defaults={'slug': None})
@bp.route('/services/<slug>')
def services(slug):
    practices = Practice.query.all()
    print('length of practices in database: ' +str(len(practices)))
    if len(practices) == 0:
        index_practices()
    industries = Industry.query.all()
    print('length of industries in database: ' +str(len(industries)))
    if len(industries) == 0:
        index_industries()
    services = Service.query.all()
    print('length of services in database: ' +str(len(services)))
    if len(services) == 0:
        index_services()
    featured_products = Product.query.filter_by(featured=True)
    #news=Document.query.filter_by(document_type='news')
    #articles=Document.query.filter_by(document_type='article')
    #contacts=Professional.query.filter_by(email='casey@mclaw.io')
    for s in practices:
        if slug == s.slug:
            return render_template('service.html', page_title=_(s.title), page_subtitle=_('Practices'), service=s, featured_products=featured_products)
        subcats = s.subcategories
        for subcat in subcats:
            if slug == subcat['slug']:
                return render_template('service.html', page_title=_(subcat['title']), page_subtitle=_('Practices'), service=subcat, featured_products=featured_products)
    for s in industries:
        if slug == s.slug:
            return render_template('service.html', page_title=_(s.title), page_subtitle=_('Industries'), service=s, featured_products=featured_products)
    for s in services:
        if slug == s.slug:
            return render_template('service.html', page_title=_(s.title), page_subtitle=_('Services'), service=s, featured_products=featured_products)

    return render_template('services.html', page_title=_('Services'), page_subtitle=_('A Full Range of Business Legal Practices'), industries=industries, practices=practices, services=services, featured_products=featured_products)


@bp.route('/blog')
def blog():
    page = request.args.get('page', 1, type=int)
    #documents = Document.query.order_by(Document.timestamp.desc()).paginate(page, current_app.config['POSTS_PER_PAGE'], False)
    posts = Document.query.order_by(Document.timestamp.desc()).paginate(
        page=page, per_page=current_app.config['POSTS_PER_PAGE'], error_out=False)
    next_url = url_for('main.blog', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.blog', page=posts.prev_num) \
        if posts.has_prev else None
    return render_template('blog.html', page_title=_('Blog'), page_subtitle=_('MC Law on Law'), documents=posts.items, next_url=next_url,                                                                                                   prev_url=prev_url)

@bp.route('/professionals', defaults={'slug': None})
@bp.route('/professionals/<slug>')
def professionals(slug):

    ### TODO all this is bad and should go in crawler
    prof = Professional.query.all()
    if len(prof) ==0:
        current_dir = os.getcwd()
        professionals_file = open(str(current_dir)+'/data/website-data/professionals.json')
        professionals_json = json.load(professionals_file)
        professionals_file.close()

        for k,p in professionals_json.items():

           prof = Professional(email=p['email'],slug=k, full_name=p['full_name'], phone_number=p['phone'], about=p['introduction'], practices=p['practices'],publications=p['publications'],experience='lorum ispsum about experince',credentials=json.dumps(p['credentials']), news = p['news'])
           db.session.add(prof)
           prof = None
        db.session.commit()

    if slug:
        articles=Document.query.order_by(func.random()).all()
        professional = Professional.query.filter_by(slug=slug).first_or_404()
        return render_template('professional.html',page_title=_(professional.full_name), professional=professional, articles=articles)
    else:
        professionals = Professional.query.order_by(Professional.timestamp.desc()).paginate(
            page=1, per_page=10, error_out=False)
        return render_template('professionals.html',page_title=_('Professionals'),professionals=professionals.items)





@bp.route('/post')
def post():
    post = Post.query(Post.title).filter(Post.id == 1).first()
    return render_template('post.html', page_title=_('Post'), page_subtitle=_('Blog post subtitle'), post = post)

@bp.route('/explore')
@login_required
def explore():
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(
        page=page, per_page=current_app.config['POSTS_PER_PAGE'], error_out=False)
    next_url = url_for('main.explore', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.explore', page=posts.prev_num) \
        if posts.has_prev else None
    return render_template('index.html', page_title=_('Explore'), page_subtitle=_('Explore MC Law'),
                           posts=posts.items, next_url=next_url,
                           prev_url=prev_url)




@bp.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    posts = user.posts.order_by(Post.timestamp.desc()).paginate(
        page=page, per_page=current_app.config['POSTS_PER_PAGE'], error_out=False)
    next_url = url_for('main.user', username=user.username,
                       page=posts.next_num) if posts.has_next else None
    prev_url = url_for('main.user', username=user.username,
                       page=posts.prev_num) if posts.has_prev else None
    form = EmptyForm()
    return render_template('user.html', user=user, posts=posts.items,
                           next_url=next_url, prev_url=prev_url, form=form)


@bp.route('/user/<username>/popup')
@login_required
def user_popup(username):
    user = User.query.filter_by(username=username).first_or_404()
    form = EmptyForm()
    return render_template('user_popup.html', user=user, form=form)


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash(_('Your changes have been saved.'))
        return redirect(url_for('main.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', page_title=_('Edit Profile'),
                           form=form)


@bp.route('/follow/<username>', methods=['POST'])
@login_required
def follow(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=username).first()
        if user is None:
            flash(_('User %(username)s not found.', username=username))
            return redirect(url_for('main.index'))
        if user == current_user:
            flash(_('You cannot follow yourself!'))
            return redirect(url_for('main.user', username=username))
        current_user.follow(user)
        db.session.commit()
        flash(_('You are following %(username)s!', username=username))
        return redirect(url_for('main.user', username=username))
    else:
        return redirect(url_for('main.index'))


@bp.route('/unfollow/<username>', methods=['POST'])
@login_required
def unfollow(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=username).first()
        if user is None:
            flash(_('User %(username)s not found.', username=username))
            return redirect(url_for('main.index'))
        if user == current_user:
            flash(_('You cannot unfollow yourself!'))
            return redirect(url_for('main.user', username=username))
        current_user.unfollow(user)
        db.session.commit()
        flash(_('You are not following %(username)s.', username=username))
        return redirect(url_for('main.user', username=username))
    else:
        return redirect(url_for('main.index'))


@bp.route('/translate', methods=['POST'])
@login_required
def translate_text():
    return jsonify({'text': translate(request.form['text'],
                                      request.form['source_language'],
                                      request.form['dest_language'])})





@bp.route('/send_message/<recipient>', methods=['GET', 'POST'])
@login_required
def send_message(recipient):
    user = User.query.filter_by(username=recipient).first_or_404()
    form = MessageForm()
    if form.validate_on_submit():
        msg = Message(author=current_user, recipient=user,
                      body=form.message.data)
        db.session.add(msg)
        user.add_notification('unread_message_count', user.new_messages())
        db.session.commit()
        flash(_('Your message has been sent.'))
        return redirect(url_for('main.user', username=recipient))
    return render_template('send_message.html', page_title=_('Send Message'),
                           form=form, recipient=recipient)


@bp.route('/messages')
@login_required
def messages():
    current_user.last_message_read_time = datetime.utcnow()
    current_user.add_notification('unread_message_count', 0)
    db.session.commit()
    page = request.args.get('page', 1, type=int)
    messages = current_user.messages_received.order_by(
        Message.timestamp.desc()).paginate(
            page=page, per_page=current_app.config['POSTS_PER_PAGE'], error_out=False)
    next_url = url_for('main.messages', page=messages.next_num) \
        if messages.has_next else None
    prev_url = url_for('main.messages', page=messages.prev_num) \
        if messages.has_prev else None
    return render_template('messages.html', messages=messages.items,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/export_posts')
@login_required
def export_posts():
    if current_user.get_task_in_progress('export_posts'):
        flash(_('An export task is currently in progress'))
    else:
        current_user.launch_task('export_posts', _('Exporting posts...'))
        db.session.commit()
    return redirect(url_for('main.user', username=current_user.username))


@bp.route('/notifications')
@login_required
def notifications():
    since = request.args.get('since', 0.0, type=float)
    notifications = current_user.notifications.filter(
        Notification.timestamp > since).order_by(Notification.timestamp.asc())
    return jsonify([{
        'name': n.name,
        'data': n.get_data(),
        'timestamp': n.timestamp
    } for n in notifications])


@bp.route('/legal/<string:slug>')
def legal(slug):
    slug = request.view_args['slug']
    #title = 'Legal' + '--' + basic_utils.titleify(slug)
    return render_template("legal/" + slug + '.html', title=_(slug))


@bp.route('/sitemap')
def sitemap():
    title = 'Sitemap'
    return render_template("sitemap/sitemap.xml", title=_(title))


@bp.route('/robots')
def robots():
    title = 'robots'
    return render_template("robots.txt", title=_(title))



@bp.route('/documents', defaults={'slug': None}, methods=['GET', 'POST'])
@bp.route('/documents/<string:slug>', methods=['GET', 'POST'])
@bp.route('/publications', defaults={'slug': None}, methods=['GET', 'POST'])
@bp.route('/publications/<string:slug>', methods=['GET', 'POST'])
def documents(slug):
    documents=Document.query.order_by(func.random()).all()
    if len(documents) ==0:
        index_documents('articles')
        index_documents('news')
    if slug:
        document=Document.query.filter_by(slug=slug).first()
        return render_template('document.html',page_title=_(document.title), document=document, documents=documents)
    if request.method=='POST':
        documents = Document.query.filter(Document.title.like('%'+request.values.get('q')+'%')).all()
        if len(documents) == 0:
            documents=Document.query.order_by(func.random()).all()
    return render_template("documents.html", page_title=_('Documents'), documents=documents)




@bp.route('/products', defaults={'slug': None},)
@bp.route('/products/', defaults={'slug': None},)
@bp.route('/products/<string:slug>')
def products(slug):

    products = Product.query.all()
    if len(products) == 0:
        index_products()
    featured_products = Product.query.filter_by(featured=True).all()

    product_pdf_preview_url =url_for('static', filename='mclawassets/images/product_images/default_product_pdf_preview.pdf')
    if slug:
        product = Product.query.filter_by(slug=slug).first_or_404()
        product_pdf_preview_url_temp = url_for('static', filename='mclawassets/images/product_images/'+product.slug+'.pdf')

        if path.exists(os.getcwd()+'/app'+product_pdf_preview_url_temp):
            product_pdf_preview_url = product_pdf_preview_url_temp
        '''
        product_image_url = 'mclawassets/images/product_images/' + slug+'.png'
        if not path.exists(os.getcwd()+'/app/static/'+product_image_url):
            random_image_path=str(get_random_image(category='general'))
            n = 2
            groups = random_image_path.split('/')
            product_image_url=['/'.join(groups[:n]), '/'.join(groups[n:])][1]
        product.image_url = product_image_url
        '''
        return render_template('product.html',page_title=_(product.title), product=product, featured_products=featured_products, product_pdf_preview_url=product_pdf_preview_url)
    return render_template("products.html", page_title=_('Virtual Legal Products'), featured_products=featured_products, products=products)


def index_products():
    current_dir = os.getcwd()
    products_file = open(str(current_dir)+'/data/website-data/products.json')
    products_json = json.load(products_file)
    products_file.close()
    products = products_json['products']
    for p in products:

        product = Product(title=p['title'], slug=p['slug'],price=p['price'], automated=p['automated'], published=p['published'], featured=p['featured'], text=p['text'], html=p['html'], date_created=p['date_created'], date_modified=p['date_modified'],fees=p['fees'],category=p['category'],subcategory=p['subcategory'], tags=p['tags'],  jurisdiction=p['jurisdiction'],state=p['state'],image_path=p['image_path'], description=p['description'],notes=p['notes'], faqs=p['faqs'], related_products=p['related_products'],form_fields=p['form_fields'])
        db.session.add(product)
        db.session.commit()



@bp.route('/search', defaults={'query': None},)
@bp.route('/search/', defaults={'query': None},)
@bp.route('/search/<string:query>')
def search(query):

    return render_template("search.html", page_title=_('Search'))



@bp.route('/consultation/<slug>', methods=['POST', 'GET'])
#@login_required
def consultation(slug):
    product = Product.query.filter_by(slug=slug).first_or_404()
    obj = None
    form = get_product_form(slug, obj)
    referrer = request.referrer
    request_ip = request.headers.get('X-Forwarded-For', request.remote_addr)
    product_pdf_preview_url='/static/mclawassets/images/product_images/'+ product.slug+'.pdf'

    form = get_product_form(slug, None)
    print(form)

    upfdid = request.values.get('upfdid')
    if upfdid and not form.validate_on_submit():
        user_product_form_data = UserProductFormData.query.filter_by(
        id=upfdid).first_or_404()
        current_user_product_form_field_data = json.loads(user_product_form_data.field_data)
        form = get_product_form(slug, user_product_form_data.field_data)
        for field in form:
            if field.name in current_user_product_form_field_data:
                print('Match' + str(field.name)+ str(current_user_product_form_field_data[field.name]))
                field.data = current_user_product_form_field_data[field.name]
        if user_product_form_data.product_download_path_pdf:
            product_pdf_preview_path = user_product_form_data.product_download_path_pdf
            product_pdf_preview_path_temp = "/".join(product_pdf_preview_path.split("/",2)[2:])
            product_pdf_preview_url = url_for('static', filename=product_pdf_preview_path_temp)




    if form.validate_on_submit() and request.method == 'POST':

        upfdid = request.values.get('upfdid')
        user_product_form_data = UserProductFormData.query.filter_by(
            id=upfdid).first_or_404()
        user_product_form_data.user_ip = request_ip
        user_product_form_data.field_data =  json.dumps(request.form.to_dict())
        db.session.commit()
        user_product_form_data_id = user_product_form_data.id
        print('INFO:: consultation() -> saved user_product_form_data for upfdid: '+ str(upfdid)+ ' with old user_product_form_data_id: '+str(user_product_form_data_id))


        if product.automated:
            make_users_docx_and_pdf(upfdid=upfdid, preview=False)

        if user_product_form_data.order_id:
            return redirect(url_for('main.orders', upfdid=upfdid))
        else:
            return redirect(url_for('main.confirmation', upfdid=upfdid))
    elif not upfdid and not form.validate_on_submit():
        if current_user.is_authenticated:
            user_id = current_user.get_id()
            user = User.query.filter_by(id=user_id).first_or_404()
            user_product_form_data = UserProductFormData(product_id=product.id, user_email=user.email, user_id=user.id, user_ip = request_ip, field_data=None)
            db.session.add(user_product_form_data)
            db.session.commit()
            upfdid = user_product_form_data.id
            print('INFO:: consultation() -> saving registered user visit: upfdid: '+ str(upfdid))
        else:

            user_product_form_data = UserProductFormData(product_id=product.id, user_ip=request_ip,field_data=None)
            db.session.add(user_product_form_data)
            db.session.commit()
            upfdid = user_product_form_data.id
            print('INFO:: consultation() -> saving unregistered user visit: upfdid: '+ str(upfdid))
        #if not product.automated:
        #    return redirect(url_for('main.checkout', upfdid=user_product_form_data_id))
    return render_template("consultation.html", page_title = product.title, product=product, form=form, upfdid=upfdid,product_pdf_preview_url=product_pdf_preview_url)


def get_product_form(product_slug, obj):

    if product_slug == 'background-check-policy':
        return product_forms.BackgroundCheckPolicy()
    if product_slug == 'copyright-registration':
        return product_forms.CopyrightRegistration()
    if product_slug == 'domain-name-cease-and-desist-letter':
        return product_forms.DomainNameCeaseAndDesistLetter()
    if product_slug == 'dmca-complaint-takedown-notice':
        return product_forms.DmcaComplaint()
    if product_slug == 'jury-duty-leave-policy':
        return product_forms.JuryDutyLeavePolicy()
    if product_slug == 'nepotism-policy':
        return product_forms.NepotismPolicy()
    if product_slug == 'outside-employment-policy':
        return product_forms.OutsideEmploymentPolicy()
    #if product_slug == 'performance-review-policy':
        #return product_forms.PerformanceReviewPolicy()
    if product_slug == 'trademark-registration':
        return product_forms.TrademarkRegistration(obj=obj)
    if product_slug == 'smoke-free-workplace-policy':
        return product_forms.SmokeFreeWorkplacePolicy()
    if product_slug == 'website-privacy-policy':
        return product_forms.WebsitePrivacyPolicy(obj=obj)
    if product_slug == 'workplace-searches-policy':
        return product_forms.WorkplaceSearchesPolicy()
    else:
        return product_forms.DefaultProductRegistrationForm()


@bp.route('/confirmation', methods=['POST', 'GET'] )
#@login_required
def confirmation():
    upfdid = request.values.get("upfdid")
    user_product_form_data = UserProductFormData.query.filter_by(
        id=upfdid).first_or_404()
    product_id = user_product_form_data.product_id
    product = Product.query.filter_by(id=product_id).first_or_404()
    slug = product.slug
    #user_product_form_data.field_data =  json.dumps(request.form.to_dict())
    current_user_product_form_field_data = json.loads(user_product_form_data.field_data)
    print(upfdid)
    print('###1')
    print(user_product_form_data.field_data)
    for k, v in json.loads(user_product_form_data.field_data).items():
        print(k, v)
    print('\n')
    print('###request')
    print(json.dumps(request.form.to_dict()))
    for k, v in request.form.to_dict().items():
        print(k, v)
    print('\n')

   ## print(json.loads(user_product_form_data.field_data).items())
    print('##### productForm: ')

    product_form = get_product_form(slug, obj=user_product_form_data.field_data)
    for field in product_form:
        if field.name in current_user_product_form_field_data:
            print('Match' + str(field.name)+ str(current_user_product_form_field_data[field.name]))
            field.data = current_user_product_form_field_data[field.name]




    if product_form.validate_on_submit() and request.method == 'POST':
        user_product_form_data.field_data =  json.dumps(request.form.to_dict())
        product_form =get_product_form(slug, obj=user_product_form_data.field_data)

        db.session.commit()
        if product.automated:
            make_users_docx_and_pdf(upfdid=upfdid, preview=False)


    product_pdf_preview_url = url_for('static', filename='mclawassets/images/product_images/'+slug+'.pdf')
    if user_product_form_data.product_download_path_pdf:
        product_pdf_preview_path = user_product_form_data.product_download_path_pdf
        product_pdf_preview_path_temp = "/".join(product_pdf_preview_path.split("/",2)[2:])
        product_pdf_preview_url = url_for('static', filename=product_pdf_preview_path_temp)


    return render_template("confirmation.html", page_title = _('Confirmation'), product=product, form_field_data=json.loads(user_product_form_data.field_data), upfdid=upfdid, current_user=current_user, user_product_form_data=user_product_form_data, product_pdf_preview_url=product_pdf_preview_url,form=product_form)


@bp.route('/checkout', methods=['POST', 'GET'])
#@login_required
def checkout():
    user_product_form_data_id = request.values.get("upfdid")
    user_product_form_data = UserProductFormData.query.filter_by(
    id=user_product_form_data_id).first_or_404()
    product_id = user_product_form_data.product_id
    product = Product.query.filter_by(id=product_id).first_or_404()
    order_form = OrderForm()
    stripe.api_key = current_app.config['STRIPE_SECRET_KEY']
    fees = 0
    trademark_classes=[]
    if product.slug == 'trademark-registration':
        field_data=user_product_form_data.field_data

        if field_data:
            field_data_json = json.loads(user_product_form_data.field_data)
            for element in field_data_json:
                if element.startswith('trademark_classes'):
                    trademark_classes.append(field_data_json[element])
        if len(trademark_classes) == 0:
            fees = 350
        else:
            fees = 350 * len(trademark_classes)
    total = (product.price + fees)
    return render_template('checkout.html', page_title=_('Checkout'),  stripe_api_key=current_app.config['STRIPE_PUBLISHABLE_KEY'],
                           product=product,
                           order_form=order_form, user_product_form_data_id=user_product_form_data_id, user=current_user, total=total, fees=fees, trademark_classes=trademark_classes)

@bp.route('/checkout-complete', methods=['POST', 'GET'])
#@login_required
def checkout_complete():
    if not request.method == "POST":
        return redirect(url_for('main.index'))
    user_product_form_data_id = request.values.get("user_product_form_data_id")
    user_product_form_data = UserProductFormData.query.filter_by(
        id=user_product_form_data_id).first_or_404()
    product_id = user_product_form_data.product_id
    product = Product.query.filter_by(id=product_id).first_or_404()
    total = request.values.get("total")
    fees = request.values.get("fees")
    stripe_total =  int(total) * 100


    if current_user.is_authenticated:
        user =User.query.filter_by(id = current_user.id).first_or_404()
        user_id = user.id
        user_product_form_data.user_id = user_id
    else:
        user = User(username=request.values.get("email").split('@')[0], email=request.values.get("email"))
        user.set_password(request.values.get("password"))
        db.session.add(user)
        db.session.commit()
        login_user(user, remember=True)
        user_id = user.id
        user_product_form_data.user_id = user_id
        user_product_form_data.user_email = user.email
    db.session.commit()



    order = Order(user_email=user.email, product_slug=product.slug, fees=fees, total=total,paid=True,user_product_form_data_id=user_product_form_data_id,billing_terms_and_conditions_accepted=True,
                  billing_first_name=request.values.get("billing_first_name"),
                  billing_last_name=request.values.get("billing_last_name"),
                  billing_email=request.values.get("billing_email"),
                  billing_street_address_1=request.values.get("billing_street_address_1"),
                  billing_street_address_2=request.values.get("billing_street_address_2"),

                  billing_phone=request.values.get("billing_phone"),
                  billing_city=request.values.get("billing_city"),
                  billing_state=request.values.get("billing_state"),
                  billing_zip=request.values.get("billing_zip"),
                  billing_order_notes=request.values.get("billing_order_notes"))

    #order = Order(user_id=user.id, total=total,paid=True, product_slug=product.slug, user_product_form_data_id=user_product_form_data_id)
   # user_product_form_data.paid = True

    db.session.add(order)
    db.session.commit()
    user_product_form_data.order_id=order.id
    db.session.add(user_product_form_data)
    db.session.commit()
    flash(_('Checkout Complete: for order ' + str(order.id) + ' and user_product_form_data '+str(user_product_form_data.id)))


    #user = User.query.filter_by(id=current_user.get_id()).first_or_404()

    try:

        customer = stripe.Customer.create(
            email=request.form['stripeEmail'],
            source=request.form['stripeToken']
        )
        stripe.Charge.create(
            customer=customer.id,
            amount=stripe_total,
            currency='usd',
            description='MC Law Charge'
        )
        message_body = '<p>Dear '+ order.billing_first_name + '</br></br> Thank you for ordering your '+product.title+' from MC Law, PLLC. We need to collect some further information from you to complete your '+product.title+' (most consultations take less than 30 minutes). An MC Law representative will contact you soon to gather the information necessary to complete your '+product.title+' and further discuss your legal issues. After you provide your information, our attorneys will draft your document (usually takes less than one week). After completing your '+product.title+', the attorney that prepared your document will contact you to discuss any questions you may have concerning your '+product.title+'.</br></br>MC Law, PLLC</br>Casey Scott McKay</br>Partner</br>1441 U St. NW, Suite 1110</br>Washington DC, D.C. 20009</br>'+ current_app.config['ORDERS_EMAIL'][0]
        send_email(_('MC Law Order No. '+str(order.id)),
                   sender=current_app.config['ORDERS_EMAIL'][0],
                   recipients=[order.billing_email,current_app.config['ORDERS_EMAIL'][0]],
                   text_body=message_body,
                   html_body=message_body,
                   attachments=None)
        #flash(_('Your message was sent!'))
        '''
        send_email(_('MC Law Order Complete'),
                   sender=current_app.config['ADMINS'][0],
                   recipients=[current_user.email, order.billing_email],
                   text_body=render_template('email/order_complete.txt',
                                             user=current_user, billing_user=user,
                                             order=order.__dict__, product=product,
                                             user_product_form_data=user_product_form_data.__dict__),
                   html_body=render_template('email/order_complete.html',
                                             user=current_user, billing_user=user,
                                             order=order.__dict__, product=product,
                                             user_product_form_data=user_product_form_data.__dict__))
        # send email to admin to alert of new order
        send_email(_('MC Law ADMIN ALERT: New Order'),
                   sender=current_app.config['ADMINS'][0],
                   recipients=['casey@mclaw.io', 'csm@csmckay.us'],
                   text_body=render_template('email/new_order_admin_alert.txt',
                                             user=current_user, billing_user=user,
                                             order=order, product=product),
                   html_body=render_template('email/new_order_admin_alert.html',
                                             user=current_user, billing_user=user,
                                             order=order, product=product))
        
        #return render_template('checkout-complete.html', order_id =order.id)
    '''
    except stripe.error.StripeError as e:
        print('ERROR: stripe error-->checkout_complete.html' + str(e))
        #return render_template('errors/500.html')

    return render_template('checkout-complete.html', page_title=_('Checkout Complete'),product=product, user=user, order=order)



@bp.route('/accounts/open-orders', methods=['POST', 'GET'])
@bp.route('/accounts/orders', methods=['POST', 'GET'])
@bp.route('/accounts/<string:user_id>/orders', methods=['POST', 'GET'])
@login_required
def orders():
    rule = request.url_rule
    orders = Order.query.filter_by(user_email=current_user.email, paid=True).all()

    upfdid = request.values.get('upfdid')
    if '/open-orders' in str(rule):
        orders = Order.query.filter_by(user_email=current_user.email,
                                       paid=False).all()

    if not upfdid or upfdid == '':
        orders = Order.query.filter_by(user_email=current_user.email, paid=True).all()
        return render_template('user/orders.html',page_title=_('My Orders'), orders=orders)
    user_id = current_user.get_id()
    #slug = request.args.get("slug")

    user_product_form_data = UserProductFormData.query.filter_by(
        id=upfdid).first_or_404()
    product_id = user_product_form_data.product_id
    product = Product.query.filter_by(id=product_id).first_or_404()
    file_name = product.slug + "_" + upfdid;

    form_field_data = json.loads(user_product_form_data.field_data)
    #del form_field_data['csrf_token']
    #del form_field_data['submit']
    form_field_data['download_available'] = user_product_form_data.download_available

    product_download_path = current_app.config[
                                'PUBLIC_DOWNLOAD_FOLDER'] + 'default_product_preview.pdf'
    print(form_field_data)
    if product.featured == True:
        #make_users_docx_and_pdf(user_product_form_data_id, preview=False)
        #time.sleep(0.5)
        product_download_path = current_app.config[
                                    'PUBLIC_DOWNLOAD_FOLDER'] + 'user/' + user_id + '/upfdid/' + upfdid + '/' + file_name

    return render_template("user/order.html", page_title=_('My Order'), upfdid=upfdid,
                           product=product,
                           product_download_path=product_download_path,
                           form_field_data=form_field_data, user_product_form_data=user_product_form_data)
@bp.route('/accounts/documents')
@bp.route('/accounts/documents/')
@login_required
def account_documents():
    upfdid = request.values.get('upfdid')
    if not upfdid or upfdid == '':
        orders = Order.query.filter_by(user_email=current_user.email, paid=True).all()
        return render_template('user/documents.html',page_title=_('My Documents'), orders=orders)
    user_id = current_user.get_id()
    #slug = request.args.get("slug")

    user_product_form_data = UserProductFormData.query.filter_by(
        id=upfdid).first_or_404()
    product_id = user_product_form_data.product_id
    product = Product.query.filter_by(id=product_id).first_or_404()
    file_name = product.slug + "_" + upfdid;

    form_field_data = json.loads(user_product_form_data.field_data)
    #del form_field_data['csrf_token']
    #del form_field_data['submit']
    form_field_data['download_available'] = user_product_form_data.download_available

    product_download_path = current_app.config[
                                'PUBLIC_DOWNLOAD_FOLDER'] + 'default_product_preview.pdf'
    print(form_field_data)
    if product.featured == True:
        #make_users_docx_and_pdf(user_product_form_data_id, preview=False)
        #time.sleep(0.5)
        product_download_path = current_app.config[
                                    'PUBLIC_DOWNLOAD_FOLDER'] + 'user/' + user_id + '/entry/' + upfdid + '/' + file_name

    return render_template("user/order.html", page_title=_('My Document'), upfdid=upfdid,
                           product=product,
                           product_download_path=product_download_path,
                           form_field_data=form_field_data, user_product_form_data=user_product_form_data)







@bp.route('/accounts/', defaults={'user_id_or_username': None})
@bp.route('/accounts',defaults={'user_id_or_username': None})
@bp.route('/accounts/<string:user_id_or_username>')
@login_required
def account(user_id_or_username):
    user = current_user
    if user_id_or_username:
        if user_id_or_username.isnumeric():
            user = User.query.filter_by(id=user_id_or_username).first_or_404()
        else:
            user = User.query.filter_by(username=user_id_or_username).first_or_404()
    if not user.id == current_user.id:
        return redirect(url_for('auth.logout'))
    return render_template("user/account.html", page_title=_('Account'),
                           user=user)









def index_documents(document_type):
    current_dir = os.getcwd()
    articles_dir = str(current_dir)+'/data/documents/'+document_type
    for filename in os.listdir(articles_dir):
        with open(os.path.join(articles_dir, filename), 'r') as f:
            slug = filename[0:-3]
            document_type=document_type
            content=f.read()
            lines=content.split("\n")
            title = lines[1].split('|')[1].strip()
            author = lines[2].split('|')[1].strip()
            date = lines[3].split('|')[1].strip()
            category = lines[4].split('|')[1].strip()
            subcategory = lines[5].split('|')[1].strip()
            tags = lines[6].split('|')[1].strip()
            summary = lines[7].split('|')[1].strip()
            text = '\n'.join(lines[9:])
            html = markdown.markdown(text)
            document= Document(slug=slug, title=title, document_type=document_type, author=author, category=category,subcategory=subcategory,tags=tags,summary=summary,text=text, html=html)
            db.session.add(document)
            db.session.commit()



def index_practices():
    current_dir = os.getcwd()
    practices_file = open(str(current_dir)+'/data/website-data/practices.json')
    practices_json = json.load(practices_file)
    practices=practices_json['practices']
    practices_file.close()

    for s in practices:

        if 'subcategories' in s:
            subcategories=s['subcategories']
        practice = Practice(title=s['title'], slug=s['slug'], overview=s['overview'], category=s['category'], subcategories=subcategories)
        db.session.add(practice)
    db.session.commit()

def index_industries():
    current_dir = os.getcwd()
    industries_file = open(str(current_dir)+'/data/website-data/industries.json')
    industries_json = json.load(industries_file)['industries']
    industries_file.close()
    for s in industries_json:
        industry = Industry(title=s['title'], slug=s['slug'], overview=s['overview'])
        db.session.add(industry)
    db.session.commit()

def index_services():
    current_dir = os.getcwd()
    services_file = open(str(current_dir)+'/data/website-data/services.json')
    services_json = json.load(services_file)['services']

    services_file.close()
    for s in services_json:
        slug=s['slug']
        slug=slug.replace('\'', '').replace('(', '').replace(')', '').replace(',', '')
        slug=slug.replace(' ', '_')
        image_path  = 'mclawassets/images/services_images/'+slug+'.jpg'
        service = Service(title=s['title'], slug=slug, overview=s['overview'], image_path=image_path,category=s['category'])
        db.session.add(service)
    db.session.commit()


def get_random_image(category):
    # Define the base directory where the pictures are stored
    base_dir = "app/static/mclawassets/images/random_images"
    # Define a dictionary mapping category names to their respective subdirectories
    category_dirs = {
        #"intellectual property": "ip",
        #"commercial": "commercial",
        #"labor and employment": "labor",
        #"litigation": "litigation",
        #"general": "general",
        "copyrights": "copyrights",
        "trademarks": "trademarks",
        "patents": "patents"
    }
    if category.lower() not in category_dirs:
        category_dir = base_dir
        #raise ValueError("Invalid category specified.")
    else:
        category_dir = os.path.join(base_dir, category_dirs[category.lower()])
    picture_files = [f for f in os.listdir(category_dir) if os.path.isfile(os.path.join(category_dir, f))]
    if not picture_files:
        return None
    return os.path.join(category_dir, random.choice(picture_files))