from datetime import datetime

from flask_babel import lazy_gettext as _l
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, validators, \
  SelectField, BooleanField, HiddenField, IntegerField, SelectMultipleField
from wtforms.validators import Optional, Length, NumberRange, DataRequired
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms.widgets import CheckboxInput, ListWidget


class DefaultProductRegistrationForm(FlaskForm):
    email = StringField(_l('Email'),
                        description="We need your email so we can save your information and document.",
                        validators=[DataRequired()])

class WebsitePrivacyPolicy(FlaskForm):
  date_modified = HiddenField("date_modified", default=datetime.today)
  COMPANY_NAME = StringField()
  COMPANY_EMAIL = StringField()
  COMPANY_STREET = StringField()
  COMPANY_STATE = StringField()
  COMPANY_ZIP = StringField()
  DOMAIN_NAME = StringField()
  website_collects_ssn = BooleanField()
  affiliates_and_subsidiaries = BooleanField()
  contest_or_promotion = BooleanField()
  website_collects_data_automatically = SelectField(
    'Does your website collect data automatically (e.g., user-tracking, cookies, web beacons',
    choices=[('yes', 'yes'), ('no', 'no')])
  automatic_data_collection_over_time_and_across_third_party_sites = BooleanField()
  automatic_data_collection_may_include_personal_information = SelectField(
    "Does the automatically collected data include personal information?",
    choices=[('may', 'yes'), ('is statistical data and does not', 'no')])
  third_party_use_of_cookies_and_other_tracking_technologies = SelectField(
    'Does your website allow third-parties to use cookes or other tracking technologies?',
    choices=[('yes', 'yes'), ('no', 'no')])
  use_user_information_to_contact_user = SelectField(
    'Will user information be used to contact the user about your (or third-parties) goods or services (i.e., are you using user information to advertise?',
    choices=[('yes', 'yes'), ('no', 'no')])
  users_may_access_and_correct_information = SelectField(
    'Can website users access and correct their information on your website?',
    choices=[('yes', 'yes'), ('no', 'no')])
  correct_information_on_website = SelectField(
    'Users can access and correct information on the website via their user account.',
    choices=[('yes', 'yes'), ('no', 'no')])
  correct_information_by_email = SelectField(
    'Users can email the company to have their information corrected?',
    choices=[('yes', 'yes'), ('no', 'no')])
  submit = SubmitField('Submit')


class DomainNameCeaseAndDesistLetter(FlaskForm):
  DATE = StringField("Date", default=datetime.today,
                     validators=[validators.DataRequired()],
                     description='Date refers to the effective date of this DMCA Complaint, which is generally the day you send it to the recipient.')
  METHOD_OF_DELIVERY = SelectField('Method of Delivery',
                                   validators=[Optional()],
                                   choices=[('mail', 'mail'),
                                            ('email', 'email'), ('fax', 'fax')],
                                   description='DMCA Complaints are usually sent by email to the Service Provider.',
                                   default="email")
  RECIPIENTS_NAME = StringField('Recipient\'s Name', validators=[Optional()])
  RECIPIENTS_ADDRESS = TextAreaField(u'Recipient\'s Mailing Address',
                                     [validators.optional(),
                                      validators.length(max=200)])
  RECIPIENTS_PHONE = StringField(_l('Recipient\'s Phone'),
                                 validators=[Optional()])
  RECIPIENTS_EMAIL = StringField(_l('Recipient\'s Email'),
                                 validators=[Optional()])
  addressee_name = StringField(label='To whom should this letter be addressed?',
                               default='To Whom it May Concern',
                               description='If you do not know the name of a person, use a title--e.g., President, Director--or something generic like "To Whom it May Concern"',
                               render_kw={
                                 "placeholder": "e.g., To Whom it May Concern"})

  MARK_OWNER = StringField(_l('Mark\'s Owner'), )
  DOMAIN_NAME = StringField(_l('Domain Name'), )
  ASSERTED_MARK = StringField(_l('Asserted Trademark'), )
  TYPE_OF_BUSINESS_OR_ORGANIZATION = StringField(
    _l('Type of business or organization'),
    description='For example, an individual, partnership, limited liability company, corporation, non-profit organization.')
  GOOD_OR_SERVICES = SelectField(
    'Is the trademark used for goods, services, or both?',

    choices=[('goods', 'goods'), ('services', 'services'),
             ('goods and services', 'good and services')],
    description='Does your business sell goods or provide services?',
    default="good and services")
  DESCRIBE_GOODS_OR_SERVICES = StringField(
    _l('Describe the goods or services.'),
    description='e.g., online advertising, perishable goods, restaurant equipment, consulting')
  REGISTERED_TRADEMARK = SelectField(
    'Is the mark registered (or pending registration) with the USPTO?',
     choices=[('registered', 'registered'), (
    'application filed', 'application filed (pending registration)'),
                                      ('no', 'no')], default="no")
  USPTO_REGISTRATION_NUMBER = StringField()
  USPTO_APPLICATION_NUMBER = StringField()
  NUMBER_OF_YEARS = StringField(
    _l('How many years have you used the mark to sell goods or services?.'),
    )
  # infringer_registered_mark= BooleanField()
  # infringer_using_mark=BooleanField()
  # offering_to_sale=BooleanField()
  incorporates_or_is_confusingly_similar_to = SelectField(
    'Does the infringer\'s use of the domain name incorporate your trademark or is it confusingly similar to your trademark?',
     choices=[('incorporates', 'incorporates'), (
    'is confusingly similar to', 'is confusingly similar to')],
    default='is confusingly similar to')
  NUMBER_OF_DAYS = StringField(
    'How many days does the infringer have to correct his action before you (or your lawyer) takes further action?',
    description='Twenty-one (21) days after the date of this letter is enough time for the recipient to take action.',
    default='21')
  submit = SubmitField('Submit')

class CopyrightRegistration(FlaskForm):
    date = StringField("Date", default=datetime.today,
                       validators=[validators.DataRequired()],
                       description='Date refers to the effective date of this DMCA Complaint, which is generally the day you send it to the recipient.')

    copyright_owner_name = StringField('Name of copyright owner.',
                                   validators=[Optional()])
    copyright_owner_address = TextAreaField(u'Copyright Owner Mailing Address',
                                       [validators.optional(),
                                        validators.length(max=200)])
    copyright_owner_phone = StringField(_l('Copyright Owner Phone'),
                                   validators=[Optional()])
    copyright_owner_email = StringField(_l('Copyright Owner Email'),
                                   validators=[Optional()])

    owner_author_publisher = SelectField(
        'Copyright owners relationship to the copyrighted works at issue.',
        validators=[Optional()], choices=[('owner', 'owner'), ('author', 'author'),
                                          ('publisher', 'publisher')],
        default='owner')
    description_of_copyrighted_work = TextAreaField(
        _l('Describe the copyrighted work you want to register.'), validators=[Optional()])

    submit = SubmitField('Submit')
class DmcaComplaint(FlaskForm):
  DATE = StringField("Date", default=datetime.today,
                     validators=[validators.DataRequired()],
                     description='Date refers to the effective date of this DMCA Complaint, which is generally the day you send it to the recipient.')
  METHOD_OF_DELIVERY = SelectField('Method of Delivery',
                                   validators=[Optional()],
                                   choices=[('mail', 'mail'),
                                            ('email', 'email'), ('fax', 'fax')],
                                   description='DMCA Complaints are usually sent by email to the Service Provider.',
                                   default="email")
  SERVICE_PROVIDER = StringField('Name of service provider.',
                                 validators=[Optional()],
                                 description='A service providder can be a website providing, an ISP, or any other service provider infringing on your copyrights.')
  DESIGNATED_AGENT = StringField('Designated Agent', validators=[Optional()],
                                 description="The DMCA safe harbor requires a service provider to: (1) Designate an individual as its copyright agent. (2) Make the agent's name and contact information (including mailing address, telephone number and e-mail address) publicly available on the service provider's website. (3) Register this information with the US Copyright Office.")
  designated_address = TextAreaField(u'Designated Mailing Address',
                                     [validators.optional(),
                                      validators.length(max=200)])
  DESIGNATED_PHONE = StringField(_l('Designated Phone'),
                                 validators=[Optional()])
  DESIGNATED_EMAIL = StringField(_l('Designated Email'),
                                 validators=[Optional()])
  COPYRIGHT_OWNWER = StringField('Name of copyright owner.',
                                 validators=[Optional()])
  owner_author_publisher = SelectField(
    'Copyright owners relationship to the copyrighted works at issue.',
    validators=[Optional()], choices=[('owner', 'owner'), ('author', 'author'),
                                      ('publisher', 'publisher')],
    default='owner')
  DESCRIPTION_OF_COPYRIGHTED_WORK = TextAreaField(
    _l('Describe the copyrighted works at issue.'), validators=[Optional()])
  SENDERS_NAME = StringField('Sender\'s Name', validators=[Optional()])
  SENDERS_ADDRESS = TextAreaField(u'Sender\'s Mailing Address',
                                  [validators.optional(),
                                   validators.length(max=200)])
  SENDERS_PHONE = StringField(_l('Sender\'s Phone'), validators=[Optional()])
  SENDERS_EMAIL = StringField(_l('Sender\'s Email'), validators=[Optional()])
  TITLE_OF_WORK = StringField(_l('Title of work.'), validators=[Optional()])

  COPYRIGHT_REGISTRATION_NUMBER = StringField(
    _l('Copyright registration number.'))
  SERVICE_PROVIDERS_SERVICE_URL = StringField(
    _l('Service Provider\'s service url.'))
  DISPLAYS_PROVIDES_ACCESS_TO_CACHES = SelectField(
    'How is the work presnted on the infringing site?', validators=[Optional()],
    choices=[('displayed', 'displays'),
             ('provides access to', 'provides access to'),
             ('caches', 'caches')])
  DESCRIPTION_OF_INFRINGING_MATERIAL = StringField(
    'Describe the infringing material.',
    description="e.g., Website page hosting an infringing photograph.")
  URL_OF_INFRINGING_MATERIAL = StringField(
    'What is the URL of the infringing content you described in the previous question.',
    description="e.g., https://www.infringing-url.com/infringing-photo.jpg")

  TERMS_OF_USE_COPYRIGHT_POLICY_CODE_OF_CONDUCT = SelectField(
    'What copyright policy does the service provider maintain?',
    validators=[Optional()], choices=[('Terms of Use', 'Terms of Use'),
                                      ('Copyright Policy', 'Copyright Policy'),
                                      ('Code of Conduct', 'Code of Conduct')],
    default="Terms of Use")
  submit = SubmitField('Submit')

class TrademarkRegistration(FlaskForm):
  trademark_type = SelectField(
      'Type of Trademark',
      choices=[('name', 'Name'), ('logo', 'Logo'), ('slogan', 'Slogan'), ('name_and_logo', 'Name and Logo')])
  trademark_name = StringField('Name of Trademark')
  trademark_logo = FileField('Image of Logo', validators=[FileAllowed(['jpg', 'png'], '.jpg or.png')
  ])

  owner_name = StringField('Name of Trademark Owner', description='The name of the individual or entity who will own the trademark rights.')
  owner_entity_type = SelectField('Entity Type of Trademark Owner', choices=[('individual', 'individual'), ('llc', 'llc'), ('corporation', 'corporation')])
  owner_address = StringField('Address of the Trademark Owner', description='Street, City, State, and Zip of Trademark Owner')
  owner_email = StringField('Email for Trademark Owner', description='Your email address will not be publicly available and will only be used to contact you about your trademark.')
  currently_using_mark = SelectField(
      'Are you currently using this trademark?',
      choices=[('yes', 'yes'), ('no', 'no')], description='Choose Yes if this trademark is currently being offered to the public in the country in which you are requesting registration.')
  products_or_services = SelectField(
      'Are you currently using this trademark?',
      choices=[('products', 'products'), ('services', 'services'), ('products_and_services', 'products_and_services')])
  description_of_products_or_services = StringField(
      'Please describe the products and/or services on which you plan to use the trademark.',
      description="Please make sure your description is accurate. For example, if you are making only wine, do not list beer.")
  paint_products = BooleanField('Paint Products', default=False)

  chemical_products = BooleanField('001: Chemical Products', default=False)
  paint_products = BooleanField('002: Paint Products', default=False)
  cosmetics_and_cleaning_products = BooleanField('003: Cosmetics and Cleaning Products', default=False)
  lubricant_and_fuel_products = BooleanField('004: Lubricant and Fuel Products', default=False)
  pharmaceutical_products = BooleanField('005: Pharmaceutical Products', default=False)
  metal_products = BooleanField('006: Metal Products', default=False)
  machinery_products = BooleanField('007: Machinery Products', default=False)
  hand_tool_products = BooleanField('008: Hand Tool Products', default=False)
  computer_and_software_products_and_electrical_and_scientific_products = BooleanField('009: Computer & Software Products & Electrical & Scientific Products', default=False)
  medical_instrument_products = BooleanField('010: Medical Instrument Products', default=False)
  environmental_control_instrument_products = BooleanField('011: Environmental Control Instrument Products (lighting, heating, cooling, cooking)', default=False)
  vehicles_and_products_for_locomotion_by_land_air_or_water = BooleanField('012: Vehicles and Products for locomotion by land, air or water', default=False)
  firearm_products = BooleanField('013: Firearm Products', default=False)
  jewelry_products = BooleanField('014: Jewelry Products', default=False)
  musical_instrument_products = BooleanField('015: Musical Instrument Products', default=False)
  paper_goods_and_printed_material = BooleanField('016: Paper Goods and Printed Material', default=False)
  rubber_products = BooleanField('017: Rubber Products', default=False)
  leather_products = BooleanField('018: Leather Products (not including clothing)', default=False)
  non_metallic_building_material_products = BooleanField('019: Non-Metallic Building Material Products', default=False)
  furniture_products = BooleanField('020: Furniture Products', default=False)
  houseware_and_glass_products = BooleanField('021: Houseware and Glass Products', default=False)
  ropes_cordage_and_fiber_products = BooleanField('022: Ropes, Cordage and Fiber Products', default=False)
  yarns_and_threads = BooleanField('023: Yarns and Threads', default=False)
  fabrics = BooleanField('024: Fabrics', default=False)
  clothing_products = BooleanField('025: Clothing Products', default=False)
  lace_ribbons_embroidery_and_fancy_goods = BooleanField('026: Lace, Ribbons, Embroidery and Fancy Goods', default=False)
  floor_covering_products = BooleanField('027: Floor Covering products', default=False)
  toys_and_sporting_goods_products = BooleanField('028: Toys and Sporting Goods Products', default=False)
  meats_and_processed_food_products = BooleanField('029: Meats and Processed Food Products', default=False)
  staple_food_products = BooleanField('030: Staple Food Products', default=False)
  natural_agricultural_products = BooleanField('031: Natural Agricultural Products', default=False)
  light_beverage_products = BooleanField('032: Light Beverage Products', default=False)
  wines_and_spirits_products = BooleanField('033: Wines and Spirits Products', default=False)
  smokers_products = BooleanField('034: Smoker\'s Products', default=False)
  advertising_business_and_retail_services = BooleanField('035: Advertising, Business & Retail Services', default=False)
  insurance_and_financial_services = BooleanField('036: Insurance & Financial Services', default=False)
  construction_and_repair_services = BooleanField('037: Construction and Repair Services', default=False)
  communications_services = BooleanField('038: Communications Services', default=False)
  transportation_and_storage_services = BooleanField('039: Transportation & Storage Services', default=False)
  treatment_and_processing_of_materials_services = BooleanField('040: Treatment & Processing of Materials Services', default=False)
  education_and_entertainment_services = BooleanField('041: Education and Entertainment Services', default=False)
  computer_and_software_services_and_scientific_services = BooleanField('042: Computer & Software Services & Scientific Services', default=False)
  restaurant_and_hotel_services = BooleanField('043: Restaurant and Hotel Services', default=False)
  medical_beauty_and_agricultural_services = BooleanField('044: Medical, Beauty & Agricultural Services', default=False)
  personal_and_legal_and_social_services = BooleanField('045: Personal & Legal & Social Services', default=False)

  date_modified = HiddenField("date_modified", default=datetime.today)
  ##submit = SubmitField('Submit')


















########################################
# COMMON FIELDS#
########################################

EMPLOYER_NAME = StringField('What is the employer\'s name?',
                            validators=[Length(min=1, max=49)],
                            render_kw={
                                "placeholder": "e.g., UpLaw Technologies, Inc."})

DEPARTMENT_NAME = StringField(
    'What department is responsible for the administration of this policy?', default="Human Resources",
    description="Human Resources is often in charge of administering employment policies and procedures.",
    render_kw={"placeholder": "e.g., Human Resources"})

Employees_Covered_Under_a_Collective_Bargaining_Agreement = SelectField(
    'Are employees unionized or otherwise covered under a collective bargaining agreement?',
    choices=[('yes', 'yes'), ('no', 'no')], default='no')

collective_bargaining_agreement_conflicting_language = SelectField(

    choices=[('yes', 'yes'), ('no', 'no')], default='yes',
    label='Include language addressing conflicts between this agreement and the employees\' collective bargaining agreement (CBA) and referring the employee to their respective CBA when conflicts arise?')

POSITION = StringField(
    'What position is allowed to make changes to this policy?', default='Manager',
    description="The CEO or Manager is often allowed to make changes to policies.",
    render_kw={"placeholder": "e.g., CEO or Manager"})

conduct_not_prohibited_by_this_policy = SelectField(
    choices=[('yes', 'yes'), ('no', 'no')], default='yes',
    label='Include a disclaimer of restrictions on employees’ rights under the NLRA?',
    description='A disclaimer is recommended because both unionized and nonunionized employers must ensure that they comply with the National Labor Relations Act (NLRA) when developing and implementing policies.')

Acknowledgment_of_Receipt_and_Review = SelectField(
    'Include an Acknowledgment of Receipt and Review for each employee to sign?',
    choices=[('yes', 'yes'), ('no', 'no')],
    description="A signed acknowledgment of receipt, review, and understanding of any employee policy minimizes the potential for employees to later claim ignorance of that policy as an excuse for non-compliance.",
    default='yes')


########################################
# END: COMMON FIELDS#
########################################


class AntiHarrassmentPolicy(FlaskForm):
    EMPLOYER_NAME = EMPLOYER_NAME
    DEPARTMENT_NAME = DEPARTMENT_NAME
    Employees_Covered_Under_a_Collective_Bargaining_Agreement = Employees_Covered_Under_a_Collective_Bargaining_Agreement
    collective_bargaining_agreement_conflicting_language = collective_bargaining_agreement_conflicting_language
    POSITION = POSITION
    Acknowledgment_of_Receipt_and_Review = Acknowledgment_of_Receipt_and_Review
    submit = SubmitField('Submit')


class BackgroundCheckPolicy(FlaskForm):
    EMPLOYER_NAME = EMPLOYER_NAME
    DEPARTMENT_NAME = DEPARTMENT_NAME
    Employees_Covered_Under_a_Collective_Bargaining_Agreement = Employees_Covered_Under_a_Collective_Bargaining_Agreement
    collective_bargaining_agreement_conflicting_language = collective_bargaining_agreement_conflicting_language
    POSITION = POSITION
    Acknowledgment_of_Receipt_and_Review = Acknowledgment_of_Receipt_and_Review
    submit = SubmitField('Submit')


class JuryDutyLeavePolicy(FlaskForm):
    EMPLOYER_NAME = EMPLOYER_NAME
    DEPARTMENT_NAME = DEPARTMENT_NAME
    employees_summoned_for_jury_duty_are_entitled_to_paid_time_off = SelectField(
        "Employees summoned for jury duty are entitled to paid time off?",
        choices=[('yes', 'yes'), ('no', 'no')])
    NUMBER_OF_DAYS = IntegerField(
        'Number of days employees receieve paid time off?',
        [NumberRange(min=0, max=10)])
    employees_may_opt_to_use_any_available_accrued_vacation_time_in_place_of_unpaid_leave = SelectField(
        "Employees may opt to use any available accrued vacation time in place of unpaid leave?",
        choices=[('yes', 'yes'), ('no', 'no')])
    Employees_Covered_Under_a_Collective_Bargaining_Agreement = Employees_Covered_Under_a_Collective_Bargaining_Agreement
    collective_bargaining_agreement_conflicting_language = collective_bargaining_agreement_conflicting_language
    POSITION = POSITION
    Acknowledgment_of_Receipt_and_Review = Acknowledgment_of_Receipt_and_Review
    submit = SubmitField('Submit')


class NepotismPolicy(FlaskForm):
    EMPLOYER_NAME = EMPLOYER_NAME
    DEPARTMENT_NAME = DEPARTMENT_NAME
    Employees_Covered_Under_a_Collective_Bargaining_Agreement = Employees_Covered_Under_a_Collective_Bargaining_Agreement
    collective_bargaining_agreement_conflicting_language = collective_bargaining_agreement_conflicting_language
    POSITION = POSITION
    Acknowledgment_of_Receipt_and_Review = Acknowledgment_of_Receipt_and_Review
    submit = SubmitField('Submit')


class OutsideEmploymentPolicy(FlaskForm):
    EMPLOYER_NAME = EMPLOYER_NAME
    DEPARTMENT_NAME = DEPARTMENT_NAME
    recognize_employees_may_seek_outside_employment = SelectField(
        "Recognize employees may seek outside employment?",
        choices=[('yes', 'yes'), ('no', 'no')])
    require_written_approval_or_notice = BooleanField(default=True)
    written_approval_or_notice = SelectField(
        choices=[('1', 'give written notice'), ('2', 'obtain written approval')],
        default='2',
        label='Should employees provide written notice or obtain written approval before pursuing outside employment?')
    Employees_Covered_Under_a_Collective_Bargaining_Agreement = Employees_Covered_Under_a_Collective_Bargaining_Agreement
    collective_bargaining_agreement_conflicting_language = collective_bargaining_agreement_conflicting_language
    POSITION = POSITION
    conduct_not_prohibited_by_this_policy = conduct_not_prohibited_by_this_policy
    Acknowledgment_of_Receipt_and_Review = Acknowledgment_of_Receipt_and_Review
    submit = SubmitField('Submit')


class PerformanceReviewPolicy(FlaskForm):
    EMPLOYER_NAME = EMPLOYER_NAME
    DEPARTMENT_NAME = DEPARTMENT_NAME
    annual_or_semi_annual = SelectField(
        choices=[('annual', 'annual'), ('semi-annual', 'semi-annual')],
        default='annual', label='How often are performance reviews conducted?')
    self_assessment = BooleanField(
        'Begin the performance review process with an employee self-assessment?')
    Employees_Covered_Under_a_Collective_Bargaining_Agreement = Employees_Covered_Under_a_Collective_Bargaining_Agreement
    collective_bargaining_agreement_conflicting_language = collective_bargaining_agreement_conflicting_language
    POSITION = POSITION
    Acknowledgment_of_Receipt_and_Review = Acknowledgment_of_Receipt_and_Review
    submit = SubmitField('Submit')


class MultiCheckboxField(SelectMultipleField):
    widget = ListWidget(prefix_label=False)
    option_widget = CheckboxInput()


class SmokeFreeWorkplacePolicy(FlaskForm):
    EMPLOYER_NAME = EMPLOYER_NAME
    DEPARTMENT_NAME = DEPARTMENT_NAME
    Employees_Covered_Under_a_Collective_Bargaining_Agreement = Employees_Covered_Under_a_Collective_Bargaining_Agreement
    collective_bargaining_agreement_conflicting_language = collective_bargaining_agreement_conflicting_language
    POSITION = POSITION
    Acknowledgment_of_Receipt_and_Review = Acknowledgment_of_Receipt_and_Review

    smoking_prohibited_outdoors = BooleanField(
        'Smoking is prohibited both indoors and outdoors.')
    designated_smoking_areas = BooleanField(
        'Smoking is allowed in designated smoking areas.')
    include_e_smoking_devices = BooleanField(
        'Electronic smoking--e.g, vaping and e-cigs--is prohibited.')
    display_no_smoking_signs = BooleanField(
        'Do you plan on displaying "No Smoking" signs in the workplace (.e.g., on bulletin boards, in stairwells, bathrooms etc)?')
    employee_assistance_program = BooleanField(
        "The employer offers an Employee Assistance Program to help employees quit smoking.")
    submit = SubmitField('Submit')


class WorkplaceSearchesPolicy(FlaskForm):
    EMPLOYER_NAME = EMPLOYER_NAME
    DEPARTMENT_NAME = DEPARTMENT_NAME
    REFUSAL_TO_ALLOW_SEARCH_OR_INSPECTION_MAY_RESULT_IN_DISCIPLINE = SelectField(
        "Refusal to allow search or inspection may result in discipline?",
        choices=[('yes', 'yes'), ('no', 'no')])
    Employees_Covered_Under_a_Collective_Bargaining_Agreement = Employees_Covered_Under_a_Collective_Bargaining_Agreement
    collective_bargaining_agreement_conflicting_language = collective_bargaining_agreement_conflicting_language
    POSITION = POSITION
    Acknowledgment_of_Receipt_and_Review = Acknowledgment_of_Receipt_and_Review
    submit = SubmitField('Submit')

