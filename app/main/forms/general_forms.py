from flask import request
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, SelectField
from wtforms.validators import ValidationError, DataRequired, Length, Optional
from flask_babel import _, lazy_gettext as _l
from app.models import User


class EditProfileForm(FlaskForm):
    username = StringField(_l('Username'), validators=[DataRequired()])
    about_me = TextAreaField(_l('About me'),
                             validators=[Length(min=0, max=140)])
    submit = SubmitField(_l('Submit'))

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError(_('Please use a different username.'))


class EmptyForm(FlaskForm):
    submit = SubmitField('Submit')


class PostForm(FlaskForm):
    post = TextAreaField(_l('Say something'), validators=[DataRequired()])
    submit = SubmitField(_l('Submit'))


class SearchForm(FlaskForm):
    q = StringField(_l('Search'), validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)


class MessageForm(FlaskForm):
    message = TextAreaField(_l('Message'), validators=[
        DataRequired(), Length(min=1, max=140)])
    submit = SubmitField(_l('Submit'))


class ContactForm(FlaskForm):
    name = TextAreaField(_l('Name'), validators=[DataRequired(), Length(min=1, max=140)])
    email = TextAreaField(_l('Email'), validators=[DataRequired(), Length(min=1, max=140)])
    phone = TextAreaField(_l('Phone'), validators=[DataRequired(), Length(min=8, max=19)])
    category = SelectField('Method of Delivery',
                                     validators=[Optional()],
                                     choices=[('commercial', 'commercial'),
                                              ('ip', 'IP'), ('trademark', 'Trademark')],
                                     description='Select Category',
                                     default="email")

    message = TextAreaField(_l('Message'), validators=[DataRequired(), Length(min=1, max=2999)])
    submit = SubmitField(_l('Submit'))


class DefaultProductRegistrationForm(FlaskForm):
    email = StringField(_l('Email'),
                        description="We need your email so we can save your information and document.",
                        validators=[DataRequired()])
    submit = SubmitField('Submit')

class UserProductForm(FlaskForm):
    first_name = TextAreaField(_l('First Name'), validators=[DataRequired(), Length(min=1, max=140)])
    last_name = TextAreaField(_l('Last Name'), validators=[DataRequired(), Length(min=1, max=140)])
    email = TextAreaField(_l('Email'), validators=[DataRequired(), Length(min=1, max=149)])
    submit = SubmitField(_l('Submit'))

class OrderForm(FlaskForm):
    billing_first_name = StringField(_l('First Name'),
                                     validators=[DataRequired()])
    billing_last_name = StringField(_l('Last Name'), validators=[DataRequired()])
    billing_street_address_1 = StringField(_l('Street Address'),
                                           validators=[DataRequired()])
    billing_street_address_2 = StringField(_l('Street Address 2'))
    billing_city = StringField(_l('City'), validators=[DataRequired()])
    billing_state = StringField(_l('State'), validators=[DataRequired()])
    billing_zip = StringField(_l('Zip'), validators=[DataRequired()])
    billing_phone = StringField(_l('Phone'), validators=[DataRequired()])
    billing_email = StringField(_l('Email'), validators=[DataRequired()])
    billing_order_notes = TextAreaField(_l('Order Notes'))
    submit = SubmitField('Submit')
