from flask_babel import _, lazy_gettext as _l
from flask_wtf import FlaskForm
from wtforms import IntegerField, StringField, SubmitField, TextAreaField, \
  HiddenField, FileField
from wtforms.validators import ValidationError, DataRequired, Length, Optional

from app.main.forms import scdb_forms
from app.models import User


class EditProfileForm(FlaskForm):
  username = StringField(_l('Username'), validators=[DataRequired()])
  about_me = TextAreaField(_l('About me'),
                           validators=[Length(min=0, max=140)])
  submit = SubmitField(_l('Submit'))

  def __init__(self, original_username, *args, **kwargs):
    super(EditProfileForm, self).__init__(*args, **kwargs)
    self.original_username = original_username

  def validate_username(self, username):
    if username.data != self.original_username:
      user = User.query.filter_by(username=self.username.data).first()
      if user is not None:
        raise ValidationError(_('Please use a different username.'))


class ImageUpload(FlaskForm):
  url = StringField('Enter the url of the image to be classified.',
                    validators=[Optional()])
  file = FileField('Upload a file with the image to be classified.',
                   validators=[Optional()])
  submit = SubmitField(_l('Submit'))


class SalaryPredictionForm(FlaskForm):
  age = IntegerField('age', validators=[Optional()], default=22)
  workclass = StringField('workclass', validators=[Optional()],
                          default='Private')
  fnlwgt = IntegerField('fnlwgt', validators=[Optional()], default=236746)
  education_num = IntegerField('education-num', validators=[Optional()],
                               default=14)
  education = StringField('education', validators=[Optional()],
                          default='Masters')
  marital_status = StringField('marital-status', validators=[Optional()],
                               default='Married-civ-spouse')
  occupation = StringField('occupation', validators=[Optional()],
                           default='Sales')
  relationship = StringField('relationship', validators=[Optional()],
                             default='Husband')
  race = StringField('race', validators=[Optional()], default='White')
  sex = StringField('sex', validators=[Optional()], default='Male')
  capital_gain = IntegerField('capital-gain', validators=[Optional()],
                              default=10520)
  capital_loss = IntegerField('capital-loss', validators=[Optional()],
                              default=0)
  hours_per_week = IntegerField('hours-per-week', validators=[Optional()],
                                default=45)
  native_country = StringField('race', validators=[Optional()],
                               default='United-States')
  salary = StringField('salary', validators=[Optional()], default='>=50k')
  submit = SubmitField(_l('Submit'))


class PostForm(FlaskForm):
  post = TextAreaField(_l('Say something'), validators=[DataRequired()])
  submit = SubmitField(_l('Submit'))


class SearchForm(FlaskForm):
  q = StringField(_l('Search'), validators=[DataRequired()])


'''
class SearchForm(FlaskForm):
    q = StringField(_l('Search'), validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        if 'formdata' not in kwargs:
            kwargs['formdata'] = request.args
        if 'csrf_enabled' not in kwargs:
            kwargs['csrf_enabled'] = False
        super(SearchForm, self).__init__(*args, **kwargs)
'''


class MessageForm(FlaskForm):
  message = TextAreaField(_l('Message'), validators=[
    DataRequired(), Length(min=1, max=140)])
  submit = SubmitField(_l('Submit'))


class IntakeForm(FlaskForm):
  email = StringField(_l('Email'), validators=[DataRequired()])
  post = TextAreaField(_l('Say something'), validators=[DataRequired()])
  submit = SubmitField(_l('Submit'))


class CommentForm(FlaskForm):
  parent_id = HiddenField(_l('Parent Id'))
  text = TextAreaField(_l('Comment'), validators=[
    DataRequired(), Length(min=1, max=199)])
  submit = SubmitField(_l('Submit'))






