
from app.api import bp

import app.crawler.blog_post_crawler as blog_post_crawler



@bp.route('/crawlers', methods=['GET'])
@bp.route('/crawlers/', methods=['GET'])
@bp.route('/crawlers/<string:name>', methods=['GET'])
def run_crawler(name):
  print('####'+name)
  if name is None:
    print("ERROR:: crawler error with "+ name)
    return 'List of available crawlers:\n- http://localhost:5000/api/crawlers/blog_post_crawler'

  if name == 'blog_post_crawler':
    blog_post_crawler.run()
  #data = request.args.get('action') or {}
  document={}
  #blog_post_crawler.run()
  #Document.query.get_or_404(id).to_dict()
  return "Successfully ran crawler: " + name
