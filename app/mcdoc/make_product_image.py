import json
import os
import subprocess
import time
from os import path
import shutil
import PyPDF2
import docx
from docx import Document as DocxDocument
from docxtpl import DocxTemplate





def text_from_docx(path):
    doc = DocxDocument(path)
    for paragraph in doc.paragraphs:
        yield paragraph.text


def combine_documents(doc1, doc2):
    for element in doc2.element.body:
        doc1.element.body.append(element)
    return doc1


def watermark(pdf_filename):
    watermark_file_path = os.path.join(current_app.root_path, 'static/images/uplaw_watermark.pdf')
    pdfFileObj = open(pdf_filename, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    first_page = pdfReader.getPage(0)
    pdfWatermarkReader = PyPDF2.PdfFileReader(open(watermark_file_path, 'rb'))
    first_page.mergePage(pdfWatermarkReader.getPage(0))
    pdfWriter = PyPDF2.PdfFileWriter()
    pdfWriter.addPage(first_page)
    for pageNum in range(1, pdfReader.numPages):
        pageObj = pdfReader.getPage(pageNum)
        pageObj.mergePage(pdfWatermarkReader.getPage(0))
        pdfWriter.addPage(pageObj)
    resultPdfFile = open(pdf_filename[0:-4] + '_preview.pdf', 'wb')
    pdfWriter.write(resultPdfFile)
    pdfFileObj.close()

    resultPdfFile.close()

def make_users_docx_and_pdf_produt_preview():

    slug='workplace-searches-policy'

    product_image_dir = '/home/casey/csm/repos/mclaw/mclaw-flask-app/app/static/mclawassets/images/product_images/'
    #current_dir = os.getcwd()
   # user_public_download_directory =  current_app.config['PUBLIC_DOWNLOAD_FOLDER'] + "user/" + str(user_id) + '/upfdid/' + str(upfdid)
    #basic_utils.delete_and_mkdires(path=user_public_download_directory)
    #abs_path_download_dir = os.path.join(current_dir, user_public_download_directory)

    #os.makedirs(abs_path_download_dir, exist_ok=True)

    template_path_document =  '/home/casey/csm/repos/mclaw/mclaw-flask-app/app/templates/docx/documents/' + slug + ".docx"
    template_path_guide = '/home/casey/csm/repos/mclaw/mclaw-flask-app/app/templates/docx/guides/' + slug + ".docx"


    #guide = DocxTemplate(template_path_guide)
    standard_document = DocxTemplate(template_path_document)
    #print(guide)
   # print(standard_document)
    final_document = standard_document#combine_documents(guide, standard_document)
    output_product_image_path = product_image_dir + slug+'.docx'
    final_document.save(output_product_image_path)
    doc = DocxTemplate(output_product_image_path)

    doc_input_data = {}
    #doc_input_data = json.loads(field_data)
    doc_input_data['title'] = slug;
    doc_input_data['COMPANY_NAME'] = 'ACME CO.';
    doc_input_data['date_modified'] = '2023 March 15';

    doc.render(doc_input_data)
    doc.save(output_product_image_path)
    # to_pdf(docx_user_public_download_path)
    make_pdf_from_docx_template(output_product_image_path)

        #pdf_filename = output_product_image_path
        #watermark(pdf_filename)
    #time.sleep(0.5)
    print('INFO:: run_docx.make_users_docx_and_pdf(). Successfully made .docx and .pdf: ' +  output_product_image_path )

    return


####################### UTILS ######################

def getText(filename):
    doc = DocxDocument(filename)
    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)
    return '\n'.join(fullText)


def append_to_doc(doc, fname, pageBreak):
    t = DocxDocument(fname)
    for p in t.paragraphs:
        print(p.style)
        print(p.text)
        doc.add_paragraph("", p.style)  # add an empty paragraph in the matching style
        for r in p.runs:
            nr = doc.paragraphs[-1].add_run(r.text)
            nr.bold = r.bold
            nr.italic = r.italic
            nr.underline = r.underline
    if pageBreak == True:
        doc.add_page_break()
    return doc



def make_pdf_from_docx_template(path):
    cmd = ["/usr/bin/abiword", "--to=pdf", path]
    subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)


def to_pdf(path):
    outdir = path[0:path.rindex('/')] + '/'
    cmd = ["soffice", "--headless", "--convert-to", "pdf", '--outdir', outdir, path]
    subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)

make_users_docx_and_pdf_produt_preview()
