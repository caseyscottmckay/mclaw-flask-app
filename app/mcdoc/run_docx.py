import json
import os
import subprocess
import time
from os import path
import shutil
import PyPDF2
import docx
from docx import Document as DocxDocument
from docxtpl import DocxTemplate
from flask import request, current_app

from app.api import bp
from app.models import Product,UserProductFormData
from app import db

def make_users_docx_and_pdf(upfdid, preview):
    if upfdid is None:
        print("ERROR:: upfdid is None")
        return
    print("making docx document and guide in .doc and .pdf for upfdid "+str(upfdid))
    user_product_form_data = UserProductFormData.query.filter_by(id=upfdid).first()
    '''
    if UserProductFormData.query.filter_by(id=upfdid).first() is None:
        user_form_data_entry = ""
    else:
        user_form_data_entry = UserProductFormData.query.filter_by(id=upfdid).first().__dict__
    '''
    user_id = user_product_form_data.user_id
    field_data = user_product_form_data.field_data

    product = Product.query.filter_by(id=user_product_form_data.product_id).first_or_404()
    slug=product.slug
    #product = basic_utils.get_product(slug)
    file_name = slug + "_" + str(upfdid)
    current_dir = os.getcwd()
    user_public_download_directory =  current_app.config['PUBLIC_DOWNLOAD_FOLDER'] + "user/" + str(user_id) + '/upfdid/' + str(upfdid)
    #basic_utils.delete_and_mkdires(path=user_public_download_directory)
    abs_path_download_dir = os.path.join(current_dir, user_public_download_directory)
    if not os.path.exists(abs_path_download_dir):
        os.makedirs(abs_path_download_dir)
    else:
        shutil.rmtree(abs_path_download_dir)
        os.makedirs(abs_path_download_dir)
    #os.makedirs(abs_path_download_dir, exist_ok=True)
    print('##### current_app_root: '+current_app.root_path+'; user_public_download_directory: ' + user_public_download_directory +'; abspath: '+abs_path_download_dir)

    docx_user_public_download_path = user_public_download_directory + '/' + file_name + ".docx"
    pdf_user_public_download_path = user_public_download_directory + '/' + file_name + ".pdf"
    template_path_document =  os.path.join(current_app.root_path, current_app.config['DOCX_TEMPLATE_FOLDER'] + 'documents/') + slug + ".docx"
    template_path_guide =os.path.join(current_app.root_path, current_app.config['DOCX_TEMPLATE_FOLDER'] + 'guides/') + slug + ".docx"

    print('####@@: ' + template_path_document + "::: "+ template_path_guide +'::: ' + docx_user_public_download_path)
    guide = DocxTemplate(template_path_guide)
    standard_document = DocxTemplate(template_path_document)
    print(guide)
    print(standard_document)
    final_document = standard_document#combine_documents(guide, standard_document)
    final_document.save(docx_user_public_download_path)
    doc = DocxTemplate(docx_user_public_download_path)

    doc_input_data = json.loads(user_product_form_data.field_data)
    doc_input_data['title'] = product.title;
    user_product_form_data.product_download_path_docx = docx_user_public_download_path
    user_product_form_data.product_download_path_pdf = pdf_user_public_download_path
    user_product_form_data.download_available = True
    db.session.add(user_product_form_data)
    db.session.commit()
    doc.render(doc_input_data)
    doc.save(docx_user_public_download_path)
   # to_pdf(docx_user_public_download_path)
    make_pdf_from_docx_template(docx_user_public_download_path)
    if preview == True:
        #time.sleep(1.5)
        pdf_filename = pdf_user_public_download_path
        watermark(pdf_filename)
    time.sleep(0.5)
    print('INFO:: run_docx.make_users_docx_and_pdf(). Successfully made .docx and .pdf: ' +  docx_user_public_download_path + '; ' + pdf_user_public_download_path )
    if path.exists('/home/casey/mclaw/mclaw-flask-app/'+pdf_user_public_download_path):
        print('###### EXISSTS')
    return docx_user_public_download_path, pdf_user_public_download_path

def generate_document(url):
    user_id=0
    upfdid=0
    document_download_path = os.path.join(current_app.root_path,
                                          current_app.config['PUBLIC_DOWNLOAD_FOLDER']) + "user/" + str(
        user_id) + '/entry/' + str(upfdid)
    print('generating document to path: ' + document_download_path)
    doc = DocxTemplate(document_download_path)
    form_data = {}
    form_data['title'] = '<h1>Document Title Here--A Grand Document it is</h1>'
    form_data['html'] = '<p>Document Title Here--A Grand Document it is</p>'

    doc.render(form_data)
    doc.save(document_download_path)
    # to_pdf(docx_user_public_download_path)
    make_pdf_from_docx_template(document_download_path)

def text_from_docx(path):
    doc = DocxDocument(path)
    for paragraph in doc.paragraphs:
        yield paragraph.text


def combine_documents(doc1, doc2):
    for element in doc2.element.body:
        doc1.element.body.append(element)
    return doc1


def watermark(pdf_filename):
    watermark_file_path = os.path.join(current_app.root_path, 'static/images/uplaw_watermark.pdf')
    pdfFileObj = open(pdf_filename, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    first_page = pdfReader.getPage(0)
    pdfWatermarkReader = PyPDF2.PdfFileReader(open(watermark_file_path, 'rb'))
    first_page.mergePage(pdfWatermarkReader.getPage(0))
    pdfWriter = PyPDF2.PdfFileWriter()
    pdfWriter.addPage(first_page)
    for pageNum in range(1, pdfReader.numPages):
        pageObj = pdfReader.getPage(pageNum)
        pageObj.mergePage(pdfWatermarkReader.getPage(0))
        pdfWriter.addPage(pageObj)
    resultPdfFile = open(pdf_filename[0:-4] + '_preview.pdf', 'wb')
    pdfWriter.write(resultPdfFile)
    pdfFileObj.close()

    resultPdfFile.close()

def make_users_docx_and_pdf_produt_preview(upfdid, preview):

    slug='website-terms-of-use'

    product_image_dir = '/home/casey/csm/repos/mclaw/mclaw-flask-app/app/static/mclawassets/images/product_images/'
    #current_dir = os.getcwd()
   # user_public_download_directory =  current_app.config['PUBLIC_DOWNLOAD_FOLDER'] + "user/" + str(user_id) + '/upfdid/' + str(upfdid)
    #basic_utils.delete_and_mkdires(path=user_public_download_directory)
    #abs_path_download_dir = os.path.join(current_dir, user_public_download_directory)

    #os.makedirs(abs_path_download_dir, exist_ok=True)

    template_path_document =  '/home/casey/csm/repos/mclaw/mclaw-flask-app/app/templates/docx/documents/' + slug + ".docx"
    template_path_guide = '/home/casey/csm/repos/mclaw/mclaw-flask-app/app/templates/docx/guides/' + slug + ".docx"


    #guide = DocxTemplate(template_path_guide)
    standard_document = DocxTemplate(template_path_document)
    #print(guide)
   # print(standard_document)
    final_document = standard_document#combine_documents(guide, standard_document)
    output_product_image_path = product_image_dir + slug+'.docx'
    final_document.save(output_product_image_path)
    doc = DocxTemplate(output_product_image_path)

    field_data = {}
    doc_input_data = json.loads(field_data)
    doc_input_data['title'] = slug;

    doc.render(doc_input_data)
    doc.save(output_product_image_path)
    # to_pdf(docx_user_public_download_path)
    make_pdf_from_docx_template(output_product_image_path)
    if preview == True:
        #time.sleep(1.5)
        pdf_filename = output_product_image_path
        watermark(pdf_filename)
    time.sleep(0.5)
    print('INFO:: run_docx.make_users_docx_and_pdf(). Successfully made .docx and .pdf: ' +  output_product_image_path )

    return


####################### UTILS ######################

def getText(filename):
    doc = DocxDocument(filename)
    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)
    return '\n'.join(fullText)


def append_to_doc(doc, fname, pageBreak):
    t = DocxDocument(fname)
    for p in t.paragraphs:
        print(p.style)
        print(p.text)
        doc.add_paragraph("", p.style)  # add an empty paragraph in the matching style
        for r in p.runs:
            nr = doc.paragraphs[-1].add_run(r.text)
            nr.bold = r.bold
            nr.italic = r.italic
            nr.underline = r.underline
    if pageBreak == True:
        doc.add_page_break()
    return doc



def make_pdf_from_docx_template(path):
    cmd = ["/usr/bin/abiword", "--to=pdf", path]
    subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)


def to_pdf(path):
    outdir = path[0:path.rindex('/')] + '/'
    cmd = ["soffice", "--headless", "--convert-to", "pdf", '--outdir', outdir, path]
    subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)


