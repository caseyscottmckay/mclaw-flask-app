
import hashlib
import logging
import os
import re
import sys
import markdown
from app.models import Document
from app import db
from datetime import datetime, timedelta

sys.path.insert(0, os.environ.get('RESOURCES'))
sys.path.insert(1, os.environ.get('HOME') + '/mclawlaw/mclaw-flask-app/app/util')



RESOURCES = os.environ.get('RESOURCES')

DIRECTORY_LIMIT = 0
BATCH_LIMIT = 10000000
DATE_PATTERN = re.compile(
    "^(.*?)(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?).*?(\d{1,2}).*?(\d{4})(.*?)$")

#state_abbreviation_bluebook = blue_book_abbreviations.STATES
md = markdown.Markdown()




def process_document_to_dict(slug, f):

  content = f.read()
  lines = content.split('\n')
  if '---' not in lines[0]:
    print("ERROR:: malformed document header for document " + slug )
    return
  title = lines[1]
  if '|' in title:
    title= title.split('|')[1].strip()
  author = lines[2]
  if '|' in author:
    author = author.split('|')[1].strip()
  date = lines[3]
  if '|' in date:
    date = date.split('|')[1].strip()
  category = lines[4]
  if '|' in category:
    category = category.split('|')[1].strip()
  subcategory = lines[5]
  if '|' in subcategory:
    subcategory = subcategory.split('|')[1].strip()
  tags = lines[6]
  if '|' in tags:
    tags = tags.split('|')[1].strip()
  summary = lines[7]
  if '|' in summary:
    summary = summary.split('|')[1].strip()

  tags = lines[6]
  if '|' in tags:
    tags = tags.split('|')[1].strip()

  content = '\n'.join([str(elem) for elem in lines[9:]])

  markdown_content = content
  html_content = md.convert(markdown_content)
  document_bundle = {}
  documents = []
  citation = slug
  country = 'United States'
  date = datetime.utcnow()
  html = html_content
  text = markdown_content
  did = hashlib.md5(slug.encode("utf-8")).hexdigest()

 # tags[basic_utils.snakify(category)] = 50

  document = Document(author='admin@mclaw.io',category=category,citation=citation,country=country,date_created=date,date_modified=date,document_type='article', html=html, jurisdiction='National',resource_type='article', slug=slug,source='mclaw.io',summary=summary,tags=tags,text=text,title=title)
  db.session.add(document)
  db.session.commit()
  return document_bundle


def process_documents():
  BLOG_POST_DIR = '../data/documents/articles/'
  files = os.listdir(BLOG_POST_DIR)
  documents = []
  for file in files:
    if '.md' in file:
      try:
        if '.md' in file: slug = file[0:-3]
        print('processing markdown article to db:'  + slug)
        f = open(os.path.join(BLOG_POST_DIR, file), 'r')



        document = process_document_to_dict(slug, f)
        '''
        action = {
          "_index": "document",
          "_source": document
        }
        documents.append(action)
        bytes_size = basic_utils.get_size(documents)
        if (bytes_size > BATCH_LIMIT):
          if index_elasticsearch: document_utils.index_documents(documents)
          documents = []
        '''
      except Exception as e:
        logging.error(
            "ERROR: BlogPostCrawler.processDocuments for single file: " + '\t::' + str(
                e))
  return "successfully ran blog_post_crawler"


def run():
  process_documents()
