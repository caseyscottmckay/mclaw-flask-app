practices[]
title = Antitrust
slug = antitrust
category = Antitrust
overview =  <p>We assist companies in the full range of legal matters—from assistance with complex commercial transactions to representation in disputes. A top-rated business lawyer can help to ensure that your firm is in the best possible position moving forward. A list of services we offer to businesses in includes the following:</p><ul><li><strong>Business Formation</strong>:  Building a new business is challenging. We assist clients with all types of business formation issues, including representing sole proprietorships, partnerships, LLCs, start-up companies, S-corps, C-corps, and more.</li><li><strong>Small Business Representation</strong>:  Small businesses have unique needs. We appreciate the challenges you face as a small business owner. Our Washington, D.C. small business lawyers are standing by, ready to help you find cost effective solutions.</li><li><strong>Employment Law</strong>:  We assist companies with the full range of employment law issues—from putting the proper employment practices in place to defending legal claims filed by current or former employees.</li><li><strong>Commercial Transactions (Contracts)</strong>:  Contracts are the basis of many modern commercial relationships. Our Washington, D.C. business lawyers assist companies in negotiating, drafting, and reviewing contracts.</li><li><strong>Intellectual Property Law</strong>:  We handle the entire spectrum of complex intellectual property matters, including those involving copyrights, trademarks, and patents. Contact our D.C. business lawyers now for additional guidance.</li><li><strong>Litigation and Alternative Dispute Resolution</strong>:  When disputes arise, it is imperative that companies take swift action to protect their interests. A proactive approach can make an enormous difference. If your company is involved in a dispute—whether you are considering filing a lawsuit, being sued, or looking to avoid legal action—our District of Columbia business attorneys can help. </li></ul>
subcategories[]
title = Antitrust Compliance
slug = Antitrust Compliance
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Antitrust Litigation
slug = Antitrust Litigation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Cartels
slug = Cartels
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = General Antitrust
slug = General Antitrust
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Horizontal Agreements
slug = Horizontal Agreements
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Merger Control
slug = Merger Control
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Unilateral Conduct
slug = Unilateral Conduct
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Vertical Agreements
slug = Vertical Agreements
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
practices[]
title = Bankruptcy
slug = bankruptcy
category = Bankruptcy
overview =  <p>We assist companies in the full range of legal matters—from assistance with complex commercial transactions to representation in disputes. A top-rated business lawyer can help to ensure that your firm is in the best possible position moving forward. A list of services we offer to businesses in includes the following:</p><ul><li><strong>Business Formation</strong>:  Building a new business is challenging. We assist clients with all types of business formation issues, including representing sole proprietorships, partnerships, LLCs, start-up companies, S-corps, C-corps, and more.</li><li><strong>Small Business Representation</strong>:  Small businesses have unique needs. We appreciate the challenges you face as a small business owner. Our Washington, D.C. small business lawyers are standing by, ready to help you find cost effective solutions.</li><li><strong>Employment Law</strong>:  We assist companies with the full range of employment law issues—from putting the proper employment practices in place to defending legal claims filed by current or former employees.</li><li><strong>Commercial Transactions (Contracts)</strong>:  Contracts are the basis of many modern commercial relationships. Our Washington, D.C. business lawyers assist companies in negotiating, drafting, and reviewing contracts.</li><li><strong>Intellectual Property Law</strong>:  We handle the entire spectrum of complex intellectual property matters, including those involving copyrights, trademarks, and patents. Contact our D.C. business lawyers now for additional guidance.</li><li><strong>Litigation and Alternative Dispute Resolution</strong>:  When disputes arise, it is imperative that companies take swift action to protect their interests. A proactive approach can make an enormous difference. If your company is involved in a dispute—whether you are considering filing a lawsuit, being sued, or looking to avoid legal action—our District of Columbia business attorneys can help. </li></ul>
subcategories[]
title = Chapter 11 Commencing A Case
slug = Chapter 11 Commencing A Case
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Chapter 11 Case Administration
slug = Chapter 11 Case Administration
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Chapter 11 Exiting A Case
slug = Chapter 11 Exiting A Case
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Bankruptcy Committees
slug = Bankruptcy Committees
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Bankruptcy Litigation
slug = Bankruptcy Litigation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Creditor and Inter Creditor Issues In Bankruptcy
slug = Creditor and Inter Creditor Issues In Bankruptcy
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Purchasing Distressed Assets and Claims In Bankruptcy
slug = Purchasing Distressed Assets and Claims In Bankruptcy
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = General Bankruptcy
slug = General Bankruptcy
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
practices[]
title = Capital Markets and Corporate Governance
slug = capital_markets_and_corporate_governance
category = Capital Markets and Corporate Governance
overview =  <p>We assist companies in the full range of legal matters—from assistance with complex commercial transactions to representation in disputes. A top-rated business lawyer can help to ensure that your firm is in the best possible position moving forward. A list of services we offer to businesses in includes the following:</p><ul><li><strong>Business Formation</strong>:  Building a new business is challenging. We assist clients with all types of business formation issues, including representing sole proprietorships, partnerships, LLCs, start-up companies, S-corps, C-corps, and more.</li><li><strong>Small Business Representation</strong>:  Small businesses have unique needs. We appreciate the challenges you face as a small business owner. Our Washington, D.C. small business lawyers are standing by, ready to help you find cost effective solutions.</li><li><strong>Employment Law</strong>:  We assist companies with the full range of employment law issues—from putting the proper employment practices in place to defending legal claims filed by current or former employees.</li><li><strong>Commercial Transactions (Contracts)</strong>:  Contracts are the basis of many modern commercial relationships. Our Washington, D.C. business lawyers assist companies in negotiating, drafting, and reviewing contracts.</li><li><strong>Intellectual Property Law</strong>:  We handle the entire spectrum of complex intellectual property matters, including those involving copyrights, trademarks, and patents. Contact our D.C. business lawyers now for additional guidance.</li><li><strong>Litigation and Alternative Dispute Resolution</strong>:  When disputes arise, it is imperative that companies take swift action to protect their interests. A proactive approach can make an enormous difference. If your company is involved in a dispute—whether you are considering filing a lawsuit, being sued, or looking to avoid legal action—our District of Columbia business attorneys can help. </li></ul>
subcategories[]
title = Broker Dealer
slug = Broker Dealer
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Corporate Governance Continuous Disclosure
slug = Corporate Governance Continuous Disclosure
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Non Us Issuers Registered Offerings
slug = Non Us Issuers Registered Offerings
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Market Regulation
slug = Market Regulation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Unregistered Offerings
slug = Unregistered Offerings
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
practices[]
title = Commercial
slug = commercial
category = Commercial
overview =  <p>We assist companies in the full range of legal matters—from assistance with complex commercial transactions to representation in disputes. A top-rated business lawyer can help to ensure that your firm is in the best possible position moving forward. A list of services we offer to businesses in includes the following:</p><ul><li><strong>Business Formation</strong>:  Building a new business is challenging. We assist clients with all types of business formation issues, including representing sole proprietorships, partnerships, LLCs, start-up companies, S-corps, C-corps, and more.</li><li><strong>Small Business Representation</strong>:  Small businesses have unique needs. We appreciate the challenges you face as a small business owner. Our Washington, D.C. small business lawyers are standing by, ready to help you find cost effective solutions.</li><li><strong>Employment Law</strong>:  We assist companies with the full range of employment law issues—from putting the proper employment practices in place to defending legal claims filed by current or former employees.</li><li><strong>Commercial Transactions (Contracts)</strong>:  Contracts are the basis of many modern commercial relationships. Our Washington, D.C. business lawyers assist companies in negotiating, drafting, and reviewing contracts.</li><li><strong>Intellectual Property Law</strong>:  We handle the entire spectrum of complex intellectual property matters, including those involving copyrights, trademarks, and patents. Contact our D.C. business lawyers now for additional guidance.</li><li><strong>Litigation and Alternative Dispute Resolution</strong>:  When disputes arise, it is imperative that companies take swift action to protect their interests. A proactive approach can make an enormous difference. If your company is involved in a dispute—whether you are considering filing a lawsuit, being sued, or looking to avoid legal action—our District of Columbia business attorneys can help. </li></ul>
subcategories[]
title = Advertising and Marketing
slug = Advertising and Marketing
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = General Contract and Boilerplate
slug = General Contract and Boilerplate
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Bribery and Corruption
slug = Bribery and Corruption
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Insurance Claims Coverage
slug = Insurance Claims Coverage
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Confidentiality
slug = Confidentiality
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Joint Ventures
slug = Joint Ventures
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = E-Commerce
slug = E-Commerce
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Outsourcing
slug = Outsourcing
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = General Commercial
slug = General Commercial
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Supply of Goods and Services
slug = Supply of Goods and Services
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
practices[]
title = Employee Benefits and Executive Compensation
slug = employee_benefits_and_executive_compensation
category = Employee Benefits and Executive Compensation
overview =  <p>Labor and employment law issues have become as globalized as the world of business and commerce. With the pervasiveness of the Internet and social media, when a business employment practice is singled out and challenged in one location the repercussions can be felt around the globe. Through an integrated worldwide strategy, MC Law brings together practitioners experienced in local and cross-border labor and employment matters to provide seamless client service across national boundaries.</p><p>MC Law’s international employment and labor law practice can help multinational clients maneuver through the best of economic times, as well as during unstable periods. We help global clients adapt to the maze of employment laws, taking into account the varied economic circumstances and customs throughout the world.</p><p>We advise Fortune Global 500 companies on the full array of labor and employment issues, including managing independent contractors, litigating and arbitrating cross-border employment claims, drafting executive contracts, assisting with workforce reductions, and assessing and drafting global ethics and data privacy policies. We also advise medium- and smaller-sized companies making their first international ventures, as well as those that are expanding their overseas operations. We assist clients in developing the framework to operate internationally with employees in various countries, whether they are on temporary assignment or have longer-term expatriate arrangements.</p>
subcategories[]
title = Executive Compensation
slug = Executive Compensation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Retirement Plans Health Welfare Plans
slug = Retirement Plans Health Welfare Plans
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Equity Compensation
slug = Equity Compensation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Employee Benefit Plans Administration Compliance
slug = Employee Benefit Plans Administration Compliance
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Employee Benefit Plans The Employment Relationship
slug = Employee Benefit Plans The Employment Relationship
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
practices[]
title = Estate Planning
slug = estate_planning
category = Estate Planning
overview = <p>We assist companies in the full range of legal matters—from assistance with complex commercial transactions to representation in disputes. A top-rated business lawyer can help to ensure that your firm is in the best possible position moving forward. A list of services we offer to businesses in includes the following:</p><ul><li><strong>Business Formation</strong>:  Building a new business is challenging. We assist clients with all types of business formation issues, including representing sole proprietorships, partnerships, LLCs, start-up companies, S-corps, C-corps, and more.</li><li><strong>Small Business Representation</strong>:  Small businesses have unique needs. We appreciate the challenges you face as a small business owner. Our Washington, D.C. small business lawyers are standing by, ready to help you find cost effective solutions.</li><li><strong>Employment Law</strong>:  We assist companies with the full range of employment law issues—from putting the proper employment practices in place to defending legal claims filed by current or former employees.</li><li><strong>Commercial Transactions (Contracts)</strong>:  Contracts are the basis of many modern commercial relationships. Our Washington, D.C. business lawyers assist companies in negotiating, drafting, and reviewing contracts.</li><li><strong>Intellectual Property Law</strong>:  We handle the entire spectrum of complex intellectual property matters, including those involving copyrights, trademarks, and patents. Contact our D.C. business lawyers now for additional guidance.</li><li><strong>Litigation and Alternative Dispute Resolution</strong>:  When disputes arise, it is imperative that companies take swift action to protect their interests. A proactive approach can make an enormous difference. If your company is involved in a dispute—whether you are considering filing a lawsuit, being sued, or looking to avoid legal action—our District of Columbia business attorneys can help. </li></ul>
subcategories[]
title = Charitable Trusts
slug = Charitable Trusts
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Estate and Gift Tax
slug = Estate and Gift Tax
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = General Estate Planning
slug = General Estate Planning
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Health Care Directives
slug = Health Care Directives
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Irrevocable Life Insurance Trusts
slug = Irrevocable Life Insurance Trusts
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Powers of Attorney For Finances
slug = Powers of Attorney For Finances
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Revocable Trusts
slug = Revocable Trusts
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Supplemental Needs Trusts
slug = Supplemental Needs Trusts
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Wills
slug = Wills
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
practices[]
title = Finance
slug = finance
category = Finance
overview = <p>We assist companies in the full range of legal matters—from assistance with complex commercial transactions to representation in disputes. A top-rated business lawyer can help to ensure that your firm is in the best possible position moving forward. A list of services we offer to businesses in includes the following:</p><ul><li><strong>Business Formation</strong>:  Building a new business is challenging. We assist clients with all types of business formation issues, including representing sole proprietorships, partnerships, LLCs, start-up companies, S-corps, C-corps, and more.</li><li><strong>Small Business Representation</strong>:  Small businesses have unique needs. We appreciate the challenges you face as a small business owner. Our Washington, D.C. small business lawyers are standing by, ready to help you find cost effective solutions.</li><li><strong>Employment Law</strong>:  We assist companies with the full range of employment law issues—from putting the proper employment practices in place to defending legal claims filed by current or former employees.</li><li><strong>Commercial Transactions (Contracts)</strong>:  Contracts are the basis of many modern commercial relationships. Our Washington, D.C. business lawyers assist companies in negotiating, drafting, and reviewing contracts.</li><li><strong>Intellectual Property Law</strong>:  We handle the entire spectrum of complex intellectual property matters, including those involving copyrights, trademarks, and patents. Contact our D.C. business lawyers now for additional guidance.</li><li><strong>Litigation and Alternative Dispute Resolution</strong>:  When disputes arise, it is imperative that companies take swift action to protect their interests. A proactive approach can make an enormous difference. If your company is involved in a dispute—whether you are considering filing a lawsuit, being sued, or looking to avoid legal action—our District of Columbia business attorneys can help. </li></ul>
subcategories[]
title = Acquisition Finance
slug = Acquisition Finance
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Asset Based Lending
slug = Asset Based Lending
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Bankruptcy In Finance
slug = Bankruptcy In Finance
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Debt
slug = Debt
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Capital Markets
slug = Capital Markets
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Guaranties
slug = Guaranties
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Islamic Finance
slug = Islamic Finance
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Lending General
slug = Lending General
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Project Finance Development
slug = Project Finance Development
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Real Estate Finance Regulatory
slug = Real Estate Finance Regulatory
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Finance Securitization
slug = Finance Securitization
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Structured Finance Security
slug = Structured Finance Security
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Swaps Derivatives
slug = Swaps Derivatives
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
practices[]
title = Intellectual Property and Technology
slug = intellectual_property_and_technology
category = Intellectual Property and Technology
overview =  Since our founding in 2016, we have represented some of the world’s greatest innovators. We put this pedigree to work every day as strategic advisors to a broad range of clients that includes thinkers, makers, and doers of every size and in every industry. The depth of our resources is unmatched by any other firm, which makes us uniquely qualified to safeguard your ideas and nurture your business. We have the unique ability to infuse the lessons learned by our court-tested litigators into every patent application we handle.
subcategories[]
title = Copyright
slug = Copyright
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = General IP
slug = General IP
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Information Technology
slug = Information Technology
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Internet
slug = Internet
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = IP and IT In Corporate Transactions
slug = IP and IT In Corporate Transactions
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Patent Counseling and Transactions
slug = Patent Counseling and Transactions
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Patent Litigation
slug = Patent Litigation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Privacy and Data Security
slug = Privacy and Data Security
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Right of Publicity
slug = Right of Publicity
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Trademarks
slug = Trademarks
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Trade Secrets
slug = Trade Secrets
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
practices[]
title = International Arbitration
slug = international_arbitration
category = International Arbitration
overview =  <p>We assist companies in the full range of legal matters—from assistance with complex commercial transactions to representation in disputes. A top-rated business lawyer can help to ensure that your firm is in the best possible position moving forward. A list of services we offer to businesses in includes the following:</p><ul><li><strong>Business Formation</strong>:  Building a new business is challenging. We assist clients with all types of business formation issues, including representing sole proprietorships, partnerships, LLCs, start-up companies, S-corps, C-corps, and more.</li><li><strong>Small Business Representation</strong>:  Small businesses have unique needs. We appreciate the challenges you face as a small business owner. Our Washington, D.C. small business lawyers are standing by, ready to help you find cost effective solutions.</li><li><strong>Employment Law</strong>:  We assist companies with the full range of employment law issues—from putting the proper employment practices in place to defending legal claims filed by current or former employees.</li><li><strong>Commercial Transactions (Contracts)</strong>:  Contracts are the basis of many modern commercial relationships. Our Washington, D.C. business lawyers assist companies in negotiating, drafting, and reviewing contracts.</li><li><strong>Intellectual Property Law</strong>:  We handle the entire spectrum of complex intellectual property matters, including those involving copyrights, trademarks, and patents. Contact our D.C. business lawyers now for additional guidance.</li><li><strong>Litigation and Alternative Dispute Resolution</strong>:  When disputes arise, it is imperative that companies take swift action to protect their interests. A proactive approach can make an enormous difference. If your company is involved in a dispute—whether you are considering filing a lawsuit, being sued, or looking to avoid legal action—our District of Columbia business attorneys can help. </li></ul>
subcategories[]
title = Arbitral Awards Challenges
slug = Arbitral Awards Challenges
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Arbitration Agreements
slug = Arbitration Agreements
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Arbitrators Appointments
slug = Arbitrators Appointments
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Commencing An Arbitration
slug = Commencing An Arbitration
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Conflicts of Law Issues
slug = Conflicts of Law Issues
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Costs Funding
slug = Costs Funding
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Enforcement
slug = Enforcement
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Institutional Ad Hoc Arbitration
slug = Institutional Ad Hoc Arbitration
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Interim Measures
slug = Interim Measures
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Investment Treaty Arbitration
slug = Investment Treaty Arbitration
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Jurisdictional Issues
slug = Jurisdictional Issues
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Multiparty Arbitration
slug = Multiparty Arbitration
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Procedure Evidence
slug = Procedure Evidence
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Remedies
slug = Remedies
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
practices[]
title = Labor and Employment
slug = labor_and_employment
category = Labor and Employment
overview =  <p>Labor and employment law issues have become as globalized as the world of business and commerce. With the pervasiveness of the Internet and social media, when a business employment practice is singled out and challenged in one location the repercussions can be felt around the globe. Through an integrated worldwide strategy, MC Law brings together practitioners experienced in local and cross-border labor and employment matters to provide seamless client service across national boundaries.</p><p>MC Law’s international employment and labor law practice can help multinational clients maneuver through the best of economic times, as well as during unstable periods. We help global clients adapt to the maze of employment laws, taking into account the varied economic circumstances and customs throughout the world.</p><p>We advise Fortune Global 500 companies on the full array of labor and employment issues, including managing independent contractors, litigating and arbitrating cross-border employment claims, drafting executive contracts, assisting with workforce reductions, and assessing and drafting global ethics and data privacy policies. We also advise medium- and smaller-sized companies making their first international ventures, as well as those that are expanding their overseas operations. We assist clients in developing the framework to operate internationally with employees in various countries, whether they are on temporary assignment or have longer-term expatriate arrangements.</p>
subcategories[]
title = Contracts
slug = Contracts
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Leave Law
slug = Leave Law
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Corporate Transactions
slug = Corporate Transactions
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Bankruptcy
slug = Bankruptcy
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Policies and Procedures
slug = Policies and Procedures
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Discipline
slug = Discipline
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Internal Investigations
slug = Internal Investigations
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Recruitment and Hiring
slug = Recruitment and Hiring
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Discrimination
slug = Discrimination
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Harassment
slug = Harassment
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Retaliation
slug = Retaliation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Restrictive Covenants Confidentiality IP
slug = Restrictive Covenants Confidentiality Ip
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Employee Data Monitoring and Privacy
slug = Employee Data Monitoring and Privacy
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Termination Layoffs Plant Closings
slug = Termination Layoffs Plant Closings
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Employment Litigation
slug = Employment Litigation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Unions
slug = Unions
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Health and Safety
slug = Health and Safety
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Wage and Hour Law
slug = Wage and Hour Law
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Immigration
slug = Immigration
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
practices[]
title = Litigation
slug = litigation
category = Litigation
overview =  <p>When faced with litigation, employers need a comprehensive winning strategy. MC Law’s Complex Litigation and Jury Trial Practice Group has talented, experienced litigators who know how to position and try cases. Our group has decades of experience with creative, trial-tested strategies and approaches so that cases can be tried or resolved from a position of strength. We have experience at every phase of employment litigation, and clients often hire us to try cases to juries when counsel at other firms lack trial experience.</p><p>We represent every kind of employer, from small to large, in virtually every industry, in state and federal courts in every state. We share and build upon our collective experience in order to provide practical resources for our clients. For example, we have extensive experience working with a variety of experts with a particular focus on the cost-effective use of experts to better position our cases. Our experienced litigators have a track record of success in disqualifying opposing experts either before trial or following voir dire at trial when their opinions lack scientific bases or are based on unreliable principles.</p><p>MC Law has the depth, experience and strategic know-how to handle all aspects of employment litigation. The substantive law areas commonly covered include: Whistleblower and retaliation claims, Sexual or racial harassment, Discrimination, Disability accommodation, Wrongful termination, Protected leaves, Unfair competition, ERISA and employee benefits, Wage and hour class actions, Discrimination class actions, Executive contract and compensation, Arbitrations.</p><p>MC Law has the nation’s largest practice devoted exclusively to representing employers in labor and employment matters, and our employment litigation team has a long record of success. Because of our exclusive focus on workplace issues, we have anticipated trends over the years that have helped our clients navigate the fluid changes in employment law. Our experience ranges from the trial courts through the highest appellate levels, and MC Law attorneys have prevailed in six U.S. Supreme Court cases since 2010.</p>
subcategories[]
title = Commencement Pleadings
slug = Commencement Pleadings
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Removal
slug = Removal
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Discovery Evidence
slug = Discovery Evidence
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Motions
slug = Motions
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Trial Post Trial
slug = Trial Post Trial
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Judgment
slug = Judgment
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Appeals
slug = Appeals
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = General Litigation
slug = General Litigation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Arbitration
slug = Arbitration
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Class Actions
slug = Class Actions
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Antitrust Litigation
slug = Antitrust Litigation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Employment Litigation
slug = Employment Litigation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Intellectual Property Litigation
slug = Intellectual Property Litigation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Securities Litigation and Enforcement
slug = Securities Litigation and Enforcement
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Other Specialist Litigation
slug = Other Specialist Litigation
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
practices[]
title = Real Estate
slug = real_estate
category = Real Estate
overview =  <p>We assist companies in the full range of legal matters—from assistance with complex commercial transactions to representation in disputes. A top-rated business lawyer can help to ensure that your firm is in the best possible position moving forward. A list of services we offer to businesses in includes the following:</p><ul><li><strong>Business Formation</strong>:  Building a new business is challenging. We assist clients with all types of business formation issues, including representing sole proprietorships, partnerships, LLCs, start-up companies, S-corps, C-corps, and more.</li><li><strong>Small Business Representation</strong>:  Small businesses have unique needs. We appreciate the challenges you face as a small business owner. Our Washington, D.C. small business lawyers are standing by, ready to help you find cost effective solutions.</li><li><strong>Employment Law</strong>:  We assist companies with the full range of employment law issues—from putting the proper employment practices in place to defending legal claims filed by current or former employees.</li><li><strong>Commercial Transactions (Contracts)</strong>:  Contracts are the basis of many modern commercial relationships. Our Washington, D.C. business lawyers assist companies in negotiating, drafting, and reviewing contracts.</li><li><strong>Intellectual Property Law</strong>:  We handle the entire spectrum of complex intellectual property matters, including those involving copyrights, trademarks, and patents. Contact our D.C. business lawyers now for additional guidance.</li><li><strong>Litigation and Alternative Dispute Resolution</strong>:  When disputes arise, it is imperative that companies take swift action to protect their interests. A proactive approach can make an enormous difference. If your company is involved in a dispute—whether you are considering filing a lawsuit, being sued, or looking to avoid legal action—our District of Columbia business attorneys can help. </li></ul>
subcategories[]
title = Commercial Finance
slug = Commercial Finance
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Commercial Leasing
slug = Commercial Leasing
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Commercial Ownership
slug = Commercial Ownership
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Residential Finance
slug = Residential Finance
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Residential Leasing
slug = Residential Leasing
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Residential Ownership
slug = Residential Ownership
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Construction
slug = Construction
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Real Estate
slug = Real Estate
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
subcategories[]
title = Corporate Transactions
slug = Corporate Transactions
overview= <p>As a market leader in intellectual property and technology legal issues, the world’s most innovative companies trust MC Law with their important, technically sophisticated matters. Our success is rooted in our deep expertise and experience, fluency in every technology area imaginable, our robust industry knowledge, and our strategic and tenacious problem-solving abilities. MC Law’s lawyers dominate in every venue--scoring more high-profile wins across the board than any other firm.</p>
  