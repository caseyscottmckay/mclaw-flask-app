---
title | Microsoft Cloud Computing Changes to Allay EU Antitrust Concerns
author | casey@mclaw.io
date | 2022-08-14
category | Intellectual Property and Technology
subcategory | Internet
tags | copyright, infringement, dmca
summary | Amended licensing deals and other changes making it easier for cloud service providers to compete will take effect on Oct. 1
---
Amended licensing deals and other changes making it easier for cloud service providers to compete will take effect on Oct. 1, Microsoft Corp (MSFT.O) said on Monday, a move triggered by complaints about the U.S. software company to EU antitrust regulators.

Microsoft President Brad Smith had announced the changes in May but did not say when they would be effective. read more

The company, which has been fined 1.6 billion euros ($1.6 billion) by the European Commission in the previous decade for various infringements, found itself once again in the EU's crosshairs following complaints by cloud service providers in Germany, Italy, Denmark and France.

"On October 1, 2022, Microsoft will implement significant upgrades to our outsourcing and hosting terms that will benefit customers worldwide," the company said in a blogpost.

The revised licensing deals mean customers can use their licenses on any European cloud provider delivering services to their own data centers.

Rivals Amazon.com (AMZN.O), Alphabet's (GOOGL.O) Google, Alibaba (9988.HK) and Microsoft's own cloud services will be excluded from the deals.

Customers also will be allowed to buy licenses just for the virtual environment without the need to buy the physical hardware.

The EU competition watchdog has yet to comment on Microsoft's proposals.

($1 = 0.9988 euros)