---
title | Win for Photographer in Ninth Circuit Reversal of Fair Use Finding
author | casey@mclaw.io
date | 2022-08-14
category | Intellectual Property and Technology
subcategory | Copyright
tags | copyright, infringement, dmca
summary | On August 3, the U.S. Court of Appeals for the Ninth Circuit issued a ruling in McGucken v. Pub Ocean Ltd. that reversed a Central District of California’s sua sponte grant of summary judgment to Pub Ocean on McGucken’s copyright infringement claims. The case involved Pub Ocean’s unauthorized use of photos of a lake that formed in Death Valley, California, in March 2019. The Ninth Circuit found that all of the fair use factors weighed against a determination that Pub Ocean’s unlicensed use of the photographs were transformative.
---
On August 3, the U.S. Court of Appeals for the Ninth Circuit issued a ruling in McGucken v. Pub Ocean Ltd. that reversed a Central District of California’s sua sponte grant of summary judgment to Pub Ocean on McGucken’s copyright infringement claims. The case involved Pub Ocean’s unauthorized use of photos of a lake that formed in Death Valley, California, in March 2019. The Ninth Circuit found that all of the fair use factors weighed against a determination that Pub Ocean’s unlicensed use of the photographs were transformative.

## Photos and Publication

In March 2019, photographer Elliot McGucken captured a series of photographs of an usual sight – an ephemeral lake stretching ten miles that had formed on the desert floor in Death Valley after heavy rains. At his own expense, McGucken traveled to the area with his camera and hiked for a few hours until the full expanse of the lake came into view. McGucken applied a classic technique in photographic art using “the golden ratio”, a subject about which he had written a couple of books. He then took and edited the series of photographs.

McGucken shared a few of the photos to Instagram, where they were widely circulated. In response, McGucken was contacted by several website hoping to publish his photos in articles about the Death Valley ephemeral lake. With express permission from McGucken, the photos appeared in SF Gate, The Daily Mail, the National Parks Conservation Association, PetaPixel, Smithsonian Magazine, AccuWeather, Atlas Obscura, and Live Science.

Pub Ocean Ltd. is a UK-based digital publisher who operates a network of websites covering travel, history, parenting, pop culture, and current events. On April 15, 2019, Pub Ocean also published an article about the lake and incorporated twelve of McGucken’s photos, but it neither sought nor received a license from McGucken. The article earned Pub Ocean $6,815.66 in advertising over the course of one year.

Pub Ocean’s article was titled, “A Massive Lake Has Just Materialized In The Middle Of One Of The Driest Places On Earth.” The focus of the articles was the ephemeral lake, including a description of how McGucken came to photograph the lake. The article also included references to ephemeral lakes throughout the world and a series of facts about Death Valley. The article comprised a series of 28 photos, including McGucken’s 12 photos, which were used to illustrate sections of text. For example, a discussion of “superblooms” was preceded by a photo of a field of wildflowers. The photos dominated the layout of the article with the article appearing as a series of photos with text broken up into tiny captions underneath.

## Fair Use Factors

McGucken initially filed this copyright suit against Pub Ocean in the Central District of California. McGucken filed a motion for summary judgment focused on Pub Ocean’s fair use defense. The district court sua sponte granted summary judgment for Pub Ocean, concluding that they were entitled to a fair use defense as a matter of law.

Fair use permits a party to use a copyrighted work without the copyright owner’s permission for purposes such as criticism, comment, news reporting, teaching, scholarship, or research. “[T]he `fair use’ doctrine . . . [is] an `equitable rule of reason’ that `permits courts to avoid rigid application of the copyright statute when, on occasion, it would stifle the very creativity which that law is designed to foster.'” Google LLC v. Oracle Am., Inc. 141 S.Ct. 1183, 1196 (2021) “[T]he fair use of a copyrighted work . . . is not an infringement of copyright.” 17 U.S.C. § 107

Courts must analyze the following non-exhaustive factors in determining whether fair use applies:

- The purpose and character of the use, including whether such use is of a commercial nature or is for non-profit educational purposes;
- The nature of the copyrighted work;
- The amount and substantiality of the portion used in relation to the copyrighted work as a whole; and
- The effect of the use upon the potential market for or value of the copyrighted work.

Courts have relied on the concept of “transformation” or transformative use, in order to identify the level of creativity required for fair use cases. “The word ‘transformative’…describe[s] a copying use that adds something new and important.” Transformative uses are those that add something new, with a further purpose or different character, and do not substitute for the original use of the work. The elements of transformative use are:

- further purpose or different character in the defendant’s work, i.e., the creation of new information, new aesthetic, new insights and understanding;
- new expression, meaning, or message in the original work, i.e., the addition of value to the original; and
- the use of quoted matter as raw material, instead of repackaging it and merely superseding the objects of the original creation. Seuss Enters., L.P. v. ComicMix LLC,983 F.3d 443, 453 (9th Cir. 2020), cert. denied, 141 S.Ct. 2803 (2021).

### No Transformation

In evaluating the first fair use factor, the Ninth Circuit considered whether Pub Ocean’s infringing use of the photos was transformative and whether it was commercial. For-profit news articles are generally considered commercial. Further, “a work that conveys factual information does not transform a copyrighted work by using it as a “clear, visual recording” of the infringing work’s subject.”

Pub Ocean’s article used McGucken’s photos to present a realistic depiction of the ephemeral lake, and the article’s title clearly conveyed that the ephemeral lake in Death Valley was the subject of the article. “The article does not present McGucken’s photos in a new or different light. It uses them for exactly the purpose for which they were taken: to depict the lake.”

The court also found that the article did not meaningfully transform the photos by embedding them within the text of the article. “We have repeatedly held that ‘[a]dding informative captions does not necessarily transform copyrighted works’”. In Pub Ocean’s article, photos were inserted every three to four sentences and the court stated that there was “at most a loose topical connection between each portion of the article and the photos…. Exploiting the beauty and intrigue of McGucken’s photos in this way without adding anything new is not transformative.”

The court’s opinion stated that to be transformative, the infringing use must “bring about a much starker change in expression”, citing the example of a thumbnail image of a photo in a search engine, which was found to be transformative in Perfect 10, Inc. v. Amazon.com, Inc.

Pub Ocean argued that they had arranged McGucken’s photos into a montage, similar to the way that CBS Broadcasting transformed footage of violence at the corner of Florence and Normandie streets during the 1992 Los Angeles uprising by using it as part of a show’s recurring introductory montage. Los Angeles News Service v. CBS Broadcasting, Inc. However, the court distinguished CBS from the present case by articulating that the “critical fact in CBS was not that the plaintiff’s footage was placed in a collection of other video clips but that the footage served a different function when used as part of an introductory montage.”

### Failure on Remaining Factors

Pub Ocean also argued that its use of the photos was for the purpose of reporting the news, which is allowed under the fair use doctrine. Judge Nguyen wrote: “We have recognized that ‘where the content of the [copyrighted] work is the story…, news reporters would have a better claim of transformation’…[b]ut the mere category of ‘news reporting’…is ‘not sufficient itself to sustain a per se finding of fair use.’” Pub Ocean’s article did not make transformative use of McGucken’s photos. Furthermore, the article was commercial, which weighed against a finding of fair use.

In evaluating the nature of the copyrighted work, the court found that McGucken’s photos were the product of artistic creativity, and the fact that they had been published in online articles and on Instagram did not weigh in favor of fair use.

The third factor considers the amount and substantiality of the portion of the copyrighted work that is used in relation to the copyrighted work as a whole. This factor weighs against fair use if the infringer publishes “the heart” of the copyrighted material without justification. Even though Pub Ocean used other photos in addition to McGucken’s photos, it did not diminish the fact that Pub Ocean used McGucken’s photos absent any attempt to edit or transform the photos. “The extent of Pub Ocean’s taking could hardly be greater,” said the court.

Finally, the court evaluated the market effect of Pub Ocean’s article and whether it would adversely affect the potential market for the original photos owned by McGucken. To negate fair use, “McGucken ‘need only show that if the challenged use should become widespread, it would adversely affect the potential market for the copyrighted work.’”

Pub Ocean’s article was found to be a “ready market substitute” for McGucken’s photos because Pub Ocean made the same use of McGucken’s photos as the publications that did obtain licenses to use them. “Pub Ocean’s use, if widespread, would destroy the market to license McGucken’s works,” therefore weighing against fair use.

The court thus concluded that all four factors weighed against a finding of fair use, reversed the district court’s grant of summary judgment, ordered the district court to grant partial summary judgment for McGucken on the fair use issue and remanded for further proceedings.