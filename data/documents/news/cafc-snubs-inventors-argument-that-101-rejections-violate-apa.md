---
title | CAFC Snubs Inventor’s Argument that 101 Rejections Violate APA
author | casey@mclaw.io
date | 2022-08-14
category | Intellectual Property and Technology
subcategory | Patent Counseling and Transactions
tags | patents, apa, cafc
summary | The U.S. Court of Appeals for the Federal Circuit (CAFC), in a precedential decision issued today, affirmed a Patent Trial and Appeal Board (PTAB) finding that claims to a computer system for identifying eligibility for Social Security Disability Insurance (SSDI) benefits are invalid as patent ineligible. The opinion was authored by Judge Chen. The case originates from an examiner’s rejection of Jeffrey Killian’s claims of U.S. Patent Application No. 14/450,042 under Section 101 as being directed to “the abstract idea of ‘determining eligibility for social security disability insurance . . . benefits’” and lacking anything “significantly more” to satisfy Step 2 of the Alice-Mayo two-part test.
---
The U.S. Court of Appeals for the Federal Circuit, (CAFC) in a precedential decision issued today, affirmed the Patent Trial and Appeal Board (PTAB) finding that claims to a computer system for identifying eligibility for Social Security Disability Insurance (SSDI) benefits are invalid as patent ineligible. The opinion was authored by Judge Chen.

The case originates from an examiner’s rejection of Jeffrey Killian’s claims of U.S. Patent Application No. 14/450,042 under Section 101 as being directed to “the abstract idea of ‘determining eligibility for social security disability insurance . . . benefits’” and lacking anything “significantly more” to satisfy Step 2 of the Alice-Mayo two-part test.

On appeal to the PTAB, the Board found the claims to be directed to the patent-ineligible abstract idea of “a search algorithm for identifying people who may be eligible for SSDI benefits they are not receiving” and said the “essential steps” of the claims can be performed in the human mind.  The remaining steps amounted to mere data gathering and therefore constituted “insignificant extra-solution activity,” said the PTAB. Rehearing was denied.

## CAFC: The Claims Fail Alice/Mayo

In its discussion, the CAFC said that the claims do not meet the threshold test of Alice/Mayo and agreed with the Board that they cover an abstract mental process. Specifically, the court said:

> “The thrust of the ’042 application’s representative claim 1 is the collection of information from various sources (a Federal database, a State database, and a caseworker) and understanding the meaning of that information (determining whether a person is receiving SSDI benefits and determining whether they are eligible for benefits under the law)…. The Board correctly concluded that ‘[t]hese steps can be performed by a human, using ‘observation, evaluation, judgment, [and] opinion,’ because they involve making determinations and identifications, which are mental tasks humans routinely do.’” [citations omitted]

That the steps are performed on a generic computer does not save the claims, continued the CAFC. And at Alice step two, the court found no inventive concept. Citing to Electric Power Group v. Alstom, the court said that, similarly to that case, “the claims here do not detail how the computer should go about determining eligibility for benefits, beyond saying that the computer should determine eligibility based ‘on the identified information’ and ‘current SSDI benefit legal requirements.’” This is the same process a person would engage in to determine benefits on their own, “with or without a computer,” said the court.

## Killian: Current 101 Rejections are Arbitrary and Capricious Under APA

On appeal, Killian had argued that the standard articulated in Alice and Mayo for determining eligibility is so indefinite and poorly defined that all decisions of the PTAB and courts finding a claim ineligible should be found “arbitrary and capricious” under the Administrative Procedures Act (APA). Killian said the terms “abstract idea” and “inventive concept” are too vague and that, after the Supreme Court’s landmark ruling in Dobbs v. Jackson Women’s Health Organization, it should be held that the various Section 101 jurisprudence handed down by the Court in Alice; Mayo; Bilski; Parker v. Flook; and Benson are “mere policy preferences” that “stand on exceptionally weak constitutional grounds.”

The CAFC was not persuaded and noted that the APA governs judicial review of “agency action,” and does not apply to courts. As to Killian’s allegations of vagueness, the CAFC acknowledged that while there have been “close cases under the Alice/Mayo standard” with respect to the “void-for-vagueness doctrine,” this is not such a case. The court said Killian’s claims are “clearly patent ineligible in view of our precedent,” and that he failed to show the CAFC’s decisions applying Alice/Mayo are improper.

## Bound by SCOTUS

As to Killian’s claim that all Board decisions finding claims ineligible are arbitrary and capricious under the APA, the court said his gripe is really with the Supreme Court, which once again dooms his argument:

    “At heart, Mr. Killian attacks the Supreme Court’s interpretation of § 101, which it determined includes an implicit exception, as well as the common law standard promulgated by the Court under that interpretation—arguing that the vagueness of those decisions has led to arbitrary and capricious agency decision making. But, as we have already explained, the APA does not empower us to review decisions of ‘the courts of the United States’ because they are not agencies.”

Ultimately, said the CAFC, “we may not announce that the Board acts arbitrarily and capriciously merely by applying binding judicial precedent, as urged by Mr. Killian.”

### The Guidance is There, Says the Court

The court next addressed Killian’s expressed desire for “a single non-capricious definition or limiting principle” for “abstract idea” and “inventive concept.” While it would be impossible to articulate such a definition for “abstract idea,” since there are various types of abstract ideas, “our court has provided guidance as to what constitutes an abstract idea,” said the CAFC. The court cited cases including Affinity Labs, Elec. Power Grp, Interval Licensing, Ericsson Inc. v. TCL Commc’n Tech. Holdings Ltd. and Finjan, Inc. v. Blue Coat Sys., Inc., as evidence of this guidance.

The CAFC also pointed to cases such as BAS- COM Glob. Internet Servs., Inc. v. AT&T Mobility LLC and buySAFE, Inc. v. Google, Inc. to demonstrate its past guidance on the definition of “inventive concept,” particularly as it applies to claims for computer implementations of abstract ideas. Specifically, claims for methods that “improve[] an existing technological process” include an inventive concept at step two, as may claims that “recite a specific, discrete implementation of the abstract idea,” said the court

However, even if the CAFC was persuaded that the Alice/Mayo test is unclear, both it and the PTAB would be bound by Supreme Court precedent and thus the court’s hands are tied until the test is overruled, the opinion explained.

Killian also argued that the “mental steps” doctrine has no foundation in modern patent law and that Diamond v. Diehr and Bilski rejected the doctrine. The CAFC called this analysis “plainly incorrect,” and a misreading of the holdings in those cases.

The court finally addressed Killian’s argument that the examiner and the PTAB had failed to properly consider 55 pages of documents presented to show evidence of eligibility, but the court said that since substantial evidence supports the finding that the claims don’t meet step two, no further evidence is needed. Furthermore, the court said Killian had failed to explain with specificity what the documents show or to include them in the joint appendix.

In an article authored by Killian’s counsel, Burman York Mathis, this May for IPWatchdog, he said of Killian’s appeal:

> “Appellant Killian’s request to the Federal Circuit was clear: Acknowledge that Alice-Mayo is an unworkable and capricious test that the USPTO uses to routinely violate due process of law, and without doubt the Supreme Court will take certiorari. Kill Alice-Mayo here and now, and let the Supreme Court resurrect Alice-Mayo if it dares. Further acknowledge that, in the absence of any constitutional violation in the express language of 35 U.S.C. § 101, the courts lack the authority to rewrite the Patent Law using the test set forth in Washington v. Glucksberg, 521 U.S. 702 (1997) (Listen HERE at time = 8.15), and let the Supreme Court respond.”