---
title | HIPAA Overview
author | casey@mclaw.io
date | 2022-12-14
category | Healthcare
subcategory | Privacy
tags | healthcare, privacy, hipaa
summary| The article explains how HIPAA (Health Insurance Portability and Accountability Act) works and what it means for healthcare providers and their business associates. HIPAA regulates the use and disclosure of protected health information (PHI) and requires covered entities to comply with certain requirements, including privacy of PHI, security of electronic PHI (ePHI), breach notification, and training. HIPAA violations can result in significant fines and legal penalties. The article emphasizes the importance of understanding HIPAA's provisions to ensure compliance with this important law.
---
HIPAA, or the Health Insurance Portability and Accountability Act, is a federal law that was enacted in 1996 to protect the privacy and security of individuals' health information. As a law firm specializing in healthcare, it is crucial to understand how HIPAA works and what it means for your clients. In this blog post, we'll provide a brief overview of HIPAA and its key provisions.

### What is HIPAA?

HIPAA is a federal law that regulates the use and disclosure of protected health information (PHI) by covered entities and their business associates. Covered entities include healthcare providers, health plans, and healthcare clearinghouses, while business associates are any third-party vendors that handle PHI on behalf of covered entities.

HIPAA consists of two main rules: the Privacy Rule and the Security Rule. The Privacy Rule establishes national standards for the protection of PHI and governs how covered entities may use and disclose PHI. The Security Rule, on the other hand, requires covered entities to implement safeguards to protect the confidentiality, integrity, and availability of electronic PHI (ePHI).

### What does HIPAA require?

HIPAA requires covered entities and their business associates to comply with certain requirements, including:

- **Privacy of PHI**: Covered entities must obtain patients' written consent before using or disclosing their PHI, except in certain circumstances. Patients also have the right to access their PHI and request that any errors in their PHI be corrected.

- **Security of ePHI**: Covered entities must implement technical, administrative, and physical safeguards to protect ePHI from unauthorized access, disclosure, and destruction.

- **Breach notification**: Covered entities must notify affected individuals and the Department of Health and Human Services (HHS) in the event of a breach of unsecured PHI.

- **Training**: Covered entities must train their employees on HIPAA's privacy and security requirements and implement sanctions for employees who violate HIPAA.

### What are the penalties for non-compliance?
HIPAA violations can result in significant fines and legal penalties. The HHS Office for Civil Rights (OCR) is responsible for enforcing HIPAA's privacy and security rules and can impose penalties for non-compliance. The penalty structure for HIPAA violations is as follows:

    Tier 1: Unintentional violations, where the covered entity did not know and could not have reasonably known about the violation, can result in a fine of $100 to $50,000 per violation.
    Tier 2: Reasonable cause violations, where the covered entity knew or should have known about the violation, can result in a fine of $1,000 to $50,000 per violation.
    Tier 3: Willful neglect but corrected within 30 days, can result in a fine of $10,000 to $50,000 per violation.
    Tier 4: Willful neglect and not corrected within 30 days, can result in a fine of $50,000 or more per violation.

## Conclusion

HIPAA is a complex law that requires covered entities and their business associates to comply with strict privacy and security requirements. Failure to comply with HIPAA can result in significant legal and financial penalties. As a law firm specializing in healthcare, it is essential to have a comprehensive understanding of HIPAA and its provisions to ensure that your clients are in compliance with this important law.


-----

Individuals, organizations, and agencies that are HIPAA Covered Entities ("CE") must comply with the Privacy Rule's requirements for protecting PHI. These entities also must provide individuals with certain safeguards regarding their PHI. Certain provisions under the Privacy Rule also apply to business associates ("BA") of CEs. However, an entity or individual that is not a CE or a BA is not subject to the Privacy Rule.

HIPAA's Privacy Rule applies to three types of CEs: (1) health plans, (2) health care clearinghouse, and (3) health care providers that conduct certain health care transactions in electronic form. 45 C.F.R. Section 160.103. A health care clearinghouse is an entity that processes nonstandard health information received from another entity into a standard electronic format or standard data content, or vice versa. 45 C.F.R. § 160.103.

Most CEs use the services of several other businesses or persons to perform their health care activities and functions, which are known as Business Associates. A BA is a person or entity that does the following:

1. performs certain functions or activities that involve creating, receiving, maintaining, or transmitting PHI on behalf of a CE, including the following: claims processing or administration; data analysis, processing, or administration; utilization review and quality assurance; certain patient safety activity; and billing, practice management, and repricing.
2. providing services to a CE that involves access to PHI, such as the following: legal, acturial, accounting, consulting, or financial services; management or administrative services; data aggregation; or accreditation.

45 C.F.R. § 160.103.

The following entities that conduct business with a CE also are BAs:

- A contractor that provides data transmission services of PHI and requires routine access to the information (for example, a health information exchange, regional health information organization, or e-prescribing gateway).
- A vendor that allows a CE to offer workforce members access to a personal health record.

Furthermore, a subcontractor that creates, receives, maintains, or transmits PHI on a BA's behalf is a BA in its own right. 45 C.F.R. § 160.103; see CVS Pharmacy, Inc. v. Press Am., Inc., 377 F. Supp. 3d 359 (S.D.N.Y. 2019) (declining to impose performance guarantee penalties under a subcontractor's agreement with a health plan's pharmacy benefit manager (PBM)—and HIPAA BA—after the subcontractor mailed individuals' PHI to the wrong addresses). For example, assume that a BA hires a company to handle document and media shredding to securely dispose of paper and electronic PHI. The shredding company is directly required to comply with the Privacy Rule.