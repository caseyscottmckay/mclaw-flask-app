---
title | Maintaining Trademark Registrations
author| casey@mclaw.io
date| 2021-12-21
category | Intellectual Property and Technology
subcategory | Trademark
tags | trademark, registration, maintenance
summary| This article discusses the deadlines and requirements for maintaining federal trademark registrations with the US Patent and Trademark Office (USPTO). The article discusses proving continued use under Section 8 and Section 71, renewing registrations under Section 9, and filing for incontestability under Section 15, including special requirements for international registrations.
Trademarks are among a business entity's most valuable assets. The owner of a US trademark registration must take steps to maintain that registration or risk forfeiting valuable rights. There are also optional filings a trademark owner can make to optimize those rights.
This Article discusses the requirements and timing for these measures, including:

- Filing the required proof of continued use (see Declaration of Continued Use or Excusable Nonuse Under Section 8 or 71).
- Filing registration renewals (see Renewal Application Under Section 9).
- Filing incontestability declarations or affidavits (see Declaration of Incontestability Under Section 15).
- Filing corrections and amendments (see Corrections and Amendments).
- Recording assignments (see Changes in Ownership).

In this Article, "trademark" and "mark" refer to both trademarks and service marks.

## The Value of a Trademark Registration
US trademark rights derive from use of a mark, not its registration. However, registering a mark on the Principal Register grants a trademark owner certain rights and privileges beyond the right of exclusive use that attaches to trademark ownership. These include:

- Prima facie evidence of validity and the registrant's ownership of the mark.
- Presumed exclusive right to use the mark nationwide.
- Constructive notice to third parties of the registrant's rights.
- Federal court subject matter jurisdiction over infringement and related unfair competition claims.
- The right to use the ® symbol and other notices of federal registration.
- Statutory remedies for federal trademark infringement claims, including, under certain circumstances:
  - treble damages; and
  - recovery of attorneys' fees and costs.

For more information on registering a mark in the US, see Practice Articles, Acquiring Trademark Rights and Registrations and Filing a Federal Trademark Application, US Trademark Application Flowchart: Use-Based Application, and US Trademark Application Flowchart: Intent-to-Use Application.
A trademark owner who registers a mark with the USPTO must take affirmative steps to maintain that registration or risk forfeiting the registration benefits that may have taken many years and dollars to acquire. A trademark owner may also choose or need to make other filings to protect and optimize its rights.

## Required Maintenance
To keep a federal trademark registration in effect, the trademark owner must periodically file the following documents, along with their required fees (see Practice Article, Acquiring Trademark Rights and Registrations: Box, USPTO Filing Fees):

- A Declaration (or Affidavit) of Continued Use or Excusable Nonuse (see Declaration of Continued Use or Excusable Nonuse Under Section 8 or 71).
- A Renewal Application (see Renewal Application Under Section 9).

With limited exceptions, these documents must be submitted through the USPTO's Trademark Electronic Application System (TEAS) (37 C.F.R. § 2.23(a); see Article, Understanding the New USPTO Trademark E-Filing and Specimen Requirements: Exceptions for Paper Submissions).
The USPTO provides TEAS forms for these and other filings relating to maintaining trademark registrations (see Box, USPTO Electronic Forms). A free USPTO.gov account is required to access TEAS (see Legal Update, USPTO Updates TEAS Access to Require USPTO.gov Account).
Failure to make these filings results in the loss or limitation of the benefits of registration. For example, a registration that is not timely renewed can expire (37 C.F.R. 2.182). (For more on trademark cancellations, see Practice Article, TTAB Oppositions and Cancellations: Grounds and Defenses.)

### Requirement for US Counsel
In 2019, the USPTO began requiring foreign-domiciled registrants to retain a qualified US-licensed attorney to represent them before the USPTO and handle any maintenance filings (37 C.F.R. § 2.11). An applicant or registrant is foreign-domiciled if it is either:

- An individual with a permanent legal residence outside the US or its territories.
- An entity with its principal place of business (headquarters) outside the US or its territories.

(37 C.F.R. §§ 2.2(b), (o), (p) and 2.11.)
### Declaration of Continued Use or Excusable Nonuse Under Section 8 or 71
To keep a federal trademark registration valid, its current owner must regularly file with the USPTO a Declaration (or Affidavit) of Continued Use or Excusable Nonuse under:

- Section 8, for most registrations (15 U.S.C. § 1058).
- Section 71, if the registration is an international registration extended to the US (15 U.S.C. § 1141k).

The trademark owner must include a fee (see Practice Article, Acquiring Trademark Rights and Registrations: Box, USPTO Filing Fees) and make the filing within one year before the end of each of the:

- Sixth year of registration.
- Tenth year of registration and each successive ten-year period after that.

(15 U.S.C. §§ 1058(a) and 1141k(a); 37 C.F.R. § 2.160; Trademark Manual of Examining Procedure (TMEP) § 1604.)
For an extra fee, the owner can take advantage of a six-month grace period following each deadline. Failure to timely file a Section 8 or 71 Declaration results in the automatic cancellation of the federal registration, though any acquired common law rights remain (37 C.F.R. § 2.160(a)(3)).
The Declaration
The Section 8 or 71 Declaration (or Affidavit) of Use must:

- Identify the trademark by registration number.
- Be filed by the registration's owner during the applicable time period.
- Include a verified statement:
  - executed during the applicable filing period; and
  - attesting to the mark's use in commerce during the applicable time period.
- Specify the goods or services to which the declaration applies, as well as those not covered by it. Unless the trademark owner is claiming excusable nonuse with respect to the excluded goods or services, they will be deleted from the registration (see Excusable Nonuse).
- State that the registered mark is in use in connection with the covered goods and services.

(37 C.F.R. §§ 2.161(a) and 7.37(a).)
As with all submissions to the USPTO, the declaration must contain the following language:

- "The signatory being warned that willful false statements and the like are punishable by fine or imprisonment, or both, under 18 U.S.C. 1001, and that such willful false statements and the like may jeopardize the validity of the application or submission or any registration resulting therefrom, declares that all statements made of his/her own knowledge are true and all statements made on information and belief are believed to be true."

(37 C.F.R. § 2.20.)
TEAS Section 8 and 71 filing forms are available on the USPTO website (see Box, USPTO Electronic Forms).
If all statutory requirements are met, a Section 8 Declaration can be combined into one filing with:

- A renewal application (see Renewal Application Under Section 9).
- An incontestability declaration (see Declaration of Incontestability Under Section 15). A Section 71 filing can also be made together with a Section 15 filing.

TEAS combined Section 8 and 9 and combined Section 8 and 15 filing forms are available on the USPTO website (see Box, USPTO Electronic Forms).
Specimens of Use
Unless the trademark owner is claiming excusable nonuse, each continued use filing must include at least one specimen per registration class showing actual use of the mark in commerce in connection with one of the goods or services in the class (see Excusable Nonuse). The specimen must also show an association between the registered mark and the goods or services covered by the registration. (37 C.F.R. § 2.56(a), (b).)
Counsel generally submits copies, photos, or web page printouts of the specimen in JPG or PDF format and retains the original for the file. However, the reproduction must show enough of the specimen to identify the specimen, the mark, and the applicable goods or services. Web page printouts must also include the URL and access or print date. (37 C.F.R. § 2.56(c).)
Acceptable specimens for a Section 8 or 71 Declaration, like those accompanying the initial application, may include:

- For goods: photos of labels or tags affixed to the goods. If not shown physically attached to the goods, a label or tag should include informational matter that typically appears in connection with that type of goods, such as net weight information or a UPC code.
- For services: a magazine advertisement, menu, letterhead, invoice, or other material that shows the mark in the sale or advertising of the services.

(See generally 37 C.F.R. § 2.56 and TMEP §§ 904.03 and 1301.04.)
For more examples of appropriate specimens for different goods and services, see Trademark Acceptable Specimen Chart.
Initially, only one specimen per class of goods or services is typically required. If anything is unclear about the specimen, for example, if the specimen is of an unusual type or if counsel is uncertain about whether the mark in the specimen sufficiently matches the drawing in the registration, the registrant should:

- Provide an explanation.
- Consider submitting an additional, alternative specimen.

This may help avoid an office action and the declaration's refusal (see Post-Registration Office Actions).
The mark in any submitted specimen should be the same as the registered mark, but the two do not need to be stylistically identical (see TMEP 1604.13). For example, the USPTO may accept a specimen showing the same mark but in a slightly modernized style or with an altered background. The USPTO decides this on a case-by-case basis. If the mark in the specimen creates the same overall commercial impression as the registered mark, the USPTO should not reject the specimen as depicting a material alteration of the registered mark. (For a discussion of more significant changes, see Amendments.)
USPTO Post Registration Proof of Use Audit Program
The USPTO randomly audits registrants filing Section 8 or 71 Declarations for more than one good or service per class. Selected registrants receive office actions requesting additional proof of use of the registered marks for two additional goods or services in each audited class, in the form of any or all of the following:

- Information.
- Exhibits.
- Affidavits or declarations.
- Specimens of use.

The USPTO deletes goods or services for which an audited registrant cannot provide sufficient proof of use and cancels a registration in full if the registrant fails to either:

- Respond to audit office actions.
- Provide acceptable proof of use for any goods or services in the registration.

The USPTO has posted questions and answers about this program on its website.
For more on the implementation of this program, see Legal Update, USPTO Publishes Final Rule Facilitating Verification of Certain Trademark Use Claims in Affidavits or Declarations.
Nonuse Petitions Under the Trademark Modernization Act of 2020
Pursuant to the Trademark Modernization Act of 2020, enacted on December 27, 2020, the USPTO is establishing new procedures to allow third parties to petition the USPTO Director to expunge or reexamine a trademark registration based on the registered mark's nonuse. Practical Law will update this resource as relevant developments arise. For more, see Legal Update, Trademark Modernization Act of 2020 Codifies Irreparable Harm Presumption and Establishes New Trademark Procedures.
Excusable Nonuse
If a registered mark is not in use due to special circumstances that excuse the nonuse, the mark's owner can fulfill its Section 8 or 71 obligation and keep the registration from being cancelled by filing a Declaration (or Affidavit) of Excusable Nonuse (37 C.F.R. § 2.161(a)(6)(ii)).
To establish the necessary special circumstances, the trademark owner must list sufficient facts to demonstrate, for each class of the registration's goods or services not in use, that the nonuse is both:

- Temporary.
- Beyond the owner's control.

(See TMEP § 1604.11.)
Depending on the details, the following may be considered special circumstances:

- War.
- Trade embargo.
- Natural disaster.
- Illness of the owner, but only if there are sufficient facts to show that the owner is indispensable to the business.

(See TMEP § 1604.11.)
Business strategy decisions and use of the mark on different good or services or in another form traditionally do not excuse the nonuse.
The basic requirements for a nonuse declaration are the same as those for the use declaration, including that the filing must be accompanied by the specified fee (see Practice Article, Acquiring Trademark Rights and Registrations: Box, USPTO Filing Fees). However, instead of attesting to use, the trademark owner must set out for the goods or services in connection with which the registered mark is not in use:

- When use of the mark in commerce stopped.
- The approximate date when use is expected to resume.
- The reason for the nonuse.
- What steps the trademark owner is taking to resume the mark's use.
- Any other relevant facts that excuse the nonuse.

(37 C.F.R. § 2.161(a)(6)(ii); TMEP § 1604.11.)

### Renewal Application Under Section 9
Most trademark registrations must be renewed every ten years. A written renewal application (Section 9 Application) must be:

- Filed during the last year of each successive ten-year period for which the registration is issued or renewed.
- Accompanied by payment of the required fee (see Practice Article, Acquiring Trademark Rights and Registrations: Box, USPTO Filing Fees).

(37 C.F.R. §§ 2.181(2), 2.182, and 2.183; TMEP § 1606.)
For an extra fee, the trademark owner can take advantage of a six-month grace period following the deadline (TMEP § 1606.13(b)). The registration expires if a timely renewal application is not filed (37 C.F.R. § 2.182; TMEP § 1606.13(c)).
The Section 9 renewal process does not apply to international registrations. International registration holders must renew their registrations with the International Bureau ( 37 C.F.R. § 7.41; TMEP § 1614).
The Application
The Section 9 Application must include:

- A request for renewal signed by the registrant or the registrant's representative.
- A list of the particular goods or services covered by the renewal, if the renewal application does not cover all goods and services in the registration.

(37 C.F.R. § 2.183.)
The renewal application is usually combined with a Section 8 Declaration, due at the same time (see Declaration of Continued Use or Excusable Nonuse Under Section 8 or 71).
TEAS Section 9 and combined Section 8 and 9 filing forms are available on the USPTO website (see Box, USPTO Electronic Forms).

### Trademark Maintenance Safeguards
Because a lot is at stake, it is important for trademark owners to retain information pertaining to their registrations and related filings, and to keep it well organized. Certain simple, common-sense practices can help a trademark owner or its counsel maintain and enforce trademark registrations effectively.
Deadlines
If the trademark holder maintains a current email address and authorizes email communication with the USPTO, the USPTO sends courtesy emails reminding of maintenance filing deadlines. However, trademark owners should also maintain their own docket or deadline system and not rely solely on those reminders.
For example, it is common for an owner of even a modest trademark portfolio or its counsel to maintain a database (either internally developed or purchased from a commercial provider) that:

- Tracks the applicable maintenance and renewal requirements and associated deadlines for each registration.
- Provides electronic notifications to the trademark owner or its counsel of upcoming maintenance and renewal deadlines.

Also, many companies establish pre-paid deposit accounts with the USPTO to ease the administrative burden associated with paying USPTO fees for registration maintenance. A deposit account application can be completed on the USPTO website.
Recordkeeping
Because evidence of a mark's use is essential to the maintenance of a trademark registration (as well as in any litigation involving the mark), it is critical that trademark owners collect and preserve relevant materials, such as:

- Representative samples of product packaging displaying trademarks.
- Representative samples of advertising and promotional materials displaying trademarks.
- Invoices, purchase orders, and other sales records documenting the dates that trademarks were first used.
- Documents demonstrating sales of products or services under the trademarks.
- Market research showing consumer recognition of the trademarks or the products or services associated with them.

Trademark owners should also maintain these records in a way that ensures their effectiveness by, for example:

- Organizing them so that counsel can readily find relevant use evidence for each relevant class of goods or services for each mark.
- Maintaining them along with whatever background or contextual information is necessary to make them useable as evidence. If the trademark holder's witness has no way of knowing the date or history of a particular specimen, the specimen may be of little value, for example, in response to an abandonment claim.

Trademark owners should also maintain records to aid in enforcement efforts against third parties, for example, to establish a mark's:

- Duration and geographic scope of use.
- Priority of use.
- Strength and recognition.
- Intended market.

For more, see Legal Update, The Right Stuff (Thorough Recordkeeping for Better Brand Protection).
For resources that can be used to educate employees on how to use and protect company trademarks, see Standard Documents, Trademark Use and Protection Guidelines (Internal Distribution) and Trademark Use and Protection Training: Presentation Materials.

## Optional Post-Registration Measures
There are also optional filings a trademark owner can take advantage of to optimize and secure its rights under a US trademark registration.
### Declaration of Incontestability Under Section 15
Once a mark registered on the Principal Register has been in continuous use in commerce for at least five consecutive years after registration, the owner can file a Declaration (or Affidavit) of Incontestability (Section 15 Declaration or Affidavit). Incontestability status offers several advantages, including that third parties can no longer challenge:

- The registrant's ownership of the trademark.
- The mark on the basis that it is merely descriptive of the goods or services or was improperly registered.

(15 U.S.C. § 1065; TMEP § 1605.)
Other than cost, which is minimal, there is no drawback to filing a Section 15 Declaration.
A trademark owner can file a Section 15 Declaration separately or combined with a six-year Declaration of Continued Use (see Declaration of Continued Use or Excusable Nonuse Under Section 8 or 71), however the USPTO does not acknowledge a Section 15 Declaration unless a use declaration or affidavit is pending or has been accepted.
The USPTO does not substantively review Section 15 filings. It merely confirms that the filing is facially sufficient and then updates the registration record to reflect receipt of the Section 15 Declaration. Therefore, whether the registration is actually incontestable is determined only if the mark is challenged in a court or TTAB proceeding. (TMEP § 1605.)

The Declaration
The Section 15 Declaration (or Affidavit) must:

- Include a fee (see Practice Article, Acquiring Trademark Rights and Registrations: Box, USPTO Filing Fees).
- Be filed by the owner of a trademark registration after the mark has been in continuous use in commerce for at least five consecutive years after the date of registration.
- Specify in connection with which goods or services the registered mark has been in continuous use in commerce for at least five consecutive years after the date of registration. (The filing does not need to cover the entire registration. The trademark owner can file more than one Section 15 Declaration at different times to cover different goods or services.)
- State that the mark is still in use in connection with those goods or services.
- State that there:
  - has been no final decision adverse to the owner’s claim of ownership of the mark for the covered goods or services; and
  - is no proceeding involving these rights pending in the USPTO or in a court and not finally disposed of.

(37 C.F.R. § 2.167.)
As with all submissions to the USPTO, the declaration in a Section 15 filing must contain the following language:

- "The signatory being warned that willful false statements and the like are punishable by fine or imprisonment, or both, under 18 U.S.C. 1001, and that such willful false statements and the like may jeopardize the validity of the application or submission or any registration resulting therefrom, declares that all statements made of his/her own knowledge are true and all statements made on information and belief are believed to be true."

(37 C.F.R. § 2.20.)
TEAS Section 15, combined Section 8 and 15, and combined Section 71 and 15 filing forms are available on the USPTO website (see Box, USPTO Electronic Forms).

### Corrections and Amendments
Corrections
The trademark owner should promptly report to the USPTO any mistakes in the certificate of registration. The USPTO issues a certificate of correction or, at its discretion, a new registration certificate:

- For a fee, if the error is due to a good faith mistake by the trademark owner and does not require republication due to a material change (15 U.S.C. § 1057(h); TMEP § 1609.10(b)).
- Free of charge, if the error is the USPTO's fault (15 U.S.C. § 1057(g); TMEP § 1609.10(a)).

A USPTO error needing a material change to the registration to correct may require the registration to be cancelled as inadvertently issued and republished (TMEP § 1609.10(a)). The USPTO does not correct an applicant error that requires a material change to the registration (TMEP § 1609.10(b)).
Amendments
A trademark owner can, for a fee, amend its trademark registration if the amendment does not materially alter the mark (15 U.S.C. § 1057(e)). For example, a trademark owner may choose to:

- Add a geographic limitation to the registration, resulting from a settlement agreement.
- Delete certain goods and services in connection with which the mark is no longer in use.
- Make slight alterations to the mark itself, for example minor changes:
  - to the style, size, or other form; or
  - to delete generic material.

However, any expansion of the scope of the protection under the registration or change in the commercial impression of the mark constitutes a material alteration that requires a new application (TMEP § 1609.02(a)).
To decide whether a proposed amendment materially alters a registered mark, the USPTO compares the proposed amendment to the originally registered mark. If the change would have required re-publication of a published application to present the mark fairly for purposes of opposition, the amendment is a material alteration (TMEP § 1609.02(a)). An amendment generally is acceptable if the modified mark:

- Contains the essence of the original mark.
- Creates essentially the same impression as the original mark.

(TMEP § 1609.02(a).)
TEAS request for amendment or correction of a registration certificate forms are available on the USPTO website (see Box, USPTO Electronic Forms).
### Changes in Ownership
If the ownership of a registered mark changes, the assignee:

- Should:
  - record the assignment in the USPTO and submit the required fee; and
  - request that the trademark register be updated (TMEP § 609.02(f)).
- May request a new certificate of registration issued in its name.

Although there is no legal requirement in the US to do so, an assignee should record the assignment and update the USPTO records because otherwise:

- A competitor attempting to clear a similar mark may rely on the outdated owner information when determining whether the registered mark is in use.
- Any renewal will list the previous owner (TMEP § 1606.06).

A TEAS assignment recordation form is available on the USPTO website (see Box, USPTO Electronic Forms).
For a form trademark assignment, see Standard Document, Trademark Assignment Agreement (Short-Form).
## Post-Registration Office Actions
The USPTO responds to deficiencies in post-registration filings by issuing an office action. For example, the Examining Attorney may request a substitute filing if it finds unacceptable the wording of an affidavit or the sufficiency of a specimen. The USPTO may also issue an office action requesting additional proof to support a use filing (see USPTO Post Registration Proof of Use Audit Program).
Typically, the trademark owner has six months to respond to a post-registration office action, just as with an office action in connection with the initial application. However, that time may actually be longer if the time period for the deficient filing has not expired by the office action response deadline (see TMEP §§ 1604.15, 1606.12).
Any substitute specimen submission must include an attestation that the new specimen was in use during the relevant period. Therefore, the USPTO refuses an otherwise satisfactory substitute specimen if the trademark owner does not affirm that it was in use during the acceptable window for its initial submission.
For office actions concerning required filings, such as Declarations of Continued Use and Renewal Applications, the USPTO cancels the registration if the owner does not respond to the office action within six months, unless time remains in the filing window or any applicable grace period. The Director of the USPTO may accept a late office action response, but only:

- In an extraordinary situation.
- Where justice requires and no other party is injured.

The unintentional delay standard for reviving abandoned applications after late office action responses does not apply to post-registration office actions. (TMEP § 1604.16.)
For optional filings, like a Declaration of Incontestability, the USPTO merely disregards the challenged filing if it does not receive a timely office action response (37 C.F.R. § 2.163).
If after an office action response the USPTO maintains the refusal, the trademark owner has six months to petition the Director of the USPTO to review the decision. Only once the Director issues a decision can the owner file for review on appeal or in a civil action. (37 C.F.R. §2.165.)
A TEAS form for responding to a post-registration office action is available on the USPTO website (see Box, USPTO Electronic Forms).

##  USPTO Electronic Forms
With limited exceptions, all registration and maintenance documents must be filed electronically, through the USPTO's Trademark Electronic Application System (TEAS) and Electronic Trademark Assignment System (ETAS) (37 C.F.R. § 2.23(a); see Article, Understanding the New USPTO Trademark E-Filing and Specimen Requirements: Exceptions for Paper Submissions). A free USPTO.gov account is required to access TEAS (see Legal Update, USPTO Updates TEAS Access to Require USPTO.gov Account).
The USPTO provides extensive information on the trademark process on its website, including at Trademarks Home. Users can send an email to TrademarkAssistanceCenter@uspto.gov or telephone the Trademark Assistance Center at (800) 786-9199 with any questions not answered on the site.
Users can send questions about technical issues and the TEAS filing process by email to TEAS@uspto.gov. The email should include a telephone number for contact and, if existing, the relevant serial number or registration number.
The USPTO provides links to commonly used forms on its website (see USPTO: Index of All TEAS Forms (log-in required to access forms)), including:

- Declarations of:
  - Use and/or Excusable Nonuse of a Mark Under Section 8;
  - Incontestability of a Mark Under Section 15; and
  - Use and Excusable Nonuse Under Section 71
- Combined Declarations of:
  - Use & Incontestability Under Sections 8 & 15;
  - Use & Incontestability Under Sections 71 & 15; and
  - Use and/or Excusable Nonuse/Application for Renewal Under Sections 8 & 9.
- Response to Post-Registration Office Action.
- Section 7 Request for Amendment or Correction of Registration Certificate.
- Trademark Assignment Recordation Form.

The USPTO also provides PDF versions of TEAS forms for filers to preview or to use when TEAS is unavailable and all requirements for paper filing are met (see Article, Understanding the New USPTO Trademark E-Filing and Specimen Requirements: Exceptions for Paper Submissions).
