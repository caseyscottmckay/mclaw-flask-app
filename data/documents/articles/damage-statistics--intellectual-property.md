---
title| Damage Statistics---Intellectual Property
author| casey@mclaw.io
date| 2023-01-25
category | Intellectual Property and Technology
subcategory | Computers
tags | copyright, trademarks, patents
summary|Intellectual property (IP) litigation is a legal process that involves disputes over the ownership and protection of creative works, inventions, and other types of intangible assets. It is a complex area of law that involves a wide range of legal issues and is often associated with high-stakes legal battles.
---


In the United States, the average amount of damages in IP litigation can vary widely depending on the type of intellectual property at issue, the severity of the infringement, and a number of other factors. In this blog post, we will explore some of the factors that can influence the amount of damages in IP litigation and provide some examples of notable cases.

## Types of Intellectual Property and Damages

There are several types of intellectual property, including patents, trademarks, copyrights, and trade secrets. Each type of IP is subject to different legal protections and can result in different types of damages.

Patents are a type of IP that protects inventions and processes. In patent litigation, damages can be calculated based on lost profits or a reasonable royalty. Lost profits are calculated based on the amount of money the plaintiff would have earned if the infringing product had not been on the market. A reasonable royalty is a hypothetical payment that the infringing party would have paid to the patent holder to license the technology.

Trademarks are a type of IP that protects words, phrases, symbols, and designs that identify the source of goods or services. In trademark litigation, damages can be calculated based on the profits that the infringing party made from the use of the trademark.

Copyrights are a type of IP that protects creative works such as books, music, and art. In copyright litigation, damages can be calculated based on the profits that the infringing party made from the use of the copyrighted work or based on the value of a hypothetical license fee.

Trade secrets are a type of IP that protects confidential information such as customer lists, manufacturing processes, and marketing strategies. In trade secret litigation, damages can be calculated based on the profits that the infringing party made from the use of the trade secret or based on the costs that the plaintiff incurred in developing the trade secret.

## Factors Affecting Damages

The amount of damages in IP litigation can be influenced by a wide range of factors, including the strength of the evidence, the severity of the infringement, the type of intellectual property at issue, and the jurisdiction in which the case is being heard.

For example, in a patent litigation case, the amount of damages can be influenced by the strength of the patent itself. If the patent is weak or has a narrow scope, the damages may be lower. Similarly, if the infringement is severe or widespread, the damages may be higher.

In trademark litigation, the amount of damages can be influenced by the strength of the trademark and the extent to which it has been diluted by the infringing use. In copyright litigation, the amount of damages can be influenced by the extent to which the infringing use was intentional or willful.

## Notable Cases

There have been many notable cases in which the damages in IP litigation have been significant. For example, in the Apple v. Samsung patent litigation case, Apple was awarded $1.05 billion in damages after a jury found that Samsung had infringed on several of Apple's patents. In the Oracle v. Google copyright litigation case, Oracle was awarded $9.3 billion in damages after a jury found that Google had infringed on Oracle's copyright in the Java programming language.



## Federal Damage Statistics for Intellectual Property
- data based on 9,914 federal district court cases dating from 2003 to 2023

#### Distribution of Damage Amounts
Distribution | Percentage of total dockets
$1-$5k  | 18%
$5k-$50k | 46%
$50k-$100k | 9%
$100k-$500k | 16%
$500k-$1m  | 4%
$1m-$50m | 8%
$50m+ | 1%  | 

#### Damage Types (Monetary Damages)
Damages Type  | Number of dockets  | Median awarded amount
Compensatory Damages  | 3,957  | $25,000
Settlement  | 1,558  | $12,472
Statutory Damages  | 894  | $35,000
Liquidated Damages  | 68  | $26,261
Interest  | 63  | $22,313
Punitive Damages  | 32  | $75,000

#### Damage Types (Attorneys Fees and Costs)
Damages Type  | Number of dockets  | Median awarded amount
Litigation Costs & Expenses  | 3,971  | $703
Attorney Fees  | 2,112  | $9,564

#### Court (Monetary Damages)
Court  | Number of dockets  | Median awarded amount
C.D.Cal.  | 942  | $3,954
N.D.Cal.  | 282  | $10,570
N.D.Ill.  | 263  | $3,685
N.D.Tex.  | 223  | $1,000
S.D.N.Y.  | 223  | $1,832
D.Ariz.  | 194  | $2,771
E.D.N.Y.  | 174  | $320
N.D.Ga.  | 165  | $3,178
E.D.Tex.  | 132  | $20,848
S.D.Fla.  | 131  | $2,700
M.D.Fla.  | 129  | $4,749
D.Colo.  | 122  | $4,434
E.D.Va.  | 119  | $4,746
D.N.J.  | 116  | $4,714
D.Nev.  | 114  | $5,714
S.D.Tex.  | 111  | $7,450
E.D.Pa.  | 101  | $3,587
S.D.Cal.  | 97  | $9,335
W.D.Wash.  | 86  | $8,898
D.S.C.  | 76  | $420

#### Court (Monetary Damages)
Court  | Number of dockets  | Median awarded amount
C.D.Cal.  | 942  | $3,954
N.D.Cal.  | 282  | $10,570
N.D.Ill.  | 263  | $3,685
N.D.Tex.  | 223  | $1,000
S.D.N.Y.  | 223  | $1,832
D.Ariz.  | 194  | $2,771
E.D.N.Y.  | 174  | $320
N.D.Ga.  | 165  | $3,178
E.D.Tex.  | 132  | $20,848
S.D.Fla.  | 131  | $2,700
M.D.Fla.  | 129  | $4,749
D.Colo.  | 122  | $4,434
E.D.Va.  | 119  | $4,746
D.N.J.  | 116  | $4,714
D.Nev.  | 114  | $5,714
S.D.Tex.  | 111  | $7,450
E.D.Pa.  | 101  | $3,587
S.D.Cal.  | 97  | $9,335
W.D.Wash.  | 86  | $8,898
D.S.C.  | 76  | $420

#### Court (Monetary Damages)
Case type  | Number of dockets  | Median awarded amount
Intellectual Property & Technology  | 6,457  | $22,433

#### Court (Attorney Fees and Costs)
Case type  | Number of dockets  | Median awarded amount
Intellectual Property & Technology  | 5,548  | $3,604

#### Law Firm (Monetary Damages)
Foley & Lardner LLP  | 276  | $6,750
Bryan Cave Leighton Paisner LLP  | 247  | $6,750
Cowan, Liebowitz & Latman, P.C.  | 212  | $6,000
Greenberg Traurig, LLP  | 150  | $102,501
Shook, Hardy & Bacon L.L.P.  | 136  | $6,750
Greer, Burns & Crain, Ltd.  | 119  | $150,000
Robinson & Cole LLP  | 115  | $6,000
J. Andrew Coombs  | 113  | $100,000
Balch & Bingham LLP  | 101  | $6,750
DLA Piper LLP (US)  | 90  | $9,875
Troutman Pepper Hamilton Sanders LLP  | 86  | $9,000
Nexsen Pruet, LLC  | 84  | $6,000
Mitchell Silberberg & Knupp LLP  | 83  | $7,800
Reed Smith LLP  | 65  | $7,920
Loeb & Loeb LLP  | 63  | $6,000
Pattishall, McAuliffe, Newbury, Hilliard & Geraldson LLP  | 60  | $7,240
Kilpatrick Townsend & Stockton LLP  | 59  | $48,029
Jenner & Block LLP  | 58  | $6,750
Mandell Menkes LLC  | 58  | $6,750
Arnold & Porter Kaye Scholer LLP  | 57  | $25,000

#### Law Firm (Attorney Fees and Costs)
Foley & Lardner LLP  | 228  | $420
Bryan Cave Leighton Paisner LLP  | 210  | $420
Cowan, Liebowitz & Latman, P.C.  | 131  | $315
DLA Piper LLP (US)  | 121  | $1,586
Greenberg Traurig, LLP  | 119  | $8,852
Perkins Coie LLP  | 113  | $11,943
Shook, Hardy & Bacon L.L.P.  | 110  | $333
Troutman Pepper Hamilton Sanders LLP  | 106  | $3,330
Kilpatrick Townsend & Stockton LLP  | 97  | $10,744
Quinn Emanuel Urquhart & Sullivan, LLP  | 92  | $23,208
Jones Day  | 85  | $7,871
Balch & Bingham LLP  | 84  | $420
Reed Smith LLP  | 83  | $620
Nexsen Pruet, LLC  | 81  | $420
Fish & Richardson P.C.  | 79  | $22,032
K&L Gates LLP  | 77  | $13,425
Morgan, Lewis & Bockius LLP  | 75  | $15,396
Arnold & Porter Kaye Scholer LLP  | 73  | $11,635
Mitchell Silberberg & Knupp LLP  | 73  | $285
Faegre Drinker Biddle & Reath LLP  | 70  | $4,202


#### Year (Monetary Damages)
Year  | Number of dockets  | Median awarded amount
2022  | 71  | $35,000
2021  | 137  | $60,000
2020  | 129  | $40,000
2019  | 171  | $33,415
2018  | 157  | $50,000
2017  | 178  | $32,982
2016  | 174  | $58,945
2015  | 182  | $35,700
2014  | 213  | $73,358
2013  | 228  | $56,768
2012  | 175  | $60,000
2011  | 239  | $60,456
2010  | 258  | $99,500
2009  | 242  | $74,957
2008  | 364  | $31,217
2007  | 537  | $10,580
2006  | 715  | $7,450
2005  | 697  | $7,500
2004  | 559  | $7,678
2003  | 215  | $25,000


#### Year (Attorney Fees and Costs)
Year  | Number of dockets  | Median awarded amount
2022  | 22  | $6,570
2021  | 77  | $8,643
2020  | 130  | $3,046
2019  | 156  | $6,000
2018  | 216  | $5,536
2017  | 206  | $7,452
2016  | 204  | $9,178
2015  | 198  | $7,131
2014  | 189  | $8,000
2013  | 244  | $7,364
2012  | 223  | $8,074
2011  | 218  | $6,891
2010  | 215  | $6,293
2009  | 190  | $6,673
2008  | 258  | $3,722
2007  | 425  | $1,338
2006  | 590  | $420
2005  | 507  | $375
2004  | 417  | $325
2003  | 240  | $3,309


## Conclusion

Intellectual property litigation is a complex area of law that involves a wide range of legal issues and can result in significant damages. The amount of damages in IP litigation can be influenced by a wide range of factors, including the strength of the evidence, the severity of the infringement, and the type of intellectual property at issue.

While the average amount of damages in IP litigation can vary widely depending on the specific case, it is important for both plaintiffs and defendants to understand the potential costs involved in pursuing or defending against an IP lawsuit. In addition to the financial costs, IP litigation can also have significant business implications, such as reputational harm or lost market share.

It is worth noting that some IP cases may also result in injunctive relief, which prohibits the infringing party from continuing to use or distribute the IP at issue. In some cases, an injunction may be more valuable to the plaintiff than damages, as it can prevent future harm and provide a stronger legal foundation for protecting their IP rights.

In conclusion, the average amount of damages in IP litigation in the United States can vary widely depending on a range of factors. While significant damages awards are not uncommon in high-profile cases, it is important for both plaintiffs and defendants to understand the potential financial and business costs of IP litigation and to seek experienced legal counsel to help navigate the complex legal landscape of IP disputes. Ultimately, protecting and enforcing IP rights is an important part of fostering innovation and promoting economic growth, and the legal system plays a vital role in ensuring that those rights are respected and upheld.