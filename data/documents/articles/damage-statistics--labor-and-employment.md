---
title| Damage Statistics---Arbitration
author| casey@mclaw.io
date| 2023-01-25
category | Intellectual Property and Technology
subcategory | Computers
tags | arbitration
summary|
---
Labor and employment litigation involves disputes between employers and employees over issues such as workplace discrimination, harassment, wrongful termination, and wage and hour violations. In the event that a lawsuit is filed in federal US district court, damages may be awarded to the prevailing party. In this article, we will discuss the damages that can be awarded for labor and employment litigation in federal US district courts.

There are several types of damages that can be awarded in labor and employment litigation cases. The most common types of damages include back pay, front pay, compensatory damages, punitive damages, and attorney's fees.

Back pay is the amount of wages and benefits that an employee would have earned had they not been wrongfully terminated or discriminated against. Back pay damages are awarded to compensate the employee for their loss of earnings and benefits. In some cases, back pay damages may be adjusted for any earnings that the employee received from other employment during the time period in question.

Front pay is a form of damages that is awarded in cases where the employee is not able to return to their former position or employer. Front pay is intended to compensate the employee for their future loss of earnings and benefits.

Compensatory damages are intended to compensate the employee for any emotional distress or other harm that they suffered as a result of the employer's actions. Compensatory damages may include damages for pain and suffering, mental anguish, and loss of enjoyment of life.

Punitive damages are intended to punish the employer for their wrongful conduct and deter similar conduct in the future. Punitive damages are only awarded in cases where the employer's conduct was particularly egregious or intentional.

Attorney's fees may also be awarded to the prevailing party in a labor and employment litigation case. Attorney's fees are intended to compensate the prevailing party for their legal fees and costs.

In addition to these types of damages, the prevailing party may also be entitled to injunctive relief. Injunctive relief is a court order that requires the employer to take certain actions or refrain from certain actions.

It is important to note that the damages that can be awarded in labor and employment litigation cases are subject to certain limitations. For example, in cases involving discrimination or harassment, the damages that can be awarded are capped based on the size of the employer. Additionally, the employee has a duty to mitigate their damages, which means that they must take reasonable steps to minimize the harm caused by the employer's actions.




## Federal Damage Statistics for Arbitration
- data based on 9,914 federal district court cases dating from 2003 to 2023



##### Distribution (Monetary Damages)
Distribution  | Percentage of total dockets
$1-$5k  | 16%
$5k-$50k  | 50%
$50k-$100k  | 14%
$100k-$500k  | 16%
$500k-$1m  | 2%
$1m-$50m  | 2%
$50m+  | 1%

##### Distribution (Attorney Fees and Costs)
Distribution  | Percentage of total dockets
$1-$5k  | 73%
$5k-$50k  | 21%
$50k-$100k  | 2%
$100k-$500k  | 3%
$500k-$1m  | 1%
$1m-$50m  | 1%
$50m+  | 0%

##### Damage Types (Monetary Damages)
Damages Type  | Number of dockets  | Median awarded amount
Compensatory Damages  | 15,469  | $25,129
Settlement  | 3,651  | $31,308
Liquidated Damages  | 348  | $8,242
Interest  | 324  | $2,937
Punitive Damages  | 61  | $150,000
Statutory Damages  | 42  | $32,555

##### Damage Types (Attorney Fees and Costs)
Damages Type  | Number of dockets  | Median awarded amount
Litigation Costs & Expenses  | 13,267  | $1,957
Attorney Fees  | 4,530  | $5,000

##### Court (Monetary Damages)
Court  | Number of dockets  | Median awarded amount
N.D.Ill.  | 3,467  | $28,323
S.D.N.Y.  | 2,156  | $26,191
D.N.J.  | 1,606  | $19,031
E.D.Pa.  | 1,129  | $26,241
E.D.N.Y.  | 1,032  | $40,542
W.D.Pa.  | 733  | $38,369
E.D.Va.  | 694  | $15,303
D.Md.  | 604  | $45,889
C.D.Cal.  | 558  | $37,956
W.D.Wash.  | 455  | $20,000
E.D.Mo.  | 421  | $20,873
N.D.Ohio  | 411  | $23,459
N.D.Ind.  | 374  | $10,518
E.D.Mich.  | 323  | $36,597
S.D.Fla.  | 309  | $9,469
M.D.Fla.  | 288  | $9,447
D.Minn.  | 286  | $19,800
S.D.Ohio  | 275  | $16,706
S.D.Ind.  | 212  | $36,795
E.D.Wis.  | 197  | $27,802

##### Court (Attorney Fees and Costs)
Court  | Number of dockets  | Median awarded amount
N.D.Ga.  | 1,056  | $2,359
C.D.Cal.  | 975  | $4,863
N.D.Ill.  | 782  | $2,575
M.D.Fla.  | 611  | $2,800
N.D.Tex.  | 512  | $3,029
S.D.N.Y.  | 473  | $5,000
N.D.Cal.  | 468  | $3,593
S.D.Tex.  | 449  | $2,219
E.D.Va.  | 444  | $1,641
D.Colo.  | 426  | $2,475
S.D.Fla.  | 423  | $2,148
D.Md.  | 409  | $1,455
D.Ariz.  | 337  | $2,453
E.D.Mich.  | 325  | $1,888
E.D.N.Y.  | 324  | $2,508
D.Or.  | 267  | $2,584
W.D.Wash.  | 260  | $1,714
N.D.Ala.  | 251  | $1,816
S.D.Ind.  | 251  | $1,681
N.D.Ohio  | 245  | $2,379

##### Case Type (Monetary Damages)
Case type  | Number of dockets  | Median awarded amount
Labor & Employment  | 19,454  | $26,045
Other  | 32  | $31,984

##### Case Type (Attorney Fees and Costs)
Case type  | Number of dockets  | Median awarded amount
Labor & Employment  | 16,336  | $2,369
Other  | 62  | $2,506

##### Law Firm (Monetary Damages)
Law Firm  | Number of dockets  | Median awarded amount
Arnold & Kadjan, Attorneys and Counselors  | 707  | $24,542
O'Donoghue & O'Donoghue LLP  | 563  | $33,735
Tucker Arensberg, P.C.  | 556  | $39,537
Baum Sigman Auerbach & Neuman, Ltd.  | 412  | $28,962
Whitfield McGann & Ketterman Attorneys at Law  | 410  | $31,130
Jennings Sigmond, P.C.  | 393  | $27,949
The Law Office of Kroll Heineman Carton, LLC  | 364  | $7,794
Hammond and Shinners, P.C.  | 360  | $21,617
Barnes, Iaccarino & Shepherd LLP  | 272  | $24,754
Virginia & Ambinder, LLP  | 244  | $44,744
Office of Fund Counsel  | 237  | $25,104
O'Dwyer & Bernstien, LLP  | 215  | $3,741
Reid, McCarthy, Ballew & Leahy, L.L.P.  | 212  | $8,487
Littler Mendelson P.C.  | 206  | $25,103
Laborers Pension and Welfare Funds  | 205  | $22,095
Asher, Gittler & D'Alba, Ltd.  | 182  | $21,505
Allison, Slutsky & Kennedy, P.C.  | 176  | $20,018
Jackson Lewis P.C.  | 173  | $26,000
Pitta LLP  | 172  | $14,481
Amalgamated Life Insurance Company  | 171  | $19,448

##### Law Firm (Attorney Fees and Costs)
Law Firm  | Number of dockets  | Median awarded amount
Littler Mendelson P.C.  | 877  | $3,085
Ogletree, Deakins, Nash, Smoak & Stewart, P.C.  | 746  | $2,936
Jackson Lewis P.C.  | 587  | $2,654
Seyfarth Shaw LLP  | 388  | $3,261
Fisher & Phillips LLP  | 302  | $2,714
Ford & Harrison LLP  | 236  | $2,459
Morgan, Lewis & Bockius LLP  | 232  | $5,142
Constangy, Brooks, Smith & Prophete, LLP  | 219  | $2,832
McGuireWoods LLP  | 153  | $2,765
O'Donoghue & O'Donoghue LLP  | 153  | $1,250
Baker & Hostetler LLP  | 113  | $3,668
Morgan & Morgan  | 103  | $2,790
Holland & Knight LLP  | 101  | $3,230
Paul Hastings LLP  | 101  | $5,316
Greenberg Traurig, LLP  | 98  | $3,846
Gordon Rees Scully Mansukhani, LLP  | 97  | $3,572
Faegre Drinker Biddle & Reath LLP  | 96  | $2,549
Hunton Andrews Kurth LLP  | 95  | $2,868
Fox Rothschild LLP  | 91  | $2,840
Baker, Donelson, Bearman, Caldwell & Berkowitz, PC  | 90  | $3,290

##### Year (Monetary Damages)
Year  | Number of dockets  | Median awarded amount
2022  | 107  | $27,099
2021  | 263  | $33,388
2020  | 316  | $37,947
2019  | 405  | $33,042
2018  | 465  | $37,105
2017  | 435  | $40,000
2016  | 547  | $36,354
2015  | 569  | $38,875
2014  | 591  | $39,515
2013  | 666  | $33,694
2012  | 889  | $41,185
2011  | 1,012  | $33,162
2010  | 1,158  | $33,762
2009  | 1,081  | $31,120
2008  | 1,135  | $29,548
2007  | 1,016  | $30,566
2006  | 1,097  | $21,023
2005  | 1,414  | $17,538
2004  | 1,544  | $21,391
2003  | 512  | $23,261

##### Year (Attorney Fees and Costs)
Year  | Number of dockets  | Median awarded amount
2022  | 50  | $402
2021  | 145  | $3,333
2020  | 389  | $3,400
2019  | 584  | $3,156
2018  | 696  | $3,094
2017  | 752  | $3,171
2016  | 805  | $3,000
2015  | 684  | $3,150
2014  | 553  | $3,494
2013  | 494  | $3,463
2012  | 517  | $2,933
2011  | 564  | $2,853
2010  | 603  | $2,735
2009  | 593  | $3,000
2008  | 706  | $2,806
2007  | 823  | $2,360
2006  | 1,006  | $2,057
2005  | 1,186  | $1,875
2004  | 1,315  | $1,850
2003  | 663  | $2,136

### Conclusion

In conclusion, damages that can be awarded in labor and employment litigation cases in federal US district courts include back pay, front pay, compensatory damages, punitive damages, and attorney's fees. The prevailing party may also be entitled to injunctive relief. The damages that can be awarded are subject to certain limitations, and the employee has a duty to mitigate their damages. Employers and employees should be aware of the types of damages that may be awarded in labor and employment litigation cases and should take steps to minimize their exposure to liability.