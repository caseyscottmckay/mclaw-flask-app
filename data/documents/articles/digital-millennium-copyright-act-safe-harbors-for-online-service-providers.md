---
title| Digital Millennium Copyright Act: Safe Harbors for Online Service Providers
author| casey@mclaw.io
date| 2022-08-14
category | Intellectual Property and Technology
subcategory | Copyright
tags | copyright, photographs, images, visual
summary| This article provides an overview of the Digital Millennium Copyright Act (DMCA) Safe Harbors for Online Service Providers. Under certain circumstances, the DMCA's safe harbor provisions protect online service providers from copyright infringement liability based on the actions of their users. This article is about the four safe harbors created by the DMCA, qualification requirements for each safe harbor, and the DMCA's notice, takedown, and counter-notice framework.
---
DMCA Safe Harbor Framework
Scope of DMCA Protection
Where an OSP qualifies for a DMCA safe harbor and complies with its procedural requirements, the safe harbor:

- Protects the OSP from all monetary liability for copyright infringement resulting from the specified activities, including monetary damages, costs, and attorneys' fees.
- Limits the OSP's exposure to injunctive and other equitable relief for the alleged infringement to only specific remedies set out in the DMCA.

(17 U.S.C. § 512(a) to (d), (j), (k)(2).)

Courts have held that the DMCA's safe harbor protections may shield OSPs from liability for:

- Direct infringement.
- Secondary infringement.

(_Viacom Int'l, Inc. v. YouTube, Inc._, 676 F.3d 19 (2d Cir. 2012); _Perfect 10, Inc. v. Amazon.com, Inc._, 508 F.3d 1146, 1175 (9th Cir. 2007).).

### The DMCA Safe Harbors

The DMCA provides safe harbors for four OSP activities:

- Serving as a conduit for transmitting material through its system or network.
- System caching.
- Storing information at the direction of users.
- Providing links or other tools for locating material online.


## Qualifying for Safe Harbor Protection
To qualify for DMCA safe harbor protection, an OSP must meet certain threshold requirements that include:

- Meeting the statute's definition of "service provider" for the particular safe harbor.
- Accommodating and not interfering with certain standard technical measures used by copyright owners to identify or protect their copyrighted works.
- Adopting, reasonably implementing, and informing subscribers and account holders of a policy for terminating repeat infringers' use of the service provider's system or network under appropriate circumstances.

(17 U.S.C. § 512(i).).

The OSP also must designate an agent to receive notices of alleged infringement to be eligible for any of the safe harbors, except for the safe harbor for transitory communications. (17 U.S.C. § 512(b)(2)(E), (c)(2), (d)(3); 37 C.F.R. § 201.38(a).).

### Definition of Service Provider
The DMCA sets out two definitions of service provider:
- The safe harbor for transitory communications applies a relatively narrow definition.
- The other three safe harbors apply a broader definition.

#### Service Provider Definition for Safe Harbor for Transitory Communications
The safe harbor for transitory communications protects a smaller category of OSPs than the other safe harbors . For this safe harbor, the DMCA defines service provider to include only entities providing transmission, routing, or connections for digital online communications that:

    Are between or among points specified by the user.
    Send material chosen by the user.
    Do not modify the content of the material sent or received.

(17 U.S.C. § 512(k)(1)(A).)


This definition is based on the definition of "telecommunications" in the Communications Act of 1934 and aims to cover an OSP only when it is acting as a mere conduit for communications. This definition includes providers of network connectivity but does not include, for example, website operators or hosting service providers. It may protect, for example: Providers of broadband networks, including cable modem and fiber optic; Dial-up internet service provider; Subject to certain additional conditions, universities providing internet or other network access for faculty and students.

#### Service Provider Definition for Other Safe Harbors
For the other three safe harbors, the statute defines service provider more broadly to include, in addition to entities that qualify under the safe harbor for transitory communications (see Service Provider Definition for Safe Harbor for Transitory Communications), entities that provide or operate facilities for:

- Online services.
- Network access.

(17 U.S.C. § 512(k)(1)(B).).

This definition includes, for example, providers of: Internet access, website hosting, websites, online forums, email services.

One court characterized this definition as being so broad that it is difficult to imagine any OSP that would not fall under the definition (_In re Aimster Copyright Litig._, 252 F. Supp. 2d 634, 658 (N.D. Ill. 2002), aff'd, 334 F.3d 643, 655 (7th Cir. 2003)).


### Repeat Infringers Policy
To qualify under any safe harbor, the service provider must have a policy for terminating repeat infringers' use of its service. The service provider must both:

- Reasonably implement the repeat infringers policy.
- Inform its subscribers and account holders of the repeat infringers policy.

(17 U.S.C. § 512(i)(1)(A).).

Website operators typically inform their subscribers and account holders of the policy in a separate section of their comprehensive website terms and conditions or in a stand-alone website copyright policy. The DMCA does not require written repeat infringers policies, and service providers are only obligated to inform users of a policy of terminating repeat infringers in appropriate circumstances. _Ventura Content, Ltd. v. Motherless, Inc._, 885 F.3d 597, 615-19 (9th Cir. 2018).

#### Reasonable Implementation
Courts have held that a repeat infringers policy is reasonably implemented if the service provider:

- Has a working system for copyright owners to report alleged infringement.
- Does not interfere with copyright owners' ability to collect the information they require to issue notices.
- Has a system in place for addressing infringements reported in compliant DMCA takedown notices.
- Under appropriate circumstances, terminates users who repeatedly and blatantly infringe copyrights.

_See Perfect 10, Inc. v. CCBill LLC_, 488 F.3d 1102, 1109-10 (9th Cir. 2007).

Because DMCA safe harbor protection is not conditioned on a service provider policing its users, a service provider's knowledge of repeat infringement has been found relevant in determining whether it has reasonably implemented a repeat infringers policy. _See Capitol Records, Inc. v. MP3Tunes_, LLC, 821 F. Supp. 2d 627, 638 (S.D.N.Y. 2011); _Corbis Corp. v. Amazon.com, Inc._, 351 F. Supp. 2d 1090, 1104-06 (W.D. Wash. 2004), abrogated in part on other grounds, _Cosmetic Ideas, Inc. v. IAC/Interactivecorp._, 606 F.3d 612, 621 (9th Cir. 2010). A repeat infringer is any user that repeatedly uploads or downloads copyrighted material for personal use, and the user does not need to know that the material is infringing. _EMI Christian Music Grp., Inc. v. MP3tunes, LLC_, 840 F.3d 69 (2d Cir. 2016), amended and superseded by 844 F.3d 79 (2d Cir. 2016), cert. denied, 137 S. Ct. 2269 (2017), reh'g denied, 138 S. Ct. 43 (2017)). Notices of infringement that do not substantially comply with the DMCA's takedown notice requirements have been held not to be sufficient evidence of repeat infringement (CCBill, 488 F.3d at 1111-13).

However, courts have declined to find that repeat infringers policies had been reasonably implemented where the service provider, for example:

- On changing the email address for receiving DMCA takedown notices, did not either:
    - timely register the change with the US Copyright Office; or
    - cause messages sent to the old address to be forwarded to the new one. Ellison v. Robertson, 357 F.3d 1072, 1080 (9th Cir. 2004).
- Instructed users on encrypting their infringing activities, making it impossible for the service provider to identify infringers (In re Aimster Copyright Litig., 334 F.3d 643, 655 (7th Cir. 2003)).
- Terminated infringing users only to reactivate them within a short timeframe (BMG Rights Mgmt. (US) LLC v. Cox Commc'ns, Inc., 881 F.3d 293, 303-05 (4th Cir. 2018)).

### Designation of Agent to Receive DMCA Notices
The safe harbors for storing material at the direction of users, information location tools, and in some cases, system caching, are only available if the service provider has designated an agent to receive notices of alleged infringement (17 U.S.C. § 512(b)(2)(E), (c)(2), (d)(3); 37 C.F.R. § 201.38(a)).

The safe harbor for transitory communications does not require designation of an agent. However, any service provider seeking protection under the DMCA should consider designating an agent to preserve the availability of the other safe harbors if the service provider fails to qualify for the safe harbor for transitory communications. In addition, properly designating an agent may be a factor in determining whether a service provider has reasonably implemented a repeat infringers policy (see Ellison, 357 F.3d at 1080; see also Repeat Infringers Policy).

##### Requirements for Designating an Agent
The service provider must comply with all statutory and regulatory requirements for designating an agent. These include:

- Making the agent's name (and, if applicable, the name of the agent's organization) and contact information available through its service, including in a publicly accessible location on its website. The agent's contact information must substantially include its:
    - physical mailing address (street address or post office box);
    - telephone number; and
    - email address.
- Registering this information with the Register of Copyrights.

(17 U.S.C. § 512(c)(2).)

The agent does not have to be an individual person. A service provider may instead designate, for example, a department within an organization (37 C.F.R. § 201.38(b)(3); see Hendrickson v. eBay, Inc., 165 F. Supp. 2d 1082 (C.D. Cal. 2001)).

Website operators typically provide their designated agent information in either or both:

- The website's terms of use.
- A separate DMCA policy.

The DMCA further requires that the following information be included in a publicly available location on the website and registered with the Copyright Office:

- The full legal name and physical street address of the service provider. The US Copyright Office does not permit:
    - the substitution of a post office box for the street address except in exceptional circumstances, subject to the Copyright Office's approval of the service provider's written request; or
    - joint designations by related or affiliated service providers that are separate legal entities (for example, corporate parents and subsidiaries), each of which must provide its own separate designation.
- All alternate names that the public would be likely to use to search for the service provider's designated agent in the US Copyright Office's online directory of designated agents, including:
    - all names under which the service provider is doing business;
    - website names and addresses (URLs);
    - software application names; and
    - other commonly used names, although separate legal entities are not considered alternate names.

(37 C.F.R. § 201.38(b).)

#### Registering an Agent
The Copyright Office provides information on the requirements and process for registering an agent on its website. As of December 2016, the US Copyright Office implemented an electronic system to designate and search for agents to receive notifications of claimed infringement and does not accept paper designations (37 C.F.R. § 201.38(c), (e)(1)).

Under the new electronic system:

- All existing designations must have been re-filed by December 31, 2017, or the registration will have expired and become invalid after December 31, 2017.
- Service providers must renew their designations every three years.

(37 C.F.R. § 201.38(c), (e).)
The Copyright Office website includes links to both the old directory and new directory of service providers' designated agents.
(see, for example, Standard Document, Website Copyright (DMCA) Policy).

## Safe Harbor for Transitory Communications


### Requirements for Safe Harbor Protection

## Safe Harbor for System Caching

### Requirements for Safe Harbor Protection
### System Caching Conditions

## Safe Harbor for Storing Material at the Direction of Users

Section 512(c) of the DMCA provides a safe harbor for service providers storing material on their systems or networks at the direction of users (17 U.S.C. § 512(c)). It shields service providers from liability for copyright infringement that occurs "by reason of the storage at the direction of a user" (see Scope of "By Reason of the Storage at the Direction of a User"). It has been broadly interpreted to extend, for example, to:

- Social media and other websites that have user-generated content.
- Online storage locker providers.

### Requirements for Safe Harbor Protection

To qualify for this safe harbor, the service provider must:

- Lack actual or red flag knowledge that the material or its use is infringing.
- Not receive a direct financial benefit from the activity, if the service provider has the right and ability to control the infringing activity.
- Respond expeditiously to remove or disable access to (take down) the material or infringing activity on receiving a DMCA takedown notice. 17 U.S.C. § 512(c)(1).

### Scope of "By Reason of the Storage at the Direction of a User"
The express language of this safe harbor shields the service provider from liability for copyright infringement "by reason of the storage at the direction of a user." Courts have generally interpreted this language to cover certain activities beyond mere storage where those activities occur by reason of the storage. For example, providing a user-generated content feature on a website anticipates that the service provider, in addition to storing the content, will perform functions necessary to make the content accessible to other users. Courts have found these activities to include automatic processes used to facilitate access to content uploaded by users, including, for example:

- Breaking down videos into smaller pieces.
- Transcoding videos into commonly viewable formats (for example, Flash).
- Streaming and enabling downloads of videos.
- Extracting metadata provided by users to help other users locate the content.
- Indexing and displaying links to related content based on an automated algorithm.

_See UMG Recordings, Inc. v. Shelter Capital Partners LLC_, 718 F.3d 1006, 1015-20 (9th Cir. 2013); _Viacom_, 676 F.3d at 38-40.

At least one court found that even where a person screened uploaded material before posting it on a website, the upload and storage were still at the direction of the user where the service provider was not involved in a selection process but only screened to ensure material was consistent with the website's purpose and for obvious infringement. _See CoStar Group, Inc. v. LoopNet, Inc._, 164 F. Supp. 2d 688, 701-702 (D. Md. 2001), aff'd, 373 F.3d 544 (4th Cir. 2004).

While not ruling on the question, in _Viacom v. YouTube_, the Second Circuit suggested that syndicating or licensing content and other business transactions are outside the scope of "storage at the direction of a user" (676 F.3d at 40).


### Knowledge of Infringing Activity

To qualify for the safe harbor for storing material at the direction of users, the service provider must:

    Not have actual knowledge of the infringement.
    Not be aware of facts or circumstances that make the infringement apparent (commonly called red flag knowledge).
    On acquiring actual or red flag knowledge of infringement, expeditiously take steps to take down the infringing material.

(17 U.S.C. § 512(c)(1)(A), (C).)
These knowledge requirements also apply to the safe harbor for information location tools (see Safe Harbor for Information Location Tools).
DMCA Takedown Notice as Knowledge
A compliant DMCA takedown notice may provide the service provider with actual or red flag knowledge of infringement. However, with certain exceptions, the DMCA precludes a trier of fact from considering a non-compliant notice in determining whether a service provider had knowledge of particular infringing activity (17 U.S.C. § 512(c)(3)(B)). Therefore, a copyright owner relying on a notice of infringement to show actual or red flag knowledge bears the burden of ensuring that the notice complies with the DMCA's requirements (see, for example, Corbis, 351 F. Supp. 2d at 1107; Shelter Capital, 718 F.3d at 1024-26).
Knowledge of Specific Infringements
Both actual and red flag knowledge require that the service provider have knowledge of specific infringements. The DMCA also expressly provides that the safe harbor for storing material at the direction of users does not require a service provider to affirmatively monitor for or investigate infringement (17 U.S.C. § 512(m)). Courts have held the following to be insufficient to establish the type of knowledge that would disqualify a service provider from this safe harbor:

    General knowledge that the service provider's service can be used to share infringing material (Shelter Capital, 718 F.3d at 1022).
    General awareness of the occurrence of non-specific instances of infringement (MP3Tunes, 821 F. Supp. 2d at 644-45).
    Notices that are not substantially DMCA-compliant (MP3Tunes, 821 F. Supp. 2d at 644-45).
    Third-party notices of alleged infringement, without evidence of blatant infringement (Corbis, 351 F. Supp. 2d at 1107-09).

However, a service provider cannot avoid liability by remaining willfully blind to infringement (see, for example, Viacom, 676 F.3d at 34-35).

### Ability to Control and Financial Benefit

An otherwise eligible service provider is disqualified from protection under the safe harbor for storing material at the direction of users if it both:

- Has the right and ability to control the infringing activity.
- Receives a direct financial benefit from the activity.

(17 U.S.C. § 512(c)(1)(B).)
Both prongs must be satisfied for a service provider to be disqualified from the safe harbor under this provision (see, for example, Hendrickson, 165 F. Supp. 2d at 1093 (C.D. Cal. 2001)).
This requirement also applies to the safe harbor for information location tools (See Safe Harbor for Information Location Tools).

#### Right and Ability to Control

Courts generally have interpreted the right and ability to control infringing activity under the DMCA to require more than the ability to remove or disable access to infringing materials. Engaging in voluntary monitoring to identify obvious infringements also has been held not to be the type of control contemplated by the statute (see Hendrickson, 165 F. Supp. 2d at 1093-94.)
However, one court found that a provider of services to websites that were part of its "network" had the right and ability to control infringing activity on those websites where, among other things, the provider:

    Pre-screened websites.
    Gave extensive content and technical advice to the website operators.
    Exercised quality control across the websites.

(Perfect 10, Inc. v. Cybernet Ventures, Inc., 213 F. Supp. 2d 1146, 1181-82 (C.D. Cal. 2002).)
In Viacom v. YouTube, the Second Circuit held that it is possible for a service provider to have the right and ability to control infringing activity without having knowledge of specific infringements (676 F.3d at 36).
Direct Financial Benefit
A service provider with the right and ability to control infringing activity on its service does not lose safe harbor protection under Section 512(c)(1)(B) unless it also receives a direct financial benefit from the infringement. Courts have held that a direct financial benefit exists only where the infringing activity is a draw for users and not just an added benefit. On this basis, courts have not found a direct financial benefit to exist where, for example:

    The service provider did not charge for the service at issue but earned revenue from other aspects of its service (CoStar, 164 F. Supp. 2d at 704-705, aff'd, 373 F.3d 544.
    The plaintiff provided no evidence that the service provider attracted or retained subscribers resulting from the infringement (Ellison, 357 F.3d at 1079).
    The service received income from providing all users with access to its services and not directly from allowing infringing activity (Wolk v. Kodak Imaging Network, Inc., 2011 WL 940056, at *7 (S.D.N.Y. Mar. 17, 2011)).

However, a direct financial benefit may exist if a service provider's income is directly related to the number of users that sign up for its service from infringing sites (see Cybernet, 213 F. Supp. 2d at 1181-82).


## Safe Harbor for Information Location Tools


Section 512(d) of the DMCA provides a safe harbor for service providers referring or linking users to another location that contains infringing material or activity. Its protections apply broadly, for example, to the following: search engines, online directories, indices, yperlinks. 17 U.S.C. § 512(d).

As with the safe harbor for storing material at the direction of users, this safe harbor requires that the service provider:

- Lack actual or red flag knowledge that the material or use of the material is infringing and, on obtaining this knowledge, act expeditiously to take down the material or activity.
- Not receive a direct financial benefit from the activity, if the service provider has the right and ability to control the infringing activity.
- Respond expeditiously to take down the material or infringing activity on receiving a DMCA takedown notice.

17 U.S.C. § 512(d).


## DMCA Notice, Takedown, and Counter-Notice Procedures
### Copyright Owner's Takedown Notice
### Service Provider Compliance with Takedown Notice

