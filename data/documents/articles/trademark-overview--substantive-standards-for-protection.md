---
title| Trademark Overview--Substantive Standards for Protection
author| casey@mclaw.io
date| 2021-12-21
category | Intellectual Property and Technology
subcategory | Trademark
tags | trademark, infringement
summary| This overview of US trademark law gives information about the nature of trademarks, the reasons why they can be commercially significant, and the type of material that is likely to be suitable for registration and use as a trademark. Topics covered include the legal framework for trademark law and substantive standards for protection.
---
#### Requirements for Federal Trademark Protection
To be protected as a trademark at the federal level, a designation must be (1) distinctive (_see_ [Distinctiveness](#Distinctiveness)) and (2) used in interstate commerce (_see_ [Use in Commerce](#)).

##### Distinctiveness
Distinctiveness is the ability of a designation to identify a single source of goods or services. Distinctiveness may be either inherent (see Inherent Distinctiveness) acquired (see Acquired Distinctiveness).

###### Inherent Distinctiveness
A mark is inherently distinctive if it is immediately capable of identifying a source of origin for a good or service. Inherent distinctiveness is generally analyzed along a spectrum of distinctiveness ranging from generic terms, which are not protectable under any circumstances, to highly distinctive fanciful marks, which have the broadest scope of inherent protection.

For more information on inherent distinctiveness, see Practical Guide, Acquiring Trademark Rights and Registrations: Distinctiveness.

###### Acquired Distinctiveness
Some terms that are not inherently distinctive may acquire distinctiveness (also known as secondary meaning), and be capable of protection as a trademark, through use and promotion over time. For example, terms that describe a product attribute or characteristic may be protectable if they acquire distinctiveness. Under the Lanham Act, a mark that has been in substantially continuous and exclusive use for five years is entitled to a presumption of acquired distinctiveness (15 U.S.C. § 1052(f)).

##### Use in Commerce
In the US, trademark rights at the federal level arise through use of a protectable mark in interstate commerce in connection with goods or services. For more information on what qualifies as use in commerce, see Practical Guide, Proving Trademark Use and Priority.

At the state level, trademark rights arise through use of the mark in commerce within the state.
