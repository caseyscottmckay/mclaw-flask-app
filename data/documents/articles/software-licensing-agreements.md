The challenge in open source licenses comes from projects that mix different types. The type of license chosen should (a) be determined by the license of code that is included; and (b) determine the potential scope of reuse in subsequent projects. This may be somewhat clarified with the diagram at "The Free-Libre / Open Source Software (FLOSS) License Slide" | David A. Wheeler | September 27, 2007 at http://www.dwheeler.com/essays/floss-license-slide.html

.

(01a) Code licensed under LGPLv2.1 can NOT include code licensed under Apache 2.0, but can include code licensed under MIT/X11.

(01b) Code licensed under LGPLv2.1 can be combined with a greater variety of licenses (e.g. GPLv2, LGPLv3, GPLv3) for a valid derivative work.

(02a) Code licensed under LGPLv3 can include code licensed under Apache 2.0 (and MIT X/11).

(02b) Code licensed under LGPLv3 can NOT be combined with code licensed under GPLv2, but can be combined with code licensed under LGPLv3 or GPLv3 for a valid derivative work.

The differences between LGPLv2.1 and LGPLv3 show up in the derivative works that result from combining inputs that bear different licenses (either as source code, or as a library). Essentially, code with (i) a less restrictive license, combined with code with (ii) a more restrictive license, results in (iii) a derivative work at the more restrictive licensing.

The resulting license from the derivative work can be read from the table on "How are the various GNU licenses compatible with each other?" | Frequently Asked Questions about the GNU Licenses at https://www.gnu.org/licenses/gpl-faq.html#AllCompatibility

. (Read down the columns towards the right.

(03) ("copy code": cells read differently for v2.1 and 3)
(R1C4) If the code is licensed under LGPLv2.1, and I copy code under GPLv2, the derivative work should become GPLv2 ([7] if you change the LGPLv2.1).
(R1C6) If the code is licensed under LGPLv3, I cannot copy code under GPLv2.

(04) ("copy code": cells read the same for v2.1 and 3)
(R3C4) If the code is licensed under LGPLv2.1, and I copy code under GPLv3, the derivative work should become GPLv3 ([7] if you change the LGPLv2.1).
(R3C6) If the code is licensed under LGPL3, and I copy code under GPLv3, the derivative work should become GPLv3, or maybe LGPLv3 ([8] with extra permissions).

(05) ("copy code": cells read differently for v2.1 and 3)
(R4C4) If the code is licensed under LGPLv2.1, and I copy code under LGPLv2.1, the derivative work should become LGPLv2.1.
(R4C6) If the code is licensed under LGPLv3, and I copy code under LGPLv2.1, the derivative work should become GPLv3 ([7] if you change the LGPLv2.1) or maybe LGPLv3 ([8] with extra permissions).

(06) ("use a library": cells read differently for v2.1 and 3 ... on the same pattern as "copy code")
(R7C4) If the code is licensed under LGPLv2.1, and I use a library under GPLv2, the derivative work should become GPLv2 ([7] if you change the LGPLv2.1).
(R7C6) If the code is licensed under LGPLv3, I cannot use a library under GPLv2.

(07) ("use a library": cells read the same for v2.1 and 3 ... on the same pattern as "copy code")
(R9C4) If the code is licensed under LGPLv2.1, and I use a library under GPLv3, the derivative work should become GPLv3 ([7] if you change the LGPLv2.1).
(R9C6) If the code is licensed under LGPL3, and I use a library under GPLv3, the derivative work should become GPLv3, or maybe LGPLv3 ([8] with extra permissions).

(08) ("use a library": cells read the same for v2.1 and 3)
(R10C4) If the code is licensed under LGPLv2.1, I can use a library under GPLv2.1.
(R10C6) If the code is licensed under LGPLv3, I can use a library under GPLv2.1.

(09) ("use a library": cells read the same for v2.1 and 3)
(R12C4) If the code is licensed under LGPLv2.1, I can use a library under GPLv3.
(R12C6) If the code is licensed under LGPLv3, I can use a library under GPLv3.

Footnotes:
[7] LGPLv2.1 gives you permission to relicense the code under any version of the GPL since GPLv2....

[8] LGPLv3 is GPLv3 plus extra permissions ....

Conclusions:

(A) If you want your code to be reused to the greatest extent ...
then choose a permissive license such as MIT/X11 or Apache 2.0 so that both commercial and non-commercial projects can include the code.
You should still get recognition for your work, because the header should include an attribution with your name as the (original) copyright owner.

(B) If you want your code to be included in potentially the most number of free software (as defined by the Free Software Foundation) projects ...
then choose an LGPLv2.1 license.

(C) If your code includes other code under a permissive license (e.g. MIT/X11, Apache 2.0) and you want others in the free software community to be able to include your work ...
then choose an LGPLv3 license.

(D) If you want your code to be reusable only by the free software community ...
then choose a GPLv3 license.

(E) If you're linking to LGPL code, there doesn't seem to be any difference in the requirements across versions. In the answer to "Does the LGPL have different requirements for statically vs dynamically linked modules with a covered work?" | Frequently Asked Questions about the GNU Licenses at https://www.gnu.org/licenses/gpl-faq.html#LGPLStaticVsDynamic
, the text starts "For the purpose of complying with the LGPL (any extant version: v2, v2.1 or v3)" ...