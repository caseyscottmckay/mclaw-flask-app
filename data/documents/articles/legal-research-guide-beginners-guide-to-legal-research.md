---
title| Legal Research Guide| Beginner’s Guides to Legal Research
author| casey@mclaw.io
date| 2021-11-14
category | Legal Research
subcategory | General
tags | research, law, library
summary| The guides listed below can help you begin a legal research project by offering quick techniques and a selection of legal resources and tools.  Each guide is prepared by a legal specialist from MC Law.
---
## Beginner’s Guides to Legal Research
If you are working in an unfamiliar area of law, and do not know where to begin, our Beginner’s Guide series gives you a great selection of treatises, cases, statutes, and regulations to jump-start your research project.  
[Read More](/documents/legal-research-guide-beginners-guide-to-legal-research/56955ecb6ceff1a2a080f111ee16931c "Beginner’s Guides to Legal Research").

## Researching Case Law and Other Judicial Decisions
In the American legal system, judicial decisions are primary sources of law, in addition to the Constitution, statutes, and regulations.  Learn how to find federal and state judicial decisions by citation or subject.  
[Read More](/documents/legal-research-guide-case-law/6092c482bc7e58028cab3ea54dc84a85 "Researching Case Law and Other Judicial Decisions").

## Administrative Law Guide
Administrative law, commonly called regulatory law, is created and enforced by an administrative body; e.g., the Department of Labor, the Federal Communications Commission, or the President.  This guide provides techniques and recommended resources to help get you started in this area of law.  
[Read More](/documents/administrative-law-guide/"Administrative Law Guide").

## Federal Legislative History: Initial Steps
Provides a basic outline of resources to consult when first researching a federal legislative history. Materials such as bills, committee hearings, committee reports, congressional debate and other documents (e.g., committee prints or presidential messages) are covered.  
[Read More](/documents/researching-judicial-decisions "Federal Legislative History: Initial Steps").

## Foreign and International Law Guides
To provide a starting point for researching foreign, international, and comparative law, the Law Library of Congress has prepared guides to reference sources, compilations, citations guides, periodicals (indexes and databases), dictionaries, Web resources, free public websites, subscription-based services, subject-specific websites, and country overviews.  
[Read More](/documents/researching-judicial-decisions "Foreign and International Law Guides").

## Researching Federal Statutes
Statutes are laws (or acts) enacted by legislatures. Statutes may be found by citation, popular name, or subject.  Use this overview to find statutes using various print and Web resources.  
[Read More](/documents/researching-judicial-decisions "Researching Judicial Decisions").

## Guide to Secondary Legal Resources
Secondary legal sources offer analyses, commentaries or restatements of primary law. Sources such as legal encyclopedias, annotated law reports, legal periodicals, treaties, and restatements are covered.  
[Read More](/documents/researching-judicial-decisions "Researching Judicial Decisions").

# Practice Areas 

### Animal Law

- [](An Introduction to Animal Law)

### Caselaw

- [](How to Locate Free Case Law on the Internet)

### Constitutional Law

- [](The Framing of the United States Constitution: A Beginner’s Guide)
- [](Locating a State Constitution: A Beginner’s Guide)

### Consumer Protection Law

- [](Consumer Protection: A Beginner’s Guide)
- [](Lemon Laws: A Beginner’s Guide)

### Contacting Congress

- [](How to Contact Your Representative or Senator: A Beginner’s Guide)

### Contracts

- [](Contract Law: A Beginner’s Guide)
- [](Government Contracts: A Beginner’s Guide)
- [](State Government Contracts – A Research Guide)

### Disability Law

- [](Social Security Disability Law: A Beginner’s Guide)

### Employment Law

- [](Employment and Labor Law: A Beginner’s Guide)

### Ethics and Professional Responsibility

- [](Legal Ethics: A Beginner’s Guide)

### Evidence

- [](Discovery: A Beginner’s Guide)
- [](Preparing Witnesses For Trial: A Beginner’s Guide)

### The Executive Branch

- [](Executive Orders: A Beginner’s Guide)
- [](Presidential Signing Statements)
- [](The War Powers Resolution)

### Family Law

- [](Family Law: A Beginner’s Guide – Part 1: Formation and Dissolution of Marriage)
- [](Family Law: A Beginner’s Guide – Part 2: Child Custody, Support, and Adoption)

### Federal Law and Legislative History

- [](Compiling a Federal Legislative History: A Beginner’s Guide)
- [](Congressional Voting Records: A Beginner’s Guide)
- [](Debates of Congress: A Beginner’s Guide)
- [](Federal Statutes: A Beginner’s Guide)
- [](Locating a Congressional Committee Print: A Beginner’s Guide)
- [](Locating Congressional Documents: A Beginner’s Guide)
- [](Locating a Compiled Federal Legislative History: A Beginner’s Guide)
- [](How to Locate a Published Congressional Hearing: A Beginner’s Guide)
- [](How to Locate a United States Congressional Committee Report: A Beginner’s Guide)
- [](How to Locate an Unpublished Congressional Hearing: A Beginner’s Guide)
- [](How to Trace Federal Legislation – A Research Guide)
- [](Presidential Communications: A Beginner’s Guide)
- [](Researching Federal Statutes)

### Homeowners’ Associations

- [](Homeowners’ Associations Law: A Beginner’s Guide)

### Immigration Law

- [](Asylum Law: A Beginner’s Guide)
- [](Immigration Law: A Beginner’s Guide)

### Legal Drafting

- [](Legal Drafting: A Beginner’s Guide)

### L.G.B.T.Q.

- [](The Law of Gender Identity and Sexual Orientation: A Beginner’s Guide)

### Municipal Law

- [](Municipal Codes: A Beginner’s Guide)

### Nonprofit Law

- [](Nonprofit Organizations: A Beginner’s Guide)

### Patent Law

- [](Patent Law: A Beginner’s Guide)

### Real Estate Law

- [](Foreclosure Defense: A Beginner’s Guide)
- [](Landlord-Tenant Law: A Beginner’s Guide)
- [](Locating Real Estate Forms: A Beginner’s Guide)

### Regulations

- [](How to Trace Federal Regulations – A Research Guide)

### Small Claims

- [](Small Claims Court: A Beginner’s Guide)

### Supreme Court Records and Briefs

- [](Finding U.S. Supreme Court Records and Briefs)

### Treaties

- [](U.S. Treaties: A Beginner’s Guide)

### Tree Law

- [](Legal Disputes Concerning Trees: A Beginner’s Guide)

### Vaccinations

- [](FALQs: Vaccination Law in the United States)

### Water Law

- [](An Introduction to Water Law)

### Wills, Probate, and Advance Directives

- [](The Administration of a Probate Estate: A Beginner’s Guide)
- [](Drafting a Will: A Beginner’s Guide)
- [](Health Care Advance Directives: A Beginner’s Guide)

## Jurisdiction – Foreign and International

### China

- [](A Guide to Chinese Legal Research: Administrative Regulations and Departmental Rules)
- [](A Guide to Chinese Legal Research and Global Legal Collection Highlights: Official Publication of Chinese Law)
- [](A Guide to Chinese Legal Research: Who Makes What?)

### Cuba

- [](FALQs: Cuban Legal System)

### Ethiopia

- [](Researching African Laws Online: Ethiopia)

### European Union

- [](A Guide to Researching EU Law)

### International

- [](Election Laws Around the Globe)
- [](War Crimes Resources – A Research Guide)

### United Kingdom

- [](No Taxation Without Representation Circa 1215 AD, or, Magna Carta: A Beginner’s Guide)
- [](UK Legal Resources Provided Online by the Law Library of Congress)
