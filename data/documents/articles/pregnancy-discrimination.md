---
title| Pregnancy Discrimination
author| casey@mclaw.io
date| 2022-10-18
category | Legal Research
subcategory | General
tags | pregnant, discrimination, workplace, pregnancy
summary| This article discusses pregnancy discrimination under Title VII of the Civil Rights Act of 1964 (Title VII), the Pregnancy Discrimination Act (PDA), the Family Medical Leave Act (FMLA), the Americans with Disabilities Act (ADA), and the Fair Labor Standards Act (FLSA). This article discusses federal law prohibiting discrimination, harassment, and retaliation against applicants and employees based on pregnancy, childbirth, and medical conditions related to pregnancy, as well as reasonable accommodation requirements. It also discusses best practices for avoiding discrimination during the interview process, during employment, and during the employee termination process.
---
# Preventing and Responding to Pregnancy Discrimination.

## Legal Basics
**Federal laws prohibit pregnancy discrimination in the workplace.** Specifically, employers must not discriminate, harass, or retaliate against applicants or employees based on pregnancy, childbirth, and pregnancy-related medical conditions under the following federal laws:

- Title VII of the Civil Rights Act of 1964 (Title VII), as amended by the Pregnancy Discrimination Act of 1978 (PDA);
- The Family and Medical Leave Act (FMLA);
- The Americans with Disabilities Act (ADA); and
- The Fair Labor Standards Act (FLSA).



**Understand the PDA.** The PDA is a federal law that amended Title VII and expanded the coverage of sex discrimination under Title VII to include discrimination on the basis of pregnancy, childbirth, and related medical conditions. The PDA primarily prohibits discrimination on the basis of pregnancy as a medical condition and establishes that pregnant employees should be treated like other employees similar in their ability or inability to work for all employment-related purposes (42 U.S.C. § 2000e(k)).

In March 2015, the US Supreme Court established a new "significant burden" standard for evaluating disparate treatment discrimination claims under the PDA, holding that a pregnant worker can show that her employer's legitimate, non-discriminatory justifications are pretextual under the McDonnell Douglas burden-shifting framework if its accommodation policies impose a "significant burden" on pregnant workers and the employer's reasons are not "sufficiently strong" (Young v. United Parcel Serv., Inc., 575 U.S. 206 (2015)).

**Note that state law may create broader protection for employees and applicants.** State law may impose stricter requirements for employers. Local law may provide additional protection as well.

**Be aware what employer conduct constitutes discrimination, harassment, and retaliation based on an individual's pregnancy.** Legal claims may arise due to an employer's:

- statements in job advertisements indicating a preference or limitation based on pregnancy status;
- refusal to hire an applicant;
- refusal to promote an employee;
- demotion of an employee;
- termination of an employee, including constructive discharge;
- discrimination regarding an employee's compensation, or other terms and conditions or privileges of employment, such as - access to training or employer resources;
- classification or segregation of employees to deprive them of employment opportunities or adversely affect their status as - employees;
- refusal or failure to prevent or eliminate harassment;
- retaliation against an employee because she opposed pregnancy discrimination, filed a charge of pregnancy discrimination, or testified, assisted, or otherwise participated in an investigation or proceeding relating to a claim of pregnancy discrimination; and
- offer of accommodations to non-pregnant employees but not to pregnant employees who are similar in their ability or inability to work.

**Avoid the appearance of unlawful practices that could form the basis of a complaint.** Eliminating pregnancy discrimination, harassment, and retaliation from employment decisions will minimize the risk of, but may not completely insulate employers from, related Equal Employment Opportunity Commission (EEOC) charges and other litigation.

**Avoid fetal protection policies.** Employers should not create policies that discriminate against women because of their capacity to bear children in an effort to protect a possible future child. For example, an employer's policy prohibiting all women, except those whose infertility was medically documented, from working in jobs involving actual or potential lead exposure is unlawful under Title VII (see Int'l Union, United Auto., Aerospace & Agric. Implement Workers v. Johnson Controls, Inc., 499 U.S. 187 (1991)).

**Avoid policies or programs that benefit non-pregnant workers but not pregnant workers.** In light of the US Supreme Court's Young decision, employers should err on the side of equal offerings of accommodations or other benefits to pregnant and non-pregnant employees (575 U.S. 206).

**Understand the bona fide occupational qualification exception, but use it rarely.** Employers should not make hiring decisions based on protected class status. Title VII recognizes a limited bona fide occupational qualification (BFOQ) exception that:
- authorizes employers to factor sex, religion, national origin, or age into an employment decision where an employer can prove that the characteristic is reasonably related to the employer's operations; and
- may only be used in extraordinary situations (for example, removing pregnant flight attendants from flight duty because of the airline's safety concerns, see Levin v. Delta Air Lines, Inc., 730 F.2d 994 (5th Cir. 1984)).

**Recognize the EEOC's role.** The EEOC is responsible for enforcing the PDA and:
- has set out binding regulations with which employers must comply (29 C.F.R. §§ 1604.1-1604.11);
- has released non-binding guidelines (Enforcement Guidance: Pregnancy Discrimination and Related Issues), questions and answers- ons and Answers About the EEOC's Enforcement Guidance on Pregnancy Discrimination and Related Issues), and a fact sheet (Fact Sheet: Pregnancy Discrimination) with which employers may comply, although the US Supreme Court's Young decision called the 2014 guidance into question; and
- is the agency to which claimants may bring claims of Title VII and ADA violations. Only after exhausting their administrative remedies and obtaining a right-to-sue letter may claimants file a lawsuit in court. The EEOC also may file a lawsuit on behalf of claimants.

**Recognize the role of state and local fair employment practices agencies.** Many state and local governments have their own fair employment practices agencies (FEPAs) that serve as equivalents to the federal EEOC and enforce equivalent state and local laws. Some FEPAs require a claimant to exhaust administrative remedies before proceeding in court. Most FEPAs also have work-sharing agreements with the EEOC, permitting the agencies to decide which will investigate the charge and issue a determination. In many instances, where there is a FEPA, either:
- the charge is automatically filed with both agencies; or
- the charging party may choose to "dual file" the charge.


## Prevent Pregnancy Discrimination in Interviewing and Hiring

## Manage Childbirth Leaves of Absence

## Minimize the Risk of Liability During Employment

### Implement and Enforce Policies and Procedures Prohibiting Pregnancy Discrimination

### Ensure Policies and Procedures Are Accessible

### Promote Compliance

## Reduce the Risk of Litigation at the End of the Employment Relationship

**Ensure that termination and layoff decisions are free from discrimination or the appearance of it.** Analyze decisions to ensure that protected characteristics are not part of the decision-making process and the decisions do not appear to have a disproportionate impact on any protected class. Review state and local laws to ensure compliance with additional protected classes recognized in particular jurisdictions (such as marital status or sexual preference).

**Document the basis for termination and layoff decisions.** Document the legitimate business reasons for termination and layoff decisions concerning each affected employee. Employers that can point to objective non-discriminatory reasons for each decision generally have a better defense against discrimination claims.

**Maintain diversity among decision makers.** It is generally harder for an employee to prove discrimination when one or more individuals involved in the decision are members of the same protected class as the employee.

**Exercise caution when responding to questions about why an employee was discharged.** Do not dismiss or joke about follow-up questions from employees subject to adverse employment decisions.

**Obtain waivers and releases from employees who present a litigation risk.** For employees who may bring claims against the employer on grounds of discrimination, harassment, or retaliation, employers can insulate themselves against litigation risks by asking departing employees to sign a waiver and release of claims.
Treat departing employees with respect. Employers that treat departing employees with respect reduce the risk of those employees filing legal complaints.
---
---
---
# Pregnancy Discrimination

## Pregnancy Discrimination Under Title VII and the PDA

Title VII of the Civil Rights Act of 1964 (Title VII) is the principal federal statute prohibiting employment discrimination. It prohibits discrimination on the basis of:

    Race.
    Color.
    National origin.
    Religion.
    Sex (including gender, pregnancy, sexual orientation, and gender identity).

(42 U.S.C. § 2000e.)
Pregnancy was not always specifically afforded legal status as a protected class under Title VII. Title VII was amended to expand its coverage of sex discrimination through the enactment of the Pregnancy Discrimination Act (PDA). It now encompasses pregnancy discrimination by extending the term "on the basis of sex" to include:

    Pregnancy.
    Childbirth.
    Related medical conditions.

Title VII also prohibits harassment and retaliation.

### Title VII and PDA Coverage
#### Covered Employers
Most private employers employing 15 or more employees are covered under Title VII. For more information on employer coverage under Title VII generally, including various exceptions and how to count employees toward the 15-employee threshold, see Practice Note, Discrimination Under Title VII: Basics: Covered Employers.

#### Covered Individuals
Title VII generally protects applicants and employees, including former employees. Title VII does not cover:

    Foreign nationals working abroad for US-controlled companies.
    US citizens working abroad for non-US-controlled companies.
    Independent contractors.

### Pregnancy Discrimination Prohibited by Title VII and the PDA
Title VII generally prohibits pregnancy discrimination in the same way it prohibits discrimination based on other protected class characteristics. Title VII specifically prohibits a broad range of discriminatory employer conduct based on an individual's pregnancy, including:

    Refusing to hire an applicant.
    Terminating an employee and constructive discharge.
    Refusing to promote an employee.
    Demoting an employee.
    Discriminating regarding an employee's compensation, terms, conditions, or privileges of employment, such as transfer, access to training, or access to equipment.
    Classifying or segregating employees in a way that:
        deprives the employees of employment opportunities; or
        adversely affects their status as employees.
    Making statements in job advertisements that indicate a preference or limitation based on pregnancy status.
    Refusing or failing to prevent or eliminate harassment.

(42 U.S.C. § 2000e-2(a).)
Title VII also prohibits retaliation against an applicant or employee because she:

    Opposed pregnancy discrimination prohibited by Title VII.
    Filed a charge of pregnancy discrimination under Title VII.
    Testified, assisted, or otherwise participated in an investigation or proceeding relating to a claim of pregnancy discrimination under Title VII.

(42 U.S.C. § 2000e-3(a).)


Under Title VII, employers must treat pregnant women the same way they treat other employees with a similar ability or inability to work for all employment-related purposes. Unlike the Americans with Disabilities Act of 1990 (ADA), Title VII and the PDA contain no reasonable accommodation requirement specific to any condition related to pregnancy.

Although pregnant women are not entitled to a "most favored nation" status, they may show that a workplace policy imposes a significant burden on pregnant workers in support of their PDA claim (Young v. United Parcel Service, Inc., 135 S. Ct. 1338, 1354 (2015)).
