---
title| Copyright Registration of Photographs
author| casey@mclaw.io
date| 2021-11-14
category | Intellectual Property and Technology
subcategory | Copyright
tags | copyright, photographs, images, visual
summary| This article provides information about registering photographs with the U.S. Copyright Office. In particular, it covers the eligibility requirements and procedures for registering a group of unpublished or a group of published photographs. Group registration is suitable for most applicants registering multiple photographs.
--
A domain name is an important asset for any business or individual with a web presence. It serves as an online identity and can influence a company's brand and reputation. However, with the proliferation of online businesses, domain name disputes have become increasingly common. In this article, we will explain what domain name disputes are, what causes them, and how to handle them.

### What Are Domain Name Disputes?

Domain name disputes arise when two or more parties claim the right to use the same domain name. The most common type of domain name dispute is cybersquatting, which occurs when someone registers a domain name that is identical or similar to a trademarked name with the intention of selling it for a profit or using it to generate traffic. However, disputes can also arise when two parties have a legitimate claim to the same domain name or when someone registers a domain name that is a variation of another party's name.

### Causes of Domain Name Disputes

One of the main causes of domain name disputes is the scarcity of desirable domain names. As the number of websites and online businesses has grown, finding an available domain name that fits a company's brand and marketing strategy has become increasingly difficult. This has led to a rise in cybersquatting, where individuals or businesses register domain names with the intention of profiting from their sale or generating traffic.

Another cause of domain name disputes is the lack of regulation in the domain name registration process. While the Internet Corporation for Assigned Names and Numbers (ICANN) oversees the registration of domain names, it does not police the actions of individual registrars or website owners. This can make it difficult for trademark owners to protect their intellectual property and for individuals to claim ownership of a domain name that they feel is rightfully theirs.

### How to Handle Domain Name Disputes

If you find yourself involved in a domain name dispute, there are several steps you can take to resolve the issue.

1. Contact the Domain Name Registrant: If you believe that someone has registered a domain name that belongs to you or your business, the first step is to contact the registrant and try to resolve the dispute informally. If the registrant is willing to transfer the domain name to you, this can be the simplest and least expensive way to resolve the dispute.

2. Use a Domain Name Dispute Resolution Service: If you are unable to resolve the dispute informally, you may want to use a domain name dispute resolution service such as the Uniform Domain-Name Dispute-Resolution Policy (UDRP) or the World Intellectual Property Organization (WIPO). These services are designed to provide a fast and cost-effective way to resolve domain name disputes without the need for legal action.

3. File a Lawsuit: If all else fails, you may need to file a lawsuit to resolve the domain name dispute. This can be an expensive and time-consuming process, but it may be necessary to protect your trademark or domain name.

#### Conclusion

Domain name disputes can be frustrating and costly for individuals and businesses. However, by taking proactive steps to protect your intellectual property and by being prepared to take legal action if necessary, you can help ensure that your online identity is protected. If you are involved in a domain name dispute, it is important to seek the advice of an experienced attorney who can help you navigate the legal process and protect your rights.

## Anticybersquatting Consumer Protection Act
The Anti-cybersquatting Consumer Protection Act (ACPA) and the Uniform Domain Name Dispute Resolution Policy (UDRP) are two legal tools that can be used to combat cybersquatting, a practice in which someone registers a domain name that is identical or confusingly similar to someone else's trademark with the intent of profiting from it. While the two tools share some similarities, there are some key differences that are worth understanding. In this blog post, we will explore the differences between ACPA and UDRP.

#### Legal Framework
ACPA is a federal law in the United States that was enacted in 1999. The law is intended to protect trademark owners from cybersquatters who register domain names with the intent to profit from the use of someone else's trademark. ACPA provides statutory damages and injunctive relief to trademark owners who can prove that their trademarks have been infringed by a cybersquatter.

UDRP, on the other hand, is a policy established by the Internet Corporation for Assigned Names and Numbers (ICANN) that applies to disputes over generic top-level domain names (gTLDs) such as .com, .org, and .net. The policy is designed to provide a quick and cost-effective way for trademark owners to resolve domain name disputes with cybersquatters.

#### Burden of Proof
Under ACPA, the burden of proof is on the trademark owner to show that the cybersquatter acted in bad faith and with the intent to profit from the use of the trademark. In other words, the trademark owner must show that the cybersquatter had the specific intent to profit from the use of the trademark in the domain name.

In contrast, under UDRP, the burden of proof is on the trademark owner to show that the domain name is identical or confusingly similar to the trademark, that the registrant has no legitimate right or interest in the domain name, and that the domain name was registered and is being used in bad faith. The trademark owner does not need to prove that the cybersquatter had the specific intent to profit from the use of the trademark in the domain name.

#### Remedies
Under ACPA, a trademark owner can obtain injunctive relief and monetary damages, including statutory damages of up to $100,000 per domain name. The law also allows for the forfeiture or cancellation of the domain name.

Under UDRP, the remedies available to the trademark owner include the cancellation or transfer of the domain name registration. UDRP does not provide for monetary damages or injunctive relief.

#### Applicability

ACPA is a U.S. law and applies only to domain name registrations that are registered or used in the United States. UDRP, on the other hand, applies to domain name registrations under gTLDs, regardless of the country in which the registrant is located.

In conclusion, ACPA and UDRP are two legal tools that can be used to combat cybersquatting. While they share some similarities, such as the goal of protecting trademark owners from cybersquatters, they differ in terms of legal framework, burden of proof, remedies, and applicability. Depending on the specific circumstances of a given case, one or the other may be a more appropriate tool for resolving a domain name dispute.

## UDRP
UDRP disputes refer to domain name disputes that are resolved through the Uniform Domain Name Dispute Resolution Policy (UDRP), a policy established by the Internet Corporation for Assigned Names and Numbers (ICANN). The UDRP is an alternative to traditional legal proceedings that can be used to resolve domain name disputes in a quicker and less expensive manner.

The UDRP applies to disputes over generic top-level domain names (gTLDs) such as .com, .org, and .net. The policy is intended to protect trademark owners from the abusive registration of domain names that are identical or confusingly similar to their trademarks. Under the UDRP, a trademark owner can file a complaint against a domain name registrant who has registered a domain name that infringes on the trademark owner's rights.

To file a UDRP complaint, the trademark owner must provide evidence that they have a valid trademark that is being infringed by the domain name, that the registrant has no legitimate right or interest in the domain name, and that the domain name was registered and is being used in bad faith. The complainant must also provide evidence of the domain name registration, such as the registration date and the identity of the registrar.

Once the complaint is filed, the domain name registrant is given a period of time to respond to the allegations. The respondent can argue that they have a legitimate right or interest in the domain name, or that the domain name was not registered and is not being used in bad faith. The respondent can also challenge the jurisdiction of the UDRP panel.

A panel of independent experts is then appointed to review the evidence presented by both parties and decide the dispute. The panel's decision can result in the cancellation of the domain name registration, the transfer of the domain name to the complainant, or the dismissal of the complaint. The panel's decision is binding on both parties and can only be challenged in limited circumstances, such as where there is evidence of fraud or bias.

Overall, UDRP disputes provide a way for trademark owners to protect their intellectual property rights in the context of domain name registrations. The UDRP process is designed to be efficient, cost-effective, and accessible to trademark owners who may not have the resources to pursue a traditional legal action. By resolving disputes through the UDRP, trademark owners can protect their brands and reputation while also promoting a fair and open internet.

### WIPO
The World Intellectual Property Organization (WIPO) is a specialized agency of the United Nations that deals with intellectual property rights. Among its services, WIPO provides a mechanism for resolving domain name disputes, known as the Uniform Domain Name Dispute Resolution Policy (UDRP). In this article, we will describe the process for a WIPO domain name dispute.

The UDRP applies to disputes between a domain name registrant and a third party who claims that the domain name is identical or confusingly similar to a trademark or service mark in which the third party has rights, and that the registrant has no legitimate interest in the domain name, and that the domain name was registered and used in bad faith. To initiate a WIPO domain name dispute, the complainant must submit a complaint to WIPO, which will then forward the complaint to the respondent (the domain name registrant).

The complaint must be submitted electronically and contain the following information:

    The domain name that is the subject of the dispute.
    The identity and contact information of the complainant and any representative authorized to act on behalf of the complainant.
    A description of the complainant's rights in the trademark or service mark that is allegedly being infringed.
    An explanation of why the domain name is identical or confusingly similar to the complainant's trademark or service mark.
    Evidence that the respondent has no legitimate interest in the domain name.
    Evidence that the domain name was registered and is being used in bad faith.

The respondent then has 20 days to file a response to the complaint. The response must be submitted electronically and contain the following information:

    A statement of the respondent's position regarding each of the allegations made in the complaint.
    Any evidence that the respondent has a legitimate interest in the domain name or that the domain name was not registered and is not being used in bad faith.
    Any other arguments or evidence that the respondent wishes to submit.

The WIPO Center will then appoint a panel of one or three independent experts to decide the dispute. The panel will consider the written submissions of both parties, as well as any other evidence that it deems relevant. The panel will then issue a decision, which may include a transfer of the domain name to the complainant or a finding that the complaint is not justified.

If the panel decides in favor of the complainant, the registrar of the domain name will be instructed to transfer the domain name to the complainant. If the panel decides in favor of the respondent, the complaint will be dismissed and the respondent will retain the domain name.

In conclusion, the process for a WIPO domain name dispute involves the submission of a complaint by the complainant, a response by the respondent, and a decision by a panel of independent experts. The decision may result in the transfer of the domain name to the complainant or the dismissal of the complaint, depending on the findings of the panel. This process provides a mechanism for resolving disputes between domain name registrants and trademark or service mark owners in a fair and impartial manner.

### Anticybersquatting Consumer Protection Act vs. UDRP


# UDRP

## Elements of UDRP Complaint:

To prevail, a complainant must prove all of the following elements:

- The domain name is identical or confusingly similar to a trademark or service mark in which the complainant has rights;
- The domain name registrant has no rights or legitimate interests in respect of the domain name; and
- The domain name has been registered and is being used in bad faith.


# Anticybersquatting Consumer Protection Act (ACPA)

## Elements of ACPA Claims
Although courts differ on how they list and count the elements of an ACPA claim, all courts require a plaintiff to show that:

- It owns a valid trademark that was distinctive or famous as of the date of- istration of the challenged domain name .
- The defendant registered, trafficked in, or used the challenged domain name.
- The challenged domain name is identical or confusingly similar to or dilutive of the plaintiff's distinctive or famous mark.
- The defendant acted with a bad faith intent to profit from the plaintiff's mark.

15 U.S.C. § 1125(d); DaimlerChrysler v. Net Inc., 388 F.3d 201, 204 (6th Cir. 2004).

### Ownership of a Protectable Trademark or Name
The plaintiff in an ACPA cybersquatting action must show ownership of a protectable trademark (15 U.S.C. § 1125(d)(1)(A)). This includes a personal name that is protected as a trademark (see Personal Name Cybersquatting). The ACPA also requires the plaintiff's protectable mark to have been distinctive or famous at the time the challenged domain name was registered (15 U.S.C. § 1125(d)(1)(A)(ii)(I) and (II)). Because a protectable trademark (including a famous mark) must necessarily be distinctive, and a distinctive mark is generally protectable, distinctiveness, fame, and protectability are overlapping issues.

Most challenges to the plaintiff's trademark in ACPA cases are to its fame or distinctiveness (see Distinctive or Famous at Time of Registration). However, a defendant occasionally challenges a mark as unprotectable on another ground, such as non-use (see, for example, Thompson v. Does 1-5, 376 F. Supp. 3d 1322, 1327 (N.D. Ga. 2019) (analyzing whether plaintiff used the mark in commerce to support trademark rights before the challenged domain name registration date)).

Ownership of a federal registration creates a presumption of valid trademark ownership (15 U.S.C. § 1057(b)). However, a plaintiff does not need a trademark registration to assert an ACPA claim (Lahoti v. VeriCheck, Inc., 586 F.3d 1190, 1196-97 (9th Cir. 2009); DaimlerChrysler, 388 F.3d at 205). It is enough to show use of a mark in US commerce sufficient to establish unregistered trademark rights, also known as common law trademark rights. 

At least one court has held that it may not be necessary for the mark owner to have a physical presence in the US to show use in commerce. For example, the Court of Appeals for the Eleventh Circuit allowed a Brazilian department store to pursue an ACPA cybersquatting claim where the owner received advertising revenues from US companies for preferred product placement and banner advertising on its website (Direct Niche, LLC v. Via Varejo S/A, 898 F.3d 1144, 1150-51 (11th Cir. 2018)).

#### Distinctive or Famous at Time of Registration
As of the date of registration of the challenged domain name, the trademark that is the basis for the plaintiff's ACPA cybersquatting claim must be either:

 Distinctive. The distinctiveness requirement is the same as the requirement for trademark protection (see Lahoti, 586 F.3d at 1197-98 (applying same analysis).
    
Famous. The fame threshold is the same as the requirement for dilution claims (Sporty's Farm L.L.C. v. Sportsmans Mkt., Inc., 202 F.3d 489, 497 n.10 (2d Cir. 2000); Omega S.A. v. Omega Eng'g, Inc., 228 F. Supp. 2d 112, 123 (D. Conn. 2002)).

Because a famous mark is necessarily distinctive (either inherently or by acquiring distinctiveness through use), it may be easier for a cybersquatting plaintiff to just rely on the distinctiveness prong. However, fame is necessary when the basis for a cybersquatting claim is dilution and not confusing similarity (see Dilutive Domain Names). Thus, if a plaintiff seeks to argue dilution (for example, for a critique site like "[MARK]sucks.com", where the domain name itself contains matter that may prevent confusion), the plaintiff must argue that its mark is famous.

##### Effect of Registration
A registration for the plaintiff's mark creates a rebuttable presumption that the mark was distinctive as of its date of registration (see Newport News Holdings Corp. v. Virtual City Vision, Inc., 650 F.3d 423, 434 n.7 (4th Cir. 2011); Mayflower Transit, LLC v. Prince, 314 F. Supp. 2d 362, 367 (D.N.J. 2004)). An incontestable registration is conclusive evidence of distinctiveness (see Web-adviso v. Trump, 927 F. Supp. 2d 32, 39 (E.D.N.Y. 2013) (citing Park 'N Fly, Inc. v. Dollar Park 'N Fly, Inc., 469 U.S. 189, 191 (1985))).

##### Timing of Distinctiveness or Fame
The ACPA's wording requires that distinctiveness or fame be analyzed as of the date of the challenged domain name's registration (15 U.S.C. § 1125(d)(1)(A)(ii)(I), (II)). In cases where the asserted mark became distinctive or famous after registration of the challenged domain name, this may be grounds for rejecting an ACPA cybersquatting claim (GoPets Ltd. v. Hise, 657 F.3d 1024, 1032 (9th Cir. 2011) (rejecting ACPA claim because plaintiff's mark became distinctive after defendant registered the challenged domain name and defendant's later re-registration does not save the claim); see also ZP No. 314, LLC v. ILM Capital, LLC, 335 F. Supp. 3d 1242, 1259-60 (S.D. Ala. 2018) (holding that presumption of distinctiveness applies only as of the date of the domain name's registration, concluding that mark had not acquired distinctiveness before registration, and dismissing claims based on alleged pre-registration violations)).

### Defendant's Domain Name Registration, Trafficking, or Use
Unlike the UDRP, which requires a complainant to show bad faith registration and bad faith use of a domain name, under the ACPA, use alone can be enough for a violation. The ACPA provides relief for a defendant's bad faith intent to profit from a domain name when engaged in any one (or more) of the following: registering it, trafficking in it, or using it. 15 U.S.C. § 1125(d)(1)(A)(ii).

Given the prohibition against bad faith registration and trafficking, there is no ACPA requirement that a domain name owner make commercial use of the domain name the way there is in trademark infringement and dilution claims involving domain names (Bosley Med. Inst., Inc. v. Kremer, 403 F.3d 672, 681 (9th Cir. 2005); see also GoPets, 657 F.3d at 1035 (recognizing that mere registration of a domain name is insufficient to support a trademark infringement claim)).

#### Domain Name Registration
Registering a domain name refers to the act of registering a domain name with a registrar. Although domain name registration is relatively straightforward, parties sometimes dispute whether it qualifies as registration under the ACPA to:

- Re-register an already-owned domain name (see Domain Name Re-Registration).
- Change the domain name ownership information (see Changing Ownership Information).

##### Domain Name Re-Registration
Courts disagree on whether re-registration of a domain name qualifies as registration under the ACPA. In Jysk Bed'N Linen v. Dutta-Roy, the defendant had initially registered the domain name in good faith on behalf of the plaintiff. After the parties ceased doing business together, the defendant re-registered the domain name under circumstances the court concluded qualified as bad faith. The defendant argued that its re-registration did not qualify as a registration under the ACPA. The Eleventh Circuit rejected this argument, holding that the defendant's re-registration is subject to its own bad faith analysis. ( 810 F.3d 767, 777 (11th Cir. 2015); see also Schmidheiny v. Weber, 319 F.3d 581, 583 (3d Cir. 2003) (reversing district court dismissal because initial registration preceded the ACPA where the defendant re-registered domain name after ACPA).)

In GoPets, the defendant registered the domain name gopets.com before the plaintiff secured trademark rights in the GO PETS mark. Although the defendant eventually attempted to sell the domain name to the plaintiff for significant profit and took other actions that arguably were in bad faith, the Ninth Circuit denied the ACPA claim because the plaintiff's mark was not distinctive at the time of the defendant's initial registration. The court rejected the plaintiff's argument that the mark was distinctive when the defendant later re-registered the domain name. (657 F.3d at 1031-32.)

##### Changing Ownership Information
A domain name owner can change the name of the domain name account holder with a registrar, effectively transferring ownership of the domain name. At least one court has held that this qualifies as domain name registration. In Christensen Firm v. Chameleon Data Corp., the defendant was authorized to maintain the plaintiff's domain name account. When a billing dispute arose between the parties, the defendant changed the plaintiff's name on the account to its own name. The court held that this qualified as domain name registration under the ACPA as a matter of law. (2006 WL 3158246, at *4-5 (W.D. Wash. Nov. 1, 2006).)

#### Domain Name Use
Use of a domain name typically involves using it to direct an internet user to a website. Only the domain name owner or its licensee may be liable for using a domain name with a bad faith intent to profit (15 U.S.C. § 1125(d)(1)(D)).
Even when a domain name was registered and used in good faith, its subsequent use in bad faith may support an ACPA claim. For example, in Newport, the plaintiff challenged the domain name newportnews.com based on its rights in the NEWPORT NEWS trademark for women's apparel. The defendant initially registered and used the domain name for a website about Newport News, Virginia and a UDRP panel rejected transfer of the domain name based on this legitimate use. Years later, after the plaintiff expressed interest in purchasing the domain name, the defendant changed the website to one emphasizing women's fashion. The US Court of Appeals for the Fourth Circuit held that this new use was grounds for a bad faith finding under the ACPA. (650 F.3d at 436.)
Another common circumstance involving good faith registration and subsequent bad faith use is where an employee registers a domain name for its employer and later holds the domain name hostage to obtain leverage in a dispute with the employer (see, for example, DSPT Int'l, Inc. v. Nahum, 624 F.3d 1213, 1220 (9th Cir. 2010); Emerald City Mgmt., LLC v. Kahn, 2016 WL 98751, at *14 (E.D. Tex. Jan. 8, 2016)).

#### Domain Name Trafficking
Trafficking in a domain name typically involves a domain name owner's attempt to sell the domain name to the trademark owner or one of its competitors (see, for example, Ford Motor Co. v. Catalanotte, 342 F.3d 543, 548-49 (6th Cir. 2003) (holding that trafficking does not require completed transactions but also includes offers for sale)). However, the ACPA defines trafficking broadly as referring to transactions that include, but are not limited to, sales, purchases, loans, pledges, licenses, exchanges of currency, and any other transfer for consideration (15 U.S.C. § 1125(d)(1)(E)).

Domain name registrars and sites hosting domain name auctions do not traffic in domain names within the meaning of the ACPA (Bird, 289 F.3d at 881).

### Identical, Confusingly Similar, or Dilutive
The ACPA provides a cause of action for bad faith registering, trafficking, or using a domain name that at the time of registration is:

- Identical or confusingly similar to a distinctive mark.
- Identical or confusingly similar to or dilutive of a famous mark.

15 U.S.C. § 1125(d)(1)(A)(ii).

Most cybersquatting claims allege that the domain name is identical or confusingly similar to the plaintiff's distinctive mark under the first prong (see Confusingly Similar Domain Names). Under the second prong, courts may also impose liability if the domain name dilutes a famous mark (see Dilutive Domain Names). However, because all famous marks are distinctive and a domain name that is dilutive of a trademark is usually confusingly similar to the mark, the second prong adds little and is rarely the basis for ACPA claims.

#### Confusingly Similar Domain Names
When assessing confusing similarity, courts compare the challenged domain name and the asserted trademark in terms of their sight, sound, and meaning, without reference to the parties' goods or services (15 U.S.C. § 1125(d)(1)(A); Omega, 228 F. Supp. 2d at 126). Courts do not consider the context of the parties' uses or the other likelihood of confusion factors used to evaluate trademark infringement claims. It does not matter if an internet user's confusion is dispelled by disclaimers or other material when arriving at the website at the challenged domain name. (Newport, 650 F.3d at 436-37 (rejecting defendant's argument that the court must compare the parties' websites and context of the domain name's use); Coca-Cola Co. v. Purdy, 382 F.3d 774, 783 (8th Cir. 2004) (recognizing that the inquiry under the ACPA is narrower than the trademark infringement likelihood of confusion test).)

Slight differences between the domain name and the plaintiff's mark generally are insufficient to render them dissimilar, for example:
- Misspellings of the plaintiff's trademark, also known as typosquatting (Shields v. Zuccarini, 254 F.3d 476, 483-84 (3d Cir. 2001)).
- The plaintiff's trademark combined with non-distinctive terms (DSPT, 624 F.3d at 1222 (holding that eq-Italy.com was confusingly similar to plaintiff's EQ mark for Italian-style clothing); DaimlerChrysler, 388 F.3d at 206 (finding foradodge.com confusingly similar to the DODGE mark)).

Most cybersquatting cases involve one of these two scenarios. However, where a domain name incorporates a generic term that clearly indicates the attached website is not associated with the trademark owner, such as a criticism or commentary website regarding the trademark owner's goods or services, a court may find that the domain name is not confusingly similar to the trademark (see Coca-Cola, 382 F.3d at 791 (giving as examples of acceptable domain names purdysupportspepsi.com and purdyscokesite.com); see also McAllister Olivarius v. Mermel, 298 F. Supp. 3d 661, 671-72 (S.D.N.Y. 2018) (denying motion to dismiss because addition of the term "truth" to the plaintiff's mark is not, unlike "sucks," so evidently intended as criticism as to warrant dismissal)).

#### Dilutive Domain Names
ACPA plaintiffs rarely argue that the challenged domain name is dilutive of the plaintiff's famous trademark, presumably because a famous mark is distinctive and a domain name that dilutes a trademark is typically confusingly similar to the mark (see 5 J. Thomas McCarthy, McCarthy on Trademarks and Unfair Competition, § 25A:50 (5th ed. 2020) (noting there is little incentive to rely on the dilution provision of the ACPA)).

However, the ACPA's protection against trademark dilution may provide an additional (and possibly preferable) basis for cybersquatting claims in cases where the domain name includes generic matter making it less likely that consumers will assume a connection with the trademark owner, for instance in the case of "[trademark]sucks" domain names. In Lucent Technologies, Inc. v. Johnson, the court concluded that the plaintiff stated a dilution-based ACPA claim where the defendants linked the lucentsucks.com domain name to a pornographic website. The court determined that this type of association could tarnish the plaintiff's mark by corroding the positive associations of the mark and reducing the mark's value. (2000 WL 1604055, at *3 (C.D. Cal. Sept. 12, 2000) (also finding that confusing similarity is a factual issue and denying dismissal of this basis for ACPA liability); see also Ford Motor Co. v. Lapertosa, 126 F. Supp. 2d 463, 466-67 (E.D. Mich. 2000) (holding use of fordrecalls.com for a pornographic website tarnishes plaintiff's FORD mark).)

However, courts often deny ACPA claims involving legitimate gripe sites (see Defendant's Fair Use of Domain Name).

### Bad Faith Intent to Profit
The ACPA requires proof that the defendant had a bad faith intent to profit from the domain name (15 U.S.C. § 1125(d)(1)(A)(i)). Two examples of a bad faith intent to profit that courts often cite are where the defendant:

- Tries to sell the domain name to the trademark owner for a high price.
- Uses the domain name to divert customers to the defendant's own website where the defendant sells its own goods or services.

See, for example, Utah Lighthouse Ministry v. Found. for Apologetic Info. & Research, 527 F.3d 1045, 1058 (10th Cir. 2008).

However, a bad faith intent to profit is not limited to these two circumstances. When evaluating the bad faith element of an ACPA cybersquatting claim, courts generally consider:

- The nine factual scenarios listed in the ACPA that may be relevant to a bad faith determination (see The ACPA's Bad Faith Factors).
- The unique facts or circumstances of the case and how they affect evaluation of the nine bad faith factors (see Unique Circumstance of Case).
- Whether the ACPA's safe harbor, which protects defendants who reasonably believe their conduct was lawful or fair, applies (see Fair Use Safe Harbor).

Rearden LLC v. Rearden Commerce, Inc., 683 F.3d 1190, 1220 (9th Cir. 2012).

Unlike in UDRP proceedings where bad-faith registration is required, under the ACPA, a bad faith intent to profit can arise at any time, including well after registration of the domain name. For example, courts have found bad faith in cases where employees properly registered domain names for their employers but later engaged in bad faith conduct after disputes arose between the parties (DSPT, 624 F.3d at 1220; Emerald, 2016 WL 98751, at *14; see also Newport, 650 F.3d at 436 (finding bad faith despite defendant's good faith registration and initial use where defendant changed its legitimate website to compete with plaintiff after plaintiff expressed interest in the domain name)).

#### The ACPA's Bad Faith Factors
The ACPA lists nine factors that courts may consider to the extent applicable when assessing the defendant's bad faith intent to profit (15 U.S.C. § 1125(d)(1)(B)(i)). The first four concern the legitimacy of the domain name owner's registration and use of the domain name and may indicate the absence of bad faith. The other five set out circumstances that would tend to support a bad faith finding. ( H.R. Rep. No. 106-412, 1999 WL 970519, at *10; Lamparello v. Falwell, 420 F.3d 309, 319 (4th Cir. 2005); Coca-Cola, 382 F.3d at 785.)
Courts often refer to the factors by number in the order listed in the ACPA, as follows:

- The defendant's intellectual property rights in the domain name (see- ain Name Reflects Owner's Trademark).
- The extent to which the domain name consists of:
  - the defendant's legal name; or
  - a name that is otherwise commonly used to identify the defendant.
- The defendant's previous use of the domain name in connection with - bona fide offering of goods or services (see Defendant's Legitimate - ercial Use of Domain Name).
- The defendant's bona fide non-commercial or fair use of the - ntiff's mark in a site accessible under the domain name (see Defendant's Fair - of Domain Name).
- The defendant's intent to divert consumers from the plaintiff's - ne location to a site accessible under the domain name that could harm the - will represented by the mark by creating a likelihood of confusion as to the - ce, sponsorship, affiliation, or endorsement of the site, either:
  - for commercial gain; or
  - with the intent to tarnish or disparage the mark.
- Whether the defendant has:
  - offered to transfer, sell, or otherwise assign the domain name to the mark- er or any third party for financial gain without having used or an intent to use it in the bona fide offering of any goods or services; or
  - engaged in a pattern of this type of conduct with respect to other domain names.
- Whether the defendant has:
  - provided material and false contact information when registering or maintaining the domain name; or
  - engaged in a pattern of this type of conduct with respect to other domain names.
- Whether the defendant has registered or acquired multiple domain names that are similar to third-party marks.
- The distinctiveness and fame of the plaintiff's mark).

15 U.S.C. § 1125(d)(1)(B)(i).

There is no formula for evaluating the factors, and some of them may not be relevant. They are intended only as a guide to help courts assess whether the defendant acted in bad faith. See Lamparello, 420 F.3d at 319-20. Courts may also consider any other relevant facts that support a bad faith finding (see Unique Circumstances of Case).

##### Domain Name Reflects Owner's Trademark

Because trademark rights are based on use of a mark in connection with particular goods or services, different parties often own rights in identical marks for different goods or services. The first ACPA bad faith factor considers whether, as of the domain name registration date, the domain name owner had any rights in the mark reflected in the domain name, which would weigh against a bad faith finding (15 U.S.C. § 1125(d)(1)(B)(i)(I)).

This factor is not limited to US rights. The defendant's rights in another jurisdiction weighs against a bad faith finding (Harrods Ltd. v. Sixty Internet Domain Names, 302 F.3d 214, 234 (4th Cir. 2002) (finding that the owner of domain names containing the term "harrods" owned rights in the name Harrods in Argentina and throughout South America and that this factor weighed against a bad faith finding); SunEarth, Inc. v. Sun Earth Solar Power Co., Ltd., 2013 WL 4528539, at *23 (N.D. Cal. Aug. 23, 2013) (finding owner of domain names containing the term "sunearth" owned rights in and was known by the name Sun Earth in China and that the first two factors weighed against a bad faith finding)).

One court has held that this factor does not favor the defendant if the domain name reflects a trademark that the defendant has abandoned (S. Grouts & Mortars, Inc. v. 3M Co., 575 F.3d 1235, 1249 (11th Cir. 2009) (ruling against the defendant on the first factor but in favor on the third factor because the defendant made prior legitimate commercial use of the domain name before abandoning the underlying mark); see Defendant's Legitimate Commercial Use of Domain Name).

In a case where the parties had rights in the same mark but for different goods and services and the defendant registered domain names combining the mark with generic terms referring to the plaintiff's services, the court found this (and the second) factor favored the defendant. However, the court noted that the generic terms were strong evidence of the defendant's intent to divert consumers away from the plaintiff (the fifth factor) (Omega, 228 F. Supp. 2d at 135 (denying plaintiff's summary judgment motion because fact issues remained regarding the defendant's bad faith); see Defendant's Intent to Divert Consumers).

##### Domain Name Reflects Owner's Name or Nickname
The second ACPA bad faith factor is similar to the first factor and considers to what extent the domain name reflects the owner's personal name or a name by which the owner was commonly known, which would weigh against a bad faith finding (15 U.S.C. § 1125(d)(1)(B)(i)(II); Harrod's, 302 F.3d at 234-35 (finding factor favors defendant because defendant is known by Harrod's name)). When including this factor in the ACPA, Congress cautioned that domain name owners cannot simply adopt a well-known mark as a nickname to evade a bad faith finding. Courts considering this factor have discretion to determine whether the domain name owner actually having the same name or nickname indicates a lack of bad faith. (House Judiciary Committee Report on H.R. 3028, H.R. Rep. No. 106-412 at *10 (Oct. 25, 1999).)

As with the first ACPA bad faith factor, this factor:

    May consider the defendant's name in jurisdictions outside the US (Harrod's, 302 F.3d at 234-35; SunEarth, 2013 WL 4528539, at *23).
    Does not consider names that the defendant did not use before the domain name registration (Sporty's, 202 F.3d at 498-99).

Even if the domain name reflects the defendant's name, this factor may weigh in favor of a bad faith finding if the challenged domain name includes other elements that specifically refer to the plaintiff and not the defendant. For example, in Reardon, the court found that the defendant Rearden Commerce's registration of the domain name reardonllc.com favored the plaintiff Rearden LLC because it depicted the plaintiff's precise legal name, which the defendant never used to identify itself (683 F.3d at 1220 (also considering that defendant registered the domain name after learning plaintiff might oppose defendant's trademark applications); but see Omega, 228 F. Supp. 2d at 135 (finding that the factor favored defendant, who registered domain name consisting of parties' shared trademark combined with generic terms identifying the plaintiff's products)).

##### Defendant's Legitimate Commercial Use of Domain Name
The third ACPA bad faith factor considers whether the defendant previously used the domain name in connection with the bona fide offering of goods or services, which generally weighs against a bad faith finding (15 U.S.C. § 1125(d)(1)(B)(i)(III)). Courts generally consider only domain name use preceding the plaintiff's assertion of the ACPA claim (Sporty's Farm, 202 F.3d at 499 (finding that defendant's posting of a site selling Christmas trees only after the litigation began weighed in favor of bad faith)). This factor considers any use before the plaintiff's claim even if the defendant has discontinued the bona fide offering of goods and services (see Southern Grouts, 575 F.3d at 1247).

Courts have held this factor weighs against a bad faith finding in cases where:

    A defendant registered epix.com as a descriptive term referring to electronic pictures, posted personal material on the page, including content relating to electronic pictures, and showed his financial investment in a plan to develop an internet portal site (Interstellar Starship Servs., Ltd. v. Epix, Inc., 304 F.3d 936, 947 (9th Cir. 2002)).
    A defendant registered and used the domain name overheaddoorcoofalbanyga.com for years as a franchisee of plaintiff-owner of OVERHEAD DOOR trademark, and the plaintiff's only bad faith allegation was defendant's continued use of the domain name after termination of the relationship (Overhead Door Corp. v. Burger, 2013 WL 3057796, at *6 (M.D. Ga. June 17, 2013)).
    A domain name owner had used the domain names swix.com and swix.net for a website selling internet services for several years (Hartog & Co. AS v. SWIX.com, 136 F. Supp. 2d 531, 540 (E.D. Va. 2001)).
    A defendant had used the diamondbrite.com domain name for promoting and selling products under the DIAMOND BRITE mark, even though it discontinued using and abandoned its rights in the mark (Southern Grouts, 575 F.3d at 1247).

**However, even a legitimate business use may not save the defendant from a bad faith finding where the defendant later changes the website in a manner that constitutes bad faith.** _See Newport_, 650 F.3d at 436 (finding bad faith despite initial legitimate use where defendant changed the website at the domain name to compete with plaintiff).

##### Defendant's Fair Use of Domain Name
The fourth ACPA bad faith factor considers whether the defendant's use of the plaintiff's trademark in connection with the domain name is a non-commercial or fair use protected by the First Amendment (15 U.S.C. § 1125(d)(1)(B)(i)(IV)). Although the defendant's non-commercial use is a factor weighing against bad faith, the defendant's commercial use of the domain name is not a prerequisite to liability (Bosley, 403 F.3d at 680-81).

This factor typically favors the defendant and weighs against a bad faith finding in cases where the defendant registered and is using the domain name for:

- Criticism (gripe) websites (see, for example, Lamparello, 420 F.3d at 321 (holding that registration and use of falwell.com domain name for non-commercial website criticizing Reverend Jerry Falwell was not bad faith); TMI, Inc. v. Maxwell, 368 F.3d 433, 439-40 (5th Cir. 2004) (finding no bad faith based on registration and use of domain name containing homebuilder's trademark for a website criticizing the builder's business practices); Lucas Nursery & Landscaping, Inc. v. Grosse, 359 F.3d 806, 811-12 (6th Cir. 2004) (finding no bad faith based on defendant's use of nursery's trademark for a website criticizing the nursery's work)).
- 
- Parody websites (see, for example, Utah, 527 F.3d at 1058-59 (holding that use of domain names for a non-commercial website parodying plaintiff's ministry is not bad faith)).

Courts carefully consider a defendant's allegation that it registered the domain name for a non-commercial, fair purpose to determine whether the allegation is genuine or pretextual (Sylvan Learning, Inc. v. sylvanfranchiseissues.com, 2011 WL 13234933, at *6 (E.D. Va. Oct. 27, 2011); Mayflower, 314 F. Supp. 2d at 372).

Where other factors favor a bad faith finding, a court may find bad faith despite a defendant's use of the challenged domain name for an allegedly fair purpose (see, for example, Coca-Cola, 382 F.3d at 787-88 (finding bad faith despite defendant's use of domain name for anti-abortion website where defendant registered numerous domain names incorporating trademarks and used them in a manner that could suggest the mark holders sponsored or authorized the web pages); People, 263 F.3d at 369 (finding bad faith despite defendant's alleged use for a website parodying the plaintiff where the site contained links to third-party websites and defendant intended to misdirect internet users); HER, Inc. v. RE/MAX First Choice, LLC, 2008 WL 11353606, at *6 (S.D. Ohio May 2, 2008) (finding bad faith despite use of domain name for website criticizing plaintiff where defendant was plaintiff's competitor and sought to benefit commercially from the criticism)).

When a defendant argues that this factor weighs against a bad faith finding, it typically also claims that its conduct falls within the ACPA's safe harbor, which states that is not bad faith if the defendant reasonably believed its conduct was lawful (see Fair Use Safe Harbor).

##### Defendant's Intent to Divert Consumers
The fifth ACPA bad faith factor considers whether the defendant is using a domain name that is likely to confuse consumers and divert them away from the plaintiff's website to a website for the defendant's own commercial gain or to disparage the plaintiff (15 U.S.C. § 1125(d)(1)(B)(i)(V)). This factor usually supports a bad faith finding when the defendant is using the domain name for an active website that benefits the defendant or that may harm the plaintiff's reputation. For example, courts have found this factor favors a bad faith intent to profit finding when:

- The domain name consisted of a misspelling of the plaintiff's trademark (also known as typosquatting) (see, for example Verizon Cal. Inc. v. Navigation Catalyst Sys., Inc., 568 F. Supp. 2d 1088, 1096-97 (C.D. Cal. 2008)).
- The website at the domain name included pay-per-click links that produce revenue for the defendant (see, for example, Lahoti, 586 F.3d at 1202; Webadviso v. Bank of Am. Corp, 448 F. App'x 95, 98 (2d Cir. 2011)).
- The domain name points to a website containing pornography or other offensive material (ViaView, Inc. v. Blue Mist Media, 2012 WL 6007204, at *4 (D. Nev. Nov. 30, 2012); Morrison & Foerster, LLP v. Wick, 94 F. Supp. 2d 1125, 1132-33 (D. Colo. 2000)).
- The domain name points to a website that pays the defendant commissions if the user purchases goods or services on the website (New York-New York Hotel & Casino, LLC v. Katzin, 2010 WL 4386497, at *3 (D. Nev. Oct. 27, 2010)).
- The domain name points to a website providing goods or services that are competitive with the plaintiff's goods or services (see, for example, Venetian Casino Resort, LLC v. Venetiangold.com, 380 F. Supp. 2d 737, 744-45 (E.D. Va. 2005)).

At least one court has held that the mere diversion of internet users due to the similarity of the domain name and the plaintiff's mark, without a showing of the defendant's intent, does not favor a bad faith finding (see Curated Works Inc. v. Deal.com, Inc., 2020 WL 2375079, at *6 (S.D.N.Y. May 12, 2020) (dismissing claim because mere allegations that consumers have been diverted from plaintiff's website are insufficient without allegations showing defendant's intent to divert consumers)).

##### Offer to Sell Domain Name
The sixth ACPA bad faith factor considers whether the defendant:

- Attempted to profit from selling the domain name without having legitimately used it.
- Engaged in a pattern of such conduct with other domain names.

15 U.S.C. § 1125(d)(1)(B)(i)(VI).

This factor typically favors the plaintiff and results in a bad faith finding where the defendant makes an actual offer to sell the domain name for a high price (see, for example, Lahoti, 586 F.3d at 1203; see also Southern Grouts, 575 F.3d at 1246 (noting that this is the principal wrong the AMCA was intended to address)). However, evidence of intent to sell the domain name for any financial benefit may weigh in favor of a bad faith finding, even where the defendant did not actually propose a sale. For example, courts have found that this factor weighs in favor of a bad faith finding based on a defendant's:

- Refusal to turn over the domain name unless the trademark owner pays a debt (see, for example, Jysk, 810 F.3d at 780; DSPT, 624 F.3d at 1221).
    Attempts to sell other domain names that include third-party trademarks (E. & J. Gallo Winery v. Spider Webs Ltd., 286 F.3d 270, 276-77 (5th Cir. 2002)).
- Statement, without proposing a sale or price, that the domain name is available for a limited time because the defendant is receiving offers from plaintiff's competitors (Catalanotte, 342 F.3d at 548-49).

**It is not inherently unlawful to sell domain names. Therefore, not all offers to sell a domain name indicate bad faith.** Virtual Works, Inc. v. Volkswagen of Am., Inc., 238 F.3d 264, 270 (4th Cir. 2001) (recognizing that the ACPA was not intended to preclude sale of domain names). For example, in a case where the defendant made legitimate use of the epix.com domain name and its counsel offered to sell the domain name to the plaintiff to settle the parties' trademark infringement and cybersquatting dispute, the Ninth Circuit held that this does not support a bad faith finding (Interstellar, 304 F.3d at 947).

The legislative history of the ACPA indicates it may not evidence a bad faith intent to profit from an offer to sell a domain name where the domain name owner:

- Registered the domain name in anticipation of a legitimate business venture that did not pan out.
- Is a concurrent user of the asserted trademark (for example, in connection with different goods or services) and it offers to sell the domain name to the other trademark owner.

H.R. Rep. 106-464, at *111-12.

Even where the domain name owner claims a legitimate reason for registering a domain name, courts consider all relevant circumstances to assess whether a later offer to sell the domain name indicates bad faith. For example, in Virtual Works, the plaintiff sought a declaratory judgment that the registration and use of the domain name vw.net was not in bad faith as "VW" were its company initials. After noting that the plaintiff's offer to sell the domain name to Volkswagen was alone insufficient to establish bad faith, the court found a bad faith intent to profit because:

    Plaintiff never did business as VW or identified itself by its initials.
    Plaintiff's officers conceded that, when they registered the domain name, they discussed the possibility of one day selling it to Volkswagen for a profit.
    Plaintiff demanded that Volkswagen accept its terms of purchase within 24 hours or it would sell the domain name to the highest bidder.

(238 F.3d at 269-70.)

##### Misleading Contact Information
Under the seventh ACPA bad faith factor, the domain name owner's provision of false or misleading contact information to the domain name registrar, or a pattern of this conduct with other domain names, weighs in favor of a bad faith finding (15 U.S.C. § 1125(d)(1)(B)(i)(VII)). It also creates a rebuttable presumption that the violation is willful and may support a plaintiff's request for statutory damages (15 U.S.C. § 1117(e); see Statutory Damages).

Early cybersquatters frequently used false contact information to make enforcement more difficult. Most registrars now offer privacy services to preserve registrant anonymity and domain name registrars must limit the registrant information that is publicly available under the GDPR (see Identifying Domain Name Owners). Domain name owners therefore may be less likely to provide false contact information. Courts have held that use of a privacy service is not the same as providing false or misleading contact information (for example, ZP No. 314, 335 F. Supp. 3d at 1262; Career Agents Network, Inc. v. careeragentsnetwork.biz, 2010 WL 743053, at *5 (E.D. Mich. Feb. 26, 2010)).

Notwithstanding these privacy protections, occasionally a plaintiff may encounter a defendant providing false information to a registrar (see, for example, Hakkasan LV, LLC v. Adamczyk, 2015 WL 4997306, at *4 (D. Nev. Aug. 19, 2015) (finding this factor weighs in favor of bad faith finding where registrant changed contact information after receiving a demand letter)).

##### Registration of Multiple Domain Names Similar to Third-Party Marks
The eighth ACPA bad faith factor considers whether the defendant has registered multiple domain names that are confusingly similar or dilutive of third-party trademarks (15 U.S.C. § 1125(d)(1)(B)(i)(VIII)). Evidence that a defendant has engaged in this practice tends to support a finding of bad faith (see, for example, DaimlerChrysler, 388 F.3d at 203-04, 207; N. Lights Tech., Inc. v. N. Lights Club, 236 F.3d 57, 65 (1st Cir. 2001) (rejecting as pretextual defendant's claim that it was building fan club websites)).

This factor may be useful in establishing bad faith against defendants that take no affirmative steps to sell their domain names but instead wait for offers from trademark owners (see, for example, Pinehurst, Inc. v. Wick, 256 F. Supp. 2d 424, 430 (M.D.N.C. 2003)). Courts often find this factor weighs in favor of bad faith in cases involving defendants that register and use numerous domain names for pay-per-click websites (see, for example, Direct Niche, LLC v. Via Varejo S/A, 2017 WL 5952896, at *9 (S.D. Fla. Aug. 10, 2017)).

It is not an indication of bad faith for a trademark owner to register numerous domain names that are variations of its own trademark (Harrod's, 302 F.3d at 239; Omega, 228 F. Supp. 2d at 136).

Counsel can run reverse WHOIS searches to find out what other domain names a defendant registered. However, due to the GDPR and registrar privacy protection services, these searches are not as complete and reliable as they once were (see Identifying Domain Name Owners). In discovery, counsel should seek a list of all domain names registered by the defendant and any of its affiliated entities.

A defendant's prior ACPA and UDRP violations may be especially persuasive evidence of bad faith (see Lahoti, 586 F.3d at 1202 (holding that defendant's status as a repeat cybersquatter, based in part on two prior UDRP violations and one prior ACPA violation, supported a bad faith finding)). A plaintiff's counsel therefore should search the federal court and UDRP dispute-resolution service provider databases for any prior violations. Counsel should at least search the following databases for the defendant's prior cybersquatting violations:

    PACER or an online legal research database, such as Westlaw.
    World Intellectual Property Organization's UDRP decision database.
    Forum's UDRP decision database.

##### The Strength of the Plaintiff's Mark
The ninth ACPA bad faith factor considers the strength of the plaintiff's mark. The more distinctive and well-known the plaintiff's mark, the more likely it is that the defendant was targeting the plaintiff's mark and, thus, the defendant's registration of a similar domain name weighs in favor of a bad faith finding. (15 U.S.C. § 1125(d)(1)(B)(i)(IX); see DaimlerChrysler, 388 F.3d at 207 (holding that DODGE mark is highly distinctive and famous and weighs in favor of bad faith finding)

#### Unique Circumstances of Case
The ACPA bad faith factors are permissive, non-exclusive considerations (15 U.S.C. § 1125(d)(1)(B)). Courts often acknowledge that facts supporting bad faith may not fit neatly within the statutory factors and that the unique circumstances of a case may be more important when assessing bad faith (see, for example, Sporty's Farm, 202 F.3d at 499; Digby Adler Grp. LLC v. Image Rent a Car, Inc., 79 F. Supp. 3d 1095, 1103 (N.D. Cal. 2015)). In Sporty's Farm, the Second Circuit analyzed all nine factors but was most moved by the unique circumstances of the case, which included evidence that the domain name owner's parent corporation:

    Knew of the strength of plaintiff's SPORTY'S trademark for aviation goods and services at the time of registration.
    Was planning to go into direct competition with the trademark owner in these markets.
    After the lawsuit was filed, created another company in an unrelated business under the name Sporty's Farm to attempt to justify use of the domain name.

Based on these facts, the court determined that the owner registered the domain name with the requisite bad faith intent to profit from it (Sporty's Farm, 202 F.3d at 499).

Facts not directly addressed by the ACPA factors that courts have held may support a bad faith intent to profit include:

    A prior dispute or rivalry between the parties (see, for example, Omega, 228 F. Supp. 2d at 137).
    The defendant's registration of a domain name it knew to be a famous trademark, its statement that it was waiting to be contacted by the defendant so it could "assist" the plaintiff, and its delay using the domain name until after the plaintiff initiated legal action (E. & J. Gallo, 286 F.3d at 276-77).
    The defendant's purchase of Google AdWords containing the plaintiff's mark so that defendant's website at the challenged domain name appeared in the search results when searching for plaintiff (Digby, 79 F. Supp. 3d at 1103-04).

#### Fair Use Safe Harbor
The ACPA states that it is not bad faith if the domain name owner reasonably believed that its use of the domain name was a fair use or otherwise lawful (15 U.S.C. § 1125(d)(1)(B)(ii)). Courts recognize that the fair use safe harbor is to be used sparingly and only in unusual cases (Lahoti, 586 F.3d at 1203; Audi AG v. D'Amato, 469 F.3d 534, 549 (6th Cir. 2006)). Finding that the defendant acted even partially in bad faith is generally grounds for rejecting the safe harbor ( Virtual Works, 238 F.3d at 270 (finding that although the vw.net domain name consisted of domain name owner's initials, its threat to auction the domain name to the highest bidder if Volkswagen did not purchase it constituted bad faith)).

The relevant considerations for assessing application of the safe harbor are effectively the same as those under the fourth ACPA bad faith factor (see Defendant's Fair Use of Domain Name (Factor 4)). Courts that have applied the safe harbor usually find that the fourth factor weighs against a bad faith finding because the defendant is making a use protected by the First Amendment (see for example, Utah, 527 F.3d at 1059 (holding that the fourth factor and the safe harbor both weigh against finding that defendant's use of domain name for parody and criticism website constituted bad faith); Mayflower, 314 F. Supp. 2d at 369 (holding that the safe harbor and the fourth ACPA bad faith factor should be considered together and finding that both weigh against bad faith given defendant's use of the domain name for a website criticizing plaintiff)).

However, unlike the fourth factor, the safe harbor assessment looks at the defendant's subjective belief regarding the lawful nature of its conduct and the objective reasonableness of that belief (see Audi, 469 F.3d at 549-50). At least one court has upheld a jury's verdict that, despite the lack of any First Amendment justification, the defendant had reasonable grounds to believe its conduct was lawful (Pensacola Motor Sales Inc. v. E. Shore Toyota, LLC, 684 F.3d 1211, 1229 (11th Cir. 2012) (affirming denial of liability under the safe harbor where auto dealership relied on marketing expert's assurance that registering domain names was lawful and dealership promptly terminated expert and surrendered domain names upon learning otherwise)).

Although defendants often argue that their conduct falls within the safe harbor, courts usually reject the defense. For example, courts have rejected the safe harbor in cases where the defendant:

    Claimed to use domain names containing the TRUMP mark for websites with information and commentary about Donald Trump, but the websites were undeveloped and featured primarily third-party content (Web-adviso, 927 F. Supp. 2d at 46).
    Posted critical content at the domain name only after receiving notice of the plaintiff's claims (Shields, 254 F.3d at 486; see also Coca-Cola, 382 F.3d at 788 (rejecting fair use safe harbor where defendant continued using the domain name for anti-abortion website after receiving demand letters from the plaintiff and the district court issued a preliminary injunction)).
    Claimed its "People Eating Tasty Animals" website parodied PETA, but the site contained links to third-party meat, fur, hunting, and leather websites, defendant used false registrant information, and evidence showed defendant clearly intended to confuse and misdirect internet users (People, 263 F.3d at 369).
    Had prevailed in a UDRP proceeding concerning a similar domain name it registered before the plaintiff established trademark rights but registered the challenged domain names years later after it knew plaintiff obtained trademark rights (GoPets, 657 F.3d at 1033).

#### Intent to Profit
Bad faith without an intent to profit is insufficient to support an ACPA cybersquatting claim (Southern Grouts, 575 F.3d at 1246). An intent to profit is not limited to a domain name owner's intent to sell the domain name or to profit from the plaintiff's goodwill in its mark. The defendant's conduct must merely be to obtain a benefit. For example, courts have found an intent to profit where the defendant:

    Was a former employee who registered a domain name for his employer and kept the domain name when he left the company to use as leverage in a financial dispute (DSPT, 624 F.3d at 1219-20).
    Threatened to post certain documents on a website at the domain name if the plaintiff law firm did not drop its lawsuit to recover legal fees owed by the defendant or substantially reduce the amounts owed (McAllister, 298 F. Supp. 3d at 675).
    Stated publicly that the trademark owner should make a settlement offer for the domain name (People, 263 F.3d at 368).

## Challenges to UDRP Decisions (Reverse Domain Name Hijacking)
If a complainant prevails in a UDRP proceeding and the adjudicating panel orders cancellation or transfer of the domain name, the domain name owner may challenge the order by initiating a civil action to establish that its use or registration of the domain name was lawful under the ACPA. If the domain name owner prevails in the civil action, the court may grant it injunctive relief, including reactivation or transfer back of the domain name. 15 U.S.C. § 1114(2)(D)(v); UDRP ¶ 4(k).

Courts often refer to these as reverse domain name hijacking claims. However, this implies the trademark owner's wrongdoing, which is not an element of the claim (see Bad Faith UDRP Claims). To prevail on a civil action to recover a domain name lost in a UDRP proceeding, the former domain name registrant only needs to show:

    It registered a domain name.
    The domain name was suspended, disabled, or tranferred under the UDRP (or a comparable policy).
    The domain name registration or use was lawful under the Lanham Act.
    The trademark owner that initiated the UDRP proceeding has notice of the civil action.

15 U.S.C. § 1114(2)(D)(v); Barcelona.com, Inc. v. Excelentisimo Ayuntamiento De Barcelona, 330 F.3d 617, 626 (4th Cir. 2003); Am. Diabetes Ass'n v. Friskney Family Trust, LLC 177 F. Supp. 3d 855, 874 (E.D. Pa. 2016).

A civil action challenging the results of a UDRP proceeding is not an appeal or review of the UDRP decision. Courts give no deference to the panel's decision and must independently analyze whether the domain name registration or use violated the ACPA without regard to the UDRP findings ( Storey v. Cello Holdings, L.L.C., 347 F.3d 370, 374 (2d Cir. 2003); Barcelona.com, 330 F.3d at 623).

A party to a UDRP proceeding may initiate an ACPA civil action at any time, including:

- While the UDRP proceeding is pending, which typically results in suspension of the UDRP proceeding until completion of the civil action (see BroadBridge Media, L.L.C. v. Hypercd.com, 106 F. Supp. 2d 505, 509 (S.D.N.Y. 2000)).
- After the UDRP proceeding is decided. If a domain name owner initiates a civil action within ten business days of a UDRP decision and notifies the applicable registrar, the registrar withholds transfer pending the result of the civil action (UDRP ¶ 4(k)). Although the ACPA only authorizes a civil action where the registrar has suspended, disabled, or transferred the domain name, courts grant relief where a registrar is withholding transfer pending resolution of an ACPA claim (see, for example, Barcelona.com, 330 F.3d at 626-27; Baklan v. All Answers Ltd., 2020 WL 6063254, at *2-3 (D. Ariz. Oct. 14, 2020)).



-----
The most harmful decision to the UDRP came in Sallen v. Corinthians
Licenciamentos LTDA, 4 where the U.S. Court of Appeals for the First Circuit
found that decisions under the Anticybersquatting Consumer Protection Act
(ACPA) effectively "trump" rulings made through ICANN's UDRP
arbitration procedure. This ruling undermines the function and purpose of
ICANN's UDRP, and removes any pressure to reform the current
procedures, thus decreasing the UDRP's efficiency and potency. In addition,
by not giving deference to UDRP proceedings, federal courts may lose a
valuable resource and turn this dispute resolution process into a burden for
domain name registrants.
-----