---
title | Copyright Overview
author | casey@mclaw.io
date | 2021-11-14
category | Intellectual Property and Technology
subcategory | Copyright
tags | copyright, infringement, dmca
summary | This articles provides an overview of a copyright law.
---
Copyright is a form of protection provided by U.S. law to authors of  "original works of authorship" from the time the works are created in a fixed form. This circular provides an overview of basic facts about copyright and copyright registration with the U.S. Copyright Office. It covers the following:
- Works eligible for protection
- Rights of copyright owners
- Who can claim copyright
- Duration of copyright

Copyright is a form of protection provided by the laws of the United States to the authors of "original works of authorship" that are fixed in a tangible form of expression. An original work of authorship is a work that is independently created by a human author and possesses at least some minimal degree of creativity. A work is "fixed" when it is captured (either by or under the authority of an author) in a sufficiently permanent medium such that the work can be perceived, reproduced, or communicated for more than a short time.

Copyright protection in the United States exists automatically from the moment the original work of authorship is fixed.  

## What Works Are Protected?  
Examples of copyrightable works include the following:  
- Literary works
- Musical works, including any accompanying words
- Dramatic works, including any accompanying music
- Pantomimes and choreographic works
- Pictorial, graphic, and sculptural works
- Motion pictures and other audiovisual works
- Sound recordings, which are works that result from the fixation of a series of musical, spoken, or other sounds
- Architectural works

These categories should be viewed broadly for the purpose of registering your work. For example, computer programs and certain "compilations" can be registered as "literary works"; maps and technical drawings can be registered as "pictorial, graphic, and sculptural works."

Note: Before 1978, federal copyright was generally secured by publishing a work with an appropriate copyright notice. U.S. works[2] that were in the public domain on January 1, 1978, when the 1976 Copyright Act took effect, remain in the public domain under the 1976 Act.

## What Are the Rights of a Copyright Owner?
Copyright provides the owner of copyright with the exclusive right to
- Reproduce the work in copies or phonorecords3
- Prepare derivative works based upon the work
- Distribute copies or phonorecords of the work to the public by sale or other transfer of ownership or by rental, lease, or lending
- Perform the work publicly if it is a literary, musical, dramatic, or choreographic work; a pantomime; or a motion picture or other audiovisual work
- Display the work publicly if it is a literary, musical, dramatic, or choreographic work; a pantomime; or a pictorial, graphic, or sculptural work. This right also applies to the individual
images of a motion picture or other audiovisual work.
- Perform the work publicly by means of a digital audio transmission if the work is a sound recording.
- 
Copyright also provides the owner of copyright the right to authorize others to exercise these exclusive rights, subject to certain statutory limitations.

## What Is Not Protected by Copyright?
Copyright does not protect the following
- Ideas, procedures, methods, systems, processes, concepts, principles, or discoveries
- Works that are not fixed in a tangible form (such as a choreographic work that has not been
notated or recorded or an improvisational speech that has not been written down)
- Titles, names, short phrases, and slogans
- Familiar symbols or designs
- Mere variations of typographic ornamentation, lettering, or coloring
- Mere listings of ingredients or contents
- 
## Who Can Claim Copyright?
The copyright in a work initially belongs to the author(s) who created that work. When two or more authors create a single work with the intent of merging their contributions into inseparable or interdependent parts of a unitary whole, the authors are considered joint authors and have an indivisible interest in the work as a whole. By contrast, if multiple authors contribute to a collective work, each author’s individual contribution is separate and distinct from the copyright ownership in the collective work as a whole. 

"Works made for hire" are an important exception to the general rule for claiming copyright. When a work is made for hire, the author is not the individual who actually created the work. Instead, the party that hired the individual is considered the author and the copyright owner of the work. 
Whether a work is made for hire is determined by the facts that exist at the time the work is created. There are two situations in which a work may be made for hire:
- 1. When the work is created by an employee as part of the employee’s regular duties, or
- 2. When an individual and the hiring party enter into an express written agreement that the work is to be considered a "work made for hire" and the work is specially ordered or commissioned for use as:
- A compilation
- A contribution to a collective work
- A part of a motion picture or other audiovisual work
- A translation
- A supplementary work
- An instructional text
- A test
- Answer material for a test
- An atlas
- 
The concept of work made for hire can be complicated and has serious consequences for both the individual who creates the work and the hiring party who is considered to be the author and copyright owner of the work.

Note: Mere ownership of a copy or phonorecord that embodies a work does not give the owner of that copy or phonorecord the ownership of the copyright in the work.

### Transfer of Copyright Ownership
Any or all of the copyright owner’s exclusive rights, or parts of those rights, can be transferred. The transfer, however, generally must be made in writing and signed by the owner of the rights conveyed or the owner’s authorized agent. Transferring a right on a nonexclusive basis does not require a written agreement. You can bequeath a copyright by will or pass it along as personal property under applicable state laws of intestate succession. It can also be conveyed by operation of law. You can "record" a transfer of copyright ownership with the Copyright Office through its Office of Public Records and Repositories. Although recordation is not required to make a valid transfer between parties, it does provide certain legal advantages.

### Termination of a Copyright Transfer
Under certain circumstances, the Copyright Act allows authors or their heirs to terminate an agreement that transferred or licensed the author’s copyright to a third party after thirty-five years. To terminate a grant, the author or the author’s heirs must serve an advance written "notice of termination" on the grantee or the grantee’s successor-in-interest and must record a copy of that notice with the Copyright Office and pay the required filing fee.

A notice of termination must be recorded before the effective date of termination specified in the notice. If a notice of termination is not recorded in a timely manner, the notice will be invalid, and the author or the author’s heirs will not be able to terminate the agreement. For more information, see chapter 2300, section 2310 of the Compendium of U.S. Copyright Office Practices.

## How Long Does Copyright Last?
In general, for works created on or after January 1, 1978, the term of copyright is the life of the author plus seventy years after the author’s death. If the work is a joint work with multiple authors, the term lasts for seventy years after the last surviving author’s death. For works made for hire and anonymous or pseudonymous works, the duration of copyright is 95 years from publication or 120 years from creation, whichever is shorter.

For works created before January 1, 1978, that were not published or registered as of that date, the term of copyright is generally the same as for works created on or after January 1, 1978. The law, however, provides that in no case would the term have expired before December 31, 2002, and if the work was published on or before that date, the term will not expire before December 31, 2047.

For works created before January 1, 1978, that were published or registered before that date, the initial term of copyright was twenty-eight years from the date of publication with notice or from the date of registration. At the end of the initial term, the copyright could be renewed for another sixtyseven years for a total term of protection of up to ninety-five years. To extend copyright into the renewal term, two registrations had to be made before the original term expired: one for the original term and the other for the renewal term. This requirement was eliminated on June 26, 1992, and renewal term registration is now optional. For more information on the term of copyright protection, see Duration of Copyright (Circular 6) and Renewal of Copyright (Circular 6A).

## How Can I Protect My Work?
Copyright exists automatically in an original work of authorship once it is fixed in a tangible medium, but a copyright owner can take steps to enhance the protections of copyright, the most important of which is registering the work. Although registering a work is not mandatory, for U.S. works, registration (or refusal) is necessary to enforce the exclusive rights of copyright through litigation.

Applying a copyright notice to a work has not been required since March 1, 1989, but may still provide practical and legal benefits. Notice typically consists of the copyright symbol or the word "Copyright," the name of the copyright owner, and the year of first publication. Placing a copyright notice on a work is not a substitute for registration.

### Benefits of Registration
Registration establishes a claim to copyright with the Copyright Office. An application for copyright registration can be filed by the author or owner of an exclusive right in a work, the owner of all exclusive rights, or an agent on behalf of an author or owner. An application contains three essential elements: a completed application form, a nonrefundable filing fee, and a nonreturnable deposit—that is, a copy or copies of the work being registered and "deposited" with the Copyright Office.

A certificate of registration creates a public record of key facts relating to the authorship and ownership of the claimed work, including the title of the work, the author of the work, the name and address of the claimant or copyright owner, the year of creation, and information about whether the work is published, has been previously registered, or includes preexisting material. You can submit an application online through www.copyright.gov or on a paper application. 

In addition to establishing a public record of a copyright claim, registration offers several other statutory advantages:
- Before an infringement suit may be filed in court, registration (or refusal) is necessary for
U.S. works.2
- Registration establishes prima facie evidence of the validity of the copyright and facts stated in
the certificate when registration is made before or within five years of publication.
- When registration is made prior to infringement or within three months after publication of a
work, a copyright owner is eligible for statutory damages, attorneys’ fees, and costs.
- Registration permits a copyright owner to establish a record with the U.S. Customs and Border Protection (CBP)[4] for protection against the importation of infringing copies.
- Registration can be made at any time within the life of the copyright. If you register before publication, you do not have to re-register when the work is published, although you can register the published edition, if desired.
- 
### Effective Date of Registration
When the Copyright Office registers a work it assigns an effective date of registration to the certificate of registration. The effective date of registration is the day that the Office receives in proper form all required elements—an acceptable application, an acceptable deposit, and a nonrefundable filing fee. The date is not set until all the required elements are in the Office’s possession. If the Office receives incomplete materials, an unacceptable deposit, or an insufficient fee, the effective date of registration will be set on the date that the Office receives all the required materials in acceptable form. The date is not based on how long it takes the Office to examine the materials or mail the certificate of registration.

You do not have to receive your certificate of registration before you publish or produce your work. Nor do you need permission from the Copyright Office to place a copyright notice on your work. But the Copyright Office must approve or refuse your application before you can file a lawsuit for copyright infringement, except in cases involving a non-U.S. work.

You may seek statutory damages and attorneys’ fees in an infringement action provided that the infringement began after the effective date of registration. The law, however, provides a grace period of three months after publication during which full remedies can be recovered for any infringement begun during the three months after publication if registration is made before this period ends.

### Copyright Notice
A copyright notice is a statement placed on copies or phonorecords of a work to inform the public that
a copyright owner is claiming ownership of the work. A copyright notice consists of three elements:
- The copyright symbol © or (p) for phonorecords, the word "Copyright," or the abbreviation 
"Copr.";
- The year of first publication of the work (or of creation if the work is unpublished); and
- The name of the copyright owner, an abbreviation by which the name can be recognized, or a generally known alternative designation.
- 
A notice should be affixed to copies or phonorecords of a work in a way that gives reasonable
notice of the claim of copyright.
Using a copyright notice is optional for unpublished works, non-U.S. works, and works published on or after March 1, 1989. However, notice conveys the following benefits:
- It puts potential users on notice that copyright is claimed in the work.
- For published works, notice may prevent a defendant from attempting to limit liability for
damages or injunctive relief based on an "innocent infringement" defense.
- It identifies the copyright owner at the time of first publication for parties seeking permission
to use the work.
- It identifies the year of first publication, which can be used to determine the term of copyright
for anonymous or pseudonymous works or works made for hire.
- It may prevent the work from becoming an "orphan" by identifying the copyright owner or specifying the term of copyright. Orphan works are original works of authorship for which prospective users cannot identify or locate copyright owners to request permission.

Notice was required for works published in the United States before March 1, 1989. Works published without notice before that date may have entered the public domain in this country.

## How Can I Use a Copyrighted Work?
Sections 107 to 122 of the copyright law contain provisions that establish limitations on the exclusive rights of the copyright owner. The provisions make certain uses of copyrighted works permissible without first obtaining permission of the copyright owner. One of the most discussed of these statutory provisions is known as fair use, a legal doctrine that promotes freedom of expression by permitting the unlicensed use of copyright-protected works in certain circumstances. For more information on fair use, see the Office’s Fair Use Index on its website.

## What Is Publication and Why Is It Important?
Under copyright law, publication is the distribution of copies or phonorecords of a work to the public by sale or other transfer of ownership or by rental, lease, or lending. Offering to distribute copies or phonorecords to a group of people for purposes of further distribution, public performance, or public display also constitutes publication.

Whether a work is published has important implications, including:
- The year of publication may determine the length of the copyright term for a work made for
hire or an anonymous or pseudonymous work.
- The year of publication may determine the length of the copyright term if the work was created before January 1, 1978, and was published or registered before that date.
- The year of publication may determine the length of the copyright term if the work was created before January 1, 1978, and was first published between January 1, 1978, and December 31, 2002.
- The date and nation of first publication may determine if a non-U.S. work is eligible for copyright protection in the United States.
- A certificate of registration creates certain legal presumptions if the work is registered before or within five years after the work was first published.
- A copyright owner may be entitled to claim statutory damages and attorneys’ fees in an infringement lawsuit if the work was registered before the infringement began or within three months after the first publication of that work.
- Many of the exceptions and limitations on the copyright owner’s exclusive rights vary depending on whether the work is published or unpublished.
- As a general rule, works published before March 1, 1989, must be published with a valid copyright notice.
- The deposit requirements for registering a published work differ from the requirements for registering an unpublished work.
- Works published in the United States may be subject to mandatory deposit with the Library of Congress. For more information, see "What Is Mandatory Deposit?" below.
- When you register your work with the Office, you must determine whether the work is published or unpublished. For further information regarding publication, see chapter 1900 of Compendium of U.S. Copyright Office Practices.

## How Do I Protect My Work in Other Countries?
There is no such thing as an "international copyright" that automatically protects an author’s works throughout the entire world. Protection against unauthorized use in a particular country depends on the national laws of that country. Most countries offer protection to non-U.S. works under certain conditions, and these conditions have been greatly simplified by international copyright treaties and conventions. Generally, a U.S. work may be protected in another country if that country has entered into an international agreement with the United States.

## What Is Mandatory Deposit?
All copyrighted works that are published in the United States are subject to the "mandatory deposit" provision of the copyright law. As a general rule, this provision requires that two complete copies of the "best edition" of a copyrightable work published in the United States be sent to the Copyright Office for the collections of the Library of Congress within three months of publication. The "best edition" of a work is "the edition, published in the United States at any time before the date of deposit, that the Library of Congress determines to be most suitable for its purposes." The owner of copyright or of the exclusive right of publication may comply with this requirement either by submitting the best edition of the work when registering the work with the Office or by submitting the work without seeking a registration and solely for the purpose of fulfilling the mandatory deposit requirement. The mandatory deposit provision helps ensure that the Library of Congress obtains copies of every copyrightable work published in the United States for its collections or for exchange with or transfer to any other library.  

#### Footnotes
1. The authoritative source for U.S. copyright law is the Copyright Act, codified in Title 17 of the United States Code. Copyright Office regulations are codified in Title 37 of the Code of Federal Regulations. Copyright Office practices and procedures are summarized in the third edition of the Compendium of U.S. Copyright Office Practices, cited as the Compendium. The copyright law, regulations, and the Compendium are available on the Copyright Office website at www.copyright.gov.
2. The Copyright Act defines a "United States work" for the purposes of registration and civil infringement actions as (a) a published work that is first published in the United States; first published simultaneously in the United States and another treaty party or parties, whose law grants a term of copyright protection that is the same as or longer than the term provided in the United States; first published simultaneously in the United States and a foreign nation that is not a treaty party; or first published in a foreign nation that is not a treaty party, and all of the authors of the work are nationals, domiciliaries, or habitual residents of, or in the case of an audiovisual work legal entities with headquarters in, the United States; (b) an unpublished work where all the authors of the work are nationals, domiciliaries, or habitual residents of the United States, or in the case of an unpublished audiovisual work, all the authors are legal entities with headquarters in the United States; or (c) a pictorial, graphic, or sculptural work incorporated in a building or structure that is located in the United States.
3. A phonorecord is a material object in which sounds, other than those accompanying a motion picture or other audiovisual work, are fixed and from which the sounds can be perceived, reproduced, or otherwise communicated either directly or with a machine. The technology for creating and using a phonorecord includes those now known or later developed.
4. CBP began accepting online applications for recordation of unregistered copyrights through the Intellectual Property Rights Electronic Recordation System (IPRR). Each unregistered copyright recordation will be valid for a period of nine months, with a potential one-time ninety-day extension of time, while an application to register that copyright is pending with the Copyright Office. Upon registration, the copyright recordation will continue to receive the benefits of border enforcement from CBP.
5. A "non-U.S. work" is any work that is not a United States work, as defined above.