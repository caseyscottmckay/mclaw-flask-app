---
title| Trademark Overview--Infringement and Related Rights Under Trademark Law
author| casey@mclaw.io
date| 2021-12-21
category | Intellectual Property and Technology
subcategory | Trademark
tags | trademark, infringement
summary| This overview of US trademark law gives information about the nature of trademarks, the reasons why they can be commercially significant, and the type of material that is likely to be suitable for registration and use as a trademark. Topics covered include trademark enforcement and protection of trademark rights.
---
### Enforcing Trademark Rights

You are responsible for enforcing your rights if you receive a registration, because the USPTO does not "police" the use of marks. While the USPTO attempts to ensure that no other party receives a federal registration for an identical or similar mark for or as applied to related goods/services, the owner of a registration is responsible for bringing any legal action to stop a party from using an infringing mark.  If a right holder suspects that a registered mark is being infringed upon or may be subject to future infringement, the registered mark can be recorded with U.S. Customs and Border Protection through its e-Recordation application.

To maintain their rights, trademark owners must be diligent in protecting their marks by taking appropriate enforcement action against infringement and other misuse by third parties.

#### How can I challenge a trademark registration or application?
Several options exist to challenge another party's trademark registration or application, depending on the particular circumstances and grounds for challenging. In the US, the principal means of trademark enforcement are:

- Cease and desist letters (see Cease and Desist Letters).
- Litigation asserting infringement and related claims (see Infringement Litigation).
    - You may file a declaratory judgment lawsuit, asking a court to declare that your mark does not infringe the trademark owner's mark, and/or that the trademark owner's mark is invalid.
    - If you are already a defendant in an infringement lawsuit, you may assert an "affirmative defense" and/or a counterclaim against the plaintiff challenging the validity of the plaintiff's trademark.
- Opposition and cancellation proceedings in the Trademark Trial and Appeal Board (TTAB) (see Opposition and Cancellation Proceedings).
    - You may challenge a trademark registration issued by the USPTO by filing a petition to cancel the registration with the [Trademark Trial & Appeal Board (TTAB)](https://www.uspto.gov/about-us/organizational-offices/trademark-trial-and-appeal-board).
    - You may challenge an application for trademark registration at the USPTO by filing an opposition with the [TTAB](https://www.uspto.gov/trademarks-application-process/trademark-trial-and-appeal-board-ttab) within 30 days after it is published in the Official Gazette.

Again, a U.S.-licensed attorney with trademark expertise can help advise you about the various options available to challenge trademark registrations and applications.

#### Cease and Desist Letters
Trademark owners faced with infringement or misuse of their marks by third parties typically send cease and desist letters to attempt to resolve the matter without the need for formal legal action. For more information on trademark cease and desist letters see Practical Guide, Drafting an Effective Trademark Cease and Desist Letter. For a model trademark infringement cease and desist letter, see Standard Document, Trademark Infringement Cease and Desist Letter.

#### Infringement Litigation
At the federal level, the Lanham Act provides causes of action for infringement of:

- Federally registered trademarks, under Section 32 (15 U.S.C. § 1114).
- Marks that are not federally registered, under Section 43(a) (false designation of origin) (15 U.S.C. § 1125(a)(1)(A)).

In infringement litigation, owners of federally registered marks typically assert false designation of origin (unfair competition) claims under Section 43(a), along with infringement claims under Section 32. Depending on the ground for cancellation, a Section 43(a) claim may survive a cancellation of the owner's registration in the litigation because the 43(a) claim does not depend on the existence of the registration.

Federal and state courts have concurrent subject matter jurisdiction over Lanham Act claims. Most suits are brought in federal court, however, because federal judges have greater familiarity with the Lanham Act. For more information on infringement claims under the Lanham Act, see Practical Guides, Lanham Act Section 43(a) Claims and Trademark Infringement and Dilution Claims, Remedies, and Defenses.

##### Likelihood of Confusion
Likelihood of confusion is the test for trademark infringement. Likelihood of confusion refers to confusion in the marketplace over source, sponsorship, or affiliation resulting from the simultaneous use by different parties of identical or similar marks for their respective goods or services. Likelihood of confusion is analyzed according to a multi-factor test. The specific tests vary somewhat from circuit to circuit, but common factors examined include:

- The strength of the plaintiff's mark.
- The similarity of the parties' marks.
- The relatedness of the parties' goods or services.
- The similarity of the parties' trade and marketing channels.
- The defendant's intent.
- The existence of actual confusion.

For more information on the likelihood of confusion analysis in infringement litigation under the Lanham Act, see Practical Guide, Trademark Litigation: Likelihood of Confusion.

##### Principal Remedies for Infringement
Principal remedies available in trademark infringement actions under the Lanham Act include:

- Preliminary and permanent injunctive relief.
- Actual damages.
- Profits of the infringer.
- Enhanced damages for willful infringement.
- Attorneys' fees in exceptional cases.

For more information, see Practical Guides, Trademark Litigation: Injunctive Relief and Trademark Litigation, Monetary Relief

##### Principal Defenses to Infringement Claims
Principal defenses to infringement claims include:

- Challenges to the ownership, validity, and enforceability of the plaintiff's mark, for example, challenges that the mark is generic, abandoned, or functional.
- Descriptive fair use.
- Nominative fair use.
- First Amendment protection, including parody.
- Equitable defenses, including laches.

For more information on these defenses, see:

    Practical Guide, Trademark Litigation: Fair Use and First Amendment Defenses.
    Practical Guide, Trademark Litigation: Laches and Other Equitable Defenses.
    Practical Guide, Genericness Challenges to Trademark Rights.
    Practical Guide, Trademark Abandonment from Discontinued Use.
    Practical Guide, Trademark Litigation: Functionality.
    Practical Guide, Trademark Infringement and Dilution Claims, Remedies, and Defenses: Principal Defenses to Trademark Infringement and Dilution Claims.

##### Additional Lanham Act Claims
Trademark owners may also protect their marks through claims under the Lanham Act for:

- **Unfair competition under Section 43(a)**. Court decisions have established that Section 43(a) of the Lanham Act provides a cause of action for infringement of trademarks not registered with the USPTO. Section 43(a) also provides a cause of action for other acts likely to cause consumer confusion or deception (15 U.S.C. § 1125(a)(1)(A)). For more information on Section 43(a) claims, see Practical Guide, Lanham Act Section 43(a) Claims.
- **Dilution**. Dilution is the lessening of the capacity of a famous mark to identify and distinguish goods or services. Dilution may be by:
    - **blurring**; and
    - **tarnishment**.
- **Cyberpiracy**. The Lanham Act provides a cause of action for trademark owners against cyberpirates who engage in bad faith registration or trafficking in domain names for profit (15 U.S.C. 1125(d)). For more information, see Practical Guide, Anticybersquatting Consumer Protection Act (ACPA) Claims.
- **Trademark counterfeiting**. Trademark counterfeiting is a form of infringement involving the use of a counterfeit of a registered mark. The Lanham Act provides a trademark owner with additional potential remedies for acts of trademark counterfeiting. For more information, see Practical Guide, Protecting Against Counterfeit Trademarks and Gray Market Goods.

##### State Claims
Trademark owners, subject to various requirements, may also protect their interests through claims under state statutes and common law, including claims for:

- Common law trademark infringement and unfair competition.
- Unfair competition under state statutes, for example, deceptive trademark practices acts.
- Dilution under state statutes and common law.
- Counterfeiting under state statutes.

For federally protected marks, state claims are typically asserted together with federal claims under the Lanham Act. For more information on state trademark claims, see Trademark Laws: State Q&A Tool.

##### Recording Trademarks with US Customs and Border Protection
To help combat counterfeiting and gray market activities, trademark owners may record their trademark registrations with US Customs and Border Protection (CBP). For more information on enforcement against counterfeiting, including recordation of marks with CBP, see Practical Guides, Protecting Against Counterfeit Trademarks and Gray Market Goods and Trademark Enforcement: Working with US Customs and Border Protection.

#### Opposition and Cancellation Proceedings

##### Opposition Proceedings
Trademark owners may, under certain circumstances, oppose registration of a trademark that is the subject of a federal trademark application for registration on the Principal Register. Principal grounds to oppose registration of the applied-for mark include that the mark is:

- Likely to cause consumer confusion about the source of goods or services.
- Likely to dilute the distinctiveness of an owner's famous mark.
- Generic for or merely descriptive of the goods or services.

Trademark opposition proceedings are conducted before the TTAB within the USPTO.

Most opposition proceedings settle before trial, often with the parties entering into an agreement for their marks to coexist in the marketplace subject to certain restrictions. For a sample trademark coexistence agreement, see Standard Document, Trademark Coexistence Agreement.
For more information on opposition proceedings, see Practical Guides, TTAB Oppositions and Cancellations: Grounds and Defenses and TTAB Oppositions and Cancellations: Practice and Procedure.
For a toolkit of resources for use in opposition and cancellation proceedings before the TTAB, see TTAB Proceedings Toolkit.

##### Cancellation Proceedings
Cancellation proceedings in the TTAB allow parties to cancel registrations after the USPTO has issued the registration. The grounds for cancellation are largely consistent with the grounds for opposition, but are limited once a registration has been in existence for five years.
For more information on cancellation proceedings, see Practical Guides, TTAB Oppositions and Cancellations: Grounds and Defenses and TTAB Oppositions and Cancellations: Practice and Procedure.


### Proper Trademark Use
To protect and keep their rights, trademark owners must both:

- Take appropriate measures to enforce their rights against third parties.
- Ensure that they use their marks properly to maintain the brand name significance of the marks.

If trademarks are not used properly, their strength and scope of protection may be eroded and, in a worst case scenario, the marks may become generic if their source-identifying significance is lost.
For more information on proper trademark use, see Practical Guide, Proving Trademark Use and Priority and Standard Document, Trademark Use and Protection Guidelines (Internal Distribution).

#### How do I know whether there is infringement?
To support a trademark infringement claim in court, a plaintiff must prove that it owns a valid mark, that it has priority (its rights in the mark(s) are "senior" to the defendant's), and that the defendant's mark is likely to cause confusion in the minds of consumers about the source or sponsorship of the goods or services offered under the parties' marks. When a plaintiff owns a federal trademark registration on the Principal Register, there is a legal presumption of the validity and ownership of the mark as well as of the exclusive right to use the mark nationwide on or in connection with the goods or services listed in the registration. These presumptions may be rebutted in the court proceedings.

Generally, the court will consider evidence addressing various factors to determine whether there is a likelihood of confusion among consumers. The key factors considered in most cases are the degree of similarity between the marks at issue and whether the parties' goods and/or services are sufficiently related that consumers are likely to assume (mistakenly) that they come from a common source. Other factors that courts typically consider include how and where the parties' goods or services are advertised, marketed, and sold; the purchasing conditions; the range of prospective purchasers of the goods or services; whether there is any evidence of actual confusion caused by the allegedly infringing mark; the defendant's intent in adopting its mark; and the strength of the plaintiff's mark.

The particular factors considered in a likelihood-of-confusion determination, as well as the weighing of those factors, vary from case to case. And the amount and quality of the evidence involved can have a significant impact on the outcome of an infringement lawsuit.

In addition to claiming likelihood of confusion, a trademark owner may claim trademark "dilution," asserting that it owns a famous mark and the use of your mark diminishes the strength or value of the trademark owner's mark by "blurring" the mark's distinctiveness or "tarnishing" the mark's image by connecting it to something distasteful or objectionable-even if there is no likelihood of confusion.

An experienced U.S.-licensed trademark attorney, taking the particular circumstances of your case into consideration, should be able to provide you with an opinion as to the validity and strength of a trademark owner's claims.

