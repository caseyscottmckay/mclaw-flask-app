---
title | Trademark Overview--Obtaining Protection and Licensing
author| casey@mclaw.io
date| 2021-12-21
category | Intellectual Property and Technology
subcategory | Trademark
tags | trademark, infringement
summary| This overview of US trademark law gives information about the nature of trademarks, the reasons why they can be commercially significant, and the type of material that is likely to be suitable for registration and use as a trademark. Topics covered include the acquisition and maintenance of trademark rights and registrations, exploiting trademarks, and loss of trademark rights.
---
#### Applying for a Trademark

##### Selecting a Mark
Once you determine that the type of protection you need is, in fact, trademark protection, then selecting a mark is the very first step in the overall application/registration process. This must be done with thought and care, because not every mark is registrable with the USPTO. Nor is every mark legally protectable, that is, some marks may not be capable of serving as the basis for a legal claim by the owner seeking to stop others from using a similar mark on related goods or services. Businesses and individuals new to trademarks and the application/registration process often choose a mark for their product or service that may be difficult or even impossible to register and/or protect for various reasons. Before filing a trademark/service mark application, you should consider:
- Whether the mark you want to register is registrable, and
- How difficult it will be to protect your mark based on the strength of the mark selected.

##### Mark Format
You must identify your mark format: a standard character mark, a stylized/design mark, or a sound mark.

##### Identification of Goods and/or Services
It is critical you identify clearly the precise goods and/or services to which the mark will apply.  For more information about properly identifying your goods and services, please watch the news broadcast-style video titled “Goods and Services” (video #6 in the TMIN series).

##### Searching
Always search the USPTO database to determine whether anyone is already claiming trademark rights in wording/design that is similar and used on related goods/services through a federal registration.  For more information about conducting a clearance search, please watch the news broadcast-style video titled “Searching” (video #3 in the TMIN series).


#### Do I have to use a Trademark Attorney?
Yes, if you are foreign-domiciled. You must be represented at the USPTO by an attorney who is licensed to practice law in the United States.
No, if you are domiciled in the United States or its territories. Although you are not required to have an attorney, we strongly encourage you to hire a U.S.-licensed attorney who specializes in trademark law to guide you through the application process.
Click for more information about hiring an attorney.

### Legal Framework for Trademark Protection in the US
Trademarks may be protected under both federal and state law.

#### Federal Law
At the federal level, the primary source of trademark protection is the Lanham Act (15 U.S.C. §§ 1051 to 1141n). The US Patent and Trademark Office (USPTO) is the federal agency that administers the Lanham Act and regulates registration of trademarks on the federal trademark register. UPSTO trademark regulations appear in Title 37 of the Code of Federal Regulations (37 C.F.R. §§ 2.2 to 7.41).

The USPTO website provides legal and regulatory information, and procedural guidance on the federal trademark application process. The Trademark Manual of Examining Procedure, published by the USPTO, provides detailed information concerning examination of a federal trademark application.

#### State Law
Trademarks are protected at the state level by:

- Common law unfair competition principles.
- State trademark statutes.


#### International Considerations
Trademark rights are territorial in nature. Generally, rights in one jurisdiction do not give rise to rights in other jurisdictions. The US is a party to certain international agreements that provide various benefits for US companies seeking trademark protection outside of the US and for foreign companies seeking protection in the US, including:

- The Paris Convention for the Protection of Industrial Property.
- The Madrid Protocol.

For more on registering marks under the Madrid Protocol, see Practical Guide, [Registering a Madrid Protocol Trademark](/documents/registering-a-madrid-protocol-trademark). For more information on international trademark protection, see Practical Guide, [International Trademark Protection](/documents/international-trademark-protection).


##### Trademark Searching and Clearance
Before using or seeking to register a trademark, a company should conduct trademark clearance. Trademark clearance typically involves the following:

- Conducting one or more trademark searches, including:
    - preliminary or "knock out" searches; and
    - full availability searches.
- Analyzing the search results.
- Taking additional steps to attempt to assess and mitigate any risks posed by potential obstacles disclosed in the searches, for example by:
    - conducting use investigations of potentially conflicting marks;
    - seeking consent or coexistence agreements from third parties that own potentially conflicting marks; or
    - petitioning to cancel conflicting registrations.

For more information on trademark searching and clearance, see Practical Guide, Trademark Searching and Clearance.
For a checklist of information to obtain from a client before conducting trademark searching and clearance, see Trademark Searching and Clearance Information Checklist.

### Trademark Registration
In the US, a trademark may be registered at the federal or state level.
#### Federal Registration
At the federal level, trademarks are registered under the Lanham Act in the USPTO. The USPTO administers two trademark registers:

- **Principal Register**. The Principal Register is the primary register of trademarks the USPTO maintains, subject to additional registrability requirements, and is reserved for marks that are inherently distinctive or that have acquired distinctiveness (see Eligibility for Registration on the Principal Register).
- **Supplemental Register**. The Supplemental Register is the secondary register and is reserved for certain nondistinctive marks that do not inherently qualify for registration on the Principal Register, but that are capable of distinguishing goods or services. If a mark on the Supplemental Register acquires distinctiveness through use and promotion over time, the owner may file a new application to register the mark on the Principal Register.

##### How long does a trademark registration last?

A trademark registration may remain in force for potentially unlimited consecutive ten-year periods as long as the owner meets the legal requirements for post-registration maintenance and renewal and timely files all necessary documents. The owner must file a "Declaration of Use" between the fifth and sixth year following registration, attesting to the continued use or excusable nonuse of the mark on or in connection with the goods and/or services in the registration. In addition, the owner must file a combined Declaration of Use (or Excusable Nonuse) and Application for Renewal between the ninth and tenth year after registration, and every 10 years thereafter, attesting to the continued use or excusable nonuse of the mark on or in connection with the goods and/or services in the registration and requesting to renew the registration. If these documents are not timely filed, the registration will expire or be cancelled and cannot be revived or reinstated. For more information, including the different renewal requirements for Madrid Protocol-based registrations, see Maintain/Renew a Registration.

##### What if the trademark registration expired or was cancelled?
Even if a trademark registration expires or is cancelled, the trademark owner may continue to have "common law" rights in the mark. Federal registration typically does not create rights in a mark but rather confers benefits and protections in addition to rights established by common law. Common law rights arise from actual use of a mark for particular goods or services and may allow the common law user to successfully challenge another party's use in court.

#### Eligibility for Registration on the Principal Register
Not all words, symbols, logos, and devices can be registered as trademarks. The Lanham Act and USPTO regulations specify the eligibility of trademarks for federal registration. Certain subject matter may not be registered under any circumstances, including:

- Marks likely to cause confusion as to source with a previously -  or registered mark.
- Generic terms.
- Deceptive matter.
- Functional matter.
- Matter that falsely suggests a connection with a person or institution.
- Government insignia.
- Names, portraits, and signatures identifying particular living individuals without their written consent.

The Lanham Act, on its face, also still prohibits registration of immoral, scandalous, and disparaging marks (15 U.S.C. § 1052(a)). However, the US Supreme Court has invalidated these restrictions as unconstitutional under the First Amendment (Iancu v. Brunetti, 139 S. Ct. 2294 (2019) (affirming Federal Circuit decision that Lanham Act immoral and scandalous mark proscriptions are unconstitutional restrictions on free speech); Matal v. Tam, 137 S. Ct. 1744 (2017) (holding that Lanham Act disparagement proscription constitutes unconstitutional government regulation of expression in violation of the First Amendment)).

Certain designations may be registered only on a showing of acquired distinctiveness (secondary meaning), including designations that are:

- Merely descriptive of the goods or services.
- Primarily geographically descriptive of the goods or services.
- Primarily merely a surname.

For more information on eligibility for federal trademark registration on the Principal Register, see Practical Guide, Acquiring Trademark Rights and Registrations: Eligibility for Trademark Registration Under the Lanham Act.

##### Benefits of Federal Registration
Although not required for the acquisition and protection of trademark rights, federal registration of a mark on the Principal Register confers significant benefits on the trademark owner, including evidentiary presumptions relating to:

- Ownership of the mark.
- Validity of the mark.
- The exclusive right to use the mark nationwide in connection with the goods or services covered by the registration.
- The right to use the registered trademark symbol: ®.

Registration of a mark on the Supplemental Register confers certain benefits, including the right to use the registered trademark symbol, but not the evidentiary benefits of registration on the Principal Register. For more information on the benefits of registration of a mark on the Principal Register and the Supplemental Register, see Practical Guide, Acquiring Trademark Rights and Registrations: Benefits of Federal Registration.

#### State Registration
Most states have statutes for registering trademarks used in the state. Requirements for obtaining state registrations, and their terms, vary by state. Generally a mark must be used in commerce within the state before a registration can be obtained. A state registration may offer some procedural and evidentiary advantages in litigation.

#### Federal Registration Process
If a company decides to seek registration of a mark after the search and clearance process has been completed, the next step is filing an application with the USPTO. Trademark applications are filed online through the USPTO's Trademark Electronic Application System.

##### Filing Basis
The most common trademark application filing bases are use in commerce and intent-to-use:

- **Use in commerce**. A use-based (Section 1(a)) application is based on existing use of the mark in interstate commerce (15 U.S.C. § 1051(a)).
- **Intent to Use**. An intent-to-use (ITU or Section 1(b)) application is based on a bona fide intent to use the mark in commerce in connection with the goods or services listed in the application, even though actual use of the mark has not yet taken place (15 U.S.C. § 1051(b)).

Applications to register a trademark on the Supplemental Register must be based on use in commerce.

For more information on filing bases, see Practical Guides, Acquiring Trademark Rights and Registrations: Filing Basis and Filing a Federal Trademark Application: Filing Bases

##### Filing Requirements
File the application online through the Trademark Electronic Application System (TEAS). ^[The application fee is a processing fee. Not all applications result in registrations. Your fee will not be refunded, even if ultimately no registration issues. All information you submit to the USPTO at any point in the application and/or registration process will become public record, including your name, phone number, e-mail address, and street address.]

A trademark application filed with the USPTO must comply with certain filing requirements and formalities. For more information, see Practical Guides, Acquiring Trademark Rights and Registrations: Application Elements and Filing a Federal Trademark Application: Filing and Prosecuting the Application.

Throughout the entire process, periodically monitor the progress of your application through the Trademark Status and Document Retrieval (TSDR) system. It is important to check the status of your application every 3-4 months after the initial filing of the application, because otherwise you may miss a filing deadline. See checking status for more information about this.

##### USPTO Examination
After the USPTO determines that you have met the minimum filing requirements, an application serial number is assigned and the application is forwarded to an examining attorney for examination. The examining attorney reviews the application for:

- Compliance with formal application requirements.
- The eligibility of the trademark for registration.

This may take a number of months. The examining attorney reviews the application to determine whether it complies with all applicable rules and statutes, and includes all required fees. Filing fees will not be refunded, even if the application is later refused registration on legal grounds. A complete review includes a search for conflicting marks and an examination of the written application, the drawing, and any specimen. For more information on the USPTO examination process, see Practical Guide, [Acquiring Trademark Rights and Registrations: USPTO Examination.]()



##### Office Actions

If the examining attorney determines that a mark should not be registered, the examining attorney will issue a letter (Office action) to you explaining any substantive reasons for refusal, and any technical or procedural deficiencies in the application. If only minor corrections are required, the examining attorney may contact you by telephone or email (if the applicant has authorized communication by email).

If the examining attorney identifies any deficiencies in the application or issues pertaining to the registrability of the mark, the examining attorney issues an Office Action identifying the deficiencies and issues. The applicant typically has six months from the date the Office Action is issued to respond. The examining attorney may issue additional Office Actions if the applicant does not provide a satisfactory response. For resources providing guidance for responding to Office Actions, see Trademark Office Action Response Toolkit.

In some cases where an application is refused based on a finding of likelihood of confusion with a prior registered mark, the applicant may be able to overcome the refusal through a consent agreement with the prior rights owner. For a sample trademark consent agreement, see Standard Document, Trademark Consent Agreement.

If the examining attorney sends you an Office action, you, or your attorney, if you have one, must submit a response to the Office action that is received by the USPTO within six (6) months of the issue date of the Office action, or the application will be declared abandoned.

##### Publication

If the examining attorney raises no objections to registration, or if you overcome all objections, the examining attorney will approve the mark for publication in the "Official Gazette," a weekly publication of the USPTO. An application for the Principal Register that passes through examination is published for opposition in the USPTO's Trademark Official Gazette. Applications to register marks on the Supplemental Register are not published. The USPTO will send you a notice of publication stating the date of publication. After the mark is published in the "Official Gazette," any party who believes it may be damaged by registration of the mark has thirty (30) days from the publication date to file either an opposition to registration or a request to extend the time to oppose. An opposition is similar to a proceeding in a federal court, but is held before the Trademark Trial and Appeal Board (TTAB), an administrative tribunal within the USPTO. If no opposition is filed or if the opposition is unsuccessful, the application enters the next stage of the registration process.  It can take three to four months from the time the notice of publication is sent before the applicant will receive official notice of the next status of the application.  During this time, you should continue to monitor the status of your application through the TSDR system. For more information, see Practical Guide, [Filing a Federal Trademark Application: Publication]().

##### Opposition
Once a mark is published for opposition, a party that is entitled to assert a statutory cause of action may oppose registration of the mark by filing a Notice of Opposition within the opposition period or any applicable extension of the period (see Opposition Proceedings).

##### Registration
If no opposition is filed, the next step in the registration process depends on whether the mark is the subject of:

- A **use-based application**. A mark that is the subject of a use-based application proceeds to registration if no opposition is filed.
- An **intent-to-use application**. A mark that is the subject of an intent-to-use application may not be registered until the applicant files a proper Statement of Use. For more information on requirements for the Statement of Use, see Practical Guide Acquiring Trademark Rights and Registrations: ITU Application.

###### Registration Certificate Issues for Applications Based on Use
If the mark is based on use in commerce, a foreign registration, or an extension of protection of an international registration to the United States under Section 66(a), and no party files an opposition or request to extend the time to oppose, the USPTO will register the mark and send the owner a certificate of registration. After the mark registers, the owner of the mark must file specific maintenance documents to keep the registration live.

###### Notice of Allowance Issues for Applications Based on an Intent to Use the Mark
If the mark is published based upon the applicant's bona fide intention to use the mark in commerce and no party files either an opposition or request to extend the time to oppose, the USPTO will issue a notice of allowance about eight (8) weeks after the date the mark was published. The applicant then has six (6) months from the date of the notice of allowance to either:

###### Use the mark in commerce and submit a statement of use (SOU); or
Request a six (6) month extension of time to file a statement of use (extension request).

A notice of allowance is a written notification from the USPTO that a specific mark has survived the opposition period following publication in the Official Gazette, and has consequently been allowed; it does not mean that the mark has registered yet. Receiving a notice of allowance is another step on the way to registration. Notices of allowance are only issued for applications that have a filing basis of intent to use a mark in commerce under Trademark Act Section 1(b).

###### Applicant Files Timely Statement of Use or Extension Request
The applicant has six (6) months from the mailing date of the notice of allowance in which to either file a statement of use (SOU) or file an extension request. More than one extension request may be filed, but a limit exists on the total number of extension requests permitted and the timeframe that they must be submitted within. Please review the additional information for the SOU use and extension request processes.


###### Applicant Does Not File Timely Statement of Use or Extension Request
If the applicant does not file a statement of use or extension request within six (6) months from the date the notice of allowance issued, the application is abandoned (no longer pending/under consideration for approval). To continue the application process, the applicant must file a petition to revive the application within two (2) months of the abandonment date.

###### USPTO Reviews Statement of Use
A statement of use (SOU) must meet minimum filing requirements before an examining attorney fully reviews it. If the SOU does meet the minimum filing requirements, then the examining attorney reviews it to determine whether it is acceptable to permit registration. Submission of an SOU does not guarantee registration. You may not withdraw the SOU and the filing fee(s) will not be refunded, even if the SOU/application is later refused registration on legal grounds. If no refusals or additional requirements are identified, the examining attorney approves the SOU.

If refusals or requirements must still be satisfied, the examining attorney issues you a letter (Office action) stating the refusals/requirements. This is the same process that occurs prior to publication of the mark if the examining attorney determines that legal requirements must be met. The process and timeframes remain the same, except that if issues are ultimately resolved and the statement of use is approved, the USPTO issues a registration within approximately two (2) months. If all issues are not resolved, the application will abandon.

###### Applicant’s Response Fails to Overcome All Objections
If your response does not overcome all objections, the examining attorney will issue a final refusal Office action. If you disagree with the final refusal, you may, for an additional fee, appeal the decision to the TTAB.

For more information on the process for registering a trademark with the USPTO, see Practical Guide, Filing a Federal Trademark Application and Registering a Trademark Checklist.

### Maintaining Trademark Rights
Trademark rights are subject to certain maintenance requirements for both common law rights and registrations.

##### Registration Certificate Issues
Within approximately two (2) months after the SOU is approved, the USPTO issues a registration. To keep the registration "live," the registrant must file specific maintenance documents. Failure to make these required filings will result in cancellation and/or expiration of the registration. If your registration is cancelled or expired, your only option is to file a brand new application and begin the entire process again from the very beginning.  The fact that your mark was previously registered does not guarantee registration when you submit a new application.

##### Monitoring Registration Status
Even if your mark registers, you should monitor the status of your registration on an annual basis through the Trademark Status and Document Retrieval (TSDR) system. It is particularly important to check the status of your registration after you make any of the filings required to keep your registration alive including between the fifth and sixth year after the registration date and between the ninth and tenth year after the registration date.

##### Registrant Address
It is critical that, as necessary, you update your correspondence address (and, where appropriate, your email address) and update your owner address. Please be advised that changing the owner’s address will not change the correspondence address, and vice versa.


#### Maintaining Common Law Rights
Common law rights in a trademark are maintained through bona fide use of the mark in commerce as a source identifier for goods or services (see Practical Guide, Proving Trademark Use and Priority). Absent federal registration, rights are generally limited to the geographic area in which the mark is used. Non-use of a mark may result in abandonment of the mark (see Loss of Trademark Rights). Trademark rights may be weakened, or even lost, through certain trademark owner conduct including:

- Misuse of the trademark.
- Failure to take appropriate action against third parties that infringe or otherwise misuse the mark.

For information on proper trademark use and protection, see Standard Document, Trademark Use and Protection Guidelines (Internal Distribution).

#### Maintaining Federal Trademark Registrations
A federal trademark registration is maintained by:

- Continuous and consistent use of the mark in commerce.
- Compliance with registration maintenance formalities, including timely filing of:
    - declarations of continued use or excusable nonuse; and
    - renewal applications.

For more information, see Practical Guide, Maintaining Trademark Registrations.

##### Incontestability
The owner of a mark registered on the Principal Register may, subject to certain requirements, file a Declaration of Incontestability to seek incontestable status for the mark. A Declaration of Incontestability may be filed once the mark has been in continuous use in commerce for at least five consecutive years after the registration date. The declaration may be filed within one year after the expiration of any five-year period of continuous use following registration.

Incontestable status confers significant benefits on the trademark owner. For example, an incontestable registration is, subject to certain exceptions, conclusive evidence of the mark owner's exclusive right to use the mark for the goods or services listed in the registration. For more information on incontestability, see Practical Guide, Maintaining Trademark Registrations: Affidavit of Incontestability Under Section 15.
A registration on the Supplemental Register is not eligible for incontestable status.

#### Maintaining State Trademark Registrations
Requirements for maintaining state registrations vary by state. 


### Exploiting Trademark Rights
Trademarks are property and are transferable. The value of a trademark is based on its goodwill and its ability to exclude others from using similar marks likely to cause marketplace confusion. In addition to actively enforcing its trademark against third parties, a trademark owner may be able to exploit its trademark in various ways, for example by:

- Licensing the mark (see Trademark Licensing).
- Selling the mark (see Selling Trademarks).
- Using the mark as collateral to secure financing (see Using Trademarks as Collateral).

#### Trademark Licensing
Trademark owners may license their marks to others to use, subject to various terms and conditions agreed on by the parties, including exclusivity, licensing fees, and royalties. Trademark licenses can be recorded with the USPTO, but recordation is not required. The trademark owner must monitor the nature and quality of the licensee's goods and services. Failure to exercise adequate quality control can lead to abandonment of the licensed trademarks (see Abandonment Through Uncontrolled Licensing).
For sample arms-length trademark license agreements, see Standard Documents, Trademark License Agreement (Pro-Licensor) and Trademark License Agreement (Pro-Licensee). For an intercompany license agreement, see Standard Document, Intercompany Trademark License Agreement.
For a checklist of common issues that a licensor should consider when starting a trademark licensing program, see Trademark License Checklist.

#### Selling Trademarks
A trademark owner may be able to obtain value by selling and assigning its trademarks and any associated registrations or applications.
Both registered and unregistered trademarks can be assigned, but the assignment of the mark must include the goodwill associated with the mark. An assignment of a registered trademark must be in writing. Recordation of a trademark assignment with the USPTO is not required to make a transfer effective, but provides certain benefits. There are restrictions on the assignment of intent-to-use trademark applications.
For more information on trademark assignments, see Practical Guide, Intellectual Property Rights: The Key Issues: Trademark Assignments.
For sample assignment agreements, see Standard Documents, Trademark Acquisition Agreement and Trademark Assignment Agreement (Short Form).

#### Using Trademarks as Collateral
State UCC Article 9 provisions govern perfection of security interests over trademarks. While the USPTO accepts documents evidencing grants of security interests in federally registered trademarks and trademark applications, these recordings are not necessary or effective to perfect the lender's security interest. However, USPTO recordation is recommended, as it may provide protection against subsequent bona fide purchasers and mortgagees.
For more information on security interests over trademarks, see Practical Guide, Security Interests: Intellectual Property: Trademarks.
For a sample agreement, see Standard Document, Trademark Security Agreement (Short Form).


### Loss of Trademark Rights
There are various ways that trademark rights may be lost. The most common means for losing rights include:

- Abandonment of a mark through nonuse (see Abandonment Through Nonuse).
- Abandonment of a mark through uncontrolled licensing (see Abandonment Through Uncontrolled Licensing).
- Genericide (see Genericide).

There are other ways that trademark rights may be limited or lost through conduct of the owner, including improper assignments (see Improper Trademark Assignments).

#### Abandonment Through Nonuse
In the US, a trademark must be used to keep rights in the mark. If a mark is not used, it is subject to abandonment. Under the Lanham Act, abandonment occurs when the trademark owner both:

- Discontinues use.
- Has an intent not to resume use.
  (15 U.S.C. § 1127.)

Under the Lanham Act there is a rebuttable presumption of abandonment after three consecutive years of nonuse (15 U.S.C. § 1127).
For more information, see Practical Guide, Trademark Abandonment from Discontinued Use.

#### Abandonment Through Uncontrolled Licensing
The Lanham Act provides that a mark is abandoned if, through conduct of the trademark owner, the mark either:

- Becomes the generic name for the goods or services (see Genericide).
- Otherwise loses its significance as a trademark.
  (15 U.S.C. § 1127.)

One way that a mark may lose its trademark significance is if the owner licenses the mark without exercising adequate quality control over the nature and quality of the licensed goods or services. This is commonly known as naked licensing. For more information on naked licensing, see Practical Guide, Loss of Trademark Rights: Abandonment Through Uncontrolled (Naked) Licensing.
For more information on quality control in trademark license agreements, see Standard Document, Trademark License Agreement (Pro-Licensor): Drafting Note: Quality Control.

#### Genericide
Trademark rights can be lost if the mark's primary meaning to the relevant public becomes the name of the genus of the goods or services. For more information on genericide, see Practical Guides, Loss of Trademark Rights: Genericide and Genericness Challenges to Trademark Rights.

#### Improper Trademark Assignments
Improper trademark assignments may also lead to the limitation or loss of trademark rights. Improper assignments include:

- **Assignments in gross**. For more information on assignments in gross, see Practical Guide, Loss of Trademark Rights: Assignments in Gross.
- **Improper assignments of intent-to-use trademark applications**. For more information on improper assignments of intent-to-use trademark applications, see Practical Guide, Loss of Trademark Rights: Improper Assignment of Intent-to-Use Applications.