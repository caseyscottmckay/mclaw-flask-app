---
title| Damage Statistics---Administrative & Government
author| casey@mclaw.io
date| 2023-01-28
category | Administrative & Government
subcategory | Administrative & Government
tags | administrative, government
summary|
---



## Federal Damage Statistics for Administrative & Government
- data based on 9,914 federal district court cases dating from 2003 to 2023


##### Distribution (Monetary Damages)

##### Distribution (Attorney Fees and Costs)


##### Damage Types (Monetary Damages)

##### Damage Types (Attorney Fees and Costs)


##### Court (Monetary Damages)

##### Court (Attorney Fees and Costs)


##### Case Type (Monetary Damages)

##### Case Type (Attorney Fees and Costs)


##### Law Firm (Monetary Damages)

##### Law Firm (Attorney Fees and Costs)


##### Year (Monetary Damages)

##### Year (Attorney Fees and Costs)


### Conclusion

