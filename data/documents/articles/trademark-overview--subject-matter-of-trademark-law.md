---
title| Trademark Overview--Subject Matter of Trademark Law
author| casey@mclaw.io
date| 2021-12-21
category | Intellectual Property and Technology
subcategory | Trademark
tags | trademark, infringement
summary| This overview of US trademark law gives information about the nature of trademarks, the reasons why they can be commercially significant, and the type of material that is likely to be suitable for registration and use as a trademark. Topics covered include the nature of trademark protection and the subject matter of trademark law.
---
### Introduction to Trademarks

#### Do You Need a Trademark?
Trademarks, patents, copyrights, domain names, and business name registrations all differ, so it is important to learn whether a trademark is appropriate for you.

A trademark typically protects brand names and logos used on goods and services. A patent protects an invention. A copyright protects an original artistic or literary work. For example, if you invent a new kind of vacuum cleaner, you would apply for a patent to protect the invention itself. You would apply to register a trademark to protect the brand name of the vacuum cleaner. And you might register a copyright for the TV commercial that you use to market the product.

A domain name is part of a web address that links to the internet protocol address (IP address) of a particular website. For example, in the web address "https://www.uspto.gov," the domain name is "uspto.gov." You register your domain name with an accredited domain name registrar, not through the USPTO. A domain name and a trademark differ. A trademark identifies goods or services as being from a particular source. Use of a domain name only as part of a web address does not qualify as source-indicating trademark use, though other prominent use apart from the web address may qualify as trademark use. Registration of a domain name with a domain name registrar does not give you any trademark rights. For example, even if you register a certain domain name with a domain name registrar, you could later be required to surrender if it infringes someone else's trademark rights.

Similarly, use of a business name does not necessarily qualify as trademark use, though other use of a business name as the source of goods or services might qualify it as both a business name and a trademark. Many states and local jurisdictions register business names, either as part of obtaining a certificate to do business or as an assumed name filing. For example, in a state where you will be doing business, you might file documents (typically with a state corporation or state division of corporations) to form a business entity, such as a corporation or limited liability company.

### What is a Trademark?

A trademark does three things:

- Identifies a single source of origin for goods and services.
- Distinguishes the owner's goods and services from those of others.
- Provides the owner of the mark with the right to prevent others from adopting similar marks likely to cause confusion in the marketplace.

A trademark is generally a word, phrase, symbol, or design, or a combination of these elements, that identifies and distinguishes the source of one party's goods from those of others. A service mark is the same as a trademark except that it identifies and distinguishes the source of a service rather than goods. The terms "trademark" or "mark" are commonly used to refer to both trademarks and service marks. Although federal registration of a mark is not mandatory, it has several advantages, including notice to the public of the registrant's claim of ownership of the mark, a legal presumption of ownership nationwide, and the exclusive right to use the mark on or in connection with the goods and/or services listed in the registration.

The trademark registration process is a legal proceeding that requires you to act within specific deadlines.

#### Types of Trademarks
Under the Lanham Act, a trademark generally includes any of the following when used to identify and distinguish goods or services: word, name, symbol, device.

The term service mark is sometimes used to refer to a trademark that is used for services.

In some circumstances trademarks may consist of colors, scents, or sounds.

For more on trademark protection for these less common trademarks, see Practical Guide, [Protecting Non-Traditional Trademarks]().

##### Trade Dress
Under certain circumstances trade dress may be protectable as a trademark. For more information on trade dress protection, see Practical Guide, Trade Dress Protection.

##### Collective Marks and Certification Marks

The Lanham Act also recognizes and provides for registration of collective marks and certification marks.

For more information certification marks, see Practical Guide, Certification Marks. For more information on the various types of trademarks, see Practical Guide, Acquiring Trademark Rights and Registrations: Types of Marks.
