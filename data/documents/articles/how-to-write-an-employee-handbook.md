---
title| How to Write an Employee Handbook
author| casey@mclaw.io
date| 2023-02-08
category | Labor and Employment
subcategory | Policies and Procedures
tags | employment, labor, handbook, policy, procedure
summary|  Writing an employee handbook is essential for any organization as it sets expectations and policies for employees. The purpose of the handbook should be determined before outlining the content, which should cover company policies, employee benefits, legal rights, and company culture. It is crucial to consult with legal professionals to ensure the handbook complies with federal and state laws. The language used should be simple and easily understood by everyone, avoiding jargon or technical terms. The handbook should be reviewed and updated regularly to ensure it remains relevant and useful.
---
An employee handbook is a valuable tool for any organization, as it sets the expectations and policies for employees. It is a guide for employees that outlines the rules, regulations, and company culture. Here are some steps to follow when writing an employee handbook.

    Determine the Purpose: Before writing the handbook, you need to determine the purpose of the handbook. Ask yourself what information needs to be included, who the audience is, and what tone the handbook should take.

    Outline the Content: Once you know the purpose of the handbook, you can begin outlining the content. A standard employee handbook includes information on company policies, employee benefits, legal rights, and company culture. Make sure to cover all necessary topics and ensure that the information is easy to understand.

    Consult with Legal Professionals: It's essential to consult with legal professionals to ensure that the handbook complies with federal and state laws. This will protect both the organization and the employees from any legal issues that may arise.

    Keep it Simple: The employee handbook should be easy to read and understand. Avoid using jargon or technical language that may confuse employees. Use a simple language that can be easily understood by everyone.

    Review and Update: Once the handbook is complete, it is essential to review and update it regularly. As laws and policies change, the handbook must be updated to reflect those changes. Regular review and update of the handbook will ensure that it remains relevant and useful.

In conclusion, an employee handbook is a crucial tool for any organization. By following these steps, you can create a comprehensive and useful handbook that will set the expectations and policies for your employees while protecting your organization from legal issues.

All companies need to communicate their mission, values and expectations to their employees. Employee handbooks are a good means to put these elements together. Here’s how to write a helpful and engaging employee handbook:

## What is the purpose of having an employee handbook?


Your handbook’s content and level of detail depends on how you intend to use it. An employee handbook may be a repository of all your policies or a way to welcome new hires.


Using your handbook to guide new hires is a good idea. An employee handbook gives new hires tangible information to help them settle into their new jobs. It’s a consistent message on who we are, how we function and what we expect from our employees.


Without the help of a handbook, it may take employees time to grasp a company’s culture and organization. In 2012, game development company Valve released a handbook for new hires to explain its organizational structure:


A good way to approach creating your handbook is to keep it focused on welcoming new hires and providing only the information they need (e.g. summaries and statements.) You could keep detailed documents of your policies in an intranet, a HR information system (e.g. BambooHR or Namely) or a shared folder.

What to include in an employee handbook

An effective employee handbook includes:



- Your company’s mission, vision and an overview of its culture.

- Guidelines for employee conduct.

- Details on legal aspects of employment.

- Summaries of perks and benefits.

- Descriptions of company processes.

- Craft an outline with these elements in mind. Here’s a possible outline with some examples for each section:



- Handbook Purpose

- Welcome statement for new hires

- How to use this handbook

- Table of contents

- Company introduction

- Mission statement

- Brief history

- Structure

- Employment

- Employee classification (e.g. exempt v. non-exempt)

- Working hours (e.g. operating hours, overtime)

- Frequently used contract terms (e.g. non-disclosure agreements)

- Benefits

- Leave/ paid-time-off (PTO) policies

- Healthcare

- Perks

- Training and development

- Company car

- Bonus incentives

- Employee Code of Conduct

- Protection of company property

- Diversity and anti-harassment

- Health & safety

- Social media

- Dress code

- Processes and procedures

- Recruitment

- Performance management

- Company Actions

- Corporate Social Responsibility (CSR)

- Recognition programs

- Mentorship programs







Decide what policies to include based on your specific needs. Provide only short summaries of complicated topics (e.g. benefits) and link or refer to full policy documents. In general, avoid overloading your handbook with prohibitions. Make it more attractive by adding a personal touch like a letter from a senior leader.


If you are creating a longer and more complete version of your handbook, explain legal issues too (e.g. at-will employment or worker’s compensation.)


As your company grows and laws change, you may need to address new topics. Communicate your plan to revise and update your handbook (annual or mid-year reviews are useful.) Put a process in place to share every significant change through bulletins, newsletters or other means.

## Mind the Presentation


An unattractive, complicated document risks remaining permanently on employees’ “to-read” list. Take some time to think about your format, layout and audience.


Printed booklets are concrete, but they are also harder to update and demand reprinting and redistributing when something changes. Even then, a creative approach goes a long way. For example, e-commerce company Zappos created an employee handbook in the form of a comic book. Zappos reframed its policies as stories and ultimately made them easier to read and remember.


Digitized books or interactive web pages capture people’s attention and make it easy to link to other resources.


This approach helps companies update their handbooks easily through GitHub and get direct feedback. It also adds to their employer brand, as passive candidates, interns and newly hired employees can get insight on how the company works right from the source.


Not all companies make handbooks that create a buzz. But, they can craft effective handbooks with the right design. If you don’t have an internal design team, consider hiring a freelancer. Add pictures, schemes and even videos, when possible, to make your handbook engaging.

## Use Cclear and Attractive Language


Employee handbooks should not read like business contracts or legal documents. To encourage employees to read and remember your messages, use language to your advantage. Here are a few tips:


Focus on the positives. Even when you are indicating a prohibition (e.g. no smoking indoors) explain why it’s important and how it adds value to your company as a whole. Keep your language welcoming and instructional, rather than authoritative.


Speak to your audience. Avoid using passive voice or addressing abstract entities (e.g. “the employee.”) Use “you” and “we” to make your handbook more personable and accessible.


Add humor when possible. Your handbook isn’t meant to be hilarious, but adding a few humorous lines (or pictures) will make it more pleasant to read.


Use a tone that matches your culture. Your handbook’s tone mirrors everyday work life at your company. A consultancy firm with strict professional standards will probably write its handbook in a professional, formal tone. Conversely, tech companies might use a more casual tone.


Keep it short and simple. Use as few words as possible and avoid jargon, technical terms and complicated words.


Employee handbooks are multi-purpose tools. Use them to inform employees about your company’s values and clear confusion on important topics. When you are done writing, ask your attorney to inspect your handbook for legality. And listen to employee feedback to ensure your policies make sense.
