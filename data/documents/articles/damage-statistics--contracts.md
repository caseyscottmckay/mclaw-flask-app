---
title| Damage Statistics---Contracts
author| casey@mclaw.io
date| 2023-01-25
category | Intellectual Property and Technology
subcategory | Computers
tags | copyright, trademarks, patents
summary|
---
When it comes to disputes arising from contractual relationships, parties often turn to the courts to resolve their differences. Contract litigation can be a lengthy and costly process, but if successful, it can result in damages being awarded to the prevailing party. In the United States, federal district courts have jurisdiction over cases that involve disputes arising from contracts that are governed by federal law or interstate commerce. In this blog article, we will take a closer look at the damages that can be awarded in contract litigation cases in federal US district courts.

### Types of Damages Awarded in Contract Litigation

The types of damages that can be awarded in contract litigation cases are generally classified as either direct or consequential damages. Direct damages are those that flow directly from the breach of the contract and are usually intended to compensate the non-breaching party for the loss suffered as a result of the breach. Consequential damages, on the other hand, are those that result from the breach but are not the direct or immediate result of the breach itself. Instead, they are the result of the non-breaching party's attempt to mitigate or remedy the harm caused by the breach.

### Direct Damages

Direct damages are typically awarded to compensate the non-breaching party for the harm caused by the breach of the contract. The types of direct damages that can be awarded in contract litigation cases include:

    Expectation damages: These are damages that are intended to put the non-breaching party in the position they would have been in had the contract been performed as agreed. Expectation damages are usually calculated based on the amount of money the non-breaching party would have received if the contract had been performed as agreed.

    Consequential damages: These are damages that are a direct result of the breach and are intended to compensate the non-breaching party for any losses that flow from the breach. For example, if a supplier breaches a contract to deliver goods to a buyer, the buyer may be entitled to consequential damages for any losses they incur as a result of the supplier's failure to deliver the goods.

    Liquidated damages: These are damages that are agreed upon by the parties at the time the contract is signed. Liquidated damages are usually intended to compensate the non-breaching party for specific types of harm that may result from a breach of the contract.

### Consequential Damages

Consequential damages are damages that flow from the breach of the contract but are not the direct result of the breach itself. These damages are often more difficult to prove than direct damages because they require the non-breaching party to show that the damages were caused by the breach and were foreseeable at the time the contract was signed.

Consequential damages can include lost profits, lost opportunities, and other types of indirect losses that are a result of the breach of the contract. In order to recover consequential damages, the non-breaching party must show that the damages were a direct result of the breach and that they were foreseeable at the time the contract was signed.

### Punitive Damages

Punitive damages are damages that are intended to punish the breaching party for their actions. Punitive damages are not typically awarded in contract litigation cases unless the breach of the contract was particularly egregious, such as in cases involving fraud, bad faith, or intentional misconduct.

### Attorney's Fees and Costs

In addition to damages, the prevailing party in a contract litigation case may also be entitled to recover their attorney's fees and costs. Attorney's fees and costs can be significant, particularly in cases that are litigated over an extended period of time. In some cases, attorney's fees and costs can even exceed the amount of damages awarded.


## Federal Damage Statistics for Contract Litigation
- data based on 23,718 federal district court cases dating from 2003 to 2023


##### Distribution (Monetary Damages)
Distribution  | Percentage of total dockets
$1-$5k  | 3%
$5k-$50k  | 11%
$50k-$100k  | 13%
$100k-$500k  | 46%
$500k-$1m  | 11%
$1m-$50m  | 15%
$50m+  | 1%

##### Distribution (Attorney Fees and Costs)
Distribution  | Percentage of total dockets
$1-$5k  | 59%
$5k-$50k  | 32%
$50k-$100k  | 3%
$100k-$500k  | 4%
$500k-$1m  | 1%
$1m-$50m  | 1%
$50m+  | 0%


##### Damage Types (Monetary Damages)
Damages Type  | Number of dockets  | Median awarded amount
Compensatory Damages  | 14,026  | $212,181
Settlement  | 3,006  | $145,737
Interest  | 965  | $22,440
Liquidated Damages  | 86  | $190,964
Punitive Damages  | 61  | $276,000
Statutory Damages  | 33  | $10,000

##### Damage Types (Attorney Fees and Costs)
Damages Type  | Number of dockets  | Median awarded amount
Litigation Costs & Expenses  | 5,753  | $1,149
Attorney Fees  | 3,631  | $8,288

##### Court (Monetary Damages)
Court  | Number of dockets  | Median awarded amount
D.N.J.  | 1,708  | $214,126
S.D.N.Y.  | 1,348  | $258,769
E.D.Pa.  | 1,248  | $171,043
N.D.Ill.  | 1,124  | $218,278
D.Md.  | 630  | $161,797
C.D.Cal.  | 523  | $189,871
E.D.Va.  | 470  | $219,411
S.D.Tex.  | 391  | $203,100
N.D.Tex.  | 369  | $187,000
N.D.Ga.  | 345  | $283,188
E.D.Mich.  | 337  | $176,997
N.D.Ohio  | 331  | $200,000
E.D.N.Y.  | 326  | $194,577
M.D.Fla.  | 312  | $200,000
N.D.Ala.  | 309  | $257,746
W.D.Pa.  | 290  | $210,031
S.D.Fla.  | 266  | $261,702
D.Ariz.  | 249  | $217,857
D.Puerto Rico  | 224  | $115,700
D.Utah  | 203  | $234,334

##### Court (Attorney Fees and Costs)
Court  | Number of dockets  | Median awarded amount
C.D.Cal.  | 759  | $4,231
N.D.Tex.  | 367  | $4,338
N.D.Ga.  | 303  | $3,183
S.D.Tex.  | 284  | $2,670
N.D.Ill.  | 283  | $5,000
M.D.Fla.  | 276  | $4,587
D.Md.  | 263  | $1,900
S.D.N.Y.  | 259  | $1,910
D.Ariz.  | 246  | $3,464
N.D.Cal.  | 237  | $3,317
S.D.Fla.  | 230  | $1,899
D.Colo.  | 228  | $3,094
D.Nev.  | 205  | $3,544
E.D.Va.  | 191  | $3,326
D.Or.  | 171  | $2,777
E.D.Cal.  | 162  | $3,416
D.N.J.  | 154  | $4,052
E.D.La.  | 149  | $2,065
W.D.Wash.  | 140  | $2,707
E.D.Mich.  | 130  | $1,541

##### Case Type (Monetary Damages)
Case type  | Number of dockets  | Median awarded amount
Contracts  | 17,174  | $200,189
Other  | 37  | $40,000

##### Case Type (Attorney Fees and Costs)
Case type  | Number of dockets  | Median awarded amount
Contracts  | 8,625  | $3,395
Other  | 34  | $1,606

##### Law Firm (Monetary Damages)
Law Firm  | Number of dockets  | Median awarded amount
LeClairRyan  | 239  | $252,534
Connell Foley LLP  | 181  | $215,350
Day Pitney LLP  | 177  | $258,783
Baker, Donelson, Bearman, Caldwell & Berkowitz, PC  | 152  | $228,772
Faegre Drinker Biddle & Reath LLP  | 130  | $266,152
Troutman Pepper Hamilton Sanders LLP  | 129  | $400,000
Fox Rothschild LLP  | 128  | $250,000
Bradley Arant Boult Cummings LLP  | 126  | $306,176
Greenberg Traurig, LLP  | 120  | $419,020
Reed Smith LLP  | 117  | $431,674
People's United Financial, Inc.  | 112  | $222,642
Burr & Forman LLP  | 110  | $383,156
Locke Lord LLP  | 110  | $571,844
McElroy, Deutsch, Mulvaney & Carpenter, LLP  | 110  | $291,493
Wilson Elser Moskowitz Edelman & Dicker LLP  | 103  | $234,232
McGuireWoods LLP  | 101  | $236,466
Dentons US LLP  | 99  | $394,517
Hinshaw & Culbertson LLP  | 99  | $195,166
Holland & Knight LLP  | 94  | $316,000
Husch Blackwell LLP  | 92  | $334,787

##### Law Firm (Attorney Fees and Costs)
Law Firm  | Number of dockets  | Median awarded amount
Hinshaw & Culbertson LLP  | 119  | $4,319
Lewis Brisbois Bisgaard & Smith LLP  | 111  | $2,264
Dentons US LLP  | 105  | $2,962
Gordon Rees Scully Mansukhani, LLP  | 90  | $4,286
Foley & Lardner LLP  | 84  | $6,089
Locke Lord LLP  | 83  | $5,632
Fox Rothschild LLP  | 82  | $4,235
Greenberg Traurig, LLP  | 82  | $4,150
Wilson Elser Moskowitz Edelman & Dicker LLP  | 80  | $3,613
Troutman Pepper Hamilton Sanders LLP  | 79  | $3,849
Snell & Wilmer L.L.P.  | 77  | $8,770
Ogletree, Deakins, Nash, Smoak & Stewart, P.C.  | 71  | $2,117
Holland & Knight LLP  | 69  | $5,404
Hunton Andrews Kurth LLP  | 68  | $3,652
Faegre Drinker Biddle & Reath LLP  | 67  | $4,291
K&L Gates LLP  | 66  | $5,382
Lewis Roca Rothgerber Christie LLP  | 66  | $2,905
Holland & Hart LLP  | 65  | $6,123
DLA Piper LLP (US)  | 64  | $3,260
Morgan, Lewis & Bockius LLP  | 61  | $8,444


##### Year (Monetary Damages)
Year  | Number of dockets  | Median awarded amount
2022  | 82  | $223,402
2021  | 245  | $243,126
2020  | 386  | $253,145
2019  | 450  | $204,660
2018  | 464  | $197,464
2017  | 467  | $201,152
2016  | 504  | $209,964
2015  | 601  | $198,504
2014  | 669  | $206,065
2013  | 778  | $272,520
2012  | 746  | $221,726
2011  | 973  | $236,179
2010  | 1,052  | $241,604
2009  | 1,148  | $236,313
2008  | 965  | $211,619
2007  | 840  | $223,158
2006  | 738  | $216,433
2005  | 836  | $200,722
2004  | 852  | $167,981
2003  | 554  | $175,835

##### Year (Attorney Fees and Costs)
Year  | Number of dockets  | Median awarded amount
2022  | 82  | $223,402
2021  | 245  | $243,126
2020  | 386  | $253,145
2019  | 450  | $204,660
2018  | 464  | $197,464
2017  | 467  | $201,152
2016  | 504  | $209,964
2015  | 601  | $198,504
2014  | 669  | $206,065
2013  | 778  | $272,520
2012  | 746  | $221,726
2011  | 973  | $236,179
2010  | 1,052  | $241,604
2009  | 1,148  | $236,313
2008  | 965  | $211,619
2007  | 840  | $223,158
2006  | 738  | $216,433
2005  | 836  | $200,722
2004  | 852  | $167,981
2003  | 554  | $175,835

### Conclusion

Contract litigation can be a complex and costly process, but if successful, it can result in damages being awarded to the prevailing party. Direct damages, consequential damages, and liquidated damages are the types of damages that can be awarded in contract litigation cases, with consequential damages being more difficult to prove. Punitive damages are not typically awarded unless the breach of the contract was particularly egregious. In addition to damages, the prevailing party in a contract litigation case may also be entitled to recover their attorney's fees and costs.

It is important to note that the damages that can be awarded in contract litigation cases are subject to certain limitations. For example, in cases involving breaches of contracts for the sale of goods, the damages that can be awarded are limited to the difference between the contract price and the market price of the goods at the time of the breach. Additionally, the non-breaching party has a duty to mitigate their damages, which means that they must take reasonable steps to minimize the harm caused by the breach.

In order to recover damages in a contract litigation case, the non-breaching party must prove that the other party breached the contract and that they suffered harm as a result of the breach. This can be a challenging task, particularly in cases where the breach is not readily apparent or where the harm suffered by the non-breaching party is difficult to quantify.

In many cases, parties to a contract will include a provision in the contract that requires disputes to be resolved through arbitration rather than litigation. Arbitration is a private process that is often faster and less expensive than litigation. However, the damages that can be awarded in arbitration are generally more limited than the damages that can be awarded in litigation.

In conclusion, contract litigation can result in damages being awarded to the prevailing party, including direct damages, consequential damages, and liquidated damages. Punitive damages are not typically awarded unless the breach of the contract was particularly egregious. The prevailing party may also be entitled to recover their attorney's fees and costs. However, the damages that can be awarded in contract litigation cases are subject to certain limitations, and the non-breaching party must prove that they suffered harm as a result of the breach. Parties to a contract may also consider including an arbitration provision in the contract as an alternative to litigation.


##