---
title | Serving Defendants in China
author | casey@mclaw.io
date | 2021-11-14
category | Federal Procedure
subcategory | LLC
tags | service, process, china
summary | This articles discusses the procedure of serving defendants in China.
---
# Serving Defendants in China
The Digital Millennium Copyright Act (DMCA) provides a legal framework for copyright owners to protect their intellectual property online. However, enforcing copyright violations can become complicated when the defendant is located in a foreign country like China. In this article, we'll discuss the steps involved in serving process to a defendant in China in the context of a DMCA complaint and copyright litigation.

#### Step 1: Hire an International Process Server

The first step in serving process to a defendant located in China is to hire an international process server. It is important to choose a process server with experience in international service of process and knowledge of the Hague Convention on the Service Abroad of Judicial and Extrajudicial Documents in Civil or Commercial Matters (the Hague Convention).

The Hague Convention is an international treaty that governs the service of process in civil and commercial matters between signatory countries. China is a signatory to the Hague Convention, which means that service of process in China must be carried out in accordance with the Convention.

#### Step 2: Prepare the Legal Documents

The next step is to prepare the legal documents for service. In the context of a DMCA complaint and copyright litigation, the legal documents may include the complaint, the summons, and any other supporting documents. The legal documents must be translated into Chinese and certified by a Chinese translator or the Chinese Embassy.

#### Step 3: File the Legal Documents with the Court

Once the legal documents have been prepared, they must be filed with the court. The court will issue a summons that must be served on the defendant in China.

#### Step 4: Serve Process on the Defendant

The Hague Convention requires that service of process in China must be carried out through the Chinese Central Authority. The Chinese Central Authority is responsible for forwarding the legal documents to the appropriate court or authority for service on the defendant.

The process server must complete a Request for Service form and send it, along with the legal documents, to the U.S. Central Authority. The U.S. Central Authority will then forward the documents to the Chinese Central Authority.

The Chinese Central Authority will then serve the documents on the defendant in accordance with Chinese law. It is important to note that service of process in China can take several months, so patience is required.

#### Step 5: Follow up on Service

After the legal documents have been served on the defendant, the process server should provide proof of service to the court. The proof of service should include an affidavit of service, which documents the date, time, and manner in which the legal documents were served on the defendant.

It is important to follow up on service to ensure that the legal documents have been properly served on the defendant. If service is not properly completed, the court may dismiss the case or delay the proceedings.

### Conclusion

Serving process to a defendant located in China in the context of a DMCA complaint and copyright litigation can be a complex process. It is important to hire an experienced international process server and to follow the procedures outlined in the Hague Convention. By taking the necessary steps, copyright owners can protect their intellectual property rights and enforce their legal rights in a foreign jurisdiction.


-----
-----
-----

U.S. companies increasingly enter into contracts with Chinese companies, which makes sense given the two countries are the largest markets in the world. It won’t surprise anyone that China’s legal system differs from the U.S. in many ways. Given that parties in contracts may sometimes find themselves in a disagreement, if the U.S. company chooses to litigate claims it needs to understand the unique requirements of serving a Chinese-based company. These requirements relate to how China has implemented a key international convention. However, a recent California Supreme Court decision may ease those burdens if U.S. companies ensure key language when drafting agreements with companies based in China.

## Understanding The Hague Service Convention

Part of the challenge a U.S. plaintiff faces stems from obligations under the Hague Service Convention. More formally known as the Convention on the Service Abroad of Judicial and Extrajudicial Documents in Civil or Commercial Matters, this international agreement governs service abroad. The Hague Service Convention’s purpose is to “provide a simpler way to serve process abroad, to assure that defendants sued in foreign jurisdictions would receive actual and timely notice of suit, and to facilitate proof of service abroad.”

It’s important to note that Article 1 of the Convention states it “applies in all cases, in civil or commercial matters, where there is occasion to transmit a judicial or extrajudicial document for service abroad.” Both China and the U.S. are among the Convention’s 79 contracting parties. Therefore, when a U.S. company sues a China-based company arising out their commercial relationship in a U.S. Court, the service requirement under Hague applies.


## Other Considerations

### Waiver of Formal Service
If a foreign defendant agrees to accept service voluntarily, the US plaintiff should consider proceeding under FRCP 4(d), which permits defendants to waive formal service of process. Treaty-based service requirements, such as those set out in the Hague Service Convention, are not implicated when a defendant voluntarily agrees to waive formal service (FRCP 4(d) advisory committee's notes to 1993 amendment; _Hoffman-La Roche, Inc. v. Invamed, Inc._, 183 F.R.D. 157, 159 (D.N.J. 1998)).
Waiving formal service of process can benefit both the plaintiff and the defendant. Specifically:

    The plaintiff benefits by avoiding the costs associated with translating and serving documents under the Hague Service Convention or other applicable laws.
    The foreign defendant who agrees to waive formal service benefits by having 90 days to respond to the complaint, instead of the standard 21 days under FRCP 12 (FRCP 4(d)(3)).

^[Note, a defendant does not waive any objection to personal jurisdiction or venue by waiving formal service (FRCP 4(d)(5)).]

## Serving Legal Documents in China
### A. Federal Rules51
Selecting a mechanism to serve process on a foreign party raises many issues, and the method selected to conduct service “can have a serious impact both on the matter pending the United States and eventual enforcement of a U.S. judgment abroad.”52 Federal Rule of Civil Procedure 4 manifests that service is a prerequisite for valid personal jurisdiction over a person abroad.53 “U.S. courts have frequently quashed service or dismissed suits against foreign defendants because service on them has been improper under U.S. law.”54
*222 Rule 4(f) authorizes six methods for serving legal documents in foreigncountries. Service can be made “by any internationally agreed means reasonably calculated to give notice, such as those means authorized by the [Hague Service Convention].”55 “[I]f there is no internationally agreed means of service or the applicable international agreement allows other means of service,” service can be made “in the manner prescribed by the law of the foreign country for service” in any courts of general jurisdiction in that country.56 When no internationally agreed means exist or the agreement permits alternatives, service is also allowed in the manner “as directed by the foreign authority in response to a letter rogatory or letter of request,” through “delivery to the individual personally” if not prohibited by the law of the foreign country, or through “any form of mail requiring a signed receipt, to be addressed and dispatched by the clerk of the court to the party to be served.”57 Service can also be effected by any “other means not prohibited by international agreement as may be directed by the court.”58

### B. Impact of Violating Foreign Law Under U.S. law
“Service by methods that would violate foreign law is not generally authorized” by the Federal Rules of Civil Procedure.59 This is the most recent indication of the intent of the advisory committee of the Federal Rules of Civil Procedure. Nonetheless, past court decisions suggest opposing positions, and it is not entirely clear what impact service in violation of foreign law has for purposes of U.S. law.

Many courts have held that “[s]ervice that is defective under foreign law usually will not invalidate service for purposes of U.S. law.”60 In Alco Standard Corp. v. Benalal,61 Benalal, a Spanish company, challenged the district court's jurisdiction on the grounds of inadequate service.62 The court held that “[a]lthough such service may not be effective under Spanish law, and consequently any judgment obtained in this Court may not be enforceable” in Spain, the service did not invalidate the court's jurisdiction because the “only relevant question for this court concerns the sufficiency of the service of process under laws of the United States.”63

Although this still appears to be good law,64 U.S. litigants violate foreign law at their risk because invalid service under a foreign country's laws may cause the judgment to be unenforceable in a foreign jurisdiction.65
### C. The Hague Convention in China

Service can be effected in China through the Hague Service Convention, which establishes a simple and expeditious service mechanism through the operation of the “Central Authorities” of each contracting country.66 The designated “Central Authority” is “responsible for receiving foreign requests for service, serving documents on local residents and returning proof of service to the requesting state.”67
The designated Chinese Central Authority is the Bureau of International Judicial Assistance, Ministry of Justice of the People's Republic of China.68 A request for service “can be made by an attorney by submitting the documents to be served under cover of form USM-94, in duplicate to the foreign central authority.”69 This means that an attorney needs to include “a completed USM-94 [form], one original English version of the documents to be served (summons must bear seal of the court); one translation of all documents to be served; plus a photocopy of all of the above” documents.70
The Hague Service Convention applies to “all cases, in civil or commercial matters, where there is occasion to transmit a judicial or extrajudicial document for service abroad.”71 The phrase “civil and commercial matters is not defined in the convention,” and the negotiating history of the convention does not provide enough guidance as to the meaning of this term.72 “Member states have construed the phrase civil and commercial matters” inconsistently.73 Although it is fairly clear that the term does not cover criminal matters, much uncertainty exists as to the “treatment of tax, administrative, ‘public’ law and family law *224 proceedings.”74 The disputes largely arise out of the notion in civil-law countries that distinguishes between private law and public law. In these jurisdictions, civil and commercial matters are deemed private law, but administrative matters and some other proceedings are governed by public law.75
In contrast, the United States and the United Kingdom have interpreted the phrase “civil and commercial matters” to include “tax, administrative, public law, and other proceedings” other than criminal proceedings.76 France and Switzerland include “all matters except ‘criminal’ and ‘fiscal’ matters.”77 “German practice excludes all criminal and ‘public law’ matters” from the scope of the Convention, and Japan excludes all administrative proceedings.78 In reported cases, Chinese courts have not clearly manifested their position on this, but as China is a civil-law jurisdiction and has always strictly retained its judicial sovereignty, its courts probably will construe the phrase “civil and commercial matter” narrowly.
D. Other Alternatives
Service via the Hague Service Convention is generally guaranteed in China and typically there are no problems regarding future enforcement proceedings in China; nonetheless, this method usually requires cumbersome forms and translations, takes a considerable time to serve, and can be very expensive.79 Therefore, U.S. litigators might wonder about other alternatives. Are other means allowed under Federal Rule 4(f) and international customary law and are they available to U.S. litigators when serving legal documents in China? Are there any restrictions imposed by Chinese law and what other factors should be considered?
1. Exclusivity of the Convention
   Most U.S. courts have concluded that the Hague Service Convention must be used when it is applicable, and that other mechanisms authorized by Rule 4 and state laws are preempted by the Convention when service is to a party in a member state.80 These courts have reasoned that under the mandate of the U.S. Constitution which provides that “all Treaties made . . . shall be the supreme Law of the Land,”81 and the Hague Service Convention which provides that it “shall apply in all cases . . . where there is occasion to transmit a judicial or extrajudicial document for service abroad,”82 the use of apparently mandatory *225 language should be interpreted to require the use of the Convention for service abroad in preference to other means of service provided by federal and state law.83
   Although the Chinese position on the issue of exclusivity of the Convention is not entirely clear,84 it seems that China will not recognize non-Convention methods when the documents sought to be served originate from countries that have a contractual relationship with China through treaty or convention.85 Chapter 29, Article 263 of the Civil Procedure Law of the PRC provides that “[i]n requesting or offering judicial assistance, the procedures spelled out in the international treaties signed or joined by the PRC shall be followed; diplomatic channels shall be pursued in cases where there are no treaties.”86 It appears that China is only willing to allow other diplomatic or customary methods when a contractual relationship through treaties is absent.
2. Alternatives
   Even though the Hague Service Convention has often been treated as exclusive, the Convention itself recognizes alternative means of extraterritorial service in Articles 8 and 10.87 “[T]he Convention permits member states to object to alternative forms of service,” and service through these alternative means is “permitted only if the receiving state has not objected.”88 Article 10(a) of the Convention permits sending judicial documents through the postal service directly to the defendant.89 10(b) and 10(c) allow sending letters of request directly to the “judicial officers, officials or other competent persons” in the receiving state.90 Article 8 permits the diplomatic *226 or consular officers of the requesting state to serve process in the receiving state, but no compulsion can be resorted to in such service.91
   China, on its accession to the Hague Service Convention, indicated that it would allow service directly through the U.S. diplomatic or consular agents only when the document is to be served upon a U.S. national.92 This position is reaffirmed by Article 263 of the Civil Procedure Law of the PRC.93 Nonetheless, this method is not available to U.S. litigants even within the Chinese law's restriction because under existing U.S. Consular Regulations,94 U.S. diplomatic or consular officers are not allowed to serve legal process on behalf of U.S. litigants “except when expressly authorized to do so.”95
   Service by mail and service by sending a letter of request directly to judicial officers, officials or other competent persons should not be used in China because China objected to service in accordance with Article 10 of the Convention when it ratified the Hague Service Convention.96 American courts have recognized that “formal objections to service by mail made by countries party to a multilateral treaty or convention on service of process at the time of accession or subsequently in accordance with the treaty are honored as a treaty obligation.”97 Litigants should not ordinarily have incentive to serve by directly sending a letter of request to “judicial officers, officials or other competent persons,” since this method can be just as time consuming and costly as the letter of request through Central Authority and Central Authority provides a more guaranteed model of service of process.
   As the Department of Justice information flyer suggests, U.S. litigants frequently serve judicial documents by agent98 or publication because these methods are usually timely and inexpensive.99 Although Chinese domestic law allows service by agent and publication, U.S. counsel should not use them in China because U.S. and Chinese law both seem to support the position that the Hague Service Convention is exclusive when it is applicable.100 The Convention does not authorize service by agent or service by publication. Serving process through these two means may invalidate jurisdiction in U.S. courts and present enforcement problems in China.

---- BELOW FEW PARAGRAPHS PROB REPEAT OF ABOVE

So how does that work in China? Well, that country has designated its Ministry of Justice, Department of Judicial Assistance and Cooperation, Division of Judicial Assistance as the Central Authority under Hague. The Ministry of Justice of China undertakes requests for service and forwards the documents to the proper Chinese court to serve the documents under the Hague Service Convention’s Article 5.

The Hague Service Convention’s Article 10 permits alternative service methods, such as service by mail, if the State of designation does not object. However, when China adopted the Convention, it objected to Article 10’s alternative service methods. Therefore, when serving a Chinese company in China, the U.S. company needs to make requests for service to the Ministry of Justice of China under the Hague Service Convention.

The Ministry of Justice of China has developed an online Civil and Commercial Judicial Assistance System. Thus, a U.S. company can submit its request via this website, rather than sending in hardcopy documents.

### Two key requirements apply:

1. The documents need to be served with a translation in simplified Chinese attached.
2. A completed Mandatory Request for Service Form annex to the Convention, signed by an attorney, must be required. This form is the same as USM-94, Request for Service Abroad of Judicial or Extrajudicial Documents.

Meanwhile, the Ministry of Justice charges a fee relating to the execution of the service on a reciprocal basis and at the equivalent amount. Since the U.S. currently charges $95 to serve document, the U.S. company needs to wire that amount to the Ministry of Justice of China.

Even when following this process, problems can arise with the turnaround time. China’s Ministry of Justice of China once stated that it takes 3 to 6 months to execute requests. In reality, a backlog means some service requests have been pending for as long as two years. Plaintiffs need to prepare for delays and plan their litigation strategy accordingly.

## When does the Hague Service Convention Not Apply?

Is there a workaround to the Hague Service Convention? The short answer is yes, when California is the venue. The California Supreme Court recently held in one dispute that the parties’ memorandum of understanding (MOU) constituted a waiver of formal service of process under California law, and thus there was no required transmittal abroad to which the Hague Service Convention could apply. Let’s look at the case.

In _Rockefeller Technology Investments (Asia) VII v. Changzhou SinoType Technology Co., Ltd_, (2020) 9 Cal.5th 125, Rockefeller and SinoType entered into an MOU. Rockefeller initiated an arbitration pursuant to certain MOU provision, and SinoType did not appear. Rockefeller obtained a $414,601,200 arbitration award, then petitioned to confirm the award and transmitted the petition and summons to SinoType through Federal Express. SinoType again didn’t appear and the award was confirmed into a judgment. Then SinoType moved “to quash and to set aside default judgment for insufficiency of service of process,” arguing Rockefeller failed to follow the service requirement under the Hague Service Convention, thus rendering the judgment void.

The California Supreme Court disagreed. It reasoned that “formal service of process involves two aspects: service a method of obtaining personal jurisdiction over a defendant and formalized notification of court proceedings to allow a party to appear and defend against the action.” With respect to personal jurisdiction, the MOU expressly stated “[t]he Parties hereby submit to the jurisdiction of the Federal and State Courts in California…” With respect to notice, the MOU stated that the parties “shall provide notice…via Federal Express…” and that the parties “consent to services of process in accord with the notice provisions above.” The MOU’s language confirms the parties’ intent to replace ‘service of process’ with the alternative notification method specified in the agreement.” The Court held “the Convention does not apply when parties have agreed to waive formal service of process in favor of a specified type of notification serves to promote certainty and give effect to the parties express intention.” The Court further explained that “to apply the Convention under such circumstance would sow confusion and encourage gamesmanship and sharp practice.”

----END OF DUPLICATE CONTGENT

## What U.S. Companies Need to Do Up Front

So what have we learned? One, when a U.S. company sues a China-based company and try to serve the company in China, the Hague Service Convention requires it to serve through China’s Ministry of Justice, which can be time-consuming.  However, if the parties previously have agreed to jurisdiction in California and waive the formal process of service, the Hague Service Convention doesn’t apply.

Therefore, before conducting business with a China-based company, a U.S. company would be wise to ensure the parties enter into an agreement providing for adequate jurisdiction and service, thus ensuring any commercial dispute can be resolved in a U.S. court or arbitral forum. Taking this approach can lead to expedited resolutions with less hassle, helping the U.S. company spend more time growing its business rather than litigating commercial disputes.




