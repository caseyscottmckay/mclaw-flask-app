---
title | USPTO Trademark Classes
author | casey@mclaw.io
date | 2021-11-14
category | Intellectual Property and Technology
subcategory | Trademarks
tags | trademark, application, registration
summary | This articles provides an overview of the USPTO's trademark classes for goods and services.
---
The trademark classes “tm classes” break down all goods and services into 45 different classes. These classes simplify the registration process and make it easier to identify potential infringement. These are only a few of the benefits of the system.

# What are Trademark Classes?

The United States Patent and Trademark Office (USPTO) has organized products and services according to specific categories or classes. Anyone wishing to register a trademark must do so under the appropriate class. In some cases, you will need to register your trademark in more than one class to cover a single product or service. USPTO classes help keep track of and differentiate between the thousands of new trademarks registered each year. Additional fees are required for each registered class.

There are 45 trademark classes, which are also referred to as Nice Classification (NCL) after the city in France where the first classes were negotiated in 1957. All products and services registered by the USPTO are categorized into one of these classes. Keep in mind, however, that these class headings are too broad to use in your actual trademark description. Plus, international class numbers alone are never acceptable.

The United States Patent and Trademark Office (USPTO) utilizes trademark classes to distinguish between different types of trademark useage.  A trademark application must categorize how a trademark will be used by selecting at least one type of class. These can range from apparel to business services, and in some cases, you may need to register under multiple trademark classes to ensure adequate protection.

Choosing a trademark class shouldn’t be viewed as a minor issue that simply needs a number selected on a trademark application. Your trademark will only be protected in the class or classes in which it’s registered. If your trademark is singularly classified as machinery, for instance, it won’t necessarily prevent someone from registering the trademark for automobile tires.  Here are a few examples of identical trademarks that are able to coexist in different classes:

- Delta (faucets) and Delta (airlines)
- Pandora (jewelry) and Pandora (music streaming)
- Graco (baby products) and Graco (industrial)

Each of these pairs share the exact same name, but since they fall under different classes, it’s doubtful that a likelihood of confusion exists. This is also the case for brand logos. Marmot and Motorola have nearly identical M logos. If the two were filed under the same trademark class, it’s highly unlikely that Marmot – which hasn’t existed as long – would have been granted protection for its logo.

Most brands typically fall under multiple trademark classes. This is especially the case if you offer more than one product or service. If you plan on manufacturing t-shirts as a method of promoting your custom guitars and drum sets, for instance, you could file for musical instruments and clothing.  You don’t have to file for multiple trademark classes, but by doing so, you’ll broaden and thus strengthen your trademark protection.

# Trademark Classes Explained

The need to understand trademark classes isn’t simply related to securing maximum protection. It’s also meant to simplify the registration process – which in turn can save you time and money. Prior to submitting a trademark application, you should perform a trademark search. This allows you to ensure that your trademark won’t be confusingly similar to another. With almost 500,000 U.S. trademark applications filed each hear, a trademark search is becoming increasingly important.

By classifying each trademark into a specific category, the USPTO makes its database far easier to search. When using the Trademark Electronic Search System (TESS), you can narrow your results down by using classes. Keep in mind that an overly narrow search could overlook registered trademarks that may block your application.  So a search should include all categories you believe your product or service may fall into.

When explaining trademark classes, it’s also necessary to consider trademark cost. Here are the trademark fees you can expect:

- TEAS Plus: $250
- TEAS Reduced Fee (TEAS RF): $350
- TEAS Regular: $400

For almost every trademark application that we file, we are able to utilize the lower $250 government filing fee.  Keep in mind that these fees are per class. This means if you’re filing a TEAS RF application encompassing the apparel and purse classes, your government filing fee will be $700.  Filing a trademark for abstract use is also not allowed.  Your trademark must either be in use, or you must have a bonafide intent to use it in the near future.  While these fees may seem costly, failing to fully protect your trademark rights can be even more damaging if it leads to trademark litigation.

Although application requirements become less strict as fees become higher, every application must meet the minimum filing requirements. If your application fails to do so, it will be rejected. Additionally, you’ll be required to pay an additional fee if you file a TEAS Plus or TEA RF registration without meeting the appropriate requirements.

# Trademark Classes List

Your trademark description will need to be more in-depth than a single class heading, but these are the 45 classes of goods and services you get to choose from. A full explanation of each of these categories – along with what qualifies under the classes – can be found here.  You can also search your goods or services to see what classes they fall into with suggested descriptions here: Trademark ID Manual.

## Classes of Goods

<table cellspacing="0" cellpadding="0" border="1">
  <tbody>
    <tr>
      <td colspan="3" style="padding: 5px 10px;">
      <p><strong>NCL Classes</strong></p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px; white-space: nowrap;">
      <p><strong>Class No.</strong></p>
      </td>
      <td style="padding: 5px 10px;">
      <p><strong>Type</strong></p>
      </td>
      <td style="padding: 5px 10px;">
      <p><strong>Description of Goods</strong></p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 1</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Chemicals</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Chemicals used in industry, science and photography, as well as in agriculture, horticulture and forestry; unprocessed artificial resins, unprocessed plastics; manures; fire extinguishing compositions; tempering and soldering preparations; chemical substances for preserving foodstuffs; tanning substances; adhesives used in industry.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 2</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Paints</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Paints, varnishes, lacquers; preservatives against rust and against deterioration of wood; colorants; mordants; raw natural resins; metals in foil and powder form for painters, decorators, printers and artists.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 3</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Cosmetics and Cleaning Preparations</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Bleaching preparations and other substances for laundry use; cleaning, polishing, scouring and abrasive preparations; soaps; perfumery, essential oils, cosmetics, hair lotions; dentifrices.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 4</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Lubricants and Fuels</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Industrial oils and greases; lubricants; dust absorbing, wetting and binding compositions; fuels (including motor spirit) and illuminants; candles and wicks for lighting.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 5</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Pharmaceuticals</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Pharmaceutical and veterinary preparations; sanitary preparations for medical purposes; dietetic substances adapted for medical use, food for babies; plasters, materials for dressings; material for stopping teeth, dental wax; disinfectants; preparations for destroying vermin; fungicides, herbicides.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 6</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Metal Goods</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Common metals and their alloys; metal building materials; transportable buildings of metal; materials of metal for railway tracks; non-electric cables and wires of common metal; ironmongery, small items of metal hardware; pipes and tubes of metal; safes; goods of common metal not included in other classes; ores.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 7</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Machinery</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Machines and machine tools; motors and engines (except for land vehicles); machine coupling and transmission components (except for land vehicles); agricultural implements other than hand-operated; incubators for eggs.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 8</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Hand Tools</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Hand tools and implements (hand-operated); cutlery; side arms; razors.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p><a href="https://www.upcounsel.com/trademark-class-9" target="_blank">Class 9</a></p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Electrical and Scientific Apparatus</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Scientific, nautical, surveying, photographic, cinematographic, optical, weighing, measuring, signaling, checking (supervision), life-saving and teaching apparatus and instruments; apparatus and instruments for conducting, switching, transforming, accumulating, regulating or controlling electricity; apparatus for recording, transmission or reproduction of sound or images; magnetic data carriers, recording discs; automatic vending machines and mechanisms for coin-operated apparatus; cash registers, calculating machines, data processing equipment and computers; fire-extinguishing apparatus.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 10</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Medical Apparatus</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Surgical, medical, dental and veterinary apparatus and instruments, artificial limbs, eyes and teeth; orthopedic articles; suture materials.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 11</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Environmental Control Apparatus</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Apparatus for lighting, heating, steam generating, cooking, refrigerating, drying, ventilating, water supply and sanitary purposes.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 12</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Vehicles</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Vehicles; apparatus for locomotion by land, air or water.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 13</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Firearms</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Firearms; ammunition and projectiles; explosives; fireworks.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 14</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Jewelry</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Precious metals and their alloys and goods in precious metals or coated therewith, not included in other classes; jewelry, precious stones; horological and chronometric instruments.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 15</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Musical Instruments</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Musical instruments.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 16</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Paper goods and Printed Matter</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Paper, cardboard and goods made from these materials, not included in other classes; printed matter; bookbinding material; photographs; stationery; adhesives for stationery or household purposes; artists' materials; paint brushes; typewriters and office requisites (except furniture); instructional and teaching material (except apparatus); plastic materials for packaging (not included in other classes); printers' type; printing blocks.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 17</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Rubber Goods</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Rubber, gutta-percha, gum, asbestos, mica and goods made from these materials and not included in other classes; plastics in extruded form for use in manufacture; packing, stopping and insulating materials; flexible pipes, not of metal.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 18</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Leather Goods</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Leather and imitations of leather, and goods made of these materials and not included in other classes; animal skins, hides; trunks and travelling bags; umbrellas, parasols and walking sticks; whips, harness and saddlery.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 19</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Nonmetallic Building Materials</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Building materials (non-metallic); non-metallic rigid pipes for building; asphalt, pitch and bitumen; non-metallic transportable buildings; monuments, not of metal.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 20</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Furniture and Articles not Otherwise Classified</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Furniture, mirrors, picture frames; goods (not included in other classes) of wood, cork, reed, cane, wicker, horn, bone, ivory, whalebone, shell, amber, mother-of-pearl, meerschaum and substitutes for all these materials, or of plastics.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 21</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Housewares and Glass</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Household or kitchen utensils and containers; combs and sponges; brushes (except paint brushes); brush-making materials; articles for cleaning purposes; steel wool; unworked or semi-worked glass (except glass used in building); glassware, porcelain and earthenware not included in other classes.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 22</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Cordage and Fibers</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Ropes, string, nets, tents, awnings, tarpaulins, sails, sacks and bags (not included in other classes); padding and stuffing materials (except of rubber or plastics); raw fibrous textile materials.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 23</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Yarns and Threads</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Yarns and threads, for textile use.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 24</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Fabrics</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Textiles and textile goods, not included in other classes; bed and table covers.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p><a href="https://www.upcounsel.com/trademark-class-25" target="_blank">Class 25</a></p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Clothing</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Clothing, footwear, headgear.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 26</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Fancy Goods</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Lace and embroidery, ribbons and braid; buttons, hooks and eyes, pins and needles; artificial flowers.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 27</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Floor Coverings</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Carpets, rugs, mats and matting, linoleum and other materials for covering existing floors; wall hangings (non-textile).</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 28</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Toys and Sporting Goods</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Games and playthings; gymnastic and sporting articles not included in other classes; decorations for Christmas trees.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 29</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Meats and Processed Foods</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Meat, fish, poultry and game; meat extracts; preserved, frozen, dried and cooked fruits and vegetables; jellies, jams, compotes; eggs, milk and milk products; edible oils and fats.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 30</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Staple Foods</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Coffee, tea, cocoa, sugar, rice, tapioca, sago, artificial coffee; flour and preparations made from cereals, bread, pastry and confectionery, ices; honey, treacle; yeast, baking-powder; salt, mustard; vinegar, sauces (condiments); spices; ice.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 31</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Natural Agricultural Products</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Agricultural, horticultural and forestry products and grains not included in other classes; live animals; fresh fruits and vegetables; seeds, natural plants and flowers; foodstuffs for animals, malt.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 32</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Light Beverages</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Beers; mineral and aerated waters and other non-alcoholic drinks; fruit drinks and fruit juices; syrups and other preparations for making beverages.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 33</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Wine and Spirits</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Alcoholic beverages (except beers).</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 34</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Smokers' Articles</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Tobacco; smokers' articles; matches.</p>
      </td>
    </tr>
  </tbody>
</table>

<p>&nbsp;</p>


## Classes of Services

<table cellspacing="0" cellpadding="0" border="1">
  <tbody>
    <tr>
      <td colspan="3" style="padding: 5px 10px;">
      <p><strong>NCL Services</strong></p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px; white-space: nowrap;">
      <p><strong>Class No.</strong></p>
      </td>
      <td style="padding: 5px 10px;">
      <p><strong>Type</strong></p>
      </td>
      <td style="padding: 5px 10px;">
      <p><strong>Description of Services</strong></p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p><a href="https://www.upcounsel.com/trademark-class-35" target="_blank">Class 35</a></p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Advertising and Business</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Advertising; business management; business administration; office functions.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 36</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Insurance and Financial</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Insurance; financial affairs; monetary affairs; real estate affairs.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 37</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Building Construction and Repair</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Building construction; repair; installation services.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 38</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Telecommunications</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Telecommunications.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 39</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Transportation and storage)</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Transport; packaging and storage of goods; travel arrangement.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 40</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Treatment of Materials</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Treatment of materials.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p><a href="https://www.upcounsel.com/trademark-class-41" target="_blank">Class 41</a></p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Education and Entertainment</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Education; providing of training; entertainment; sporting and cultural activities.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p><a href="https://www.upcounsel.com/trademark-class-42" target="_blank">Class 42</a></p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Computer and Scientific</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Scientific and technological services and research and design relating thereto; industrial analysis and research services; design and development of computer hardware and software.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 43</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Hotels and Restaurants</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Services for providing food and drink; temporary accommodation.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 44</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Medical, Beauty &amp; Agricultural</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Medical services; veterinary services; hygienic and beauty care for human beings or animals; agriculture, horticulture and forestry services.</p>
      </td>
    </tr>
    <tr>
      <td style="padding: 5px 10px;">
      <p>Class 45</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Personal</p>
      </td>
      <td style="padding: 5px 10px;">
      <p>Legal services; security services for the protection of property and individuals; personal and social services rendered by others to meet the needs of individuals.</p>
      </td>
    </tr>
  </tbody>
</table>


## Classes of Services

- Class 35: Advertising and Business
- Class 36 : Insurance and Financial
- Class 37: Building Construction and Repair
- Class 38 : Telecommunications
- Class 39: Transportation and storage)
- Class 40 : Treatment of Materials
- Class 41 : Education and Entertainment
- Class 42 : Computer and Scientific
- Class 43 : Hotels and Restaurants
- Class 44 : Medical, Beauty & Agricultural
- Class 45: Personal

# Trademark ID Manual

The USPTO keeps a listing of acceptable identifications of goods and services known as the Trademark ID Manual. The manual identifies trademark goods and services descriptions which can be accepted without an examining attorney requesting a differing description. The Trademark ID manual is far more in-depth than the trademark class lists.

The ID Manual is updated every Thursday to maintain the most up-to-date information. By searching the Trademark ID Manual, you’ll receive the in-depth information on trademark classes. This ensures you’ll make an informed decision when choosing the correct class.

If you feel that your product or service doesn’t fit into any category, there’s a chance that you could be correct. Technology is constantly evolving, and recently developed goods and services can hit the market at any point. If you feel a new description is needed, you can create one and it will then be up to the examining attorney whether it is acceptable.  This will also mean that you will have to pay the higher $275 per class government filing fee.

## Design Search Codes

These codes are six-digit numbers that simplify the classification of design elements of trademarks. While searching for a trademarked word then narrowing by the appropriate class may seem daunting, doing so with design elements can be even more complex.

Consider the fact that Twitter, Nestle, NBC, Hollister and Dove all identify their brand with a bird design. Countless other companies likely do the same. By using design search codes, you can avoid browsing through untold numbers of logos within your trademark class. Even better, you can narrow down your search to the most specific classes. Here are a few categories related to the bird logo example:

- 15.01: Eagles
- 15.05: Turkeys
- 15.07: Owls
- 15.10: Doves

The first two digits are the category number. In this case, 03 signifies “animal” design elements. The second number is the division of that category – 15 signifies “birds and bats.” The final number is the specific identifier (01 for eagles, 05 for turkeys, etc.). You can also query design search codes based on multiple elements of a logo. While these designations aren’t part of the trademark classes list, they serve many of the same categorical functions.

# USPTO US Trademark Classes

When searching for trademark classes online, you may also come across lists that are obsolete. In 1973, the United States entered into a treaty which standardized categories across member countries. This nullified the USPTO US trademark classes that were already in use. Prior to this agreement, there were 52 categories listed under “Goods.” An additional eight “Services” categories were also available for use.

To make things even more confusing, there were additional classes based on “Collective Membership” and “Certification Marks.” These old US trademark classes can still be found in the database, but the current classes are automatically generated when you’re filing a trademark application.

# Nice Trademark Classes

The Nice Trademark Classes are the current categorizations in use at the USPTO. When America entered the Nice Agreement in 1973, there weren’t even 1 million trademark applications submitted yearly worldwide. By 1993, this number had jumped to nearly 1.6 million. In 2016, it stood at over 3.1 million. This huge increase showcases just how important standardization was – especially when many trademark owners now seek international protection.

There have been several versions and editions of Nice Trademark Classes released. The beginning of 2019 saw the eleventh edition come into force. One of the greatest benefits of the Nice Agreement is the fact that your American trademark can serve as a basis for international registration. Standardization across countries will make this process much simpler. This is essential for those who want to prevent overseas infringement.

# Trademark International Classes

The Nice Agreement was beneficial in the world of intellectual property, but it doesn’t cover all trademark international classes. Even though there are nearly 200 countries in the world, less than 90 are members of the treaty. While some non-member nations use the same criteria, it’s still possible that classes may be different in certain countries where you need protection. Canada, for instance, didn’t even sign on until 2017.

When filing for an international trademark, it’s imperative to understand the classification system and trademark laws in each country where you’re filing. The Nice Agreement was an important step in standardizing international trademark classes, but until every country designates the treaty as law, complex issues will continue to occur when seeking foreign protection of your intellectual property rights.
