---
title| Copyright vs. Trademark vs. Patent
author| casey@mclaw.io
date| 2021-11-14
category | Intellectual Property and Technology
subcategory | General
tags | copyright, trademark, patent
summary| This article provides information on the differences between the various branches of IP law: copyright, trademark, and patent.
---
A trademark is a word, phrase, symbol, and/or design that identifies and distinguishes the source of the goods of one party from those of others. A service mark is a word, phrase, symbol, and/or design that identifies and distinguishes the source of a service rather than goods. Some examples include brand names, slogans, and logos. The term "trademark" is often used in a general sense to refer to both trademarks and service marks.

Unlike patents and copyrights, trademarks do not expire after a set term of years. Trademark rights come from actual “use” (see below). Therefore, a trademark can last forever - so long as you continue to use the mark in commerce to indicate the source of goods and services. A trademark registration can also last forever - so long as you file specific documents and pay fees at regular intervals.

Must all trademarks be registered? No, registration is not mandatory. You can establish “common law” rights in a mark based solely on use of the mark in commerce, without a registration. However, federal registration of a trademark with the USPTO has several advantages, including a notice to the public of the registrant's claim of ownership of the mark, a legal presumption of ownership nationwide, and the exclusive right to use the mark on or in connection with the goods or services set forth in the registration. For more information about “common law” trademark rights and the advantages of federal registration see the Basic Facts About Trademarks brochure.

Each time you use your mark, it is best to use a designation with it.  If registered with the USPTO, use the ® symbol after your mark. If not yet registered, you may use TM for goods or SM for services, to indicate that you have adopted this as a “common law” trademark or service mark.

__NOTE__:
 Use of a business name does not necessarily qualify as trademark use, though other use of a business name as the source of goods or services may qualify it as both a business name and a trademark. Many states and local jurisdictions register business names, either as part of obtaining a certificate to do business or as an assumed name filing. For example, in a state where you will be doing business, you might file documents (typically with a state corporation commission or state division of corporations) to form a business entity, such as a corporation or limited liability company. You would select a name for your entity, for example, XYZ, Inc. If no other company has already applied for that exact name in that state and you comply with all other requirements, the state likely would issue you a certificate and authorize you to do business under that name. However, a state’s authorization to form a business with a particular name does not also give you trademark rights and other parties could later try to prevent your use of the business name if they believe a likelihood of confusion exists with their trademarks. For more information on when a designation may function as both a business name (“trade name”) and a trademark or service mark, see TMEP §1202.01.  

## What is a patent?  
A patent is a limited duration property right relating to an invention, granted by the United States Patent and Trademark Office in exchange for public disclosure of the invention. Patentable materials include machines, manufactured articles, industrial processes, and chemical compositions. The duration of patent protection depends on the type of patent granted:  
 - __Design Patents:__ 15 years from issuance for applications filed on or after May 13, 2015 (14 years from issuance if filed before May 13, 2015)  
 - __Utility patents and plant patents:__ 20 years from the date on which the application for the patent was filed in the United States or, in special cases, from the date an earlier related application was filed. Under certain circumstances, patent term extensions or adjustments may be available.

## What is a copyright?
A copyright protects original works of authorship including literary, dramatic, musical, and artistic works, such as poetry, novels, movies, songs, computer software, and architecture. The duration of copyright protection depends on several factors.  For works created by an individual, protection lasts for the life of the author, plus 70 years. For works created anonymously, pseudonymously, and for hire, protection lasts 95 years from the date of publication or 120 years from the date of creation, whichever is shorter.