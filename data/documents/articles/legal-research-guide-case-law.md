---
title| Legal Research Guide--Case Law
author| casey@mclaw.io
date| 2021-11-14
category | Legal Research
subcategory | General
tags | research, law, library
summary| The guides listed below can help you begin a legal research project by offering quick techniques and a selection of legal resources and tools.  
---
## Introduction  
Case law is law based on judicial decisions. This guide cites resources for locating and identifying judicial decisions from the U.S. courts using primary and secondary sources of case law. Each branch of government produces a different type of law. Case law is the body of law developed from judicial opinions or decisions over time (whereas statutory law comes from legislative bodies and administrative law comes from executive bodies). This guide introduces beginner legal researchers to resources for finding judicial decisions in case law resources. Coverage includes brief explanations of the court systems in the United States; federal and state case law reporters; basic Bluebook citation style for court decisions; digests; and online access to court decisions.  

### Court Systems and Decisions   
The United States has parallel court systems, one at the federal level, and another at the state level. Both systems are divided into trial courts and appellate courts. Generally, trial courts determine the relevant facts of a dispute and apply law to these facts, while appellate courts review trial court decisions to ensure the law was applied correctly.  

###### Hierarchy and precedent in the federal court system.  
- **U.S. Supreme Court**: One court that creates binding precedent on all courts below.  
- **U.S. Courts of Appeals**
  - Thirteen circuits (12 regional and 1 for the federal circuit) that create binding precedent on the District Courts in their region, but not binding on courts in other circuits and not binding on the Supreme Court.  
- **U.S. District Courts**
  - Ninety-four districts (each with 1 district court and 1 bankruptcy court) plus the U.S. Court of International Trade and the U.S. Court of Federal Claims. District Courts must adhere to the precedents set by the Supreme Court and the Circuit Court of Appeals in which they sit.

#### ***Stare Decisis (Precedent)***  
In Latin, stare decisis means "to stand by things decided." In the U.S. legal system, this Latin phrase represents the "doctrine of precedent, under which a court must follow earlier decisions when the same points arise again in litigation." (Black's Law Dictionary, 11th ed.) Typically, a court will deviate from precedent only if there is a compelling reason. Under "vertical" stare decisis, the decisions of the highest court in a jurisdiction create mandatory precedent that must be followed by lower courts in that jurisdiction. For example, the U.S. Supreme Court creates binding precedent that all other federal courts must follow (and that all state courts must follow on questions of constitutional interpretation). Similarly, the highest court in a state creates mandatory precedent for the lower state courts below it. Intermediate appellate courts (such as the federal circuit courts of appeal) create mandatory precedent for the courts below them. A related concept is "horizontal" stare decisis, whereby a court applies its own prior decisions to similar facts before it in the future.

#### **Case Law Reporters**
Decisions are published in serial print publications called “reporters,” and are also published electronically. Reporters are discussed in greater detail in the following sections of this guide: "Federal Court Decisions" and "State Court Decisions." Information about how to cite decisions in a reporter is covered in the section entitled "Citations."

## Federal Court Decisions
Decisions issued by federal courts are printed in reporters. Reporters group cases from the same court "level" together; for example, decisions from the Supreme Court will be published together in a reporter, but that reporter will not include decisions from cases at the circuit or district level. Reporters are issued consecutively, which means that each time a new group of decisions is ready to publish, a new volume of the reporter is printed. Decisions in reporters are typically published in the order in which they were decided; for example if a volume of a reporter covers decisions published from October 1999 through June 2000, then the decisions issued in October will be in the front of the volume and the decisions issued in June will be in the back of the volume.  

Federal court decisions may also be accessed via subscription databases and free online resources.

### U.S. Supreme Court Reporters  
United States Supreme Court decisions are available in three different print reporters (Bluebook citation abbreviations follow each title in parentheses):  

- United States Reports (U.S.)  
- Supreme Court Reporter (S. Ct.)  
- United States Supreme Court Reports, Lawyers’ Edition (L. Ed. & L. Ed. 2d).  

U.S. Supreme Court decisions are binding precedent on all other federal courts and all state courts on questions of constitutional interpretation.

The following titles link to fuller bibliographic information in the Library of Congress Online Catalog. Links to additional online content are included when available.

United States Reports
West's Supreme Court Reporter
United States Supreme Court Reports, Lawyers' Edition

### Federal Reporters

Below, please find information about federal appellate court and district (trial) court reporters. All titles below link to fuller bibliographic information in the Library of Congress Online Catalog. Links to additional online content are included when available.  

**"Published" Federal Appeals Court Decisions**  
Decisions issued by U.S. circuit courts of appeals (the intermediate level of appeal in the federal system) that are selected for publication ("published") are published in the Federal Reporter. The first, second, and third series of the Federal Reporter are cited as "F.", "F.2d", and "F.3d", respectively. Published appellate court decisions are binding precedent on the district courts in that circuit.  
- [West's Federal Reporter](https://lccn.loc.gov/90657017)

**"Unpublished" Federal Appeals Court Decisions**  
Decisions (from 2001 to date) issued by U.S. circuit courts of appeals that are not selected for publication in the Federal Reporter are published in the Federal Appendix. Unpublished federal appellate court decisions generally lack precedential value, but may be considered by courts as persuasive.  
- [West's Federal Appendix](https://lccn.loc.gov/2001208088)

**District Court Decisions**  
Occasionally, decisions issued by federal district courts (the trial court level in the federal system) are published in a reporter known as the Federal Supplement, the first and second series of which are cited as “F. Supp.” and “F. Supp. 2d”. The Federal Supplement includes both decisions selected for publication ("published") and decisions not selected for publication ("unpublished"). Neither published nor unpublished district court decisions constitute binding precedent, but may be considered by courts as persuasive, with published district court decisions being viewed by courts as more persuasive than unpublished decisions.  
- [West's Federal Supplement](https://lccn.loc.gov/89642487)

## State Court Decisions   
Published decisions issued in state appellate court cases are often printed in two or more sources, including regional reporters and official state reporters. State court decisions may also be accessed via subscription databases and free online resources. The Bluebook's Table T1.3 provides guidance as to which reporter (regional, official state, or otherwise) is the preferred standard for citation in the state.

### Official State Reporters  
Some states print their published court decisions in an official state reporter, while others only print decisions in unofficial reporters or rely on the regional reporters. For example, Virginia has an official reporter for printing published Virginia Supreme Court decisions, called Virginia Reports, and for Virginia Court of Appeals decisions, called Virginia Court of Appeals Reports. In contrast, Iowa does not print an official state reporter and has designated West as the official reporter of Iowa court decisions (compiled in the North Western Reporter). Table T1.3 of the Bluebook provides informahttps://guides.loc.gov/case-law/citations#s-lg-box-wrapper-26416493tion about states' official reporters and citation preferences.  

### Regional Reporters  
West's National Reporter System compiles state appellate court decisions from a particular region of the country into one "regional" reporter. For example, the Southern Reporter covers appellate decisions issued in Alabama, Florida, Louisiana, and Mississippi. The regional reporters are issued in series (i.e., first series, second series, and third series).

Below, find a list of regional reporters offered by West (Bluebook citation abbreviations follow each title in parentheses). Note that California and New York also have their own single-state reporters by West. The following titles link to fuller bibliographic information in the Library of Congress Online Catalog. Links to additional online content are included when available.

- Atlantic Reporter: Includes state appellate court decisions from Connecticut, Delaware, Maine, Maryland, New Hampshire, New Jersey, Pennsylvania, Rhode Island, Vermont, and the District of Columbia. Currently in its third series (abbreviated as A.3d).
- North Eastern Reporter: Includes state appellate court decisions from Illinois, Indiana, Massachusetts, New York, and Ohio. Currently in its second series (abbreviated as N.E.2d).
- North Western Reporter: Includes state appellate court decisions from Iowa, Michigan, Minnesota, Nebraska, North Dakota, South Dakota, and Wisconsin. Currently in its second series (abbreviated as N.W.2d).
- Pacific Reporter: Includes state appellate court decisions from Alaska, Arizona, California, Colorado, Hawaii, Idaho, Kansas, Montana, Nevada, New Mexico, Oklahoma, Oregon, Utah, Washington, and Wyoming. Currently in its third series (abbreviated as P.3d).
- South Eastern Reporter: Includes state appellate court decisions from Georgia, North Carolina, South Carolina, Virginia, and West Virginia. Currently in its second series (abbreviated as S.E.2d).
- South Western Reporter: Includes state appellate court decisions from Arkansas, Kentucky, Missouri, Tennessee, and Texas. Currently in its third series (abbreviated as S.W.3d).
- Southern Reporter: Includes state appellate court decisions from Alabama, Florida, Louisiana, and Mississippi. Currently in its third series (abbreviated as So.2d, So.3d).


## Citations  
Legal citations, in general, are used to identify the source of information supporting a particular point in a legal document (such as a motion, a brief, or a decision). Citations that refer to court decisions identify where a particular decision has been published in a reporter; they are laid out in a specific and consistent manner so that a reader can easily find the text of the decision in a reporter. The typical form of a citation to a decision includes:

1. the names of the lead parties (in most cases, the plaintiff or appellant versus the defendant or appellee), 
2. a number representing the volume of the reporter,
3. an abbreviation of the name of the reporter,
4. a second number providing the first page of the decision, and
5. in parentheses, an abbreviation for the court and the year the decision was issued. 

For example, the citation *Stearns v. Ticketmaster Corp.*, 655 F.3d 1013 (9th Cir. 2011), identifies a decision in a case between an appellant, named Stearns, and an appellee, named Ticketmaster Corporation. The citation indicates that the decision was published in volume 655 of the Federal Reporter, Third Series (identified by the abbreviation “F.3d”), beginning on page 1013. The citation also shows the decision was issued by the United States Court of Appeals for the Ninth Circuit (abbreviated as 9th Cir.), in 2011.

### The Bluebook  
The rules governing the most widely used legal citation style are found in The Bluebook: A Uniform System of Citation. Below, find a link to fuller bibliographic information about this resource in the Library of Congress Online Catalog.

- The Bluebook : A Uniform System of Citation by Harvard Law Review Association: Citation of legal authorities. Compiled by the editors of the Columbia law review, the Harvard law review, the University of Pennsylvania law review, and the Yale law journal.

### Online Legal Citation Guides
Online resources provide basic legal citation guidance, but should be used as a supplement to (not a substitute for) the Bluebook.

- [Introduction to Basic Legal Citation](https://www.law.cornell.edu/citation/), An online guide to basic legal citation by Peter W. Martin, Professor Emeritus at Cornell Law School. 
-  [Bluebook Guide](https://guides.ll.georgetown.edu/bluebook), An introduction to The Bluebook and basic concepts of legal citation, prepared by the Georgetown Law Library.

## Decisions by Topic (Digests)  
West (Thomson Reuters) is one of the major legal publishers in the United States. West's digests are helpful for locating decisions relevant to a particular topic. Federal, state, and some regional and topical digests are available. Digests are organized by topic and contain brief summaries of a decision's main issues. West calls its issue summaries Headnotes. In West's digest system, any given Headnote will be classified according to West's Key Number System.

The Key Number System categorizes the law into approximately 400 topics, and each topic is further subdivided into Key Numbers (there are approximately 100,000 distinct Key Numbers).; A topic’s classification and relevant Key Numbers may be determined by consulting the digest’s Descriptive-Word Index. Once the appropriate topic and Key Number are identified, cases on that subject may be located using that Key Number in the federal digest or any regional or state digest.

Additionally, if the name of a case and the court in which that case was decided are known, a citation may be identified in the Table of Cases volume of the appropriate digest. Supplementary pocket parts (placed in the back of bound volumes) or separate interim pamphlets are used to update the digests.  
**Note**: LexisNexis is another major U.S. legal publisher and it uses its own system of Headnotes and Summaries, although it does not publish a digest system like West does.

### Examples of Digests  
The following titles link to fuller bibliographic information in UpLaw. Links to additional online content are included when available.

- [United States Supreme Court Digest](https://lccn.loc.gov/43013732)
- [West's Federal Practice Digest 5th](https://lccn.loc.gov/2012540595)
- [District of Columbia Digest](https://uplaw.us)

## Databases and Online Resources
Court decisions often may be accessed via subscription databases and the Internet. On-site at the Library of Congress, patrons may access court decisions through Nexis Uni (formerly LexisNexis Library Express). Additionally, the Law Library Reading Room provides public access on-site to Westlaw Patron Access (which includes court decisions indexed using West's Key Number System) and Bloomberg Law Patron Access.

### Subscription Databases for Case Law Research
The following are popular paid subscription based legal research services. 

- [Bloomberg Law](https://news.bloomberglaw.com)
- (https://www.lexisnexis.com)[LexisNexis] 
- (https://www.westlaw.com/)[Westlaw]
   
### Government Websites for Case Law Research
Government websites are increasingly offering free access to court decisions online. The Library of Congress has digitized volumes of the United States Reports and makes them available to the public as part of its digital collections. The U.S. Government Publishing Office's govinfo website provides access to select federal court decisions from 2004 to the present. Additionally, many individual federal and state appellate courts' websites provide access to recent decisions issued by that court. To find links to individual federal and state court websites, use the Law Library of Congress's Guide to Law Online (GTLO).
- *[https://www.uscourts.gov/federal-court-finder/search](Federal Court Finder)*: The Administrative Office of the U.S. Courts provides a federal court finder tool to locate the websites of individual federal courts, many of which include the text of recent decisions from that court.
- *[https://www.supremecourt.gov/opinions/opinions.aspx](Opinions of the U.S. Supreme Court (1992- ))*
  - The Supreme Court of the United States provides access to recent decisions and bound volumes of the U.S. Reports from 1992 to the present.
- *[https://www.govinfo.gov/app/collection/uscourts](United States Court Opinions)*
  - A project by the U.S. Government Publishing Office and the Administrative Office of the U.S. Courts to provide access to opinions from selected U.S. courts, with content from April 2004 to the present.
- *[http://www.loc.gov/law/help/us-reports.php](United States Reports (1754-2003))*
  - UpLaw provides access to digitized bound volumes of the U.S. Reports from 1754 to 2003.

### Law Library of Congress Guides and Resources
Finally, there are several freely-available options for finding case law online.  

- [https://www.loc.gov/law/help/guide/federal/usjudic.php](Guide to Law Online: U.S. Judiciary): Bibliography of links to federal courts' websites and online repositories of decisions issued out of the federal courts.
- [https://www.loc.gov/law/help/guide/states.php](Guide to Law Online: States): Bibliography of links to state-related online resources; open the individual state to find links to state court websites and online repositories of decisions issued by the state's courts.  
- [https://guides.loc.gov/free-case-law](How to Find Free Case Law Online): Law Library of Congress guide to freely-available options for tracking down electronic case law.

## Dockets and Court Filings  
A docket is defined by the Administrative Office of the U.S. Courts as a "log containing the complete history of each case in the form of brief chronological entries summarizing the court proceedings." Every case is assigned a unique docket number, which researchers can use to find information such as the names of the parties, dates of appearances before the court, and a brief summary of the claims or charges. A docket is also useful for identifying court filings—the underlying documents (pleadings, motions, briefs, etc.) filed in a case. Although dockets and court filings are not considered "case law" and do not have precedential value, the information contained in these resources can sometimes help researchers better understand why a court issued a particular decision or opinion.  

Dockets and court filings are generally maintained by the clerk of the court where the case was filed. Some clerks have made dockets and court filings available electronically, but the availability varies by court and time period. Even those courts that have made these materials available electronically have not necessarily done so in a free and open public database—many of these databases either restrict access to members of the legal profession and/or charge for access.

Below, researchers will find information and links to select federal, state, and subscription resources for accessing dockets and court filings. For detailed guidance on how to find briefs, oral argument transcripts, and docket information for cases considered by the Supreme Court of the United States and the U.S. federal appellate courts, visit the Law Library's companion research guides:

- [U.S. Supreme Court Records and Briefs](https://guides.loc.gov/supreme-court-records-and-briefs) 
- [U.S. Federal Appellate Courts: Records and Briefs](https://guides.loc.gov/federal-appellate-court-records-briefs)

Researchers should keep in mind that for certain topics and cases of interest—including very current cases—third-party websites will often publish or link to dockets and court filings. For example, the Turtle Talk Blog External frequently posts court filings related to Indian and Tribal law, and the Election Law at Ohio State External website posts court filings in major pending election cases External. Researchers can often find these documents simply by conducting internet searches with the case name or topic combined with phrases like "court filings" or "court documents." However, the authenticity of court documents posted on third-party websites should always be checked against pleadings and rulings available from the court clerk or on official government websites.
### [Public Electronic Access to Dockets & Court Filings](https://pacer.uscourts.gov/)


#### [Federal Courts: PACER and RECAP](https://pacer.uscourts.gov/)

While some federal courts have allowed for selected records and briefs to be accessed via their websites, the most complete coverage can be found in the Public Access to Court Electronic Records (PACER) service and the RECAP Archive:

- [Public Access to Court Electronic Records (PACER)](https://pacer.uscourts.gov/): The PACER service is the federal court system's official service for electronic public access to federal court records and documents. Users must register for an account and can be charged fees for using PACER, depending on how heavily the user is accessing the service. Information about fees is available at Pricing: How PACER fees work.  
- [RECAP Archive External](https://www.courtlistener.com/recap/): The RECAP Archive is a private website that archives court filings found on PACER. The RECAP Archive is free and open to the public and fully searchable (although not every court filing on PACER is available on RECAP).

#### State Courts
Electronic access to state court dockets and filings varies from state to state. For more information, visit:  

- [National Center for State Courts: Privacy/Public Access to Court Records State Links External](https://www.ncsc.org/topics/access-and-fairness/privacy-public-access-to-court-records/state-links5): In this resource, the National Center for State Courts External has compiled a list of state and local courts that provide public electronic access to their dockets and filings.

### Subscription Databases with Dockets & Court Filings
Many subscription legal research databases provide access to dockets and court filings, but often these features are "add-on" packages, meaning that not every subscription includes full access to these resources. Of the Law Library's databases and eResources available in the Reading Room, Bloomberg Law (Patron Access) provides the most comprehensive electronic access to federal and state dockets and court filings. Nexis Uni also has a collection of "Briefs, Pleadings, and Motions," primarily from federal courts.

The subscription resources marked with a padlock are available to researchers on-site at the Library of Congress. If you are unable to visit the Library, you may be able to access these resources through your local public or academic library.

- Bloomberg Law (Patron Access) External (Restricted Access): Collection includes federal and select state and foreign jurisdiction court dockets and court documents.
- Nexis Uni External (Restricted Access): Collection includes select federal briefs, motions, pleadings, and other court documents.

