---
title | Company Structure--Holding and Operating Companies
author | casey@mclaw.io
date | 2021-11-14
category | Corporate
subcategory | LLC
tags | llc, holding company, operating company
summary | This articles provides an overview of a corporate law and how to structure your businesses.
---
The ideal business structure consists of two entities:

- an operating entity that has possession of the assets, but does not own the assets (unless they are encumbered in favor of the holding entity or owner), and
- a holding entity that actually owns the business's assets.

To some extent, an operating entity's assets can be protected using only a single entity and leases, loans and liens, as well as through the use of a separate holding company. This is an example of multi-layered protection. That said, the simplest option---the one-entity approach---generally does not provide the flexibility and asset protection of the multiple-entity approach. It comes down to how willing you are to risk everything you work for in order to avoid a little bit of effort and paperwork.

## Different Organizational Structures
Starting a business is an exciting venture, but before you can start selling products or services, you need to choose the right legal structure for your company. Each legal structure has its advantages and disadvantages, so it is crucial to understand the differences between them to make an informed decision. In this blog post, we will discuss the differences between LLC, PLLC, S-Corporation, C-Corporation, Partnership, and Individual Proprietor.

##### LLC (Limited Liability Company)
An LLC is a type of business structure that combines the pass-through taxation of a partnership or sole proprietorship with the limited liability protection of a corporation. Owners of an LLC are called members, and they are not personally liable for the company's debts or obligations. LLCs have the flexibility to choose their tax treatment, either as a sole proprietorship, partnership, S-Corporation, or C-Corporation.

##### PLLC (Professional Limited Liability Company)
A PLLC is similar to an LLC, but it is reserved for licensed professionals such as lawyers, doctors, and accountants. The main difference is that a PLLC offers liability protection for the professional's work but not for any malpractice or professional negligence.

##### S-Corporation
An S-Corporation is a type of corporation that allows for pass-through taxation, similar to an LLC. However, an S-Corporation is limited to 100 shareholders who must be U.S. citizens or residents. Owners of an S-Corporation pay themselves a reasonable salary and then receive any additional profits as distributions, which are not subject to self-employment tax.

##### C-Corporation
A C-Corporation is a separate legal entity from its owners and is taxed independently from its owners. Shareholders of a C-Corporation have limited liability protection, and the company can have an unlimited number of shareholders. However, C-Corporations are subject to double taxation, meaning that the company's profits are taxed at the corporate level, and any distributions to shareholders are taxed again at the individual level.

##### Partnership
A partnership is a business owned by two or more people who share the profits and losses of the company. Partnerships can be either general partnerships, where all partners have equal liability, or limited partnerships, where there is at least one general partner who is personally liable for the company's debts and obligations. Partnerships are also subject to pass-through taxation.

##### Individual Proprietor
An individual proprietor is a single-person business, also known as a sole proprietorship. The owner has full control over the business and is personally responsible for all debts and obligations of the company. An individual proprietorship is also subject to pass-through taxation.

In conclusion, choosing the right legal structure for your business is an essential step that can have significant consequences for your company's success. Each legal structure has its own advantages and disadvantages, so it is important to consider your company's goals, risks, and financial situation when deciding which one is the best fit for your business. We hope this post has helped you understand the differences between LLC, PLLC, S-Corporation, C-Corporation, Partnership, and Individual Proprietor, and has given you a solid foundation for making an informed decision about your business's legal structure.

### Using multiple business entities

Using holding and operating companies is an asset protection planning strategy that helps to limit liability in your business structure. As noted earlier, the ideal business structure consists of an operating entity that does not own any vulnerable assets and a holding entity that actually owns the business's assets. With this structure, the small business owner can eliminate or substantially limit liability for both business debts and personal debts.

The operating entity conducts all of the business's activities and, thus, bears all the risk of loss. The owner's limited liability for business debts turns out to be no liability at all, because the operating entity contains little or no vulnerable assets, and the holding entity is not legally responsible for the other entity's debts. At the same time, the owner's liability for personal debts is reduced because assets are within the protective framework of a business form (i.e., the holding entity).

### Advantage of using an LLC

### What are your options for creating entities

The individual owner can create and fund the holding entity. The holding entity can then create and fund the operating entity. Technically, the individual owns the holding entity, and the holding entity owns the operating entity. This is the approach taken frequently with corporations, where the operating entity is a subsidiary of the holding entity. However, the same approach also can be used with respect to the LLC.

This strategy is more suited toward the operation of two limited liability companies (LLCs), as opposed to two corporations, provided that the holding LLC is formed in a state that has adopted the Revised Uniform Limited Partnership Act view preventing foreclosure and liquidation of the business interest to satisfy a personal creditor.

Two corporations will not accomplish protect business assets from personal creditors because the law allows personal creditors to attach, and then vote, the owner's interest, to force a liquidation of the corporation. However, the statutory close corporation does provide another option. If it is formed as the operating entity and coupled with an LLC formed as the holding entity, you can achieve the same protections.

Alternatively, the owner could personally create and fund both entities, so that he directly owns both entities. Or the owner could decide to use the one-entity approach, although this structure provides very little protection for your business and personal assets.

_Tip_: Thirteen states have adopted Series LLC statutes. These statutes present an ideal and unique opportunity to form all of the separate entities within a single LLC.

The better approach will usually be for the holding entity to own the operating entity. The multiple entities are then strategically funded to minimize vulnerable assets within the business form.

### Effective use of the holding entity

In the multiple-entity approach, the holding entity is where all wealth is located within the business structure. But because the holding company conducts no business activities, it has almost no exposure to liability, and therefore these assets are protected.

The small business owner or owners create the holding entity. Then, in turn, the holding entity creates and owns the operating entity, where actual business operations (and risks) occur. Limited liability for the operating company runs to the holding entity and is limited to its investment in the operating entity, stopping short of the owners of the holding company because they do not own the operating company. One holding entity may be used to operate many different operating companies, but care should be taken to keep each operating company and its activities separate from one another.

When funding the entities, ideally, a business's most valuable assets should be owned by the holding company and leased to the operating company, which secures the assets from creditors and provides a way of taking vulnerable cash out of the operating company.

Further, the holding entity can loan money to the operating company to buy other business assets, but it should secure the collateral for the mortgage with liens that run to the holding company. Again, the assets are secured because the holding company is a priority lien holder, and vulnerable cash is taken out of the operating company through loan repayment.

When properly structured, the multiple-entity approach is successful because it seeks to maximize wealth within the entity with no liability issues, and minimize assets with the entity taking all the risks. And because the holding company itself, and not its owners, creates and funds the operating company, the holding company is liable for the operating company's debts, but only up to the amount it has invested, if it is in a business form that offers limited liability, such as the limited liability company (LLC).

### Effective use of the operating entity

When using holding and operating companies in a multiple-entity business structure, your operating entity is your primary business entity. All business functions occur within that company. Likewise, all of the risks to the business will occur within that entity as well.

It is important from an asset protection standpoint to minimize vulnerable assets and cash within the entity through continuous withdrawal strategies. These should be in place and operating as part of the normal course of business.

In addition, separate operating entities should be formed for each operating activity, so that any liability runs only to that particular entity's assets. A "series LLC" is especially suited for the use of multiple entities.

### Using series LLCs in a multiple entity structure

If you are considering using holding and operating companies in a multiple-entity business structure, the pioneering Delaware limited liability company (LLC) statute provides for incomparable flexibility and simplicity in operating LLCs. It clearly allows for the establishment of "series LLCs" which allow different classes of interests, including voting and nonvoting interests.

The Delaware law was so successful that several other states have passed similar statutes.

**States Permitting "Series LLCs"**
Delaware 	Illinois 	Iowa
Kansas 	Minnesota* 	Nevada
North Dakota* 	Ohio 	Oklahoma
Tennessee 	Texas 	Utah
Wisconsin* 	*allowed, but no liability shield between the interests

These statutes generally allow a single LLC to house multiple separate entities. Thus, the holding entity and each operating entity can be formed within a single LLC.

Each unit can have separate owners and its own classes of ownership interests. Each unit can own its own assets and incur its own liabilities. Each unit should have its own accounting system, which could simply consist of separate files within a single accounting system. Importantly, the recordkeeping must be done as if each entity were organized as a separate LLC.

The designation of the units, or "series" of separate entities within the single LLC as they are referred to in the statutes, must be done in the articles of organization. This designation serves as constructive notice that each unit is a separate legal entity and that, accordingly, the other units are not liable for its debts. When the single LLC registers to do business in the owner's home state, or wherever it will conduct operations, this registration will also serve as constructive notice in those states, as the registration is a link to the original articles that were filed in one of the Series LLC Statue states noted above.

As a cautionary note, although Series LLCs are often used in real estate businesses and can be applied in other industries, do not attempt to create this kind of multiple entity before getting expert legal counsel. Series LLC are very newish animals and there is no case law to guide us in their care and feeding. No one knows how the IRS might deal with the vagaries and technicalities that might arise from this new form.

It is essential that the registration be done properly to separate liability among the entities. Consistent with the generally flexible in most Series LLC statutes, each unit does not have to be immediately funded. They can be held in abeyance for future use.

**Warning**

Remember, professionals can form an LLC, limited liability partnership (LLP) or a corporation only if all of the owners are licensed within the same profession.

Keep this in mind if you're a professional (physician, dentist, attorney) forming a holding entity and an operating entity.

Only the operating entity has to meet this requirement. The holding entity, which will contain nearly all of the wealth of the business, will not be engaged in the practice of any profession. Thus, children or other family members, for example, can still be co-owners of the holding company, even when it is formed by professionals. This allows the use of a family LLC as an estate planning tool. However, in this case, the professional would have to form each entity directly, because the holding entity could not be the owner of the operating entity.

If the entities were being formed within a single LLC in Delaware, for example, the holding entity would have to be formed as a separate LLC in this situation. Each operating entity could still be formed within the single LLC.

Obviously there are additional costs involved in creating two or more entities rather than one. However, the concept of an entity within an entity, embodied Series LLC statutes, can significantly lessen these costs. Moreover, these costs, which really are relatively modest, represent a type of inexpensive insurance against the risk of loss.

### Securitization requires multiple entities

Strategies that rely on the use of an operating entity and a holding entity also are used by large businesses. For example, one of the fast growing areas in corporate finance is called "securitization."

A corporation, the operating company, sells its receivables to a second corporation, which is created as the holding company. The only real asset of the holding company is the receivables it purchases. The holding company sells stock to the public, in effect allowing the public to buy an interest in the receivables, through the purchase of the stock. This is termed securitization. This trend started with the sale of mortgages by banks, an ironic choice of words in today's economic environment! Large corporations now sell accounts receivables in this way.

The holding company (the master LLC so to speak) is insulated from liability for all of the activities of the operating company that created the accounts receivable. Commentators have said that, if it were not for the creation of a holding entity, securitization could not work, because the risk of liability exposure from the operating entity's activities would be too high to enable this kind of stock offering to the public. (Commentators or false prophets...that's for history to determine.)

At this same time, the operating entity has protected its assets against the claims of its creditors. Cash that is brought in from the sales of the receivables is quickly drawn off to pay the operating entity's expenses, including the salaries of its owners. The small business owner might use a version of this strategy in withdrawing assets from the operating entity, but only after careful thought and planning.

