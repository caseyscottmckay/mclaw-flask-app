 <guide>  
 <title>TradeMark Registration Guide</title>  
 <description>Filing for a trademark can protect your brand. This simple guide talks you through  
  filing the paperwork, what it costs, and more.</description>  
 Your brand is an essential part of your business. Filing a trademark isn't a simple process, but the protection it affords your brand is worth your time and effort.
 To register a trademark with the U.S. Patent and Trademark Office (USPTO), you will need to fill out and submit a trademark application.  
 You can do this online, using the Trademark Electronic Application System (TEAS), an online trademark filing service, or you can submit a paper application.  
 Your trademark application will need to include the following information.  
 
 ##### Owner of the Mark and Contact Information  
 If your business is a formal legal entity such as a corporation or LLC, the name of the owner is the business name. An individual can also own a trademark.  
 For contact information, you will need at least a mailing address, but you may also include an email address, fax number, and other methods of contact.  
 ##### Type of Mark  
 There are three types of mark formats. You must choose only one of the following formats:  
     Standard character format. Use this format to register words, letters or numbers, or combinations thereof, without any particular font or formatting. A standard character format trademark allows you to use your trademark in any style or design.  
     Special character format. Use this format if your mark has a design element or particular stylized words, letters or numbers that you want to protect. Examples might include a logo, a symbol or words in a particular font, color or typeface, such as Apple’s apple symbol or Coca-Cola’s unique typography that appears on their soda cans.  
     A sound mark. Examples include the three-note chime for NBC, the lion’s roar for MGM and Intel’s sequence used to denote its Pentium processor.  
 ##### A Drawing and Specimen  
 The trademark application asks for a drawing of your mark and a specimen of your mark.  
 ###### Drawing  
 A drawing of your mark is a visual depiction of what your mark looks like, by itself, without anything else around it and without it being attached to a product.  
 If you are applying for a trademark in standard character format, your drawing will consist of typed letters, numbers or punctuation.  
 If you are applying for a trademark in a special character format, your drawing will be a .jpg file showing the mark you want to register.  
 ###### Specimen  
 A specimen of your mark shows how you use the mark in connection with your goods or services.  
 For example, if you sell t-shirts, you might submit a picture of your label attached to one of your shirts. If you provide services, you can send a marketing brochure or advertising materials containing your mark.  
 If you use a sound to brand your products, you may include a video clip, such as the roaring MGM lion, that contains only your mark.  
 If you are applying for a trademark in a sound or motion video formation, you must submit the specimen as an electronic file in one of the following formats: .wav, .wmv, .wma, .mp3, .mpg, or .avi.   
 The specimen cannot exceed 5 MB in size for audio files or 30 MB for video clips because the TEAS cannot accommodate larger files.  
 ##### Identifying your goods or services  
 You must accurately identify the goods or services associated with your mark. If you make a mistake, you cannot change or add additional goods or services later.  
     You should only identify goods or services for which you are currently using your mark, or for which you actually plan to use your mark in the near future.  
 Your trademark application must correctly identify whether your mark is used for goods or services: “Goods” are products that you sell, while “services” are activities that you perform for other people.   
 Your application must also describe the goods or services.  
 The USPTO’s Trademark Manual of Acceptable Identification of Goods and Services contains descriptions of acceptable identifications.  
 If you can find an accurate description of your good or service in the manual, you should use it; otherwise you can describe your good or service in your own words. You can search the manual on the USPTO website.  
 ##### Trademark Filing Basis   
 You must choose a filing basis from among four possible choices:  
     Use in Commerce (Section 1(a)). Use this filing basis if you are already using your mark “in commerce,” meaning in connection with goods or services that you offer across state lines or internationally. If you only offer your goods or services within one state, you can still file for this type of trademark.  
     Intent to Use (Section 1(b)). Use this filing basis if you plan to use your mark in commerce within the next three or four years. You will need to begin using the trademark and provide a specimen before it can be registered.  
     Foreign application exists for the same goods or services (Section 44(d)). Use this filing basis if you are registering a foreign application for the same trademark within six (6) months of filing the U.S. application.   
     Foreign registration exists for the same goods or services (Section 44(e)). Using this filing basis if a foreign registration for the mark already exists in a defined treaty country. If it does, you must provide all the relevant information in the U.S. application, including a copy of the foreign registration certificate.  
 ##### Trademark Application Fees  
 At the time of publication, the filing fee for a trademark application is $225–$400 per class of goods and/or services. The fee is not refundable if your application is rejected and could change.  
 Check with the USPTO for the most recent fees.  
 ##### What Happens After You File?  
 Your application will be assigned to an examining attorney, who will typically begin reviewing it within three months.   
 If the attorney identifies issues with your application, you may receive an Office action (a notification that there is a problem with your application), to which you must respond before your application can move forward.   
 After any issues identified by the examining attorney have been resolved, your mark will be published in The Trademark Official Gazette. That is the weekly USPTO publication that lists newly published marks as well as canceled and renewed registrations.   
 During the application process, other trademark owners could formally oppose your application. If a corporation feels your logo is too similar to their own, for example, they may oppose you.  
 This would slow up your registration process, but it doesn’t mean your application is automatically dismissed. If no one opposes your registration, or if an opposition is unsuccessful, your mark will then be registered.   
 Depending on the specific circumstance of the trademark application, it can take anywhere from  
  several months to a few years for a registrant to respond to questions from the examining attorney and obtain final approval and trademark registration.  
  </guide>  