Rights of Publicity Basics|United States|Intellectual Property and Technology|Patent|patent law|If you are starting a new business, these legal documents for startup can make the difference between a successful venture and one that is headed for failure.

The Right of Publicity: Whose Right Is It?




 
 
 
  
 
   
What are our rights to our own images, likenesses, and stories, commonly known in the legal world as one's “right of publicity"?
 
   
Celebrities confront this issue all the time. Their public images are their stock in trade and they spend endless hours honing and protecting them. When someone not authorized to use that image goes ahead and uses it anyway, they enter the legal miasma called “right of publicity."
 
   
 
    - Can anybody use your name and likeness without your permission?
 
    - Does it depend on who “you" are, or how your image is being used?
 
    - Do media companies have to own the rights to someone's life story before adapting it for the screen?
 
    - And does your right of publicity die with you?
 
   
 
   
Read on.
 
   
__ Defining the Right of Publicity

 
   
Under many 
state statutes
, the right of publicity prevents the use of another's name, image, likeness, or other recognizable aspects of his or her persona for commercial gain without permission. Plainly put, this body of law grants an individual the right to control commercial use of his or her identity, although the specifics vary by state.
 
   
The right of publicity, where it exists, applies to everyone, though we often think of it in terms of famous people because their names, images, and likenesses tend to be worth more than those of the not-so-famous among us.
 
   
Not all states have a delineated right of publicity but may include it under the broad umbrella of privacy. Invasions of privacy, according to the Second Restatement of Torts, are intrusion, appropriation of name or likeness, unreasonable publicity, and false light, with the right of publicity most closely aligned with 
appropriation of name or likeness
.
 
   
__ The Right of Publicity and Intellectual Property

 
   
Right of publicity is often closely associated with intellectual property, a category that includes concepts like copyrights and trademarks.
 
   
Trademark law, in particular, has much in common with the right of publicity, as at the core of both is the prevention of unfair competition through the suggestion of a connection between two entities where such a connection does not actually exist.
 
   
In both areas of the law, the idea is to stop unfair gain through the use of something that people associate with the business of another. What business? Can someone's name or face be a business? Two words: celebrity endorsements.
 
   
__ Right of Publicity Cases Through the Years

 
   
Far from a rarity in American jurisprudence, there have been many right of publicity lawsuits over the years, most of which have involved celebrities.
 
   
Some of the most famous examples arose out of companies using celebrity impersonators to sell products without the impersonated celebrities' permission. 
Bette Midler
 and 
Tom Waits
 both secured large damage awards (from Ford Motor Company and Frito-Lay, respectively) when they brought right of publicity claims for vocal impersonators of the stars hawking Mercury Sables and Salsa Rio Doritos.
 
   
According to professor Jonathan Faber at 
RightOfPublicity.com
, the Supreme Court has only dealt with one right of publicity case, and it involved 
Hugo Zacchini
, the “human cannonball," in 1977. The Court found that a local television station violated Zacchini's right of publicity when it aired his entire 15-second performance on the news, adversely affecting the public's desire to pay for and attend the show in person.
 
   
More recently, sports-related claims have dominated right of publicity news. Video game maker Electronic Arts (EA) 
settled lawsuits
 brought against it by former college athletes over games in which the company used the players' names and other identifiable characteristics. And, from the golf course, 
Tiger Woods
 lost an appeal over the use of his image in a lithograph by artist Rick Rush.
 
   
__ Owning Rights to Life Stories

 
   
One interesting question related to the right of publicity is whether someone needs to own the rights to someone else's life story in order to produce a work based on it. The answer is usually, technically, no—because the resulting story would be based on facts available in the public domain.
 
   
But there are lots of good reasons to pursue the rights to someone's life story, not the least of which is to procure a better story, that is, something beyond simple facts, from the person who actually lived it. You also can protect yourself from a lawsuit should the persons involved later decide he or she didn't like their depiction in the resulting work. Bigger media companies, for obvious financial and legal reasons, often prefer to have these protections in place.
 
   
In one noted life story rights case, Jeffrey Sarver, an Iraq war veteran, sued the makers of 
The Hurt Locker
, alleging that they used his life story without permission as the basis for the movie's plot. 
He lost his case
, badly.
 
   
In what could be the next big lawsuit in this area, an attorney for Mexican drug lord 
Joaquín “El Chapo" Guzmán
 has already threatened legal action against Univision and Netflix if they move forward on plans for a TV series on his client's life without compensating him.
 
   
__ Right of Publicity Isn't Just for the Famous—or the Living

 
   
One doesn't have to be famous for a 
right of publicity issue
 to arise—or, in some places, even alive.
 
   
In a number of jurisdictions, the 
right of publicity may survive the death
 of the person, with the number of years post-mortem included in statutory language ranging from Virginia's 20 years to Indiana's 100. 
Minnesota 
will reportedly consider similar legislation soon, inspired by the death of native son and music legend Prince in 2016.
 
   
Has anyone been using your or a loved one's name, image, or likeness without permission and for commercial gain? It just might be time to learn more about the right of publicity and how to protect it. LegalZoom's 
intellectual property comparison chart
 is a good place to start.
 
  
 
 
 




