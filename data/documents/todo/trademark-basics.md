Trademark Basics|United States|Intellectual Property and Technology|Trademark|trademark|UpLaw explores the latest topics in law by leading a culture of innovation.
<article role="article" about="/trademarks-getting-started/trademark-basics"
         class="node node--type-page node-page node--view-mode-full view-mode-full">
    


        

            
What you should know before filing


            #### The trademark application process is a legal proceeding governed by U.S. law


            

                - If you are a __ 
foreign-domiciled applicant

, you
                    must have a U.S.-licensed attorney represent you at the USPTO.
                

                - If you are an __ applicant domiciled in the United States
, you are not required to
                    have a U.S.-licensed attorney represent you, but we strongly encourage you to hire one who
                    specializes in trademark law to guide you through the application process.
                

                - 
Foreign attorneys and non-attorneys are not permitted to provide legal advice,&nbsp;help you fill out a form, sign documents for you, or otherwise take action on your application for you.

                

                - Hiring someone who cannot represent you at the USPTO could delay review of your application and
                    jeopardize the validity of any resulting registration.
                

            

            #### Your application must meet many legal requirements before your trademark can be registered&nbsp;


            

                - Is your trademark federally registrable? Can you properly identify your goods or services? Can you
                    identify the proper filing basis for your application?&nbsp;<span
                            style='font-family: "Segoe UI", Helvetica, Tahoma, Arial, sans-serif; font-size: 14px;'>If you&rsquo;re not sure how to answer these and other questions, review&nbsp;this webpage to avoid mistakes that cost you time, money, and potentially your legal rights.

                

                - The&nbsp;<a href="http://www.uspto.gov/trademarks-application-process/filing-online/teas-tutorial"
                                style='font-family: "Segoe UI", Helvetica, Tahoma, Arial, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; text-decoration-line: underline;'>TEAS
                    new application tutorial
&nbsp;takes you through the necessary steps before you file through
                    filling out your online application form.
                

                - The&nbsp;
trademark
                    registration process
&nbsp;is a legal proceeding that requires you to act within strict deadlines
                    (based on&nbsp;<a
                            href="http://www.uspto.gov/custom-page/trademark-electronic-application-system-teas-home-page-available-public">Eastern
                        Time
). See the <a
                            href="https://www.uspto.gov/trademark/trademark-timelines/trademark-application-and-post-registration-process-timelines">trademark
                        application and post-registration timelines
.
                

                - Information you submit will become public record and will permanently remain 
searchable in USPTO online databases, Internet search engines, and other databases.&nbsp;
&nbsp;This
                    includes your name, phone number, email address, and street address. &nbsp;For more, see the&nbsp;<a
                            href="http://www.uspto.gov/trademarks-application-process/faqs-personal-information-trademark-records">FAQs
                        on Personal Information in Trademark Records.


            

            

            <h2 id="heading-2"
                style='font-family: "Segoe UI", Helvetica, Tahoma, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal;'>
                Basic facts about Trademarks: What every small business should know now, not later


            

                This simulated presentation features reporters from the USPTO's news broadcast-style video series, the
                Trademark Information Network (TMIN). While the setup is staged for educational purposes, the
                information provided is real.


            

                
The video is a must for anyone interested in starting a business to sell a product or offer a service. It highlights the important role of trademarks in that process, including a discussion of how trademarks, patents, copyrights, domain names, and business name registrations all differ. It gives guidelines on how to select the right mark&mdash;one that is both federally registrable and legally protectable. It also explains the benefits of federal registration and suggests
<span
                    style='font-family: "Segoe UI", Helvetica, Tahoma, Arial, sans-serif; font-size: 14px;'>&nbsp;free and reduced-price resources that can help you with your trademark
<span
                    style='font-family: "Segoe UI", Helvetica, Tahoma, Arial, sans-serif; font-size: 14px;'>. By the end of the video, you'll understand why having a trademark component of your business plan is critical to your success.

            


            
NOTE: The run time for this video is approximately 42 minutes, so please allow adequate viewing time. If
                you do not have time to watch at one time, you may wish to watch the <a
                        href="/trademarks-getting-started/trademark-basics/basic-facts-about-trademarks-videos">Basic
                    Facts about Trademarks animated series
, which covers the same information using user-friendly
                visuals in shorter video segments. Alternatively, if you wish to read the information (instead of watch
                a video), you may download the __ <a href="http://www.uspto.gov/sites/default/files/BasicFacts.pdf"
                                                          target="_blank">Basic Facts About Trademarks
                    booklet

 that covers the same material.


            
After watching this video, if you have any questions about proper mark selection (one that is both
                federally registrable and legally protectable) or the benefits of federal registration, please feel free
                to email 
TMFeedback@uspto.gov
. The Office may
                answer&nbsp;__ general questions
__ ,
 but may not provide specific legal
                advice.&nbsp;For legal advice, please consider contacting a <a
                        href="http://www.uspto.gov/trademarks-getting-started/using-private-legal-services">__ private
                    attorney 

who specializes in intellectual property.


            
Before filing your application, be sure to watch the <a
                    href="http://www.uspto.gov/trademarks-getting-started/process-overview/trademark-information-network">Trademark
                Information Network
 (&ldquo;TMIN&rdquo;), the USPTO&rsquo;s news broadcast-style video series that
                covers important topics and critical application filing tips.


            
&nbsp;


            
&nbsp;


            <div data-embed-button="video" data-entity-embed-display="view_mode:media.media_teaser"
                 data-entity-type="media" data-entity-uuid="864e4ee1-022b-492a-abcc-eaa33c2f0ff4" data-langcode="en"
                 class="embedded-entity">
                

                    

                        


                    


                    

                        <iframe src="https://www.youtube.com/embed/qHDRV2NTSEk" frameborder="0" width="480"
                                height="270">

                    


                    ## Basic Facts About Trademarks: What Every Small Business Should Know

                    

                        

                            ##### Other viewing options:

                            

                                
Enhanced
                                    accessibility

                            


                        

                        

                            ##### Other resources:

                            

                                <a href="/sites/default/files/5_What_Every_Small_Business_Should_Know_Now%20%281%29.txt"
                                   download="">Download a printable transcript




                        

                    

                

            

            
&nbsp;


            
&nbsp;


            
&nbsp;


            

            
What is a Trademark or Service Mark and How do they differ from Patents and
                Copyrights?


            
A trademark is a brand name. A trademark or service mark includes any word, name, symbol, device, or any
                combination, used or intended to be used to identify and distinguish the goods/services of one seller or
                provider from those of others, and to indicate the source of the goods/services.


            
This animated video explains how trademarks, patents, copyrights and also domain names and business names
                all differ. [run time: 8:38]


            
&nbsp;


            
&nbsp;


            <div data-embed-button="video" data-entity-embed-display="view_mode:media.media_teaser"
                 data-entity-type="media" data-entity-uuid="02800012-40d6-4dad-9255-a9b8d5af9c97" data-langcode="en"
                 class="embedded-entity">
                

                    

                        


                    


                    

                        <iframe src="https://www.youtube.com/embed/4cIBcl7dD4w" frameborder="0" width="480"
                                height="270">

                    


                    ## Basic Facts: Trademarks, Patents, and Copyrights

                    

                        

                            ##### Other viewing options:

                            

                                
Enhanced
                                    accessibility

                            


                        

                        

                            ##### Other resources:

                            

                                <a href="/sites/default/files/1_Trademarks_Patents_%20and_Copyrights_Transcript%20%282%29.txt"
                                   download="">Download a printable transcript




                        

                    

                

            

            
&nbsp;


            
&nbsp;


            
It is important to understand whether you should file for a trademark/service mark, a patent, and/or a
                copyright. While all are types of intellectual property, each protects something very specific. In
                addition to watching the video above, you can study how <a
                        href="/trademarks-getting-started/trademark-basics/trademark-patent-or-copyright">trademarks,
                    patents, and copyrights
 differ to ensure you are making the proper filing decision at the outset
                of the filing process.

                &nbsp;


            

            
How do I select a strong mark?


            
It is vitally important that you select or create a trademark that is both federally registrable and
                legally protectable. This animated video will help you understand how a strong trademark identifies the
                source of your goods and services, as well as distinguishes them from the goods and services of others.
                [run time 10:55]


            
&nbsp;


            
&nbsp;


            <div data-embed-button="video" data-entity-embed-display="view_mode:media.media_teaser"
                 data-entity-type="media" data-entity-uuid="fe7040a3-404a-4b22-a524-91278a9fbb2a" data-langcode="en"
                 class="embedded-entity">
                

                    

                        


                    


                    

                        <iframe src="https://www.youtube.com/embed/z4jmtzR4_NU" frameborder="0" width="480"
                                height="270">

                    


                    ## Basic Facts: Selecting A Mark

                    

                        

                            ##### Other viewing options:

                            

                                
Enhanced
                                    accessibility

                            


                        

                        

                            ##### Other resources:

                            

Download a
                                printable transcript




                        

                    

                

            

            
&nbsp;


            
&nbsp;


            
&nbsp;


            

            
Should I register my Trademark?


            
Although federal registration of a mark is not mandatory, it has several advantages, including notice to
                the public of the registrant's claim of ownership of the mark, legal presumption of ownership
                nationwide, and exclusive right to use the mark on or in connection with the goods/services listed in
                the registration.


            
This animated video explains the benefits of a federal trademark registration and how to put people on
                notice that your mark is registered with the USPTO. [run time 5:33]


            
&nbsp;


            
&nbsp;


            <div data-embed-button="video" data-entity-embed-display="view_mode:media.media_teaser"
                 data-entity-type="media" data-entity-uuid="bdf355ba-3848-458c-bd1b-d0e7324b90fb" data-langcode="en"
                 class="embedded-entity">
                

                    

                        


                    


                    

                        <iframe src="https://www.youtube.com/embed/6qzZL2yfUgI" frameborder="0" width="480"
                                height="270">

                    


                    ## Basic Facts: Should I Register My Mark?

                    

                        

                            ##### Other viewing options:

                            

                                
Enhanced
                                    accessibility

                            


                        

                        

                            ##### Other resources:

                            

Download
                                a printable transcript




                        

                    

                

            

            
&nbsp;


            
&nbsp;


            

            
Hire a Trademark attorney?


            
If you are a 
foreign-domiciled
&nbsp;applicant, you must have a
                U.S.-licensed attorney represent you at the USPTO. Non-U.S.-licensed attorneys and non-attorneys may
                give you inaccurate information and legal advice about your trademark rights and the registration
                process in the United States.


            
If you are domiciled in the United States, you are not required to have a U.S.-licensed attorney
                represent you, but we strongly encourage you to hire one who specializes in trademark law to guide you
                through the application process. Although USPTO staff can provide information about the federal
                application process, USPTO employees cannot give you legal advice or help you fill out forms.


            
<a data-entity-substitution="canonical" data-entity-type="node"
                  data-entity-uuid="888826d8-9378-4c16-b666-2e84b0ade3e7"
                  href="/trademarks-getting-started/why-hire-private-trademark-attorney"
                  title="Hiring a U.S.-licensed attorney">Why hire a private trademark attorney



            
This animated video explains what the Trademark Office does and does not do. It also explains what kind
                of assistance is available to you during the application process, as well as factors to consider when
                deciding whether to hire an attorney to represent you. [run time: 11:36]


            
&nbsp;


            
&nbsp;


            <div data-embed-button="video" data-entity-embed-display="view_mode:media.media_teaser"
                 data-entity-type="media" data-entity-uuid="af328b6f-f49e-48f4-8188-c2fc9d4a4c0c" data-langcode="en"
                 class="embedded-entity">
                

                    

                        


                    


                    

                        <iframe src="https://www.youtube.com/embed/oXuNW-c3yLY" frameborder="0" width="480"
                                height="270">

                    


                    ## Basic Facts 05: How Do I Get Help With My Application?

                    

                        

                            ##### Other viewing options:

                            

                                
Enhanced
                                    accessibility

                            

                            

                                
Spanish subtitles

                            

                        

                        

                            ##### Other resources:

                            
<a href="/sites/default/files/video_transcripts/4_How_Do_I_Get_Help_Transcript.txt"
                                    download="">Download a printable transcript




                        

                    

                

            

            
&nbsp;


            
&nbsp;


            

            
Other initial considerations


            
Before starting the application process, it is important to have clearly in mind (1) the mark you want to
                register; (2) the goods and/or services in connection with which you wish to register the mark; and (3)
                whether you will be filing the application based on actual existing use of the mark or a bona fide
                intention to use the mark in the future. This will make your search of the USTPO database more useful
                and may simplify the application process. More details on mark types, goods and services, filing basis,
                and searching are provided in the next four sections.&nbsp;


            

            
Identifying your mark: Select one of three possible formats


            
An important consideration is the depiction of your mark. Every application must include a clear <a
                    href="/trademarks-getting-started/trademark-basics/representation-mark">representation of the
                mark
 you want to register. We use this representation to file the mark in the USPTO search records
                and to print the mark in the Official Gazette (OG) and on the registration certificate. The OG, a weekly
                online publication, gives notice to the public that the USPTO plans to issue a registration.


            
For further information about this topic, please watch a news broadcast-style video on "Drawing Issues"
                (video #5 in the 
Trademark Information Network (TMIN) series
).&nbsp;
            


            

            
Identifying your goods and/or services


            
Once you have chosen your mark, you must also be able to identify the goods and/or services to which the
                mark will apply, clearly and precisely. The identification of goods and/or services must be specific
                enough to identify the nature of the goods and/or services. The level of specificity depends on the type
                of goods and/or services. For examples of acceptable identifications, please consult the <a
                        href="https://tmidm.uspto.gov/">Acceptable Identification of Goods and Services Manual
 (ID
                Manual). __ NOTE: 
Under U.S. Trademark law, class headings from the <a
                        data-entity-substitution="canonical" data-entity-type="node"
                        data-entity-uuid="77692201-afca-4de1-9a93-d509e538f7bc"
                        href="/trademark/trademark-updates-and-announcements/nice-agreement-current-edition-version-general-remarks"
                        title="Nice Agreement current edition version - general remarks, Class headings and explanatory notes">International
                    Schedule of Classes of Goods and Services
 by themselves are __ not 
acceptable for
                registration purposes. The specific items of goods and/ or services must be listed.


            
<span
                    style='font-family: "Segoe UI", Helvetica, Tahoma, Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: 400; font-variant-ligatures: normal; font-variant-caps: normal;'>Applications for trademarks used on regulated products (e.g. cannabis, drug paraphernalia, ivory, whalebone) and activities (e.g. gambling and wagering, retail stores featuring controlled substances) are subject to additional review. See our <a
                    href="https://www.uspto.gov/trademark/laws-regulations#Regulated%20products%20and%20activities">Laws and Regulations
 page for more information.&nbsp;

            


            
For further information about this topic, please watch a news broadcast-style video on "Goods and
                Services" (video #6 in the 
Trademark Information Network (TMIN)
                    series
).&nbsp;


            

            
Searching marks in USPTO database


            
You should 
search
                the USPTO database
 before filing your application, to determine whether anyone already claims
                trademark rights in a particular mark through a federal registration. Failure to conduct a proper search
                may result in your not making a proper assessment as to whether an application should even be filed.


            
For further information about this topic, please watch a news broadcast-style video on "Searching" (video
                #3 in the 
Trademark Information Network (TMIN) series
).&nbsp;
            


            

            
Identifying the proper "Basis" for filing a Trademark application


            
A trademark application must specify the proper <a
                    href="/trademarks-getting-started/trademark-basics/basis-filing">"basis" for filing
, most likely
                either a current use of the mark in commerce or on an intent to use the mark in commerce in the future.
                Understanding the distinction between these filing bases, and the implications of selecting one, are
                important considerations before starting the application process.


            
For further information about this topic, please watch a news broadcast-style video on "Filing Basis
                Information" (video #7 in the 
Trademark Information Network
                    (TMIN) series
).&nbsp;


            

            
Filing a Trademark application


            
You may <a data-entity-substitution="canonical" data-entity-type="node"
                          data-entity-uuid="58bbb257-7367-44d4-a75c-2e2440658cc1"
                          href="/trademarks-application-process/filing-online" title="Apply online">file your trademark
                application online
 using the Trademark Electronic Application System (TEAS).


            
__ NOTE: For short videos explaining how to fill out the application, access the <a
                    href="/trademarks/process/TEAS_Nuts_and_Bolts.jsp">TEAS "Nuts and Bolts" video
                series
.&nbsp;



            

            
Paying required fees


            
What the proper filing fee is for your application will be based specifically on__  
<a
                    data-entity-substitution="canonical" data-entity-type="node"
                    data-entity-uuid="7f10f5b8-b173-4bf7-b29d-46afc9fc6c6f" href="/trademark/trademark-fee-information"
                    title="Trademark fee information">three distinct factors
.__  
It is important to
                understand these variables, as it may impact how you may wish to proceed. Also, be aware that the filing
                fee is a processing fee that the USPTO will __ not 
refund, even if you ultimately do not
                receive a registration for your mark. View <a data-entity-substitution="canonical"
                                                              data-entity-type="node"
                                                              data-entity-uuid="7f10f5b8-b173-4bf7-b29d-46afc9fc6c6f"
                                                              href="/trademark/trademark-fee-information"
                                                              title="Trademark fee information">trademark fee
                    information
.&nbsp;


            

            
Monitoring status of your application


            
You should monitor the progress of your application through the 
Trademark
                Status and Document Retrieval (TSDR)
 system. It is important to check the status of your application
                every 3-4 months after the initial filing of the application, because otherwise you may miss a filing
                deadline. Please review the additional information on <a
                        href="/trademarks-getting-started/trademark-basics/checking-status-application-or-registration">checking
                    status
 to ensure you understand this important step in the overall registration process.&nbsp;
            


            

            
Protecting your rights


            
You are responsible for enforcing your rights if you receive a registration, because the USPTO does not
                "police" the use of marks. While the USPTO attempts to ensure that no other party receives a federal
                registration for an identical or similar mark for or as applied to related goods/services, the owner of
                a registration is responsible for bringing any legal action to stop a party from using an infringing
                mark.&nbsp;


            

            
Trademark manual of examining procedure (TMEP)


            
For additional information on various topics, you may also wish to consult another USPTO electronic
                resource, the 
Trademark Manual of Examining Procedure
                    (TMEP)
. The TMEP provides trademark examining attorneys in the USPTO, trademark applicants, and
                attorneys and representatives for trademark applicants with a reference work on the current law,
                practices, and procedures relative to the federal trademark application and registration process. The
                TMEP contains information and guidelines designed to assist USPTO examining attorneys in reviewing
                trademark application. This information also may be useful to an applicant to better understand the
                trademark application process. The TMEP includes an <a
                        href="http://tess2.uspto.gov/tmdb/tmep/index1.htm">alphabetical index
 by subject matter to
                help users locate pertinent information.&nbsp;


            

            
Other government resources for Trademark owners


            

                - 
                    
__ 
Record Trademarks with Customs and Border Protection (CBP)



                

                - 
                    
__ 
Information for Customers with Special Interests and Requirements



                

                - 
                    
__ 
State Trademark Links



                

                - 
                    
__ 
Related non-USPTO Links



                

                - 
__ 
Fastener Quality Act (FQA)



                

                - 
                    
__ 
Geographical Indications



                

                - 
__ 
Native American Tribal Insignia



                

            

        


    





<div class="views-element-container block block-views block-views-blockuspto-media-gallery-block-3 block-views-block-uspto-media-gallery-block-3"
     id="block-views-block-uspto-media-gallery-block-3">


    

        



        

    



