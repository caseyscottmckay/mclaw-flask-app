Copyright Registration for Derivative Works|United States|Intellectual Property and Technology|Copyright|copyright, registration, derivative works|A derivative work is a work based on or derived from one or more already exist-ing works. Common derivative works include translations, musical arrange-ments, motion picture versions of literary material or plays, art reproductions, abridgments, and condensations of preexisting works. Another common type of derivative work is a “new edition” of a preexisting work in which the edito-rial revisions, annotations, elaborations, or other modifications represent, as a whole, an original work.
# Copyright Registration for Derivative Works


A derivative work is a work based on or derived from one or more already exist-ing works. Common derivative works include translations, musical arrange-ments, motion picture versions of literary material or plays, art reproductions, abridgments, and condensations of preexisting works. Another common type of derivative work is a “new edition” of a preexisting work in which the edito-rial revisions, annotations, elaborations, or other modifications represent, as a whole, an original work. 


To be copyrightable, a derivative work must incorporate some or all of a preexisting “work” and add new original copyrightable authorship to that work. The derivative work right is often referred to as the adaptation right. The fol-lowing are examples of the many different types of derivative works:


    - A motion picture based on a play or novel

    - A translation of an novel written in English into another language

    - A revision of a previously published book

    - A sculpture based on a drawing

    - A drawing based on a photograph

    - A lithograph based on a painting

    - A drama about John Doe based on the letters and journal entries of John Doe

    - A musical arrangement of a preexisting musical work

    - A new version of an existing computer program

    - An adaptation of a dramatic work

    - A revision of a website







__ 
Compilations




Compilations of data or compilations of preexisting works (also known as “col-lective works”) may also be copyrightable if the materials are selected, coordi-nated, or arranged in such a way that the resulting work as a whole constitutes a new work. When the collecting of the preexisting material that makes up the compilation is a purely mechanical task with no element of original selection, coordination, or arrangement, such as a white-pages telephone directory, copy-right protection for the compilation is not available. Some examples of compi-lations that may be copyrightable:


    - A directory of the best services in a geographic region

    - A list of the best short stories of 2011

    - A collection of sound recordings of the top hits of 2004

    - A book of greatest news photos

    - A website containing text, photos, and graphics

    - An academic journal containing articles on a particular topic

    - A newspaper comprised of articles by different journalists

    - A catalog comprised of text and photographs






In the above examples, original authorship may be involved in deciding which were the best stories, the biggest hits, greatest photos, the appropriate article for the serial, and in what order to present the respective works within the compilation.

## Copyright Protection in Derivative Works


The copyright in a derivative work covers only the additions, changes, or other new material appearing for the first time in the work. Protection does not extend to any preexisting material, that is, previously published or previously regis-tered works or works in the public domain or owned by a third party.


As a result, it is not possible to extend the length of protection for a copyrighted work by creating a derivative work. A work that has fallen into the public domain, that is, a work that is no longer protected by copyright, is also an underlying “work” from which derivative authorship may be added, but the copyright in the derivative work will not extend to the public domain material, and the use of the public domain material in a derivative work will not prevent anyone else from using the same public domain work for another derivative work.

## Copyright Protection in Compilations  and Collective Works


The copyright in a compilation of data extends only to the selection, coordination or arrangement of the materials or data, but not to the data itself. In the case of a collective work containing “preexisting works”—works that were previously published, previously registered, or in the public domain—the registration will only extend to the selection, coordina-tion or arrangement of those works, not to the preexisting works themselves. If the works included in a collective work were not preexisting—not previously published, registered, or in the public domain or owned by a third party —the reg-istration may extend to those works in which the author of the collective work owns or has obtained all rights.

## Right to Prepare Derivative Works


Only the owner of copyright in a work has the right to pre-pare, or to authorize someone else to create, an adaptation of that work. The owner of a copyright is generally the author or someone who has obtained the exclusive rights from the author. In any case where a copyrighted work is used without the permission of the copyright owner, copyright protection will not extend to any part of the work in which such mate-rial has been used unlawfully. The unauthorized adaption of a work may constitute copyright infringement.

## Notice of Copyright


Before March 1, 1989, the use of copyright notice was man-datory on all published works, and any work first published before that date should have carried a notice. For works published on or after March 1, 1989, use of copyright notice is optional.


Although not required by law, it is perfectly acceptable (and often helpful) for a work to contain a notice for the original material as well as for the new material. For example, if a previously registered book contains only a new introduc-tion,   the notice might be © 1941 John Doe; introduction © 2008 Mary Smith. For information about copyright notice,see Circular 3, Copyright Notice.


In addition, anyone interested in identifying a copyright owner of a preexisting work can search the online or physical records of the Copyright Office, or request the Office to con-duct a search of its records for an hourly fee. For details, see Circular 22,How to Investigate the Copyright Status of a Work.

## Copyright Registration of Derivative  Works and Compilations


To register copyright claims in derivative works and com-pilations, information will be required regarding previous registrations of preexisting material, limitations of the claim, the material excluded, and a description of the new material added to the derivative work or compilation.


Unfortunately, registration is often delayed because of mistakes or omissions in completing copyright applications. The following points should be helpful for those registering derivative works. The categories specified appear on copy-right applications. 


__ Author
 · Name the author or authors of the copyrightable material being claimed. Ordinarily, the author is the person who actually created the work. Where the work or any con-tribution to it is a work made for hire, the employer is con-sidered the author. Do not name the author of previously published or registered work(s) or public-domain material incorporated into the derivative work unless that person is also the author of the new material. The application should name only the author(s) of the new material in which copy-right is claimed. 


__ Author Created
 · Specify what the author(s) created. Exam-ples include “text,” “translation,” “music,” “lyrics,” “musical arrangement,” “photographs,” “artwork,” “compilation.” 


__ Copyright claimant
 · The copyright claimant is either the author of the work or a person or organization who has obtained from the author all the rights the author initially owned. When the claimant named is not the author, a brief transfer statement is required to show how the claimant acquired the copyright. Examples are “by written agreement”and “by inheritance.” Do not send copies of documents oftransfer with the application.When the name of the claimant is not the name of the author, but the two names identify one person, the relation-ship between the names should be explained. Examples are “Doe Publishing Company, solely owned by John Doe” or “John Doe doing business as Doe Recording Company.”


__ Year of Completion
 · The year of completion is the year in which the completed new work—the particular version for which registration is sought—was fixed in a copy or pho-norecord for the first time, even if other versions exist or if further changes or additions are planned. Do not confuse completion with publication.


__ Publication
 · Copyright law defines “publication” as “the distribution of copies or phonorecords of a work to the public by sale or other transfer of ownership, or by rental, lease, or lending. The offering to distribute copies or pho-norecords to a group of persons for purposes of further dis-tribution, public performance, or public display, constitutes publication. A public performance or display of a work does not of itself constitute publication.”The following do not constitute publication: performing the work, preparing phonorecords, or sending the work to the Copyright Office.The date of publication is the month, day, and year when the work for which registration is sought was first published.  If the work has not been published, no date of publication should be given on the application.


    Where someone, for example, an owner of an individual exclusive right or an agent of an author or owner, who is not a claimant (author or owner of all rights) is filing an application for a work that has never been registered, the applicant should list the author or owner of all rights as the claimant. The appli-cant can then explain their relationship to or interest in the copyright in the certification section of the application



__ Previous registration
 · If no registration has been made for this version or an earlier version of this work, leave this por-tion of the application blank.If a previous registration for this work or another ver-sion of it was completed and a certificate of registration was issued, give the requested information about the previous registration, if known.


__ Limitation of claim
 · Complete this portion of the appli-cation if the work being registered contains a substantialamount of material that:
    

    - was previously published

    - was previously registered in the U.  S. Copyright Office,

    - is in the public domain, or

    - is owned by a third party.






__ Material excluded
 · In this portion of the application, give a brief identification of any preexisting work or works that the work is based on or incorporates


__ New material included
 · Briefly, in general terms, describe all new copyrightable authorship covered by the copyright claim for which registration is sought. See examples below. All elements of authorship described in “author created” should be accounted for in “new material included.” If the claim is in the compilation only, state “compilation.”  If the claim is in the compilation and new material, identify both, such as “compilation and forward” or “compilation of photographs, additional photography, and forward.” Examples for “Material Excluded” and    “New Material Included” entries for derivative works:


    - Motion picture based on the novel Little Women. Material Excluded: Text. New Material Included: Entire motion picture.

    -  New arrangement of preexisting music for piano. Material Excluded: Music. New Material Included: Musical arrangement. 

    - Two-act play expanded to a three-act play. Material Excluded: Preexisting text. New Material Included: Text of third act.

    - Revision of a catalog that adds new text and photographs. Material Excluded: Text,   photographs. New Material Included:Text, Photographs.







