Contributions to a Collective Work|United States|Intellectual Property and Technology|Copyright|copyright|Copyright is a form of protection provided by U.S. law to authors of  "original works of authorship" from the time the works are created in a fixed form.
# Contributions to a Collective Work

If you have written an article, column, puzzle, cartoon, map, advertisement, drama, or short story that has been published in a magazine, newspaper, or other periodical, you may make a separate registration for your individual work. This kind of work is called a “contribution to a collective work.”

The copyright in a separate contribution to a published collective work such as a periodical is distinct from the copyright in the collective work as a whole. In the absence of an express transfer from the author of the individual article, the copyright owner in the collective work is presumed to have acquired only the privilege of using the contribution in the collective work and in subsequent revisions and later editions of the collective work.

As is the case with all published works, a contribution, such as a pictorial or graphic work, to a collective work may appear with its own notice of copyright. However, the law provides that a single notice covering the collective work as a whole can defeat a defense of “innocent infringement.” See sl-35, Registering a Copyright with the U.S. Copyright Office, for information on how to register a single contribution to a collective work.

If you anticipate publishing a series of contributions during a 12‑month period, you may be interested in group registration. Registrations of groups of contributions to periodicals must be filed on paper Form GR/CP. Go to the Copyright Office website at www.copyright.gov, click on Forms, and see adjunct application Form GR∕CP. Note applicable instructions and eligibility requirements.

The deposit will be one complete copy of the best edition of the entire collective work, the complete section containing the contribution if published in a newspaper, the entire page containing the contribution, the contribution cut from the paper in which it appeared, or a photocopy of the contribution itself as it was published in the collective work. For further information on copyright, deposit requirements, and registration procedures, _see_ [Copyright Basics]("document/copyright-basics).
