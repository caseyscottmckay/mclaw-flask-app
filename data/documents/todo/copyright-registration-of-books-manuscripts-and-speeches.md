Copyright Registration of Books, Manuscripts, and Speeches|United States|Intellectual Property and Technology|Copyright|copyright, registration, books, manuscripts, speeches|Claims to copyright in published and unpublished books or manuscripts can be registered as literary works in the Copyright Office.

# Copyright Registration of Books, Manuscripts, and Speeches

Claims to copyright in published and unpublished books or manuscripts can be registered as literary works in the Copyright Office. Textual works with or without illustrations are eligible, as are other nondramatic literary works, including fiction, nonfiction, poetry, contributions to collective works, compilations, directories, catalogs, dissertations, theses, reports, speeches, bound or loose‑leaf volumes, pamphlets, brochures, and single pages containing text. See sl‑35, Registering a Copyright with the U.S. Copyright Office, for the methods available for copyright registration. 

Deposit requirements for literary works depend on whether a work has been published at the time of registration. For unpublished works, one complete copy or phonorecord is required. If you use the electronic Copyright Office (eCO) to register your unpublished work, you can submit your deposit electronically. See sl‑35 for details about eCO. If you choose to submit a physical deposit or to register using a paper application, there is no specific require­ment regarding the printing, binding, format, or paper size and quality of the unpublished manuscript material you deposit. Typewritten, photocopied, and legibly handwritten manu­scripts, preferably in ink, are all acceptable. However, because deposit material represents the entire copyrightable content of a work submitted for registration, copies deposited in a format that will facilitate handling and long‑term storage (for example, stapled, bound, or clipped material) are greatly appreciated by the Copyright Office. 

The deposit requirements for published works are as follows:

- if the work was first published in the United States on or after January 1, 1978, two com­plete copies or phonorecords of the best edition 

- if the work was first published outside the United States, one complete copy or phono­record of the work as first published 

- if the work is a contribution to a collective work and published after January 1, 1978, one complete copy or phonorecord of the best edition of the collective work or a photocopy of the contribution itself as it was published in the collective work.

Copyright protects an author’s expression in literary, artistic, or musical form. Copyright protection does not extend to any idea, system, method, device, name, or title. _See_ [Copyright Basics]("document/copyright-basics") for more information about copyright, deposit requirements, and registra­tion procedures.
