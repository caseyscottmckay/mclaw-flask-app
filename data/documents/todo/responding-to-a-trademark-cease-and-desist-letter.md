---
title: Trademark Overview--Infringement and Related Rights Under Trademark Law
author: casey@mclaw.io
date: 2021-12-21
category | Intellectual Property and Technology
subcategory | Trademark
tags | trademark, infringement, dmca
summary: This overview of US trademark law gives information about the nature of trademarks, the reasons why they can be commercially significant, and the type of material that is likely to be suitable for registration and use as a trademark. Topics covered include trademark enforcement and protection of trademark rights.
---

Responding to a Trademark Cease and Desist Letter
by Practical Law Intellectual Property & Technology
Maintained
•
USA
A Practice Note explaining the key issues that counsel should consider when preparing a client's response to a cease and desist
letter objecting to the client's use of an allegedly infringing trademark.
This Note discusses the key issues that counsel should consider when preparing a client's response to a trademark cease and
desist letter objecting to the client's use of an allegedly infringing mark.
In many cases, a well­researched and well­crafted response to a cease and desist letter can cause the objecting party to
significantly compromise its position or drop its claims entirely. In contrast, a poorly crafted response may embolden the
challenger to pursue its claims in litigation or other formal legal action. If the client does not respond to a cease and desist letter,
the objecting party may seek to use the non­response as evidence of willful infringement.
For a sample trademark infringement cease and desist letter, see Standard Document, Trademark Infringement Cease and Desist
Letter.
Take Timely Initial Actions
At the outset of the representation of a client that has received a trademark cease and desist letter, counsel should:
Confirm representation of the client (see Confirm Representation of the Client).
Determine whether the objecting party has already filed suit (see Determine Whether the Objecting Party has Already Filed
Suit).
Consider sending a holding letter (see Consider Sending a Holding Letter).
Consider the costs of preparing the response (see Consider the Costs of Preparing the Response).
Confirm Representation of the Client
Counsel must determine whether they can represent the client and then take proper steps to confirm representation.
Run a Conflicts Check
An attorney may not represent a party if the representation is adverse to another of the attorney's current clients or, in certain
circumstances, the attorney's former clients. For more information on conflicts rules, see Practice Note, Commencing a Federal
Lawsuit: Initial Considerations: Run a Conflicts Check.
Sign a Retainer Agreement
Counsel and the client should sign an appropriate retainer agreement. For more information on retainer agreements, see Practice
Note, Commencing a Federal Lawsuit: Initial Considerations: Sign a Retainer Agreement.
Determine Whether the Objecting Party Has Already Filed Suit
In some cases, to attempt to prevent the other party from bringing or succeeding with a declaratory judgment action, the
objecting party may send a cease and desist letter only after it has filed a lawsuit. Counsel should check relevant databases to
determine whether the objecting party has already filed a complaint because the appropriate response and strategy may change if
an action has already been brought. The Public Access to Electronic Records (PACER) database provides access to docket
information and documents in federal civil actions. Litigation searches may also be made using commercial search providers.
Consider Sending a Holding Letter
A trademark cease and desist letter typically sets out a short deadline for compliance. It may be difficult to investigate the
matter, gather the relevant facts and prepare a substantive response before the deadline. It is therefore common practice for
counsel to send a "holding letter" to the sender of the cease and desist letter, typically the attorney for the objecting party. A
holding letter typically advises the objecting party:
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fna…
1/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
That its claims are being investigated.
When it can expect to receive a substantive response to the cease and desist letter.
Sending a holding letter poses a risk that the objecting party will not wait for a substantive response and will instead proceed to
initiate formal legal action. However, in the typical case a holding letter does not cause the objecting party to take immediate
action. Counsel should consider its client's particular situation when evaluating the risks and benefits of sending a holding letter.
For example, the risk of the objecting party taking immediate action may be higher where:
The tone and demands of the objecting party's cease and desist letter are particularly aggressive.
The client's conduct is allegedly willful or is causing substantial harm to the objecting party.
Consider the Costs of Preparing the Response
Before starting work on the response, counsel should advise the client of the likely costs involved. These may include:
Investigation costs. An experienced trademark investigator is commonly retained to conduct a formal investigation of the
objecting party's allegations. The cost of investigation can be significant, often ranging upwards of several thousand dollars
depending on the scope and complexity of the investigation. Some investigators charge an hourly rate while others offer flat
fee arrangements.
Search costs. If counsel conducts one or more full trademark searches, the costs can be expected to range from several
hundred to several thousand dollars, which includes the costs of the searches and counsel's time spent analyzing the search
results.
Costs of any consumer survey. Trademark surveys of likely and actual confusion are typically conducted by outside survey
experts and can cost tens of thousands of dollars or more. The cost is determined by various factors including:
the scope of the survey;
the survey methodology; and
how quickly the survey is conducted.
Costs of drafting the response. Depending on its complexity, the cost of counsel drafting a substantive response to a cease
and desist letter can range from several hundred to several thousand dollars.
Evaluate the Objecting Party and its Counsel
Assessing the objecting party and its counsel is an often overlooked but critical component of analyzing the risk presented by the
cease and desist letter and advising the client on an appropriate response to it.
Evaluate the Objecting Party
To make an informed assessment of the risks raised by the cease and desist letter's allegations, counsel should evaluate the
objecting party, including its:
Size.
Financial resources.
Type and scope of operations.
History of trademark enforcement and defense for the asserted mark and the objecting party's other marks.
A large company with substantial financial resources and a history of aggressive trademark enforcement may be better positioned
to pursue its claims than a small company with limited resources.
The objecting party's trademark enforcement history may include infringement litigation and opposition and cancellation
proceedings before the Trademark Trial and Appeal Board (TTAB).
Counsel should evaluate how these adversarial proceedings were resolved (for example, in a settlement, summary judgment or
trial). Counsel should also consider obtaining copies, where available, of pleadings, motions, discovery requests and responses
and other documents in these proceedings (see Obtain Relevant Records from Trademark Litigation Involving the Objecting Party).
These documents may show:
The objecting party's general aggressiveness and willingness to litigate or compromise.
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fna…
2/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
Strengths and weaknesses in the protectability and enforceability of the objecting party's mark.
Arguments and positions previously taken by the objecting party that may contradict the cease and desist letter's allegations.
Evaluate the Objecting Party's Attorney
Where, as is typical, the cease and desist letter is sent by an attorney, counsel should evaluate the attorney's:
Trademark expertise.
Trademark litigation history, and, in particular, any other trademark matters in which the attorney represented the objecting
party.
While trademark cease and desist letters are often sent by experienced trademark attorneys, in some cases the letter may be
sent by a generalist or a specialist in a different intellectual property area, particularly patents. The attorney's level of trademark
expertise may suggest a particular approach or strategy in the response. For example, when responding to a non­specialist,
counsel may wish to cover certain basic trademark principles that might not need to be addressed when responding to a
trademark specialist.
Counsel should carefully assess any previous trademark litigation matters handled by the objecting party's attorney, especially
any actions or proceedings in which the attorney represented the objecting party or another challenger to the accused mark. This
history may provide insight into the attorney's:
General litigation strategy and tactics.
Particular approach in handling trademark matters for the objecting party.
Understand the Contents of the Cease and Desist Letter
To properly prepare a response to the cease and desist letter, counsel must clearly understand its contents, including:
The allegedly infringing marks (see Identify the Objecting Party's and the Client's Respective Marks).
The objecting party's legal claims and potential remedies (see Identify the Objecting Party's Legal Claims and Potential
Remedies).
The objecting party's demands and associated deadlines (see Identify the Demands and Deadlines Set Out in the Cease and
Desist Letter).
The letter's tone (see Consider the Cease and Desist Letter's Tone).
Identify the Objecting Party's and the Client's Respective Marks
In its response to a cease and desist letter, counsel must clearly identify the trademarks in which the objecting party claims
exclusive rights and the specific mark it claims violate those rights, including any:
Word marks.
Stylized word marks.
Design marks.
Trade dress.
Identify the Objecting Party's Legal Claims and Potential Remedies
For purposes of its analysis and advice to the client, counsel should attempt to identify the legal claims made in the letter and the
remedies that would be available to a successful plaintiff on those claims.
Federal Claims
Counsel should identify any federal Lanham Act claims set out in the cease and desist letter, which may include:
Infringement of a federally registered mark under Section 32 of the Lanham Act (15 U.S.C. § 1114).
Trademark counterfeiting of a federally registered mark under Section 32 of the Lanham Act.
Infringement of an unregistered mark or trade dress under Section 43(a) of the Lanham Act (15 U.S.C. § 1125(a)).
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fna…
3/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
Unfair competition and false designation of origin under Section 43(a) of the Lanham Act.
Dilution of a famous mark under Section 43(c) of the Lanham Act (15 U.S.C. § 1125(c)).
Cyberpiracy under Section 43(d) of the Lanham Act (15 U.S.C. § 1125(d)).
False advertising under Section 43(a) of the Lanham Act.
Secondary infringement claims.
For more information on trademark infringement and dilution claims, see Practice Note, Trademark Infringement and Dilution
Claims, Remedies and Defenses.
For more information on trademark counterfeiting claims, see Practice Note, Protecting Against Counterfeit Trademarks and Gray
Market Goods: Federal Civil Causes of Action for Protecting Against Counterfeit Marks.
For more information on cyberpiracy claims, see Practice Note, Internet Brand Protection: The Anticybersquatting Consumer
Protection Act.
State Claims
Counsel should identify any state law claims asserted in the letter, such as:
Trademark infringement under state statutes or common law.
Counterfeiting under state statutes.
Unfair competition under state statutes or common law.
Deceptive or unfair trade practices under state statutes.
Dilution under state anti­dilution statutes or common law.
False advertising.
Trade libel.
Violations of a right of publicity or privacy claims.
Unjust enrichment.
Breach of contract, for example a license, franchise, distribution or settlement agreement.
For more information on state trademark laws, see Trademark Laws: State Q&A Tool.
Potential Remedies
Counsel should identify the potential remedies that may be available to the objecting party if successful on its federal or state
claims. Depending on the claims, these may include:
Temporary, preliminary or permanent injunctive relief.
Actual or enhanced damages.
Profits.
Affirmative equitable relief, such as product recall and destruction.
Statutory damages on counterfeiting or cyberpiracy claims.
Attorneys' fees.
Exemplary damages under state law.
For more information on remedies for trademark infringement and dilution, see Practice Note, Trademark Infringement and
Dilution Claims, Remedies and Defenses: Remedies for Trademark Infringement and Dilution.
For more information on remedies for trademark counterfeiting, see Practice Note, Protecting Against Counterfeit Trademarks and
Gray Market Goods: Federal Civil Causes of Action for Protecting Against Counterfeit Marks.
For more information on remedies for cyberpiracy, see Practice Note, Internet Brand Protection: Remedies Under ACPA.
Identify the Demands and Deadlines Set Out in the Cease and Desist Letter
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fna…
4/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
To provide a risk assessment to the client and prepare a response to the cease and desist letter, counsel must identify and
understand the objecting party's demands and associated deadlines.
In most cases, the objecting party's principle demand is that the client stop using the allegedly infringing mark either immediately
or within a specified time period.
In some cases, a trademark cease and desist letter will include additional demands, such as demands for the client to:
Provide the objecting party with an accounting of sales and profits for goods and services sold under the allegedly infringing
mark.
Make a monetary payment to the objecting party.
Recall, relabel or destroy infringing goods.
Abandon any trademark applications it owns for the mark.
Cancel any trademark registrations it owns for the mark.
Cancel or transfer to the objecting party any domain name registrations it owns for the mark.
Consider the Cease and Desist Letter's Tone
While not a substantive legal consideration, the tone of a trademark cease and desist letter can suggest how seriously the
objecting party views the matter and how aggressively it will pursue its claims. A letter with strident demands and short deadlines
may indicate that the objecting party considers the matter to be serious and is inclined to pursue its claims further if it does not
receive a satisfactory response. In contrast, a "weak" letter with measured demands and long or no deadlines may suggest the
objecting party is less likely to pursue its claims or that it is willing to compromise.
Gather the Relevant Facts
After analyzing the contents of the cease and desist letter, counsel should gather the additional facts necessary to perform a
thorough risk assessment and prepare a substantive response.
Identify the Client's Insurance Policies
Counsel should obtain from the client any insurance policies that might obligate the client's insurer to:
Defend the asserted claims.
Indemnify the client for any damages, settlement payments or attorneys' fees paid.
Counsel should review any relevant policy to determine the requirements and deadlines for providing the carrier with notice of the
objecting party's claim.
For more information on insurance coverage for trademark and other intellectual property claims, see Practice Note, Commercial
General Liability Insurance Policies: Personal and Advertising Injury Coverage (Coverage B).
Review Relevant Agreements
Counsel should review all relevant agreements including:
Agreements between the client and the objecting party (see Review Any Relevant Agreements between the Client and the
Objecting Party).
Any available agreements between the objecting party and third parties (see Review Any Available Relevant Agreements
between the Objecting Party and Third Parties).
Review Any Relevant Agreements between the Client and the Objecting Party
Many trademark actions arise out of the breach of commercial agreements. For example, infringement claims (and breach of
contract claims) may arise from a trademark licensee using a licensed mark beyond the scope of the license or after the end of
the license term.
Counsel should review any written agreements between the client and the objecting party to determine whether the parties must
resolve the dispute in arbitration or in any other specified manner. Relevant agreements in trademark actions may include:
License agreements.
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fna…
5/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
Franchise agreements.
Distribution agreements.
Settlement agreements.
Coexistence and consent agreements.
For a sample trademark license agreement, see Standard Document, Trademark License Agreement (Pro­licensor).
For a sample trademark coexistence agreement, see Standard Document, Trademark Coexistence Agreement.
For a sample trademark settlement agreement, see Standard Document, Trademark Settlement Agreement and Release (Pro­
trademark owner).
For a sample trademark consent agreement, see Standard Document, Trademark Consent Agreement.
The agreements should be reviewed for:
Arbitration clauses. Many agreements contain provisions requiring the parties to resolve disputes between them in
arbitration. For more information on arbitration clauses, see Practice Note, Drafting Arbitration Agreements Calling for
Arbitration in the US.
Choice of law provisions. Parties to commercial transactions commonly include a choice of law provision in their contracts.
For example, a trademark license agreement typically contains a choice of law provision requiring the agreement be
interpreted under the law of a specified state. Counsel should analyze the impact of a choice of law provision on the objecting
party's claims and the client's defenses and counterclaims. For a sample choice of law provision in a trademark license
agreement, see Standard Document, Trademark License Agreement (Pro­licensor): Section 15.11.
Forum selection clauses. A forum selection clause may permit or even require the parties to litigate a dispute in a particular
district court or other court. Often the clause also contains a waiver preventing the parties to the agreement from arguing that
the designated court is an inconvenient forum or does not have personal jurisdiction over them. For a sample forum
selection clause in a trademark license agreement, see Standard Document, Trademark License Agreement (Pro­licensor):
Section 15.11.
Consent to personal jurisdiction. Even if the parties agree to submit to a particular court's jurisdiction, the plaintiff must still
serve the defendant with a summons and complaint to invoke the court's jurisdiction (FRCP 4(c)). However, the parties may
specify the manner of proper service in their agreement.
State law may limit the parties' freedom to choose a particular state's law and the court in which to resolve disputes arising out of
a contract. For more information, see Practice Note, Commencing a Federal Lawsuit: Initial Considerations: Limitations on Choice
of Law and Forum Selection Clauses.
Counsel should also consider any provision for:
Either party's recovery of court costs and attorneys' fees.
Waiver of the requirements of posting a bond or proving actual injury to obtain injunctive relief.
Review Any Available Public Documents and Agreements between the Objecting Party and Third Parties
In some cases it may be possible to obtain from public records (for example, court, US Patent and Trademark Office (USPTO)
and TTAB databases) copies of agreements between the objecting party and third parties or other relevant documents involving
the objecting party's mark. These may include:
Settlement agreements.
Coexistence agreements.
Consent agreements.
Disclaimers of rights.
USPTO refusals to register the mark for specified goods or services.
Investigate Use of the Parties' Marks
To assess risk and prepare a response, counsel must investigate and understand the parties' use of their respective marks,
including:
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fna…
6/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
The nature and extent of use. This includes:
the goods or services for which the marks are used;
the geographic region of use;
the trade channels in which goods or services bearing the marks are promoted and sold; and
the customers of the goods or services.
The duration of use. The duration of the parties' use of their marks is important because:
the client may have a laches or acquiescence defense if it has long­standing use;
long­standing marketplace coexistence without actual confusion may suggest that confusion is not likely; and
if the client's use actually predates the objecting party's actual or constructive use, it may have a complete defense or
possibly an infringement claim against the objecting party.
Gather Materials from the Client Showing Use of its Mark
Counsel should ask the client to provide representative samples of existing and contemplated:
Product packaging or labeling bearing the client's mark.
Marketing, advertising and promotional materials featuring the mark.
Gather Available Materials Showing the Objecting Party's Use of its Mark
In most cases, there are a variety of public sources from which counsel may obtain additional information about the objecting
party's use of its mark. These include:
The objecting party's websites which may show samples of its products and services available in the marketplace and related
advertising and promotional materials.
Third­party websites selling or promoting the objecting party's goods or services.
Annual reports and, if a public company, the objecting party's SEC filings.
If the objecting party's mark is used on products, counsel should obtain product samples if feasible.
Gather Available Evidence of the Parties' Sales and Advertising Figures
Counsel should determine whether any publicly available sources disclose sales or advertising figures for the goods or services
provided under the objecting party's mark. These may include the objecting party's:
Website.
Press releases.
SEC filings, if a public company.
Counsel should obtain from the client similar marketing and sales information to determine:
The magnitude of the client's investment in and profits associated with the accused mark (see Determine the Client's
Investment in the Mark and Future Plans).
The extent of the client's potential liability if the objecting party prevails on its claims.
Consider Privilege and Ethics Issues
Before investigating the objecting party and its activities, counsel should consider potential:
Privilege and work product issues. In some circumstances, where an investigator is a fact witness, the information
collected by the investigator may not be privileged (see, for example, Brown v. Trigg, 791 F.2d 598, 601 (7th Cir. 1986)).
Ethics issues. Investigating references in publicly available reports or records ordinarily does not raise ethical issues.
However, certain external investigative techniques may not be ethically permissible. For example, using private investigators
and other techniques that involve direct contact with an alleged infringer may require constraints to avoid ethical issues.
Before conducting any investigation, counsel must also consider ethical rules that bar contact with a party known to be
represented by counsel.
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fna…
7/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
For more information on trademark investigations (in the trademark searching and clearance context), see Practice Note,
Trademark Searching and Clearance: Investigations of Potentially Conflicting Marks.
Determine the Client's Investment in the Mark and Future Plans
Key factors in whether a client may be willing to stop using a mark in response to a cease and desist letter are:
The client's investment in the mark.
The client's future plans for the mark.
Where the client has invested significant financial or other resources in developing and using a mark or when it intends to make
expanded or long­term use of the mark, it may be less inclined to agree to discontinue using the mark.
The client's future plans, such as projected product or line extensions, may also impact the likelihood of confusion analysis
depending on the nature of any expansion plans and whether they bridge any gap between the client's goods or services and those
of the objecting party.
Assess the Harm the Client Would Sustain by Stopping Use
A direct corollary of the client's investment in and plans for its mark is the harm the client would likely incur if it stops using the
mark. This harm may include:
Financial losses, including from:
lost sales;
recall or destruction of any inventory of materials bearing the mark;
advertising commitments that can no longer be fulfilled; and
costs of developing, implementing and promoting a new mark.
Reputational damage, for example, from loss of goodwill with customers.
If the client would likely be significantly harmed by stopping use, other alternatives should be considered, including:
Defending the client's position.
Pursuing a resolution allowing the client to continue using the mark, for example, a license agreement.
Gather any Evidence of Actual Confusion
While generally not required to establish infringement, credible evidence of actual confusion in the marketplace is typically
considered strong evidence of likelihood of confusion. Counsel should ask the client for copies of any records of:
Misdirected correspondence or telephone calls.
Consumer complaints that confuse the parties' products or services.
Any other evidence showing confusion in the marketplace between the parties' products or services.
Evaluate any Evidence of the Client's Intent
Intent is an important factor in assessing infringement and dilution liability and remedies. If the client willfully trades on the
objecting party's mark, it faces heightened risk and exposure. Counsel should gather any available evidence showing the client's
willful or good faith intent (or lack of intent), including:
Whether the client conducted a trademark search before adopting its mark and whether the objecting party's mark appeared
in the search results.
Any other evidence of the client's knowledge of the objecting party's mark before adopting its mark.
Documents showing the client's specific intent to trade on or associate itself with the objecting party or its mark.
The client's use of color schemes, graphics or other indicia suggesting an intent to trade on the goodwill of the objecting party
and its mark.
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fna…
8/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
Identify any Trademark Applications or Registrations Owned by the Client
Counsel should identify any federal or state trademark applications or registrations owned by the client for its mark and analyze
the details of those applications or registrations, including:
The goods or services covered by the applications or registrations.
Any alleged dates of first use of the mark.
Any constructive use date afforded by any registrations.
Obtain Relevant USPTO Records for the Objecting Party's Mark
Counsel should obtain from the USPTO copies of available prosecution and maintenance records for the objecting party's mark,
including copies of:
Registration certificates.
Trademark applications.
Office actions and responses.
Statements of use and amendments to allege use.
Declarations of use.
Declarations of incontestability.
Renewals.
Assignment records.
A cease and desist letter typically sets out the details of any registration claimed by the objecting party. Counsel should verify the
ownership and status of the claimed registration using its own independent check of USPTO records. Trademarks may be searched
online using the USPTO's Trademark Electronic Search System (TESS).
Counsel may use the USPTO's Trademark Status & Document Retrieval online database to check the status of a registration or
application and obtain copies of associated prosecution and maintenance documents.
For assignment records, the USPTO maintains a searchable online trademark assignment records database that includes
assignments recorded from 1955 onward.
Obtain Relevant Records from Trademark Litigation Involving the Objecting Party
Counsel should obtain and review relevant records from litigations involving the objecting party, particularly records for any
proceeding involving the objecting party's mark. These records are available from various public and commercial databases,
including databases for:
Federal litigation. The Public Access to Electronic Records (PACER) database provides access to docket information and
documents filed in federal civil actions. Litigation searches may also be made using commercial search providers.
USPTO inter partes proceedings. The USPTO's Trademark Trial and Appeal Board Inquiry System (TTABVUE) provides
access to information and documents in TTAB inter partes proceedings, including oppositions and cancellations. Final TTAB
decisions are accessible on the USPTO e­FOIA page.
Domain name proceedings. Decisions in uniform domain name dispute resolution policy (UDRP) proceedings involving
domain names may be obtained from:
the UDRP proceedings archive maintained by the Internet Corporation for Assigned Names and Numbers (ICANN), although
this archive is incomplete; and
the individual dispute proceedings sites of ICANN's approved list of dispute resolution service providers.
Consider Conducting Trademark Searches of the Marks
When preparing a response to a cease and desist letter, counsel should consider conducting a trademark search of the objecting
party's mark to further assess the marketplace and the mark's strength and scope of protection. Counsel should also consider a
search of the client's mark if it is different from the objecting party's mark or a review of any search that was not already
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fna…
9/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
conducted as part of the mark's clearance.
A search should be analyzed in particular for:
The nature and extent of third­party use and registration of similar marks.
The market penetration of the objecting party's mark.
Any misuse of the objecting party's mark by the objecting party or third­parties.
Counsel should consider potential privilege and work product issues before beginning a search. While it can be argued that a
search by an outside firm commissioned by counsel in anticipation of litigation should be protected from disclosure by the
attorney work product doctrine, the case law is not sufficiently developed to fully support this argument.
For more information on trademark searches (in the trademark searching and clearance context), see Practice Note, Trademark
Searching and Clearance.
Consider Conducting Consumer Surveys
Before preparing a response to a cease and desist letter, counsel should consider whether to conduct one or more consumer
surveys, including on:
Consumer confusion.
Consumer recognition of the objecting party's mark.
Fame of the objecting party's mark.
Dilution.
While consumer surveys can be expensive, they can help identify significant weaknesses (and strengths) in the objecting party's
case. The results may suggest a particular strategy including whether to aggressively defend the client's position or pursue
settlement.
Counsel should consider potential privilege and work product issues before initiating a survey. For more information on these and
other considerations for surveys, see Trademark Litigation: Online Consumer Surveys: Considerations for Deciding to Conduct a
Survey.
Assess the Strength of the Objecting Party's Claims and the Client's Defenses and
Counterclaims
Once counsel has gathered the relevant background information and materials it should assess the strength of the objecting
party's claims and the client's defenses and counterclaims in light of that information.
Evaluate the Ownership and Validity of the Objecting Party's Mark
Counsel should assess whether the objecting party's mark presents any ownership or validity issues.
To prevail on trademark infringement and related claims under the Lanham Act, the objecting party generally must establish as a
threshold matter that it owns a valid trademark that is subject to federal protection. For a mark to be valid and enforceable, it
must generally be both:
Distinctive (either through inherent or acquired distinctiveness).
Used in commerce and not abandoned.
For more information on the requirements for federal trademark protection, see Practice Note, Acquiring Trademark Rights and
Registrations: Requirements for Federal Trademark Protection.
Documents relevant to assessing the ownership and validity of the objecting party's mark include:
Federal trademark registration certificates for the mark.
Documents showing the status of registrations and applications for the mark, for example, whether they are still in effect and
have not expired or been abandoned.
Documents showing the incontestable status of any registration for the mark.
USPTO assignment records for the mark.
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fn…
10/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
Documents and other materials showing use of the mark as a trademark in commerce by the owner or the owner's licensees,
for example, product labels and packaging.
While not necessary for protection, federal trademark registration of a mark on the principal trademark register affords various
evidentiary benefits and presumptions in litigation, including presumptions of ownership and validity. Incontestable status affords
additional presumptions. For more information on the benefits of federal registration, see Practice Note, Acquiring Trademark
Rights and Registrations: Benefits of Federal Registration. For more information on incontestable status, see Practice Note,
Acquiring Trademark Rights and Registrations: Declaration of Incontestability.
Evaluate Ownership of the Objecting Party's Mark
Counsel should determine whether any evidence suggests that the objecting party does not own its asserted mark. This may
include:
Ownership and assignment records of any trademark registrations or applications.
Gaps or discrepancies in the chain of title of registrations or applications.
Evaluate the Validity of the Objecting Party's Mark
Counsel should identify any evidence that may be used to challenge the validity of the objecting party's mark. This may include
evidence showing that the mark:
Is not distinctive, including:
because of descriptiveness and lack of secondary meaning;
due to third­party use of the mark for similar goods or services disclosed in searches; or
as evidenced by the results of any consumer surveys.
Is functional.
Has been abandoned because of non­use.
For information on ways trademark rights can be lost, see Practice Note, Loss of Trademark Rights.
Evaluate the Commercial Strength and Scope of Protection of the Objecting Party's Mark
Counsel should assess the mark's commercial strength and scope of protection of the objecting party's mark, including evidence
showing:
The nature and extent of the objecting party's use and promotion of its mark.
Third­party use of the mark for similar goods or services disclosed in searches.
The objecting party's misuse of the mark.
The objecting party's enforcement and defense history for the mark.
Any territorial limits on the objecting party's rights.
Counsel should also assess any available agreements, for example, settlement, consent or coexistence agreements that may
affect the scope of protection for the objecting party's mark and the strength of the objecting party's case against the client. For
example, the client may be able to use a coexistence or consent agreement between the objecting party and a third party for a
similar mark to show:
Dilution of the objecting party's mark resulting from the coexistence of similar marks in the marketplace for similar goods or
services.
No likelihood of confusion or dilution because of the objecting party's acknowledgment of no likelihood of confusion or dilution
with the similar third­party mark covered by the agreement.
If the objecting party has licensed its trademark to third parties but has failed to exercise adequate quality control over the nature
and quality of the licensed goods or services, the objecting party's mark may be vulnerable to a defense of abandonment. For
more information on abandonment of trademark rights because of uncontrolled (naked) licensing, see Practice Note, Loss of
Trademark Rights: Abandonment through Uncontrolled (Naked) Licensing.
Evaluate Likelihood of Confusion
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fn…
11/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
If the objecting party owns a valid trademark, to prevail in an infringement, action it must still prove likelihood of confusion
resulting from the client's use of the allegedly infringing mark. Likelihood of confusion is the touchstone for infringement and
unfair competition claims under both the Lanham Act and state law. While the specific likelihood of confusion factors vary
somewhat by circuit the key factors are largely consistent across the circuits. In some circuits the analysis may vary depending
on the type of confusion alleged, for example, forward or reverse confusion.
For more information on the likelihood of confusion analysis in infringement actions, see Practice Note, Trademark Infringement
and Dilution Claims, Remedies and Defenses: Likelihood of Confusion.
For a chart summarizing the likelihood of confusion tests used in the 12 regional circuits, see Trademark Litigation: Likelihood of
Confusion Tests by Circuit Chart.
In addition to any other or different standards used in the relevant circuit, counsel should evaluate the available evidence
concerning the following commonly considered likelihood of confusion factors:
The strength (inherent and commercial) of the objecting party's mark.
The relatedness of the goods or services offered by objecting party and the client under their respective marks.
The overall presentations of the client's mark and the objecting party's mark, including:
the presence or absence of house marks; and
the use of similar or dissimilar type styles or color schemes.
The trade channels used or intended to be used by the parties to distribute and sell goods and services under the marks.
The consumers of the goods or services offered under the parties' marks.
The client's good faith or willful intent.
Any actual consumer confusion.
Counsel should also assess whether the objecting party has taken any positions in previous matters, including litigations,
opposition or cancellation proceedings, or office actions responses that are inconsistent with or undermine any arguments it
makes in the cease and desist letter.
Assess Likelihood of Dilution
If a federal dilution claim is asserted, counsel should assess whether:
The objecting party's mark is famous for purposes of a federal or state dilution claim.
Dilution by blurring or tarnishment is likely.
For more information on federal dilution claims, see Practice Note, Trademark Infringement and Dilution Claims, Remedies and
Defenses: Federal Trademark Dilution Claims.
Assess the Client's Potential Liability Under Any Other Claims
Counsel should analyze the evidence to assess the client's potential liability under any other federal or state claims asserted by
the objecting party. While the elements of state law claims are often similar to the elements of parallel federal claims, in some
cases state claims (for example, of dilution) may have lower evidentiary burdens.
For more information on state trademark laws, see Trademark Laws: State Q&A Tool.
Evaluate the Objecting Party's Potential Relief
Counsel should carefully consider the likelihood of the objecting party attaining relief under its asserted claims, including:
Injunctive relief (see Injunctive Relief).
Other equitable relief (see Other Equitable Relief).
Damages, profits and attorneys' fees (see Damages, Profits and Attorneys' Fees).
Injunctive Relief
In trademark actions, the trademark owner typically seeks a permanent injunction to stop the infringing activity. In many cases,
the alleged infringer's ongoing infringement is causing or threatening to cause immediate, substantial and irreparable harm to the
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fn…
12/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
trademark owner and its brand that cannot be adequately remedied by money damages. In these cases, a trademark owner often
seeks immediate injunctive relief by obtaining a preliminary injunction.
Where the existing or threatened harm is particularly egregious, the trademark owner may seek a temporary restraining order
(TRO). A trademark owner typically seeks a TRO to enjoin the infringing activity pending the outcome of a preliminary injunction
motion.
For more information on injunctive relief in trademark actions, see Practice Notes, Trademark Litigation: Injunctive Relief and
Trademark Infringement and Dilution Claims, Remedies and Defenses: Injunctive Relief.
Other Equitable Relief
In many trademark actions, the plaintiff seeks affirmative equitable relief, including:
Product recall.
Destruction of articles bearing the infringing mark.
Corrective advertising.
Compulsory disclaimers.
Forfeiture, cancellation or transfer of one or more domain names.
For more information see, Practice Note, Trademark Infringement and Dilution Claims, Remedies and Defenses: Affirmative
Relief.
Damages, Profits and Attorneys' Fees
Actual damages, profits of the infringer and reasonable attorneys' fees may all be awarded in trademark actions. Damages and
profits may be enhanced in certain situations, typically where there is actual confusion or willful intent.
Statutory damages may be available for certain claims such as counterfeiting and cyberpiracy.
For more information on monetary awards in trademark actions, see Practice Note, Trademark Infringement and Dilution Claims,
Remedies and Defenses: Monetary Recovery.
Evaluate Other Potential Defenses and Counterclaims
Counsel should consider any other potential defenses and counterclaims that the client may have, including:
Laches.
Acquiescence.
Equitable estoppel.
Fair use.
First Amendment defenses.
Fraud.
Statute of limitations defenses.
Claim or issue preclusion.
For more information on these and other principal defenses in trademark actions, see Practice Note, Trademark Infringement and
Dilution Claims, Remedies and Defenses: Principal Defenses to Trademark Infringement and Dilution Claims.
Consider Preemptive Legal Action
Counsel should evaluate the potential benefits and risks of the client taking preemptive legal action against the objecting party
including bringing a:
Declaratory judgment action (see Consider a Declaratory Judgment Action).
Cancellation or opposition proceeding in the TTAB (see Consider a Cancellation or Opposition Proceeding).
Consider a Declaratory Judgment Action
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fn…
13/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
Counsel should consider the viability, risks and benefits of the client bringing a declaratory judgment action against the objecting
party, for example, for a declaration of:
Non­infringement.
No dilution.
Invalidity of the objecting party's mark.
Potential benefits of a declaratory judgment action may include litigating:
Where the governing law favors the client's position.
In a convenient forum.
Consider the Live Controversy Requirement
Federal courts have the power to decide only cases that present live controversies (U.S. Const. art. III, § 2, cl. 1).
The live controversy requirement may be an issue in the context of declaratory judgment actions brought in response to a cease
and desist letter.
Federal courts assessing whether a live controversy exists are governed by the standards set out in the 2007 Supreme Court
MedImmune, Inc. v. Genentech, Inc. decision, which broadened the circumstances in which a declaratory judgment action may be
brought in the patent context (549 U.S. 118 (2007)). While MedImmune concerned patent litigation, federal courts have applied its
expanded standards for assessing the existence of a live controversy to suits for trademark declaratory judgments. Some courts
have found a live controversy and declaratory judgment jurisdiction in trademark cases:
Where litigation was not expressly threatened in a cease and desist letter but the trademark owner demonstrated a
willingness to enforce its rights (see Poly­Am., L.P., v. Stego Indus., L.L.C., 694 F. Supp. 2d 600, 606 (N.D. Tex. 2010)).
The declaratory judgment plaintiff sued for a declaration of non­infringement and non­dilution after the declaratory judgment
defendant filed an opposition proceeding against the declaratory judgment plaintiff's trademark application and the notice of
opposition contained allegations of infringement (see Neilmed Prods., Inc. v. Med­Sys., Inc., 472 F. Supp. 2d 1178, 1180 (N.D.
Cal. 2007)).
Consider a Cancellation or Opposition Proceeding
Counsel should consider whether grounds exist to:
Seek cancellation of any trademark registrations asserted by the objecting party.
Oppose any applications for trademark registration relied on by the objecting party.
Commencing a cancellation or opposition proceeding may sometimes serve to increase the client's leverage for a favorable
resolution of the matter. However, in other cases, filing a cancellation or opposition may provoke the objecting party into filing an
infringement action. Typically the TTAB suspends an opposition or cancellation proceeding motion where the parties are involved
in litigation. The client's leverage resulting from bringing an opposition or cancellation may therefore be limited.
Determine the Substance of the Response
After evaluating the available evidence and the client's legal position, counsel should confer with the client and agree on the
substance of the response. The substance can vary significantly from case to case depending on a variety of factors, including:
The relative strengths and weaknesses of the objecting party's claims and the client's defenses and counterclaims.
The relative resources of the objecting party and the client.
The investment that each party has made in its mark.
The potential harm to the objecting party from the client's activities.
The hardship to the client from stopping use and adopting a new mark or modifying use of the current mark.
Denying Liability
Regardless of whether the client is willing to agree in whole or in part to the objecting party's demands or rejects them, responses
to cease and desist letters typically deny liability and include supporting factual and legal arguments. These often include
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fn…
14/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
arguments:
Challenging the ownership or validity of the objecting party's mark.
Refuting allegations or likelihood of confusion or dilution.
Counsel may consider attaching any materials supporting the client's position, including:
Evidence of third­party use.
Relevant USPTO records or agreements.
Consider Settlement Options
If the client is willing to consider a settlement, counsel should evaluate all potential options. These may include the client:
Terminating use of the mark either immediately or subject to a sell­off period for affected inventory and other materials (see
Stopping Use of the Mark).
Modifying the mark or its use (see Agreeing to Modify the Mark or its Use).
Seeking a license to use the mark (see Seeking a License).
Purchasing the objecting party's rights (see Purchasing the Objecting Party's Rights).
Voluntarily abandoning any applications or surrendering for cancellation any registrations for the disputed mark (see
Voluntarily Abandoning and Trademark Applications or Cancelling any Trademark Registrations).
Stopping Use of the Mark
In many cases, if the client agrees to stop using the disputed mark the objecting party will consider the matter resolved. In some
cases, the objecting party may be willing to permit a sell­off of existing or in­pipeline materials bearing the disputed mark.
However, in situations where the violation is causing substantial damage to the objecting party, the objecting party may seek:
An immediate stoppage of use.
Retrieval of materials bearing the disputed mark from the marketplace.
A monetary payment.
Generally, the stronger the client's legal position, the more likely that it will be able to negotiate a sell­off period.
In some cases, the client may be able to negotiate a payment from the objecting party to defray the cost of stopping use of its
mark and changing over to a new mark. This may be possible in particular where:
The client's legal position is strong.
The objecting party determines that is more cost effective to make a reasonable payment than to litigate the matter.
Sometimes the objecting party is satisfied with a letter confirming the client's agreement to stop use, while in other cases,
particularly where the objecting party's position is strong, it may insist on a formal settlement agreement. For a sample
trademark settlement agreement drafted in favor of the prior rights owner, see Standard Document, Trademark Settlement
Agreement and Release (Pro­trademark owner).
Agreeing to Modify the Mark or its Use
In some cases, modifications to the client's mark or restrictions on its use may make confusion or dilution unlikely. Changes to
the client's mark may include:
The addition of a house mark.
Adjustments to the size or type style of the mark.
Changes in color schemes for the mark and associated trade dress.
Restrictions on the client's use of the mark may include restrictions on:
The goods or services for which the mark is used.
The trade channels in which goods or services are sold under the mark.
The marketing media and channels for goods and services bearing the mark.
https://a.next.westlaw.com/Document/I5f597b5b1c8a11e38578f7ccc38dcbee/View/FullText.html?navigationPath=Search%2Fv3%2Fsearch%2Fresults%2Fn…
15/168/5/2015
Responding to a Trademark Cease and Desist Letter ­ WestlawNext
Counsel should discuss with the client the feasibility of potential changes to the mark or restrictions on its use that could be
proposed to attempt to settle the matter. If the objecting party is amenable it may seek a formal coexistence agreement setting
out the agreed on changes and restrictions. For a sample trademark coexistence agreement, see Standard Document, Trademark
Coexistence Agreement.
Seeking a License
Counsel should consider the suitability of the client obtaining a license from the objecting party permitting the client to continue to
use the disputed mark. A license, which would typically involve the client paying a royalty to the objecting party, may be
particularly viable in cases where the parties do not directly compete in the marketplace. For a sample trademark license
agreement (drafted in favor of the licensor), see Standard Document, Trademark License Agreement (Pro­licensor).
Purchasing the Objecting Party's Rights
In rare instances the client may be in a position to resolve the dispute by purchasing the objecting party's trademark rights. This
may be a feasible resolution, for example, in cases where:
The objecting party is small.
The client has ample resources and a strong desire to:
continue using the mark; and
obtain ownership of the mark.
In most cases involving a rights purchase, the objecting party agrees to stop using the mark following the sale. In some cases,
however, the purchasing party may issue a license back to the objecting party to enable the objecting party to continue to use the
mark. Issuing a license back may help lower the purchase price because the objecting party can then continue to use the mark. A
license back may be viable where the parties do not directly compete.
Voluntarily Abandoning Any Trademark Applications and Cancelling any Trademark Registrations
If the client has applied to register (or registered) its mark, any settlement will likely involve the client agreeing to voluntarily
abandon any trademark applications or cancelling any trademark registrations it owns for the disputed mark. In cases where the
parties agree to a coexistence arrangement, the settlement may include amendments to the applications or registrations,
particularly to limit or restrict the goods or services or trade channels associated with the parties' respective marks.
In some cases, the objecting party may be willing to consent to the client's continued unrestricted use of its mark provided that
the client abandons its applications or cancels its registrations for the mark. These cases typically arise where the objecting party
is more concerned with keeping the trademark register "clean" from potentially diluting registrations than with the client's actual
use of the disputed mark.