Copyright Registration of Computer Programs|United States|Intellectual Property and Technology|Copyright|copyright, registration, computer program, software, code|This circular describes the process for registering computer programs and related works with the Copyright Office. It covers (1) HTML, (2) Deposits and trade secrets, (3) Derivative computer programs, (4) CD-ROMs, (5) Computer screen displays, (6) User manuals, (7) Video games, (8) Object code.
# Copyright Registration of Computer Programs

## Computer Program


A computer program is a set of statements or instructions to be used directly or indirectly in a computer to bring about a certain result. Copyright protection for a computer program extends to all of the copyrightable expression embodied in the program. The copyright law does not protect the func-tional aspects of a computer program, such as the program’s algorithms, formatting, functions, logic, or system design.

## Copyright Registration


An application for copyright registration contains three essential elements: a completed application form, a nonre-fundable filing fee, and a nonreturnable deposit—that is, a copy or copies of the work being registered and “deposited” with the Copyright Office. CopyrightRegistration (Circular 2) contains general information about submitting an applica-tion, including details about what to expect after you file and the effective date of registration.


As a general rule, you must submit a separate application, filing fee, and deposit for each work you want to register. However, the Office has established exceptions for certain types of works that allow you to register multiple works on one application. 


The Office strongly encourages you to submit your appli-cation before you publish your work. In addition, the Office encourages you to apply online through the Office’s elec-tronic registration system, accessible through the Office’s website www.copyright.gov.


Note that each version of a computer program containing new, copyrightable authorship is considered a separate work. You must submit a separate application, filing fee, and deposit for each version you want to register. If, however, your program is unpublished, you may be able to register multiple versions of the same program by submitting them together through group registration of unpublished works. 


A registration for each new version of a computer program covers the new material that the author contributed to that version, including any changes, revisions, additions, or other modifica-tions that the author made to that version. Except in limited circumstances described below, the registration does not cover earlier versions of the same program or preexisting material that may be contained within the source code. In particular, a registration for a specific version of a program does not cover the following:
    

    - Previously published source code;

    - Previously registered source code;

    - Source code in the public domain; or 

    - Copyrightable source code owned by a third party.






There is a limited exception to this rule. A registration may cover both new and preexisting source code if (1) the preexisting source code has never been published or registered, and (2) the claimant owns the copyright in both the new and the preexisting source code. When completing your application, make sure to address the following issues:


    - If the source code contains both new material and preexisting material, complete the “Limitation of Claim” field and identify the material being claimed in the application.

    - If your deposit contains multiple version numbers or a date range in the copyright notice (e.g., © 2014, 2015, 2016), indicate in the “Note to Copyright Office” field whether these ver-sion numbers and dates refer to the developmental history of the program or to previous versions of the program that have not been previously published or registered. If you fail to provide an explanation, the examiner may question the claim, which will delay the comple-tion of your registration.






## Deposit Requirements


Submit the source code for the specific version of the computer program you want to register. Source code is the set of statements and instructions authored using a programming language (e.g., C++, PERL, Java). Source code is comprehensible to a person familiar with programming language but requires conversion into object code before it is comprehensible to a computer or other electronic device.


You can upload the source code to the electronic registration system, preferably as a PDF file or other file type accepted by the Copyright Office. The list of acceptable file types is available online. Alternatively, you can print out the source code on paper and mail it to the Office. In all cases, add the title and version number of the program to the first page of the code.


NOTE: For programs written in JavaScript or other scripted languages, the script is considered the equivalent of source code. Thus you should submit the same number of pages from the script that would be required for a submission of source code. Copies of on-screen text, buttons, or commands are not appropriate substitutes for this requirement. 


You can submit the entire source code for the program, or you can submit a representative por-tion. The specific material required may vary depending on the following factors:


    - Whether your source code contains trade secret material;

    - Whether your work is a derivative computer program;

    - Whether the program is fixed on a CD-ROM;

    - Whether different parties own the copyright in the source code and the screen displays generated by the source code;

    - Whether you want to register a user manual or other documentation for the computer program; or

    - Whether you want to register a computer program that runs a videogame.







__ 
Code without Trade Secret Material




If the source code does not contain trade secrets, submit one copy of the first twenty-five pages and last twenty-five pages of the source code for the specific version you want to register. If the source code does not have a precise beginning, middle, or end, submit fifty pages that reason-ably represent the first and last portions of the code. If the entire program is fifty pages or fewer, submit the entire source code and notify the Office you are submitting the entire code. Regardless of the option you select, submit a copy of the page that contains the copyright notice (if any) for the version you want to register.



__ 
Code with Trade Secret Material




If the source code does contain trade secrets, you must indicate in writing to the Office that the code contains trade secret material. Using one of the following options, submit a portion of the code for the specific version you want to register:


    - One copy of the first ten pages and last ten pages, blocking out none of the code;

    - One copy of the first twenty-five pages and last twenty-five pages, blocking out the portions of the code containing trade secret material, provided the blocked out portions are less than fifty percent of the deposit;

    - One copy of the first twenty-five pages and last twenty-five pages of the object code for the program, together with ten or more consecutive pages of source code, blocking out none of the source code (see subheading about object code below);

    - If the source code for the entire program is fewer than fifty pages, one copy of the entire code, blocking out the portions of the code containing trade secret material, provided the blocked out portions represent less than fifty percent of the deposit; or

    - If the source code does not have a precise beginning, middle, or end, twenty to fifty pages that reasonably represent the first and last portions of the code.



Regardless of the option you select, submit the portion of the source code that contains the copy-right notice (if any) for the version you want to register.The Copyright Office strictly applies these rules for submitting source code containing trade secret material. It will refuse to accept a deposit that does not conform to the standards of redaction. Specifi-cally, the blocked out portions of the source code must be proportionately less than the portions that remain unblocked, and the unblocked portions must contain an appreciable amount of copyrightable expression. For guidance on blocking out trade secret material, see chapter 1500, section 1509.1(C)(4)(d), of the Compendium.




__ 
Derivative Computer Programs




You can apply to register a derivative computer program if it contains new material that is both original and sufficiently different from the preexisting work to qualify the derivative program as an original work of authorship. Making only a few minor changes or revisions to a preexisting work, or making changes or revisions that were determined by the functionality of the hardware, does not satisfy this requirement. A registration for a derivative computer program covers the new or revised material that the author contributed to that work. It does not cover any previously published, previ-ously registered, or public domain material that may appear in the program or any authorship fully or partially owned by a third party.


To register a derivative computer program, submit a portion of the source code for the specific version you want to register as follows:
    

    - If the new or revised material appears throughout the entire program, submit the first twenty-five pages and last twenty-five pages of the source code.

    - If the new or revised material does not appear in the first twenty-five pages and last twenty-five pages of the source code, submit any fifty pages of source code that contain new or revised material.

    - If the source code contains trade secret material, submit a portion of the program code using one of the options discussed above regarding source code containing trade secrets.



Regardless of the option you select, submit the portion of the source code that contains the copy-right notice (if any) for the version you want to register.




__ 
Computer Programs on CD-ROMs




If your computer program is fixed in a CD-ROM, submit one complete copy of the entire CD-ROM package, including any instructional material for the program. In addition, submit a portion of the source code for the version of the program you want to register using one of the options discussed above.



__ 
Computer Screen Displays




A computer program and the screen displays it generates are generally considered the same work, because most screen displays are created by the program code. If the same party owns the copyright in the program code and the screen displays, the program and any related screen displays can be registered with the same application. But if different parties own the copyright in the program code and the screen displays, separate applications are required.


To register program code and screen displays owned by the same party, check the box marked “computer program” when completing the “Author Created” field in the online application. There is no need to expressly assert a claim in the screen displays. A registration for a computer program covers the copyrightable expression in the program code and any copyrightable screen displays it generates, even if the application does not specifically mention screen displays and you do not submit a copy of screen displays.


If you specifically claim screen displays in the application, you must submit a representative sampling of those screen displays. For information about submitting material related to computer screen displays, see chapter 1500, section 1509.1(C)(6), of the Compendium.



__ 
User Manuals




You can register user manuals, instructional booklets, flow charts, and other documentation that explain the development or operation of a computer program, provided that they contain a sufficient amount of copyrightable authorship. You can register a computer program and its documentation with one application if the same party owns the copyright in the program and the documentation. If the works have been published, they must have been physically bundled together for distribution to the public and first published in an integrated unit. If the program and the docu-mentation were published separately, or if different parties own them, each element is considered a separate work, and you must submit a separate application for each.


To register a computer program with its documentation, submit one complete copy of the docu-mentation and a portion of the source code using one of the options discussed above.


To register a user manual or other documentation without registering the computer program, submit two complete copies of the documentation if it has been published or one complete copy if it is unpublished. You can upload the documentation to the electronic registration system, prefer-ably as a PDF file, or you can mail it to the Office in a physical form.

## HTML


For registration purposes, hypertext markup language (HTML) is not a computer program. HTML is a standard markup language used in the design of websites. The Copyright Office will not register HTML as a computer program, because HTML does not constitute source code. Source code is typi-cally written by a human being using a computer programming language, such as Java, C, or C++. By contrast, HTML is frequently generated by automated website design software.


You can register HTML as a literary work if it was created by a human being, not a website design program, and if it contains a sufficient amount of creative expression. To register HTML, you must submit a complete copy of the entire code; you cannot submit an isolated portion of the code using the options discussed below.


For more information about registering HTML and website content generally, see Copyright Reg-istration of Websites and Website Content.

## Videogames


A videogame typically contains two major components: audiovisual material that appears on screen and the computer program that runs the game. You can register the audiovisual material for a videogame and the computer program that runs it with one application if the same party owns the copyright in the program and the audiovisual material. If the works have been published, they must have been published together as a single unit. If the program and the audiovisual material were published separately, or if different parties own them, each element is considered a separate work, and you must submit a separate application for each.


If the videogame has been fixed in a CD-ROM, mail one complete copy of the entire CD-ROM package, including a complete copy of any accompanying instruction manual. If the videogame has not been fixed in a CD-ROM, submit identifying material, such as a photograph, that contains rep-resentative portions of the audiovisual elements of the videogame and a brief written description of the game. You can upload identifying material to the electronic registration system, or you can mail it to the Office in a physical form. To register a videogame together with the computer program that runs it, include a portion of the source code using one of the options discussed above.


For more information about deposit requirements for videogames, see chapter 1500, section 1509.2(E)(2), of the Compendium.

## Applications Submitted with Object Code Deposits


The Copyright Office strongly prefers that you submit your copyright application using a source code deposit. You can submit a deposit using the object code of the computer program; however, your claim will be subject to the Copyright Office’s Rule of Doubt. The rule notifies interested parties that although the Office has accepted the claim submitted, it is unwilling to grant a presumption of validity to certain aspects of the claim. For more information about the Rule of Doubt, see chapter 600, section 607, of the Compendium.


If you decide to submit an object code deposit, you must state in writing that the object code contains copyrightable authorship, and you must request registration under the Rule of Doubt. If the object code contains the copyright notice for the program, you must submit the portion of the code where the notice appears. The notice should be underlined or highlighted, and the content of the notice should be presented in words and numbers that an examiner can read.


The Office will examine the object code to determine whether the formal and legal requirements for registration have been met. The Office will not, however, make any determination concerning the copyrightability of object code. Instead, it will accept the applicant’s assertion that the object code contains copyrightable authorship and will register the program under the Rule of Doubt.

## Special Relief from Deposit Requirements


The Copyright Office is authorized to grant special relief from deposit requirements in appropriate circumstances. If the source code for a particular version of a computer program is no longer avail-able, you still may be able to register that version if a substantial portion of the code appears in an earlier or later version of the same program. 


All requests for special relief must be made in writing. You must state the specific reason(s) why you cannot submit the source code for the specific version you want to register, and you must explain what percentage of that code appears in the earlier or later version of the same program. In addition, you must submit a portion of the code from the other version, and your deposit must include at least some portion of the version you want to register. The Office will evaluate your request upon receipt.
