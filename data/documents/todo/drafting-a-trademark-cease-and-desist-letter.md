---
title: Drafting a Trademark Cease and Desist Letter
author: casey@mclaw.io
date: 2021-12-29
category | Intellectual Property and Technology
subcategory | Trademark
tags | trademark, infringement, cease and desist
summary: This guide explains the key issues to consider when responding to a cease and desist letter objecting to the use of an allegedly infringing trademark. In many cases, a well-researched and well-crafted response to a cease and desist letter can cause the objecting party to significantly compromise its position or drop its claims entirely. In contrast, a poorly crafted response may embolden the challenger to pursue its claims in litigation or other formal legal action. Failing to respond to a cease and desist letter may allow the objecting party to use the non-response as evidence of willful infringement.
---