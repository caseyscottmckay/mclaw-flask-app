Drafting a maternity leave policy: 5 things you should know|United States|Labor and Employment|Policies and Procedures|maternity leave|Maternity leave policies in the United States have improved over the last century.

Maternity leave policies in the United States have come a long way from the 1960s when working mothers were considered temporarily disabled under state law. Women now occupy almost half of the workforce and national laws guarantee 12 weeks of unpaid leave (for employees in companies with more than 50 employees) under the Family and Medical Leave Act passed in 1993. But the US remains a laggard when compared with other advanced economies like the United Kingdom, which offers statutory maternity pay covering to 90% of earnings for up to 39 weeks; or Sweden which offers more than 34 weeks maternity leave with an additional 180 days that can be taken or transferred to a partner.


The debate isn’t going away while millions of working mothers remain either uncovered or back at work sooner than is healthy. Roughly a quarter of US mothers are back at work two weeks after giving birth, when most doctors agree at least three months are required to recover from childbirth and establish a proper bond with their newborn.


An increasing number of companies aren’t waiting for the government to take the lead and are offering more than the legal minimum. When most companies are struggling to retain talent and boost productivity, offering sensible maternity leave benefits, among other attractive policies, can be a competitive advantage for small businesses. If your company is looking at this issue, UpLaw’s sample maternity leave policy template can also help you get started.


Meanwhile, here are five things you should know before drafting a maternity leave policy:

## The legal obligations


Maternity leave is guaranteed in most parts of the world. Depending on legislation, it can range from a mere 42 days in Papua New Guinea to a startling 410 days in Bulgaria. Maternity leave pay is compulsory in most countries, except the US, Papua New Guinea and Oman. Whether it involves the full normal pay or only a percentage of it depends on each country’s rules. Knowing what the law demands from you can be tricky, if, for example, you operate on an international level or if local laws differ from the federal/national standards. Getting help from official sources is the definite starting point of developing a company policy.

## Don’t be afraid to give more


Deciding what benefits to offer over and above the legal minimum, may be taxing for employers but it can be worth the effort. In countries with ample statutory maternity leave guidelines, companies may not consider anything more to be necessary. Under more stingy legislation however, they may reap great value from giving their employees generous maternity leave benefits within their capabilities. Big multinationals already lead the way in this regard; small businesses, on the other hand, tend to overestimate the costs while underestimating the benefits involved. So, a local US business for example, must think very hard on what to give its employees to make a maternity leave policy worthwhile.

## Be prepared to solve issues before they arise


After establishing a maternity leave policy, your employees will at some point use the benefits you offer. And then you’ll be faced with the difficult task of rearranging the workload of temporarily vacant positions (some may even involve high responsibility). This can be much easier if you have the foresight to do something before it becomes critical. Strategies for hiring interims or independent contractors, cross-training your employees etc. are things that take time but will reward you in the end.

## A maternity leave policy is not the end of the line


So you have a generous maternity leave policy in place. Your excellent employees feel valued and see you as a fair employer who cares. At the end of their leave, they return to their old role. You quickly see that things have changed: they’ve been away for quite some time and now they have heavier life responsibilities than before. Unfortunately, a maternity leave policy on its own doesn’t make you family-friendly. You must make provision for what happens afterwards: orientation tactics, flexible work arrangements, a workplace breastfeeding policy and even childcare provisions are examples of things you need to consider.

## Companies shouldn’t focus on mothers only


The vast majority of countries have maternity leave guidelines. However, many do not mention any similar benefits for fathers despite a definite trend towards more paternal involvement in caring for children. Even if the law does include them, male employees may still not consider it a given that they are entitled to paternity leave. Employers should not draft a generous maternity leave policy, if they are not willing to draft a paternity policy as well. That way they can show that they value equal opportunity in parenting and avoid anyone feeling underprivileged.
