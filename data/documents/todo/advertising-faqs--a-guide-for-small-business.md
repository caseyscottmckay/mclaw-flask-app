Advertising FAQ's: A Guide for Small Business|United States|Commercial|Advertising and Marketing|advertising, marketing|Focusing on federal truth-in-advertising standards, this A-to-Z primer is an essential resource for businesses of any size. (source = source: https://www.ftc.gov/tips-advice/business-center/guidance/advertising-faqs-guide-small-business)
# Advertising FAQ's: A Guide for Small Business

##### What truth-in-advertising rules apply to advertisers?

Under the Federal Trade Commission Act:
- Advertising must be truthful and non-deceptive;
- Advertisers must have evidence to back up their claims; and
- Advertisements cannot be unfair.
Additional laws apply to ads for specialized products like consumer leases, credit, 900 telephone numbers, and products sold through mail order or telephone sales. And every state has consumer protection laws that govern ads running in that state.
##### What makes an advertisement deceptive?

According to the FTC's Deception Policy Statement, an ad is deceptive if it contains a statement - or omits information - that:
Is likely to mislead consumers acting reasonably under the circumstances; and
Is "material" - that is, important to a consumer's decision to buy or use the product.
##### What makes an advertisement unfair?

According to the Federal Trade Commission Act and the FTC's Unfairness Policy Statement, an ad or business practice is unfair if:
- it causes or is likely to cause substantial consumer injury which a consumer could not reasonably avoid; and
- it is not outweighed by the benefit to consumers.
##### How does the FTC determine if an ad is deceptive?

A typical inquiry follows these steps:
- The FTC looks at the ad from the point of view of the "reasonable consumer" - the typical person looking at the ad. Rather than focusing on certain words, the FTC looks at the ad in context - words, phrases, and pictures - to determine what it conveys to consumers.
- The FTC looks at both "express" and "implied" claims. An express claim is literally made in the ad. For example, "ABC Mouthwash prevents colds" is an express claim that the product will prevent colds. An implied claim is one made indirectly or by inference. "ABC Mouthwash kills the germs that cause colds" contains an implied claim that the product will prevent colds. Although the ad doesn't literally say that the product prevents colds, it would be reasonable for a consumer to conclude from the statement "kills the germs that cause colds" that the product will prevent colds. Under the law, advertisers must have proof to back up express and implied claims that consumers take from an ad.
- The FTC looks at what the ad does not say - that is, if the failure to include information leaves consumers with a misimpression about the product. For example, if a company advertised a collection of books, the ad would be deceptive if it did not disclose that consumers actually would receive abridged versions of the books.
- The FTC looks at whether the claim would be "material" - that is, important to a consumer's decision to buy or use the product. Examples of material claims are representations about a product's performance, features, safety, price, or effectiveness.
- The FTC looks at whether the advertiser has sufficient evidence to support the claims in the ad. The law requires that advertisers have proof before the ad runs.
##### What kind of evidence must a company have to support the claims in its ads?

Before a company runs an ad, it has to have a "reasonable basis" for the claims. A "reasonable basis" means objective evidence that supports the claim. The kind of evidence depends on the claim. At a minimum, an advertiser must have the level of evidence that it says it has. For example, the statement "Two out of three doctors recommend ABC Pain Reliever" must be supported by a reliable survey to that effect. If the ad isn't specific, the FTC looks at several factors to determine what level of proof is necessary, including what experts in the field think is needed to support the claim. In most cases, ads that make health or safety claims must be supported by "competent and reliable scientific evidence" - tests, studies, or other scientific evidence that has been evaluated by people qualified to review it. In addition, any tests or studies must be conducted using methods that experts in the field accept as accurate.
##### Are letters from satisfied customers sufficient to substantiate a claim?

No. Statements from satisfied customers usually are not sufficient to support a health or safety claim or any other claim that requires objective evaluation.
##### My company offers a money-back guarantee. Very few people have ever asked for their money back. Must we still have proof to support our advertising claims?

Yes. Offering a money-back guarantee is not a substitute for substantiation. Advertisers still must have proof to support their claims.
##### What kind of advertising claims does the FTC focus on?

The FTC pays closest attention to:
- Ads that make claims about health or safety, such as:
- ABC Sunscreen will reduce the risk of skin cancer.
- ABC Water Filters remove harmful chemicals from tap water.
- ABC Chainsaw's safety latch reduces the risk of injury.
- Ads that make claims that consumers would have trouble evaluating for themselves, such as:
- ABC Refrigerators will reduce your energy costs by 25%.
- ABC Gasoline decreases engine wear.
- ABC Hairspray is safe for the ozone.
- Ads that make subjective claims or claims that consumers can judge for themselves (for example, "ABC Cola tastes great") receive less attention from the FTC.
##### How does the FTC decide what cases to bring?

The FTC weighs several factors, including:
- FTC jurisdiction. Although the FTC has jurisdiction over ads for most products and services, Congress has given other government agencies the authority to investigate advertising by airlines, banks, insurance companies, common carriers, and companies that sell securities and commodities.
- The geographic scope of the advertising campaign. The FTC concentrates on national advertising and usually refers local matters to state, county, or city agencies.
- The extent to which an ad represents a pattern of deception, rather than an individual dispute between a consumer and a business or a dispute between two competitors. State or local consumer protection agencies or private groups such as the Better Business Bureau (BBB) often are in a better position to resolve disputes involving local businesses or local advertising. To get the address and phone number of your state Attorney General's office, your local consumer agency, or the nearest BBB, check your telephone directory.
- The amount of injury - to consumers' health, safety, or wallets - that could result if consumers rely on the deceptive claim. The FTC concentrates on cases that could affect consumers' health or safety (for example, deceptive health claims for foods or over-the-counter drugs) or cases that result in widespread economic injury.
##### What penalties can be imposed against a company that runs a false or deceptive ad?

The penalties depend on the nature of the violation. The remedies that the FTC or the courts have imposed include:
- Cease and desist orders. These legally-binding orders require companies to stop running the deceptive ad or engaging in the deceptive practice, to have substantiation for claims in future ads, to report periodically to FTC staff about the substantiation they have for claims in new ads, and to pay a fine of $43,280 per day per ad if the company violates the law in the future.
- Civil penalties, consumer redress and other monetary remedies. Civil penalties range from thousands of dollars to millions of dollars, depending on the nature of the violation. Sometimes advertisers have been ordered to give full or partial refunds to all consumers who bought the product.
- Corrective advertising, disclosures and other informational remedies. Advertisers have been required to take out new ads to correct the misinformation conveyed in the original ad, notify purchasers about deceptive claims in ads, include specific disclosures in future ads, or provide other information to consumers.
##### Will the FTC review my company's ads before they run to make sure that we've complied with the law?

- FTC staff cannot clear your ads in advance. However, there is guidance to help you comply with the law. Information about advertising particular kinds of products (for example, foods, dietary supplements, or "environmentally friendly" merchandise), advertising credit, and guidelines for advertising on the Internet is available at www.ftc.gov. For more general information on advertising policies, call the FTC's Division of Advertising Practices at 202-326-3090.
##### How can I keep up-to-date on what's going on at the FTC?

- The Federal Trade Commission website (www.ftc.gov) is updated almost every day, so bookmark it for instant access to FTC news and views, including recent enforcement actions, speeches, public hearings, and other business information. Before running an ad, check out what the FTC has had to say about products or advertising claims similar to yours. From the homepage, you can search the entire FTC website using key words or phrases. For example, a search using the word "diet" will yield cases, reports, news releases, and other materials related to FTC policies about the advertising of diet products and services. In addition, you can visit www.consumer.gov for consumer and business information from the FTC, FDA, SEC, and other federal agencies. You also may want to check with the Better Business Bureau for tips on truthful advertising, the BBB's voluntary Code of Advertising, and information about scams targeting small businesses.
##### How does the FTC address the needs of small businesses?

- In its continuing commitment to regulatory reform, the FTC has repealed almost 50% of its trade regulation rules and has streamlined and simplified remaining rules. The FTC's Small Business Compliance Assistance Policy Statement describes other forms of assistance available to small businesses to help them comply with truth-in-advertising laws. For example, the Business Guidance section of the FTC's website (www.ftc.gov) includes an expanding library of materials written especially for small businesses. Small businesses also may contact the FTC headquarters or one of the FTC's regional offices with specific inquiries about how to comply with the law. In addition, one of the FTC's top law enforcement priorities is fighting fraudulent and deceptive practices aimed at small businesses. The agency has taken the lead in challenging deceptive invention promotion services, questionable franchise opportunities, bogus office supply scams, and other practices that prey on aspiring entrepreneurs.
##### What can my company do if a competitor is running an ad that I think is deceptive?

You can:
- Explore your legal options under federal and state statutes that protect businesses from unfair competition. For example, the Lanham Act gives companies the right to sue their competitors for making deceptive claims in ads.
- File a complaint with the National Advertising Division (NAD) of the Council of Better Business Bureaus, if your competitor's ad is running nationally or regionally. The NAD is a private, self-regulatory group affiliated with the BBB. It investigates allegations of deceptive advertising and gives advertisers a mechanism for resolving disputes voluntarily.
- Call your local BBB or file an online complaint with the Better Business Bureau if the ad is local. Many BBBs have procedures for resolving disputes between businesses.
- Contact the radio station, television station, or publication where the ad ran. Let them know that they're running an ad you think may be deceptive.
- Contact your state Attorney General's Office or your city, county, or state Office of Consumer Affairs. To get their phone numbers, check your telephone directory.
Contact the FTC. By mail: Federal Trade Commission, Consumer Response Center, 600 Pennsylvania Avenue, NW, Washington, DC 20580; by telephone: toll-free 1-877-FTC-HELP.
##### If my company files a complaint about a competitor with the FTC, will the FTC resolve the dispute?

The FTC is authorized to act when it appears that a company's advertising is deceptive and when FTC action is in the public interest. Although the FTC cannot intervene in an individual dispute between two companies, the agency relies on many sources - including complaints from consumers and competitors - to find out about ads that may be deceptive. To file a complaint against a competitor who you believe has engaged in false advertising, contact:
Federal Trade Commission
Consumer Response Center
600 Pennsylvania Avenue, NW
Washington, DC 20580
Toll-free 1-877-FTC-HELP (382-4357)
Online Complaint Form
##### If my company files a complaint against a competitor with the FTC, will we be kept informed about the status of any investigation?

No. The FTC keeps investigations confidential. Matters become public only after the FTC reaches a settlement with a company or files a lawsuit. However, you can be assured that complaints received from companies alleging that competitors are advertising deceptively are reviewed carefully.
##### Can I find out if the FTC already has an investigation against a company?

The FTC can tell you if it has already taken formal action (e.g., filed or settled a lawsuit) against a particular company or against similar kinds of advertisements or products. But the FTC cannot disclose whether an investigation is going on. To find out if a company or product has been the subject of a recent FTC action, search the FTC's website (www.ftc.gov).
## OTHER ADVERTISING ISSUES

### Advertising Agencies

##### Are advertising agencies subject to the FTC Act?

Yes. In addition to the advertiser, the advertising agency also may be held legally responsible for misleading claims in ads. Advertising agencies have a duty to make an independent check on the information used to substantiate ad claims. They may not rely on an advertiser's assurance that the claims are substantiated. In determining whether an ad agency should be held liable, the FTC looks at:
- The extent of the agency's participation in the preparation of the challenged ad; and
- Whether the agency knew or should have known that the ad included false or deceptive claims.
### Alcohol Advertising

##### Does the FTC regulate ads for alcoholic beverages?

The FTC can take action if an alcohol ad is deceptive or unfair. The Bureau of Alcohol, Tobacco and Firearms (ATF) also has jurisdiction over deceptive or misleading alcohol labeling and advertising.
##### Is it legal to advertise distilled spirits on TV?

Until 1996, there was a voluntary policy within the distilled spirits industry not to advertise on television. Many broadcasters still do not accept ads for distilled spirits.
##### Are there limits on where ads for alcoholic beverages can run and what they can say?

Like ads for all other products, ads for alcoholic beverages must be truthful and any claims must be substantiated. In addition, alcohol ads by their content or placement may not be directed to underage consumers. Some broadcasters and publishers place additional restrictions on where or when alcohol ads can run. For more information about voluntary codes, self-regulatory programs, and industry "best practices" in the area of alcohol advertising, ask the FTC for its Report to Congress, Self-Regulation in the Alcohol Industry: A Review of Industry Efforts to Avoid Promoting Alcohol to Underage Consumers.
### Bait and Switch

##### How does the FTC define "bait and switch" advertising?

It's illegal to advertise a product when the company has no intention of selling that item, but instead plans to sell a consumer something else, usually at a higher price. For more information, ask the FTC for its Guides Against Bait Advertising.
### Catalogs

##### My company distributes a catalog of products manufactured by other companies. What's our responsibility for ensuring the accuracy of what's in the catalog?

Rather than just repeating what the manufacturer says about a product, catalog marketers - including companies with online catalogs - should ask for material to back up the claims. If the manufacturer doesn't come forward with proof or turns over questionable material, a catalog marketer should see a yellow "caution light." This is especially true for products with extravagant performance claims, health or weight loss promises, earnings guarantees, and the like. In writing ad copy, catalogers should stick to the claims that can be supported and avoid embellishing manufacturers' representations. Most importantly, catalog marketers should trust their instincts when a product sounds too good to be true. For more information about selling merchandise by catalog or through direct marketing, ask the FTC for the Business Guide to the Federal Trade Commission's Mail or Telephone Order Merchandise Rule and Business Checklist for Direct Marketers.
### Children's Advertising

##### What standards does the FTC apply when evaluating claims in ads aimed at children?

The FTC pays particular attention to ads aimed at children because children may be more vulnerable to certain kinds of deception. Advertising directed to children is evaluated from a child's point of view, not an adult's. The FTC also works with the Children's Advertising Review Unit (CARU) of the Council of Better Business Bureaus. CARU is a private, self-regulatory group affiliated with the BBB that publishes self-regulatory guides for children's advertising.
##### My company is thinking about doing a website for kids. Are there any issues involving children and the Internet that we should be aware of?

The Children's Online Privacy Protection Act, a federal law that requires websites to obtain verifiable parental consent before collecting, using, or disclosing personal information from children, including their names, home addresses, email addresses, or hobbies. The FTC has issued a rule outlining the procedures for commercial websites to use in obtaining parental consent. The rule applies to operators of commercial websites and online services directed to children under 13, and general audience sites that know that they are collecting personal information from a child. For more information, ask the FTC for How to Comply with the Children's Online Privacy Protection Rule. Visit the FTC's website (www.onguardonline.gov/topics/kids-privacy.aspx) for more information about how to protect kids' privacy online.
### Clothing and Textiles

##### Are there any requirements for advertising clothing or other textiles?

The labeling and advertising of clothing and textiles are governed by special statutes and regulations. For example, mail order catalogs and websites must disclose whether the fabric was imported or made in the United States. There are other requirements also. For more information, ask the FTC for the Rules under the Wool Products Act, the Fur Products Labeling Act, and the Textile Fiber Products Identification Act, and Threading Your Way Through the Labeling Requirements Under the Textile and Wool Acts. Visit the FTC’s website (/os/statutes/textilejump.htm) for information about the FTC’s Registered Identification Number (RN) system for companies that manufacture, import, or sell textiles.
### Comparative Advertising

##### Is it legal for a company to compare its product to another company's product in an ad?

Comparative advertising is legal as long as it is truthful. For more information, ask the FTC for the Comparative Advertising Policy Statement.
### Contests and Sweepstakes

##### Are there any rules about ads for contests or sweepstakes?

Sweepstakes-type promotions that require a purchase by participants are illegal in the United States. Other agencies, including the United States Postal Service (USPS) and the Federal Communications Commission (FCC), also enforce federal laws governing contests and prize promotions. And each state has laws that may require promoters to make disclosures, seek licensing, or post a bond. Since state laws vary, check with the Attorney General's Office in the state(s) in which you plan to advertise. If a contest or promotion involves telephone calls, the FTC's Telemarketing Sales Rule requires specific disclosures, such as the odds of winning a prize, how to participate without buying anything, and that no purchase or payment is required to win. If pay-per-call services are involved, the FTC's 900 Number Rule requires certain disclosures. For more information, ask the FTC for the publications Complying with the Telemarketing Sales Rule and Complying with the 900 Number Rule.
### Credit

##### What information must be included in ads for consumer credit?

According to the Truth in Lending Act and other federal and state laws, ads for consumer credit must include certain disclosures about the terms and conditions of credit. These laws specifically require the disclosures to be "clear and conspicuous" so that reasonable consumers can read (or hear) and understand the information.
Dietary Supplements
##### How does the FTC evaluate claims for "health foods," vitamins, dietary supplements, and similar products?

Claims for dietary supplements and similar products must be truthful and advertisers must have substantiation for any objective product claims they make. Ask the FTC for Dietary Supplements: An Advertising Guide for Industry.
Disclosures and Disclaimers
##### Does FTC law specify how disclaimers or disclosures must appear in ads?

Some laws and regulations enforced by the FTC, such as the 900 Number Rule, the Truth in Lending Act, and the Consumer Leasing Act, have specific requirements that apply to advertising, including that certain information must be "clearly and conspicuously" disclosed. For more information, ask the FTC for the publications Complying with the 900 Number Rule and Advertising Consumer Leases.
##### How prominent does a disclaimer or disclosure have to be in other kinds of ads?

When the disclosure of qualifying information is necessary to prevent an ad from being deceptive, the information should be presented clearly and conspicuously so that consumers can actually notice and understand it. A fine-print disclosure at the bottom of a print ad, a disclaimer buried in a body of text unrelated to the claim being qualified, a brief video superscript in a television ad, or a disclaimer that is easily missed on a website are not likely to be effective. Nor can advertisers use fine print to contradict other statements in an ad or to clear up misimpressions that the ad would leave otherwise. For example, if an ad for a diet product claims "Lose 10 pounds in one week without dieting," the fine-print statement "Diet and exercise required" is insufficient to remedy the deceptive claim in the ad. To ensure that disclosures are effective, advertisers should use clear and unambiguous language, place any qualifying information close to the claim being qualified, and avoid using small type or any distracting elements that could undercut the disclosure. Although there is no hard-and-fast rule about the size of type in a print ad or the length of time a disclosure must appear on TV, the FTC often has taken action when a disclaimer or disclosure is too small, flashes across the screen too quickly, is buried in other information, or is otherwise hard for consumers to understand. Most importantly, if you are concerned that a disclaimer or disclosure may be necessary to clarify a claim, evaluate your ad copy and substantiation carefully to ensure that you are not misleading consumers.
##### What about disclaimers and disclosures online?

Regardless of whether you advertise on TV or radio, in print ads, through direct mail or online, the law is the same: disclaimers and disclosures must be "clear and conspicuous." Dot Com Disclosures offers special guidance for online advertisers regarding 'Net specific issues such as banner ads, pop-up windows, scrolling, hyperlinks, etc.
### Drug Advertising

##### Does the FTC have rules on advertisements for over-the-counter (OTC) drugs?

The FTC handles most matters regarding claims in advertisements for over-the-counter drugs. The Food and Drug Administration (FDA) handles most matters regarding the labeling of OTC drugs. As with any other product, claims for OTC drugs must be truthful and non-deceptive. Given the health and safety issues that can arise in marketing these products, advertisers should take care in substantiating their claims. Depending on the claim, advertisers may be required to back up their representations with competent and reliable scientific evidence, including tests, studies, or other objective data. For more information about labeling OTC drugs, visit the FDA's website at www.fda.gov or call the FDA Inquiry Line, 1-888-INFO-FDA.
##### Does the FTC have specific rules on advertisements for prescription drugs?

No. The FDA handles most matters related to the advertising of prescription drugs. For more information about marketing prescription drugs, visit the FDA's website at www.fda.gov or call the FDA Inquiry Line, 1-888-INFO-FDA.
### Endorsements and Testimonials

##### Are there any rules on how endorsements may be used in ads?

The FTC's Guides Concerning the Use of Testimonials and Endorsements offer practical advice on endorsements by consumers, celebrities, and experts. All endorsements must reflect the honest experience or opinion of the endorser. Endorsements may not contain representations that would be deceptive, or could not be substantiated, if the advertiser made them directly.
Endorsements by consumersmust reflect the typical experience of consumers who use the product, not the experience of just a few satisfied customers. If an endorsement doesn't reflect users' typical experience, the ad must clearly disclose either what consumers can expect their results to be or the limited applicability of the endorser's experience. Saying "Not all consumers will get these results" or "Your results may vary" is not enough.
Endorsements by celebrities must reflect the celebrity's honest experience or opinion. If the endorsement represents that the celebrity uses the product, that celebrity actually must use the product. Once a celebrity (or expert) has endorsed a product, the advertiser has an obligation to make sure the endorsement continues to reflect the endorser's opinion.
To give an expert endorsement, a person must have sufficient qualifications to be considered an expert in the field. But just being an expert isn't enough. Expert endorsements must be supported by an actual evaluation, examination, or testing of the product that other experts in the field normally would conduct to support the conclusions in the endorsement.
Advertisers also must disclose any material connection between a person endorsing a product and the company selling the product. A "material connection" is defined as a relationship that might affect the weight or credibility of the endorsement. For example, if an endorser is an employee or relative of the advertiser, that fact must be disclosed because it is relevant to how much weight a consumer would give to the endorsement. Similarly, an advertiser must disclose if a consumer has been paid for giving an endorsement.
### Energy Savings Claims

##### Are there rules for making energy savings claims in ads?

The FTC's Appliance Labeling Rule and the R-Value Rule address energy savings claims for appliances, lighting products, and insulation. For example, under these rules, energy efficiency claims in ads must be based on specific standardized tests.
### Environmental Advertising

##### Are there rules for using environmental claims like "recycled" or "ozone-friendly"?

The FTC's Guides for the Use of Environmental Claims cover how words like biodegradable, recyclable, and environmentally friendly can be used in ads. In addition, some states have laws governing environmental claims. Check with the Attorney General's office of the state(s) where you plan to advertise. If you make environmental claims or use environmental symbols on your direct mail advertising, ask the FTC for Making Environmental Marketing Claims on Mail.
### Food Advertising

##### What kind of health claims can be made in food ads? When can advertisers use words like “lite,” "low fat" or "high fiber"?

The FTC handles most matters regarding claims in food advertisements. The FDA handles most matters regarding food labels. For guidance on how the FTC evaluates claims made in food ads, ask the FTC for the Enforcement Policy Statement on Food Advertising. For information about product labeling, visit the FDA's website at www.fda.gov or call the FDA Inquiry Line, 1-888-INFO-FDA. For information about meat and poultry, visit the Department of Agriculture (USDA) website, www.usda.gov, or call the USDA's Center for Nutrition Policy and Promotion, 202-418-2312.
### Food and Drug Administration

##### When are claims regulated by the Federal Trade Commission and when are they regulated by the Food and Drug Administration?

The FTC and the FDA have a long-standing liaison agreement to allocate their efforts efficiently. As a general rule, advertising for foods, over-the-counter drugs, dietary supplements, medical devices, and cosmetics is regulated by the FTC. Labeling for these products is regulated by the FDA. In addition, the FDA handles most matters related to prescription drug advertising and labeling. For more information about marketing a product within the FDA's jurisdiction, visit the FDA's website at www.fda.gov or call the FDA Inquiries Line, 1-888-INFO-FDA.
### Franchises and Business Opportunities

##### What rules apply to ads for franchises or business opportunities?

The FTC's Trade Regulation Rule on Franchises and Business Opportunities ("Franchise Rule") governs the sale of franchises and business opportunities. The law requires sellers to make specific disclosures, give prospective buyers a document containing certain key information about the business opportunity, and be able to substantiate any earnings claims. For more information about buying or selling a franchise, ask the FTC for the Franchise Rule and Your Legal Rights: A Guide To The FTC Franchise Rule. In addition, visit the FTC website for information on what entrepreneurs need to know before buying any kind of business opportunity and alerting them to the latest "bizop" scams. Many states also have laws governing these transactions. Check with the Attorney General's office in the state(s) where you plan to advertise.
### "Free" Claims and Rebate Offers

##### When can a company advertise something as "free"?

When a "free" offer is tied to the purchase of another product, the price of the purchased product should not be increased from its regular price. For more information, ask the FTC for the Guides Concerning Use of the Word "Free" and Other Representations and the Guides Against Deceptive Pricing. In addition, if you're advertising a product as "free" or offering it at a low cost in conjunction with the purchase of another item, the ad should clearly and conspicuously disclose the terms and conditions of the offer. Disclose the most important information - like the terms affecting the cost of the offer - near the advertised price. For more information, ask the FTC for Big Print. Little Print. What's the Deal? You also may want to check with the Attorney General's office in the state(s) where you plan to advertise. In addition, the Better Business Bureau has voluntary standards for when something can be advertised as "free."
##### What are the rules on advertising rebates to consumers?

Ads that include rebate promotions should prominently state the before-rebate cost, as well as the amount of the rebate. Only then will consumers know their actual out-of-pocket cost and have the information they need to comparison shop. Rebate promotions also should clearly disclose any additional terms and conditions that consumers need to know, including the key terms of any purchase requirements, additional fees, and when consumers can expect to receive their rebate. The FTC's brochure Big Print. Little Print. What's the Deal? outlines other factors advertisers should bear in mind when making rebate promotions.
### Guarantees

##### When a company advertises that products are sold with a guarantee or warranty, what information about the terms and conditions must be included in the ads?

If an ad mentions that a product comes with a guarantee or warranty, the ad should clearly disclose how consumers can get the details. Any conditions or limits on the guarantee or warranty (such as a time limit or a requirement that the consumer return the product) also must be clearly disclosed in the ad. Finally, the law requires companies to make copies of any warranties available to consumers before the sale. This applies to retail sales, sales by phone or mail, and online transactions. For more information, ask the FTC for the Guides for the Advertising of Warranties and Guarantees.
### Infomercials

##### Does the FTC have any special policies relating to infomercials?

Infomercial advertisers must have proof to back up all express and implied claims that reasonable consumers would take from an ad. In addition, advertisers should make sure that the infomercial doesn't deceptively mimic the format of news reports, talk shows, or other independent programming. FTC cases have required companies to clearly disclose that "THE PROGRAM YOU ARE WATCHING IS A PAID ADVERTISEMENT FOR [NAME OF PRODUCT]" at the beginning of an infomercial and before ordering information is given. Since many infomercials feature endorsements from consumers, celebrities, or experts, ask the FTC for the Guides Concerning the Use of Testimonials and Endorsements for more information.
##### My company produces infomercials for other businesses. What responsibility do we have to make sure that the claims are truthful?

The Commission looks at the facts of each case to determine whether the infomercial producers' role in the promotion makes them liable for deceptive claims in the ad. In many instances, the FTC has taken action against both the manufacturer or marketer of a product and the company that produced the infomercial. Therefore, infomercial producers should ask for materials to back up the claims in the ad. The bottom line: no one involved in the promotion of a product or the dissemination of an ad - whether it's an infomercial, print ad, catalog, or broadcast spot - should look the other way when the claims seem questionable.
### Internet Advertising

##### Is advertising on the Internet subject to the same laws as other advertising?

Yes. Ad claims on the Internet must be truthful and substantiated. Ask the FTC for a copy of Advertising and Marketing on the Internet: The Rules of the Road for more information. Dot Com Disclosures offers special guidance for online advertisers regarding how to make sure that any disclaimers and disclosures in online ads are clear and conspicuous. It addresses 'Net specific issues such as banner ads, pop-up windows, scrolling, hyperlinks, etc. Internet marketers also should be aware that the FTC's Mail or Telephone Order Merchandise Rule ("Mail Order Rule") applies to online transactions. For specific guidance on complying with the Mail Order Rule online, ask the FTC for a copy of Selling on the Internet: Prompt Delivery Rules, as well as A Business Guide to the Federal Trade Commission's Mail or Telephone Order Merchandise Rule.
##### My website is attracting visitors from outside the United States. What do I need to know?

Because the World Wide Web is, as its name implies, worldwide, even small online businesses can reach customers around the globe. Electronic Commerce: Selling Internationally - A Guide for Business discusses some online commerce guidelines endorsed by the United States government and 28 other countries.
##### What do I need to know about consumer privacy online?

Advertisers should be aware of the privacy issues raised by Internet marketing. For more information about recent FTC Reports to Congress on consumer privacy on the Internet, visit the FTC's website (www.ftc.gov). Basically, the FTC strongly encourages companies to implement four fair information practices: giving consumers notice of a website's information practices; offering consumers choice as to how their personally identifying information is used; providing consumers with access to the information collected about them; and ensuring the security of the information collected. In addition, companies need to know about the Children's Online Privacy Protection Act and the rule that implements it. The law requires websites to obtain verifiable parental consent before collecting, using, or disclosing personal information from children, including their names, home addresses, email addresses, or hobbies. For more information, ask the FTC for How to Comply with the Children's Online Privacy Protection Rule.
### Jewelry

##### Are there special guides for advertising jewelry?

The FTC's Jewelry Guides cover claims made for gold, silver, platinum, pewter, diamonds, gemstones, and pearls and define how certain common terms may be used in ads. For example, the Guides explain when a product can be called "gold plated" or when a diamond can be called "flawless." For more information, ask the FTC for the Guides for the Jewelry, Precious Metals, and Pewter Industries and In The Loupe: Advertising Diamonds, Gemstones and Pearls.
### Leasing

##### What information must a company include when advertising leases for cars, household goods, or other products?

The Consumer Leasing Act and Regulation M include specific rules that apply to ads for consumer leases. For example, if a lease advertisement includes certain terms - such as the amount of any payment due before or at lease inception - the ad also must make other clear and conspicuous mandatory disclosures about the terms of the lease. These rules also apply if the ad contains phrases like "no money down" or "no down payment." For more information, ask the FTC for a copy of Advertising Consumer Leases.
### Made in the U.S.A.

##### When can my company advertise that our product is "Made in the U.S.A."?

A product has to be "all or virtually all made in the United States" for it to be advertised or labeled as "Made in the U.S.A." For more information, ask the FTC for the Enforcement Policy Statement on U.S. Origin Claims.
### Mail Order Advertising

##### What rules must a company follow if it sells products via mail order?

Truth-in-advertising standards - including that companies must have proof to back up express and implied claims made about its products - apply to mail order marketers. In addition, the FTC's Mail or Telephone Order Merchandise Rule ("Mail Order Rule") applies when a consumer places an order by mail, telephone, fax, or computer. Under the Rule, a company must have a reasonable basis for believing that it can ship the product within the time period stated in the ad. If the ad doesn't specify a time period, the company must have a reasonable basis for believing that it can ship within 30 days. The Mail Order Rule applies equally to online marketers. For more information, ask the FTC for A Business Guide to the Federal Trade Commission's Mail or Telephone Order Merchandise Rule and A Business Checklist for Direct Marketers. Companies that advertise online also should get a copy of Selling on the Internet: Prompt Delivery Rules. In addition, the Direct Marketing Association, a trade group for members of the direct marketing industry, has voluntary guidelines on ethical business practices.
##### Is it okay for a company to "dry test" a product?

"Dry testing" describes the practice of placing an ad for a product to see if there is sufficient consumer interest before actually going to the expense of manufacturing the item. Although the Mail Order Rule doesn't specifically deal with this situation, the FTC has issued an advisory opinion that such ads must clearly disclose to consumers the fact that the merchandise is only planned and may not ever be shipped. For more information, ask the FTC for A Business Guide to the Federal Trade Commission's Mail or Telephone Order Merchandise Rule.
### Negative Option Offers

##### Are there any rules regarding ads for "negative option" plans?

The FTC's Negative Option Rule applies to sellers of subscription plans who ship merchandise like books or compact discs to consumers who have agreed in advance to become subscribers. The Rule requires that ads clearly and conspicuously disclose material information about the terms of the plan. Further, once consumers agree to enroll, the company must notify them before shipping to allow them to decline the merchandise. Even if an automatic shipment or continuity program doesn't fall within the specifics of the Negative Option Rule, companies should be careful to clearly disclose the terms and conditions of the plan before billing consumers or charging their credit cards. For more information, ask the FTC for the Negative Option Rule.
### "New" Claims

##### When can a company advertise a product as "new"?

The answer depends on how the ad uses the word "new." For example, under the rules governing the identification of textiles, fabric cannot be advertised as "new" if it has been reclaimed or respun. The rules governing advertising claims for tires prohibit the use of the word "new" to describe retreads. However, when no specific regulation applies, each case must be considered within the context of the ad. At least one FTC advisory opinion has suggested a six-month limit on the use of the word when advertising the introduction of a "new" product not previously on the market.
### Pricing

##### Are there any standards governing the advertising of prices?

The same standards for truthfulness apply when companies make claims about price comparisons, "sale" prices, and the like. For more information, ask the FTC for the Guides Against Deceptive Pricing. Since many pricing issues involve local practices, you also may want to contact the Attorney General's office in the state(s) where you plan to advertise.
##### What responsibility does a company have to make sure that prices are accurate?

In many jurisdictions, companies are legally required to charge no more than the advertised or shelf price for a product, so good pricing practices are important for both customer satisfaction and a company’s bottom line. For tips on accurate pricing practices in advertising and in retail stores, ask the FTC for Good Pricing Practices? SCAN DO.
### Rainchecks

##### How much of an advertised product is a retail store required to stock?

According to the FTC’s Retail Food Store Rule, grocers must offer rainchecks or product substitutes of comparable value when they run out of advertised items. They also can comply by ordering quantities of the item sufficient to meet reasonably anticipated demand or by disclosing in ads that items are available only in limited quantities or only at some stores. Although the specific terms of the Rule apply only to retail food stores, other companies advertising products available in limited quantity or only at some stores may want to make similar disclosures to reduce the risk of deception. For more information, ask the FTC for a copy of Retail Food Store Advertising and Marketing Practices.
### Sales

##### When may a company advertise that a product is "on sale"?

The same standards for truthfulness apply when a company makes advertising claims about sale prices or products being “on sale.” For more information, ask the FTC for the Guides Against Deceptive Pricing. Since this issue often involves local practices, you may also want to contact the Attorney General’s office in the state(s) where you plan to advertise.
##### When can a company advertise a "going out of business sale"?

The short answer is: only when a store is going out of business. It would be deceptive to advertise a “going out of business sale” when a store is not going out of business. If a store in your area is advertising what looks to be a bogus “going out of business sale,” contact your state Attorney General’s office.
### Subliminal Advertising

##### Is it legal to use subliminal advertising techniques?

It would be deceptive for marketers to embed ads with so-called subliminal messages that could affect consumer behavior. However, most consumer behavior experts have concluded that such methods aren’t effective.
### Telemarketing

##### What rules must a company follow if it sells goods or services over the telephone?

As with any other form of advertising or promotion, claims made through telemarketing must be truthful and substantiated. In addition, the FTC’s Telemarketing Sales Rule applies to transactions involving the use of interstate telephone calls to sell goods or services. The Rule requires companies to make specified disclosures in their telephone calls and prohibits misrepresentations. For more information, ask the FTC for a copy of Complying with the Telemarketing Sales Rule.
### Telephone Services

##### What rules apply to the advertising of long-distance services, including long-distance calling plans, dialaround (or "10-10" numbers), and pre-paid phone cards?

The FTC and the Federal Communications Commission work together to ensure that ads for long-distance services are truthful. If ads tout price, companies should make sure that the ads clearly and conspicuously disclose the actual cost to consumers, including monthly fees or per-call surcharges, not just a perminute rate. In addition, ads should clearly and conspicuously disclose any limitations on the advertised service, including time, day, or geographic restrictions. Ask the FTC for the Joint FTC-FCC Policy Statement on the Advertising of Dial-Around and Other Long-Distance Services to Consumers. In addition, advertisers should visit the FCC’s web page (www.fcc.gov) and check out any state laws in the jurisdictions they plan to advertise in.
##### What information must be included in ads for 900 number or "pay-per-call" services?

The FTC’s 900 Number Rule requires that ads for these services “clearly and conspicuously” disclose the cost of the call. The law is very specific about what “clear and conspicuous” means, depending on whether the ad appears on radio, TV, or in print. The rule also requires that 900 number ads directed at consumers under age 18 disclose that parental permission is required before calling. The rule prohibits 900 number services directed at children under 12, unless it is a “bona fide educational service.” In addition, 900 number ads that promote sweepstakes must state the odds of winning or, if the odds cannot be determined, the factors that determine the odds. Ads for 900 number services that provide information on federal programs, but are not affiliated with the government, also must contain certain disclosures. For more information, ask the FTC for a copy of Complying with the 900 Number Rule.
### Tobacco Advertising

##### How does the FTC regulate ads for cigarettes, cigars, and smokeless tobacco?

The FTC can take action if an ad for cigarettes, cigars, or a smokeless tobacco product is deceptive or unfair. In addition, federal law prohibits the advertising of cigarettes, smokeless tobacco, and little cigars on radio, TV, or other forms of electronic media regulated by the Federal Communications Commission. The FTC also enforces various federal requirements mandating health warnings on advertising, point-ofpurchase displays, and packaging of tobacco products. For more information, see Memorandum to Potential Cigarette Manufacturers and Importers.
### Weight Loss Products

The FTC has taken action against hundreds of advertisers who have falsely promised easy weight loss. Marketers who promote diet products or services or who make representations about fat loss, weight loss, calorie burning, or the loss of inches or cellulite must make sure that their claims are backed up by sound scientific evidence. The FTC is a member of the Partnership for Healthy Management and supports its Voluntary Guidelines for Providers of Weight Loss Products or Services.
For More Information
The FTC works for the consumer to prevent fraudulent, deceptive, and unfair business practices in the marketplace and to provide information to help consumers spot, stop, and avoid them. To file a complaint or to get free information on consumer issues, visit ftc.gov or call toll-free, 1-877-FTC-HELP (1-877-382-4357); TTY: 1-866-653-4261. The FTC enters consumer complaints into the Consumer Sentinel Network, a secure online database and investigative tool used by hundreds of civil and criminal law enforcement agencies in the U.S. and abroad.
[Note: Edited January 2020 to reflect Inflation-Adjusted Civil Penalty Maximums.]