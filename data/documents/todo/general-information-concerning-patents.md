https://www.uspto.gov/patents-getting-started/general-information-concerning-patents#heading-2
General Information Concerning Patents|United States|Intellectual Property and Technology|Patent|patent law|This article covers general information concerning patents.
# General information concerning patents

## Functions of the United States Patent and Trademark Office


The United States Patent and Trademark Office (USPTO or Office) is an agency of the U.S. Department of Commerce. The role of the USPTO is to grant patents for the protection of inventions and to register trademarks. It serves the interests of inventors and businesses with respect to their inventions and corporate products, and service identifications. It also advises and assists the President of the United States, the Secretary of Commerce, the bureaus and offices of the Department of Commerce, and other agencies of the government in matters involving all domestic and global aspects of “intellectual property.” Through the preservation, classification, and dissemination of patent information, the Office promotes the industrial and technological progress of the nation and strengthens the economy.


In discharging its patent related duties, the USPTO examines applications and grants patents on inventions when applicants are entitled to them; it publishes and disseminates patent information, records assignments of patents, maintains search files of U.S. and foreign patents, and maintains a search room for public use in examining issued patents and records. The Office supplies copies of patents and official records to the public. It provides training to practitioners as to requirements of the patent statutes and regulations, and it publishes the Manual of Patent Examining Procedure to elucidate these. Similar functions are performed relating to trademarks. By protecting intellectual endeavors and encouraging technological progress, the USPTO seeks to preserve the United States’ technological edge, which is key to our current and future competitiveness. The USPTO also disseminates patent and trademark information that promotes an understanding of intellectual property protection and facilitates the development and sharing of new technologies worldwide.

## What Are Patents, Trademarks, Servicemarks, and Copyrights?


Some people confuse patents, copyrights, and trademarks. Although there may be some similarities among these kinds of intellectual property protection, they are different and serve different purposes.

### What is a Patent?


A patent for an invention is the grant of a property right to the inventor, issued by the United States Patent and Trademark Office. Generally, the term of a new patent is 20 years from the date on which the application for the patent was filed in the United States or, in special cases, from the date an earlier related application was filed, subject to the payment of maintenance fees. U.S. patent grants are effective only within the United States, U.S. territories, and U.S. possessions. Under certain circumstances, patent term extensions or adjustments may be available.


The right conferred by the patent grant is, in the language of the statute and of the grant itself, “the right to exclude others from making, using, offering for sale, or selling” the invention in the United States or “importing” the invention into the United States. What is granted is not the right to make, use, offer for sale, sell or import, but the right to exclude others from making, using, offering for sale, selling or importing the invention. Once a patent is issued, the patentee must enforce the patent without aid of the USPTO.


There are three types of patents:


    - 1) __ Utility patents
 may be granted to anyone who invents or discovers any new and useful process, machine, article of manufacture, or composition of matter, or any new and useful improvement thereof;

    - 2) __ Design patents
 may be granted to anyone who invents a new, original, and ornamental design for an article of manufacture; and

    - 3) __ Plant patents
 may be granted to anyone who invents or discovers and asexually reproduces any distinct and new variety of plant.





###  What Is a Trademark or Servicemark?


A trademark is a word, name, symbol, or device that is used in trade with goods to indicate the source of the goods and to distinguish them from the goods of others. A servicemark is the same as a trademark except that it identifies and distinguishes the source of a service rather than a product. The terms “trademark” and “mark” are commonly used to refer to both trademarks and servicemarks.


Trademark rights may be used to prevent others from using a confusingly similar mark, but not to prevent others from making the same goods or from selling the same goods or services under a clearly different mark. Trademarks that are used in interstate or foreign commerce may be registered with the USPTO. The registration procedure for trademarks and general information concerning trademarks can be found in the separate book entitled “Basic Facts about Trademarks.” (http://www.uspto.gov/trademarks/basics/Basic_Facts_Trademarks.jsp).

### What is a Copyright?


Copyright is a form of protection provided to the authors of "original works of authorship" including literary, dramatic, musical, artistic, and certain other intellectual works, both published and unpublished. The 1976 Copyright Act generally gives the owner of copyright the exclusive right to reproduce the copyrighted work, to prepare derivative works, to distribute copies or phonorecords of the copyrighted work, to perform the copyrighted work publicly, or to display the copyrighted work publicly.


The copyright protects the form of expression rather than the subject matter of the writing. For example, a description of a machine could be copyrighted, but this would only prevent others from copying the description; it would not prevent others from writing a description of their own or from making and using the machine. Copyrights are registered by the Copyright Office of the Library of Congress.

## Patent Laws


The Constitution of the United States gives Congress the power to enact laws relating to patents, in Article I, section 8, which reads "Congress shall have power . . . to promote the progress of science and useful arts, by securing for limited times to authors and inventors the exclusive right to their respective writings and discoveries." Under this power Congress has from time to time enacted various laws relating to patents. The first patent law was enacted in 1790. The patent laws underwent a general revision which was enacted July 19, 1952, and which came into effect January 1, 1953. It is codified in Title 35, United States Code. Additionally, on November 29, 1999, Congress enacted the American Inventors Protection Act of 1999 (AIPA), which further revised the patent laws. See Public Law 106-113, 113 Stat. 1501 (1999).


The patent law specifies the subject matter for which a patent may be obtained and the conditions for patentability. The law establishes the United States Patent and Trademark Office to administer the law relating to the granting of patents and contains various other provisions relating to patents.

## What Can Be Patented


The patent law specifies the general field of subject matter that can be patented and the conditions under which a patent may be obtained.


In the language of the statute, any person who “invents or discovers any new and useful process, machine, manufacture, or composition of matter, or any new and useful improvement thereof, may obtain a patent,” subject to the conditions and requirements of the law. The word “process” is defined by law as a process, act, or method, and primarily includes industrial or technical processes. The term “machine” used in the statute needs no explanation. The term “manufacture” refers to articles that are made, and includes all manufactured articles. The term “composition of matter” relates to chemical compositions and may include mixtures of ingredients as well as new chemical compounds. These classes of subject matter taken together include practically everything that is made by man and the processes for making the products.


The Atomic Energy Act of 1954 excludes the patenting of inventions useful solely in the utilization of special nuclear material or atomic energy in an atomic weapon. See 42 U.S.C. 2181(a).


The patent law specifies that the subject matter must be “useful.” The term “useful” in this connection refers to the condition that the subject matter has a useful purpose and also includes operativeness, that is, a machine which will not operate to perform the intended purpose would not be called useful, and therefore would not be granted a patent.


Interpretations of the statute by the courts have defined the limits of the field of subject matter that can be patented, thus it has been held that the laws of nature, physical phenomena, and abstract ideas are not patentable subject matter.


A patent cannot be obtained upon a mere idea or suggestion. The patent is granted upon the new machine, manufacture, etc., as has been said, and not upon the idea or suggestion of the new machine. A complete description of the actual machine or other subject matter for which a patent is sought is required.

## Novelty And Non-Obviousness, Conditions For Obtaining A Patent


In order for an invention to be patentable it must be new as defined in the patent law, which provides that an invention cannot be patented if:


    - “(1) the claimed invention was patented, described in a printed publication, or in public use, on sale, or otherwise available to the public before the effective filing date of the claimed invention” or

    - “(2) the claimed invention was described in a patent issued [by the U.S.] or in an application for patent published or deemed published [by the U.S.], in which the patent or application, as the case may be, names another inventor and was effectively filed before the effective filing date of the claimed invention.”






There are certain limited patent law exceptions to patent prohibitions (1) and (2) above. Notably, an exception may apply to a “disclosure made 1 year or less before the effective filing date of the claimed invention,” but only if “the disclosure was made by the inventor or joint inventor or by another who obtained the subject matter disclosed… from the inventor or a joint inventor.”


In patent prohibition (1), the term “otherwise available to the public” refers to other types of disclosures of the claimed invention such as, for example, an oral presentation at a scientific meeting, a demonstration at a trade show, a lecture or speech, a statement made on a radio talk show, a YouTube™ video, or a website or other on-line material.


Effective filing date of the claimed invention: This term appears in patent prohibitions (1) and (2). For a U.S. nonprovisional patent application that is the first application containing the claimed subject matter, the term “effective filing date of the claimed invention” means the actual filing date of the U.S. nonprovisional patent application. For a U.S. nonprovisional application that claims the benefit of a corresponding prior-filed U.S. provisional application, “effective filing date of the claimed invention” can be the filing date of the prior-filed provisional application provided the provisional application sufficiently describes the claimed invention. Similarly, for a U.S. nonprovisional application that is a continuation or division of a prior-filed U.S. nonprovisional application, “effective filing date of the claimed invention” can be the filing date of the prior filed nonprovisional application that sufficiently describes the claimed invention. Finally, “effective filing date of the claimed invention” may be the filing date of a prior-filed foreign patent application to which foreign priority is claimed provided the foreign patent application sufficiently describes the claimed invention.


Even if the subject matter sought to be patented is not exactly shown by the prior art, and involves one or more differences over the most nearly similar thing already known, a patent may still be refused if the differences would be obvious. The subject matter sought to be patented must be sufficiently different from what has been used or described before that it may be said to be non-obvious to a person having ordinary skill in the area of technology related to the invention. For example, the substitution of one color for another, or changes in size, are ordinarily not patentable.

## The United States Patent and Trademark Office


Congress established the United States Patent and Trademark Office to issue patents on behalf of the government. The Patent Office as a distinct bureau dates from the year 1802 when a separate official in the Department of State, who became known as “Superintendent of Patents,” was placed in charge of patents. The revision of the patent laws enacted in 1836 reorganized the Patent Office and designated the official in charge as Commissioner of Patents. The Patent Office remained in the Department of State until 1849 when it was transferred to the Department of Interior. In 1925 it was transferred to the Department of Commerce where it is today. The name of the Patent Office was changed to the Patent and Trademark Office in 1975 and changed to the United States Patent and Trademark Office in 2000.


The USPTO administers the patent laws as they relate to the granting of patents for inventions, and performs other duties relating to patents. Applications for patents are examined to determine if the applicants are entitled to patents under the law and patents are granted when applicants are so entitled. The USPTO publishes issued patents and most patent applications 18 months from the earliest effective application filing date, and makes various other publications concerning patents. The USPTO also records assignments of patents, maintains a search room for the use of the public to examine issued patents and records, and supplies copies of records and other papers, and the like. Similar functions are performed with respect to the registration of trademarks. The USPTO has no jurisdiction over questions of infringement and the enforcement of patents.


The head of the Office is the Under Secretary of Commerce for Intellectual Property and Director of the United States Patent and Trademark Office (Director). The Director’s staff includes the Deputy Under Secretary of Commerce and Deputy Director of the USPTO, the Commissioner for Patents, the Commissioner for Trademarks, and other officials. As head of the Office, the Director superintends or performs all duties respecting the granting and issuing of patents and the registration of trademarks; exercises general supervision over the entire work of the USPTO; prescribes the rules, subject to the approval of the Secretary of Commerce, for the conduct of proceedings in the USPTO, and for recognition of attorneys and agents; decides various questions brought before the Office by petition as prescribed by the rules; and performs other duties necessary and required for the administration of the United States Patent and Trademark Office.


The work of examining applications for patents is divided among a number of examining technology centers (TCs), each TC having jurisdiction over certain assigned fields of technology. Each TC is headed by group directors and staffed by examiners and support staff. The examiners review applications for patents and determine whether patents can be granted. An appeal can be taken to the Patent Trial and Appeal Board from their decisions refusing to grant a patent, and a review by the Director of the USPTO may be had on other matters by petition. In addition to the examining TCs, other offices perform various services, such as receiving and distributing mail, receiving new applications, handling sales of printed copies of patents, making copies of records, inspecting drawings, and recording assignments.


At present, the USPTO has over 11,000 employees, of whom about three quarters are examiners and others with technical and legal training. Patent applications are received at the rate of over 500,000 per year.


Effective November 15, 2011, any regular nonprovisional utility application filed by mail or hand-delivery will require payment of an additional $400 fee called the “non-electronic filing fee,” which is reduced by 50 percent (to $200) for applicants that qualify for small entity status under 37 CFR 1.27(a). The 75 percent micro entity discount does not apply to the non-electronic filing fee and consequently the non-electronic filing fee is also $200 for applicants that qualify for micro entity status under 37 CFR 1.29(a) or (d). This fee is required by Section 10(h) of the Leahy-Smith America Invents Act, Public Law 112-29 (Sept. 16, 2011; 125 Stat. 284). The only way to avoid having to pay the additional $400 non-electronic filing fee is to file the regular nonprovisional utility patent application via EFS-Web.Design, plant, and provisional applications are not subject to the additional non-electronic filing fee and may continue to be filed by mail or hand-delivery without additional charge. See the information available at www.uspto.gov/patents/process/file/efs/index/jsp. Any questions regarding filing applications via EFS-Web should be directed to the Electronic Business Center at 866-217-9197.

## General Information and Correspondence


All business with the United States Patent and Trademark Office should be transacted in writing. Regular nonprovisional utility applications must be filed via EFS-Web in order to avoid the additional $400 non-electronic filing fee.


Other patent correspondence, including design, plant, and provisional application filings, as well as correspondence filed in a nonprovisional application after the application filing date (known as “follow-on” correspondence), can still be filed by mail or hand-delivery without incurring the $400 non-electronic filing fee.


Such other correspondence relating to patent matters should be addressed to the following: 
COMMISSIONER FOR PATENTS P.O. Box 1450 Alexandria, VA 22313-1450
when sent by mail via the United States Postal Service. If a mail stop is appropriate, the mail stop should also be used.
Mail addressed to different mail stops should be mailed separately to ensure proper routing. For example, after final correspondence should be mailed to
Mail Stop AF Commissioner for Patents P.O. Box 1450 Alexandria, VA 22313-1450
and assignments should be mailed to
Mail Stop Assignment Recordation Services Director of the U.S. Patent and Trademark Office P.O. Box 1450 Alexandria, VA 22313-1450


Correspondents should be sure to include their full return addresses, including zip codes. The principal location of the USPTO is 600 Dulany Street, Alexandria, Virginia. The personal presence of applicants at the USPTO is unnecessary.


You do not have to be a Registered eFiler to file a patent application via EFS-Web. However, unless you are a Registered eFiler, you must not attempt to file follow-on correspondence via EFS-Web, because Unregistered eFilers are not permitted to file follow-on correspondence via EFS-Web. Follow-on correspondence filed by anyone other than an EFS-Web Registered eFiler must be sent by mail or hand-delivered to the address specified in the paragraph above.


Applicants and attorneys are required to conduct their business with decorum and courtesy. Papers presented in violation of this requirement will be returned.


Separate letters (but not necessarily in separate envelopes) should be written for each distinct subject of inquiry, such as assignments, payments, orders for printed copies of patents, orders for copies of records, and requests for other services. None of these inquiries should be included with letters responding to Office actions in applications.


When a letter concerns a patent application, the correspondent must include the application number (consisting of the series code and the serial number, e.g., 12/123,456) or the serial number and filing date assigned to that application by the Office, or the international application number of the international application number of the international application. When a letter concerns a patent (other than for purposes of payment of a maintenance fee), it should include the name of the patentee, the title of the invention, the patent number, and the date of issue.


An order for a copy of an assignment should identify the reel and frame number where the assignment or document is recorded; otherwise, an additional charge is made for the time consumed in making the search for the assignment.


Applications for patents, which are not published or issued as patents, are not generally open to the public, and no information concerning them is released except on written authority of the applicant, his or her assignee, or his or her attorney, or when necessary to the conduct of the business of the USPTO. Patent application publications and patents and related records, including records of any decisions, the records of assignments other than those relating to assignments of unpublished patent applications, patent applications that are relied upon for priority in a patent application publication or patent, books, and other records and papers in the Office are open to the public. They may be inspected in the USPTO Search Room, or copies may be ordered.


The Office cannot respond to inquiries concerning the novelty and patentability of an invention prior to the filing of an application; give advice as to possible infringement of a patent; advise of the propriety of filing an application; respond to inquiries as to whether, or to whom, any alleged invention has been patented; act as an expounder of the patent law or as counselor for individuals, except in deciding questions arising before it in regularly filed cases. Information of a general nature may be furnished either directly or by supplying or calling attention to an appropriate publication.

## Scientific and Technical Information Center, Public Search Facility, and Patent and Trademark Resource Centers


The Scientific and Technical Information Center of the United States Patent and Trademark Office located at 1D58 Remsen, 400 Dulany Street, Alexandria, Va., has available for public use over 120,000 volumes of scientific and technical books in various languages, about 90,000 bound volumes of periodicals devoted to science and technology, the official journals of 77 foreign patent organizations, and over 40 million foreign patents on paper, microfilm, microfiche, and CD-ROM.


The Scientific and Technical Information Center is open to the public from 8 a.m. to 5 p.m., Monday through Friday except federal holidays.


The Public Search Facility located at Madison East, First Floor, 600 Dulany Street, Alexandria, Va., is where the public may search and examine U.S. patents granted since 1790 using state of the art computer workstations. A numerical sequence patent backfile from 1790 to 2000 is available on microfilm.  Patents from 2000 forward may be found using a variety of the patent database available on workstations. Official Gazettes, Annual Indexes (of inventors), the Manual of Classification and its subject matter index, and other search aids are available in various formats. Patent assignment records of transactions affecting the ownership of patents, microfilmed deeds, and indexes are also available.


The Public Search Facility is open from 8 a.m. to 8 p.m. Monday through Friday except on federal holidays.  Research assistance is offered between the hours of 8 a.m. and 5 p.m. Monday through Friday.  Self-service access is permitted between 5 p.m. and 8 p.m.


Many inventors attempt to make their own search of the prior patents and publications before applying for a patent. This may be done in the Public Search Facility of the USPTO, and in libraries located throughout the United States that have been designated as Patent and Trademark Resource Centers (PTRCs). An inventor may make a preliminary search through the U.S. patents and publications to discover if the particular invention or one similar to it has been shown in the prior patent. An inventor may also employ patent attorneys or agents to perform the preliminary search. This search may not be as complete as that made by the USPTO during the examination of an application, but only serves, as its name indicates, a preliminary purpose. For this reason, the patent examiner may, and often does, reject claims in an application on the basis of prior patents or publications not found in the preliminary search.


Those who cannot come to the Public Search Facility may order from the USPTO copies of lists of original patents or of cross-referenced patents contained in the subclasses comprising the field of search, or may inspect and obtain copies of the patents at a Patent and Trademark Resource Center. The PTRCs receive current issues of U.S. patents and maintain collections of earlier issued patent and trademark information. The scope of these collections varies from library to library, ranging from patents of only recent years to all or most of the patents issued since 1790.


These patent collections are open to public use. Each of the PTRCs, in addition, offers the publications of the U.S. Patent Classification System (e.g., Manual of Classification, Index to the U.S. Patent Classification System, Classification Definitions, etc.) and other patent documents and forms, and provides technical staff assistance in their use to aid the public in gaining effective access to information contained in patents. The collections are organized in patent number sequence.


Available in all PTRCs is the Cassis CD-ROM system. With various files, it permits the effective identification of appropriate classifications to search, provides numbers of patents assigned to a classification to facilitate finding the patents in a numerical file of patents, provides the current classification(s) of all patents, permits word searching on classification titles, and on abstracts, and provides certain bibliographic information on more recently issued patents. These libraries also provide access to the USPTO website.


Facilities for making paper copies from microfilm, the paper bound volumes or CD-ROM are generally provided for a fee.


Due to variations in the scope of patent collections among the PTRCs and in their hours of service to the public, anyone contemplating the use of the patents at a particular library is advised to contact that library, in advance, about its collection, services, and hours, so as to avert possible inconvenience. For a complete list of PTRCs, refer to the USPTO website at www.uspto.gov/products/library/ptdl/index.jsp.

## Attorneys and Agents


The preparation of an application for patent and the conducting of the proceedings in the United States Patent and Trademark Office to obtain the patent is an undertaking requiring the knowledge of patent law and rules and Office practice and procedures, as well as knowledge of the scientific or technical matters involved in the particular invention.


Inventors may prepare their own applications and file them in the USPTO and conduct the proceedings themselves, but unless they are familiar with these matters or study them in detail, they may get into considerable difficulty. While a patent may be obtained in many cases by persons not skilled in this work, there would be no assurance that the patent obtained would adequately protect the particular invention.


Most inventors employ the services of registered patent attorneys or patent agents. The law gives the USPTO the power to make rules and regulations governing conduct and the recognition of patent attorneys and agents to practice before the USPTO. Persons who are not recognized by the USPTO for this practice are not permitted by law to represent inventors before the USPTO. The USPTO maintains a register of attorneys and agents. To be admitted to this register, a person must comply with the regulations prescribed by the Office, which require a showing that the person is of good moral character and of good repute and that he or she has the legal, scientific, and technical qualifications necessary to render applicants for patents a valuable service. Certain of these qualifications must be demonstrated by the passing of an examination. Those admitted to the examination must have a college degree in engineering or physical science or the equivalent of such a degree.


The USPTO registers both attorneys at law and persons who are not attorneys at law. The former persons are now referred to as “patent attorneys,” and the latter persons are referred to as “patent agents.” Both patent attorneys and patent agents are permitted to prepare an application for a patent and conduct the prosecution in the USPTO. Patent agents, however, cannot conduct patent litigation in the courts or perform various services that the local jurisdiction considers as practicing law. For example, a patent agent could not draw up a contract relating to a patent, such as an assignment or a license, if the state in which he or she resides considers drafting contracts as practicing law.


Some individuals and organizations that are not registered advertise their services in the fields of patent searching and invention marketing and development. Such individuals and organizations cannot represent inventors before the USPTO. They are not subject to USPTO discipline, but the USPTO does provide a public forum (www.uspto.gov/web/offices/com/iip/complaints.htm) where complaints and responses concerning invention promoters/promotion firms are published.


The USPTO cannot recommend any particular attorney or agent, or aid in the selection of an attorney or agent, as by stating, in response to inquiry that a named patent attorney, agent, or firm, is “reliable” or “capable.” The USPTO maintains a directory of registered patent attorneys and agents at https://oedci.uspto.gov/OEDCI/.


The telephone directories of most large cities have, in the classified section, a heading for patent attorneys under which those in that area are listed. Many large cities have associations of patent attorneys.


In employing a patent attorney or agent, the inventor executes a power of attorney, which is filed in the USPTO and made of record in the application file. When a registered attorney or agent has been appointed, the Office does not communicate with the inventor directly but conducts the correspondence with the attorney or agent since he or she is acting for the inventor thereafter although the inventor is free to contact the USPTO concerning the status of his or her application. The inventor may remove the attorney or agent by revoking the power of attorney.


The USPTO has the power to disbar, or suspend from practicing before it, persons guilty of gross misconduct, etc., but this can only be done after a full hearing with the presentation of clear and convincing evidence concerning the misconduct. The USPTO will receive and, in appropriate cases, act upon complaints against attorneys and agents. The fees charged to inventors by patent attorneys and agents for their professional services are not subject to regulation by the USPTO. Definite evidence of overcharging may afford basis for USPTO action, but the Office rarely intervenes in disputes concerning fees.

## Who May Apply For A Patent


According to the law, the inventor, or a person to whom the inventor has assigned or is under an obligation to assign the invention, may apply for a patent, with certain exceptions. If the inventor is deceased, the application may be made by legal representatives, that is, the administrator or executor of the estate. If the inventor is legally incapacitated, the application for patent may be made by a legal representative (e.g., guardian). If an inventor refuses to apply for a patent or cannot be found, a joint inventor may apply on behalf of the non-signing inventor.


If two or more persons make an invention jointly, they apply for a patent as joint inventors. A person who makes only a financial contribution is not a joint inventor and cannot be joined in the application as an inventor. It is possible to correct an innocent mistake in erroneously omitting an inventor or in erroneously naming a person as an inventor.


Officers and employees of the United States Patent and Trademark Office are prohibited by law from applying for a patent or acquiring, directly or indirectly, except by inheritance or bequest, any patent or any right or interest in any patent.

## Independent lnventor Resources


A section of the USPTO’s website, found at www.uspto.gov/inventors, is devoted to independent inventors (the site is titled “Inventors Resources”) and offers a broad range of material covering most aspects of the patent and trademark process. The website also endeavors to educate independent inventors about fraudulent invention development and marketing firms and the scams that may affect these inventors and offers tips and warning signs on avoiding these scams. The site also publishes complaints against these firms and any responses received from them. The site further provides links to other USPTO sites, as well as links to other federal agencies.


The Inventors Assistance Center (IAC) provides the primary point of contact to the independent inventor community and the general public for general information about filing a provisional patent application, or a regular, nonprovisional patent application.


Inventors also have the option of filing a Provisional Application for Patent. Provisional applications are described in more detail below. To receive more information on provisional applications, please visit the USPTO website or request a print brochure by calling 800-786-9199 or 571-272-1000.

## Application For Patent

### Nonprovisional Application for a Patent


A nonprovisional application for a patent is made to the Director of the United States Patent and Trademark Office and includes:


    - A written document which comprises a specification (description and claims);

    - Drawings (when necessary);

    - An oath or declaration; and

    - Filing, search, and examination fees. Fees for filing, searching, examining, issuing, appealing, and maintaining patent applications and patents are reduced by 50 percent for any small entity that qualifies for reduced fees under 37 CFR 1.27(a), and are reduced by 75 percent for any micro entity that files a certification that the requirements under 37 CFR 1.29(a) or (d) are met.

    

        - Small Entity Status: Applicant must determine that small entity status under 37 CFR 1.27(a) is appropriate before making an assertion of entitlement to small entity status and paying a fee at the 50 percent small entity discount. Fees change each October. Note that by filing electronically via EFS-Web, the filing fee for an applicant qualifying for small entity status is further reduced.

        - Micro Entity Status: Applicant must determine that micro entity status under 37 CFR 1.29(a) or (d) is appropriate before filing the required certification of micro entity status and paying a fee at the 75 percent micro entity discount. The patent forms Web page is indexed under the section titled Forms, Patents on the USPTO website at www.uspto.gov. There are two micro entity certification forms – namely form PTO/SB/15A for certifying micro entity status on the “gross income basis” under 37 CFR 1.29(a), and form PTO/SB/15B for certifying micro entity status on the “institution of higher education basis” under 37 CFR 1.29(d). Effective November 15, 2011, any regular nonprovisional utility application filed by mail or hand-delivery will require payment of an additional $400 fee called the “non-electronic filing fee,” which is reduced by 50 percent (to $200) for applicants that qualify for small entity status under 37 CFR 1.27(a) or micro entity status under 37 CFR 1.29(a) or (d). The only way to avoid having to pay the additional $400 non-electronic filing fee is by filing the regular nonprovisional utility application via EFS-Web.

    






Other patent correspondence, including design, plant, and provisional application filings, as well as correspondence filed in a nonprovisional application after the application filing date (known as “follow-on” correspondence), can still be filed by mail or hand-delivery without incurring the $400 non-electronic filing fee. You do not have to be a Registered eFiler to file a patent application via EFS-Web. However, unless you are a Registered eFiler, you must not attempt to file follow-on correspondence via EFS-Web, because Unregistered eFilers are not permitted to file follow-on correspondence via EFS-Web. Follow-on correspondence filed by anyone other than an EFS-Web Registered eFiler must be sent by mail or be hand-delivered. (See the “General Information and Correspondence” section of this brochure.) In the event you receive from the USPTO a “Notice of Incomplete Application” in response to your EFS-Web filing stating that an application number has been assigned but no filing date has been granted, you must become a Registered eFiler and file your reply to the “Notice of Incomplete Application” via EFS-Web in order to avoid the $400 non-electronic filing fee. To become a Registered eFiler and have the ability to file follow-on correspondence, please consult the information at www.uspto.gov/patents/process/file/efs/guidance/register.jsp, or call the Electronic Business Center at 866-217-9197.


The specification (description and claims) can be created using a word processing program such as Microsoft® Word or Corel® WordPerfect. The document containing the specification can normally be converted into PDF format by the word processing program itself so that it can be included as an attachment when filing the application via EFS-Web. Other application documents, such as drawings and a hand-signed declaration, may have to be scanned as a PDF file for filing via EFS-Web. See the information available at www.uspto.gov/patents/process/file/efs/index.jsp. Any questions regarding filing applications via EFS-Web should be directed to the Electronic Business Center at 866-217-9197.


All application documents must be in the English language or a translation into the English language will be required along with the required fee set forth in 37 CFR 1.17(i).


Each document (which should be filed via EFS-Web in PDF format) must have a top margin of at least 2 cm (3/4 inch), a left side margin of at least 2.5 cm (1 inch), a right side margin of at least 2 cm (3/4 inch) and a bottom margin of at least 2 cm (3/4 inch) with no holes made in the submitted papers. It is also required that the spacing on all papers be 1.5 or double-spaced and the application papers must be numbered consecutively (centrally located above or below the text) starting with page one.


The specification must have text written in a nonscript font (e.g., Arial, Times Roman, or Courier, preferably a font size of 12pt) lettering style having capital letters that should be at least 0.3175 cm (0.125 inch) high, but may be no smaller than 0.21 cm (0.08 inch) high (e.g., a font size of 6). The specification must have only a single column of text.


The specification must conclude with a claim or claims particularly pointing out and distinctly claiming the subject matter that the applicant regards as the invention. The portion of the application in which the applicant sets forth the claim or claims is an important part of the application, as it is the claims that define the scope of the protection afforded by the patent. The claims must commence on a separate sheet.


More than one claim may be presented provided they differ from each other. Claims may be presented in independent form (e.g. the claim stands by itself) or in dependent form, referring back to and further limiting another claim or claims in the same application. Any dependent claim that refers back to more than one other claim is considered a “multiple dependent claim.”


The application for patent is not forwarded for examination until all required parts, complying with the rules related thereto, are received. If any application is filed without all the required parts for obtaining a filing date (incomplete or defective), the applicant will be notified of the deficiencies and given a time period to complete the application filing (a surcharge may be required)—at which time a filing date as of the date of such a completed submission will be obtained by the applicant. If the omission is not corrected within a specified time period, the application will be returned or otherwise disposed of; the filing fee if submitted will be refunded less a handling fee as set forth in the fee schedule.


The filing fee and declaration or oath need not be submitted with the parts requiring a filing date. It is, however, desirable that all parts of the complete application be deposited in the Office together; otherwise, each part must be signed and a letter must accompany each part, accurately and clearly connecting it with the other parts of the application. If an application that has been accorded a filing date does not include the filing fee or the oath or declaration, applicant will be notified and given a time period to pay the filing fee, file an oath or declaration and pay a surcharge.


All applications received in the USPTO are numbered in sequential order, and the applicant will be informed of the application number and filing date by a filing receipt.


The filing date of an application for patent is the date on which a specification (including at least one claim) and any drawings necessary to understand the subject matter sought to be patented are received in the USPTO; or the date on which the last part completing the application is received in the case of a previously incomplete or defective application.

### Provisional Application for a Patent


Since June 8, 1995, the USPTO has offered inventors the option of filing a provisional application for patent, which was designed to provide a lower-cost first patent filing in the United States and to give U.S. applicants parity with foreign applicants. Claims and oath or declaration are NOT required for a provisional application. A provisional application provides the means to establish an early effective filing date in a patent application and permits the term “Patent Pending” to be applied in connection with the invention. Provisional applications may not be filed for design inventions.


The filing date of a provisional application is the date on which a written description of the invention, and drawings if necessary, are received in the USPTO. To be complete, a provisional application must also include the filing fee, and a cover sheet specifying that the application is a provisional application for patent. The applicant would then have up to 12 months to file a nonprovisional application for patent as described above. The claimed subject matter in the later filed nonprovisional application is entitled to the benefit of the filing date of the provisional application if it has support in the provisional application.


If a provisional application is not filed in English, and a nonprovisional application is filed claiming benefit to the provisional application, a translation of the provisional application will be required. See title 37, Code of Federal Regulations, Section 1.78(a)(5).


Provisional applications are NOT examined on their merits. A provisional application will become abandoned by the operation of law 12 months from its filing date. The 12-month pendency for a provisional application is not counted toward the 20-year term of a patent granted on a subsequently filed nonprovisional application that claims benefit of the filing date of the provisional application.


A surcharge is required for filing the basic filing fee or the cover sheet on a date later than the filing of the provisional application. Unlike nonprovisional utility applications, design, plant, and provisional applications can still be filed by mail or hand-delivery without having to pay the additional $400 non-electronic filing fee. Design and provisional applications can also be filed via EFS-Web. Plant applications, however, are not permitted to be filed via EFS-Web.

### Publication of Patent Applications


Publication of patent applications is required by the American Inventors Protection Act of 1999 for most plant and utility patent applications filed on or after November 29, 2000. On filing of a plant or utility application on or after November 29, 2000, an applicant may request that the application not be published, but only if the invention has not been and will not be the subject of an application filed in a foreign country that requires publication 18 months after filing (or earlier claimed priority date) or under the Patent Cooperation Treaty. Publication occurs after the expiration of an 18-month period following the earliest effective filing date or priority date claimed by an application. Following publication, the application for patent is no longer held in confidence by the Office and any member of the public may request access to the entire file history of the application.


As a result of publication, an applicant may assert provisional rights. These rights provide a patentee with the opportunity to obtain a reasonable royalty from a third party that infringes a published application claim provided actual notice is given to the third party by applicant, and patent issues from the application with a substantially identical claim. Thus, damages for pre-patent grant infringement by another are now available.

## File Your Application Electronically Using EFS-Web


Effective November 15, 2011, any regular nonprovisional utility application filed by mail or hand-delivery will require payment of an additional $400 fee called the “non-electronic filing fee,” which is reduced by 50 percent (to $200) for applicants that qualify for small entity status under 37 CFR 1.27(a) or micro entity status under 37 CFR 1.29(a) or (d). The only way to avoid having to pay the additional $400 non-electronic filing fee is by filing your nonprovisional utility application via EFS-Web.A small entity applicant who files electronically not only avoids the additional non-electronic filing ($200 for small entity and micro entity applicants); the small entity applicant who files electronically also receives a bigger discount on the regular filing fee. Any questions regarding filing applications via EFS-Web should be directed to the Electronic Business Center at 866-217-9197.


Other patent correspondence, including design, plant, and provisional application filings, as well as correspondence filed in a nonprovisional application after the application filing date (known as “follow-on” correspondence), can still be filed by mail or hand-delivery without incurring the $400 non-electronic filing fee. You do not have to be a Registered eFiler to file a patent application via EFS-Web. However, unless you are a Registered eFiler, you must not attempt to file follow-on correspondence via EFS-Web, because Unregistered eFilers are not permitted to file follow-on correspondence via EFS-Web. Follow-on correspondence filed by anyone other than an EFS-Web Registered eFiler must be sent by mail or be hand-delivered. (See the “General Information and Correspondence” section of this brochure.) In the event you receive from the USPTO a “Notice of Incomplete Application” in response to your EFS-Web filing stating that an application number has been assigned but no filing date has been granted, you must become a Registered eFiler and file your reply to the “Notice of Incomplete Application” via EFS-Web in order to avoid the $400 non-electronic filing fee. To become a Registered eFiler and have the ability to file follow-on correspondence, please consult the information at www.uspto.gov/patents/process/file/efs/guidance/register.jsp, or call the Electronic Business Center at 866-217-9197.


EFS-Web allows customers to electronically file patent application documents securely via the Internet via a Web page. EFS-Web is a system for submitting new applications and documents related to previously-filed patent applications. Customers prepare documents in Portable Document Format (PDF), attach the documents, validate that the PDF documents will be compatible with USPTO internal automated information systems, submit the documents, and pay fees with real-time payment processing. Some forms are available as fillable EFS-Web forms. When these fillable EFS-Web forms are used, the data entered into the forms is automatically loaded into USPTO information systems.


EFS-Web can be used to submit:


    - New utility patent applications and fees

    - New design patent applications and fees

    - Provisional patent applications and fees

    - Requests to enter the national stage under 35 U.S.C. 371 and fees

    - Most follow-on documents and fees for a previously filed patent application






Further information on EFS-Web is available at www.uspto.gov/patents/process/file/efs/guidance.


See the “Legal Framework” document on that Web page for a list of correspondence that may not be filed via EFS-Web and answers to frequently asked questions.

## Oath or Declaration, Signature


An oath or declaration is a formal statement that must be made by the inventor in a nonprovisional application. Each inventor must sign an oath or declaration that includes certain statements required by law and the USPTO rules, including the statement that he or she believes himself or herself to be the original inventor or an original joint inventor of a claimed invention in the application and the statement that the application was made or authorized to be made by him or her. See 35 U.S.C 115 and 37 CFR 1.63. An oath must be sworn to by the inventor before a notary public. A declaration may be submitted in lieu of an oath. A declaration does not need to be notarized. Oaths or declarations are required for design, plant, utility, and reissue applications. In addition to the required statements, the oath or declaration must set forth the legal name of the inventor, and, if not provided in an application data sheet, the inventor’s mailing address and residence. In lieu of an oath or declaration, a substitute statement may be signed by the applicant with respect to an inventor who is deceased, legally incapacitated, cannot be found or reached after diligent effort, or has refused to execute the oath or declaration. When filing a continuing application, a copy of the oath or declaration filed in the earlier application may be used provided that it complies with the rules in effect for the continuing application (i.e., the rules that apply to applications filed on or after September 16, 2012).


Forms for declarations are available by calling the USPTO General Information Services at 800-786-9199 or 571-272-1000 or by accessing USPTO website at www.uspto.gov, indexed under the section titled “Forms, Patents.” Most of the forms on the USPTO website are electronically fillable and can be included in the application filed via EFS-Web without having to print the form out in order to scan it for inclusion as a PDF attachment to the application.

## Filing, Search, and Examination Fees


A patent application is subject to the payment of a basic fee and additional fees that include a search fee, an examination fee, and issue fee. Consult the USPTO website at www.uspto.gov for the current fees. Total claims that exceed 20, and independent claims that exceed three are considered “excess claims” for which additional fees are due. For example, if applicant filed a total of 25 claims, including four independent claims, applicant would be required to pay excess claims fees for five total claims exceeding 20, and one independent claim exceeding three. If the same applicant later filed an amendment increasing the total number of claims to 29, and the number of independent claims to six, applicant would be required to pay more excess claims fees for the four additional total claims and the two additional independent claims.


In calculating fees, a claim is singularly dependent if it incorporates by reference a single preceding claim that may be an independent or dependent claim. A multiple dependent claim or any claim depending therefrom shall be considered as separate dependent claims in accordance with the number of claims to which reference is made. In addition, if the application contains multiple dependent claims, an additional fee is required for each multiple dependent claim.


If the owner of the invention is a small entity, (an independent inventor, a small business concern or a nonprofit organization), most fees are reduced by half if small entity status is claimed. If small entity status is desired and appropriate, applicants should pay the small entity filing fee. Applicants claiming small entity status should make an investigation as to whether small entity status is appropriate before claiming such status.


Most of the fees are subject to change in October of each year.







## Specification [Description and Claims]









## Drawing




## Examination of Applications and Proceedings in the United States Patent and Trademark Office




## Time for Reply and Abandonment




## Appeal to the Patent Trial and Appeal Board and to the Courts




## Allowance and Issue of Patent




## Patent Term Extension and Adjustment




## Nature of Patent and Patent Rights




## Maintenance Fees




## Correction of Patents




## Assignments and Licenses




## Infringement of Patents




## Patent Marking and Patent Pending




## Design Patents




## Plant Patents




## Treaties and Foreign Patents




## Foreign Applicants for U.S. Patents



