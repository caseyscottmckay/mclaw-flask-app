Writing a Demand Letter Basics|United States|Intellectual Property and Technology|Patent|cell phone|UpLaw explores the latest topics in law by leading a culture of innovation.

You've decided it's time to file a small claim. You've heard it's relatively easy and pain-free. But you realize that you have no idea where to start. How do you even initiate your claim? Well, you begin by writing a demand letter. A demand letter is the key to getting your dispute off the ground and into the small claims courthouse.

__What is your first step in filing a small claim?__ 


The saying "the first step is the most important," applies here. The demand letter is the layperson's version of a legal complaint. In it, you state what your dispute is and why you want to handle it in court. The demand letter must also contain the amount for which you are suing or the specific relief you seek. You submit this letter to the person with whom you have the dispute. If and when you prepare to file, submit it to the clerk of court. While you do not need legal drafting experience to compose a demand letter, it does need to be clear and well organized.

__What should you write in a demand letter?__ 

First, give a brief history of the dispute. Explain why there has been no agreement so far. While your opponent may know exactly what happened, a judge or court clerk who may end up reading it will not. Organize it chronologically, but be brief. That way, an uninvolved observer will be able to follow it easily.
 
Be polite, concise and specific about what you want and what your next step will be if the problem is not resolved. For example, if it is a debt settlement, include the amount owed and the date it was due. If you are willing to accept a lower amount in settlement, say so. If not, make that clear as well. Let the other person know that if you are not able to come to an agreement, you will file in small claims court. Hopefully, he or she will decide it makes sense to compromise.
 
__What should you absolutely avoid when writing a demand letter?__ 

Avoid threatening or disparaging the other person. Do not use language that will convey your frustration or anger. Creating a negative mood will only lessen your chances of reaching an agreement. The idea of the letter is to show the other person you are serious and give them the chance to consider their legal choices. It is not an opportunity to insult them or create an adversarial relationship. If the dispute ends up in court, remember that the same judge who will hear your case will read your demand letter. This is another reason to keep it objective and professional. The last thing you would want is for the judge to perceive you as being antagonistic.
 
__How long should the letter be?__ 

There is no minimum or maximum length for a demand letter. However, shorter is better. It should only be long enough to clarify your intent, not a page more. A letter that gets to the point will show your opponent that you are serious about the lawsuit. It will also help the court clerk and the judges who are extremely busy. The less you give them to read, the better it will be for your case.
 
Finally, make and keep several copies of the letter for yourself and to use in any future court appearances. If you are lucky, you will not end up in front of a judge but if that happens, you will be prepared.
 
 