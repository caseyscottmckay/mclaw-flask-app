I. Style is Not Copyrightable.

Copyright law protects original works of authorship. It does not protect facts, procedures, concepts, principles, or ideas. Only the expression of ideas, not the ideas themselves, are copyrightable. Thus, style is not copyrightable.

More specifically, style is merely one ingredient of "expression" and, while a particular expression of idea is protectable, the idea itself is not protectable. _See_ _Steinberg v. Columbia Pictures Indus._, Inc., 663 F. Supp. 706, 712 (S.D.N.Y. 1987). For there to be infringement, there has to be substantial similarity between the original work and the new, purportedly infringing, work.

Put another way, in _Dave Grossman Designs v. Bortin_, the court said that:

    "The law of copyright is clear that only specific expressions of an idea may be copyrighted, that other parties may copy that idea, but that other parties may not copy that specific expression of the idea or portions thereof. For example, Picasso may be entitled to a copyright on his portrait of three women painted in his Cubist motif. Any artist, however, may paint a picture of any subject in the Cubist motif, including a portrait of three women, and not violate Picasso's copyright so long as the second artist does not substantially copy Picasso's specific expression of his idea."

_Dave Grossman Designs, Inc. v. Bortin_, 347 F. Supp. 1150, 1156-57 (N.D. Ill. 1972).

The courts are saying that the "expression" of an idea is the result of a wide array of factors: composition, content, style, framing, color, narrative, artistic intent, etc. These factors add up to create the expression of a particular work, and it's the totality of all those factors that earns copyright protection. One single factor, one particular piece of the overall expression, is not enough to warrant protection. Style does not tell us anything about the artist's inspirations or motivations; it does not tell us anything about what the composition of the work will be or what the artist will say. Style simply gives us an outline of what a work of art may look like. In other words, style may tell us about the artist, but it tells us nothing about the work.

II. Typefaces are Not Copyrightable; Fonts are Copyrightable 

Typefaces are generally not copyrightable in the US. Fonts may be protected if the font qualifies as a computer program, so bitmapped fonts are generally not protectable, but scalable fonts are protectable. This means that copyright law protects only the font software, not the artistic design of the typeface. So how are you generating the fonts you use? You downloaded the fonts and are using them without a license?
