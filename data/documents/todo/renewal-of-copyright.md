Renewal of Copyright|United States|Intellectual Property and Technology|Copyright|copyright foundations, copyright renewal|This article provides basic information regarding renewal registration for works that first secured federal protection between January 1, 1964, and December 31, 1977. For these works, renewal registration is optional, but still carries a number of benefits. Topics covered include the following: (1) incentives for registering a  renewal claim; (2) copyright ownership during the renewal term; and (3) the procedure for registering  a renewal claim.
# Renewal of Copyright


Works that secured copyright protection before January 1, 1978, were eligible for up to two terms of protection: an origi-nal term of twenty-eight years, followed by a renewal term.1Copyright owners of these works had to file a renewal appli-cation before the end of the twenty-eighth year to extend copyright protection into the renewal term.

1

 If a renewal registration was not properly filed, copyright protection ended on the twenty-ninth anniversary date of the original term, and the work entered the public domain.


NOTE: There is an exception to this rule for certain foreign works whose copyright protection was restored under the terms of the Uruguay Round Agreement Act (URAA). For more information, see Copyright Restoration under the URAA (Article 38B).


Congress amended the copyright law in 1992 to make renewal automatic for works that secured federal copyright protection in the United States between January 1, 1964, and December 31, 1977. In 1998, Congress extended the length of the renewal term. As a result, these works enjoy a copyright term of ninety-five years: twenty-eight years in the original term and sixty-seven years in the renewal term. For more information about the legislative developments related to renewal, see chapter 2100, parts I and II, of the Compendium of U.S. Copyright Office Practices.Although filing a renewal registration for works that secured federal copyright protection between January 1, 1964, and December 31, 1977, is optional, registration during the renewal term provides certain benefits, including• Eligibility to file an infringement action in federal court when no registration was made for the original term• Eligibility for statutory damages and attorneys’ fees when no registration was made for the original term• Creation of a public record of the initial vesting of the re-newal copyright and ownership at the time of registration.


NOTE: Renewal of works protected by copyright can be complicated and has serious legal implica-tions. This article is a general introduction to this topic. Refer to chapter 2100 of the Compendiumfor further information.

## Determining Whether a Work Was Automatically Renewed


To be eligible for automatic renewal, a work must have secured federal copyright protection between January 1, 1964, and December 31, 1977, and it must have maintained that copyright throughout the original term. For unpublished works, copyright was secured by registering the work with the Copyright Office. For published works, copyright generally was secured at the date of publication with adequate notice. Notice typically consists of three elements: (1) the word “copyright,” the abbreviation “copr.,” or the symbol ©; (2) the name of the copyright proprietor; and (3) the year of publication. This notice must be in English and appear in a place prescribed by the law in effect at the time of publication. For issues related to notice, renewal, and specific categories of works see 
chapter 2100 of the Compendium.


## Identifying the Claimant in the Renewal Copyright


The law states that the renewal copyright vests at the beginning of the renewal term, and it identifies who can claim it. Generally, the claimant is the individual or entity that was entitled to claim the renewal copyright on December 31 of the twenty-eighth year of the original term.


The following individuals are entitled to claim the renewal copyright:



    - If the author was alive on the last day of the original term, the author can claim the renewal copyright as the “author” on the renewal application.

    - If the author died during the original term, the author’s widow or widower or any child who was alive on the last day of the original term can claim the renewal copyright on the applica-tion as the “widow of the deceased author”; the “widower of the deceased author”; the “child of the deceased author”; or the “children of the deceased author.”

    - . If the author, the author’s widow or widower, and the author’s children all died during the original term, and if the author left a will that was not yet discharged on the last day of the original term, the author’s executors can claim the renewal copyright on the application as the “executors of the deceased author.”

    - If the author, the author’s widow or widower, and the author’s children all died during the original term, and if the author did not leave a will, the author’s next of kin can claim the renewal copyright on the application as the “next of kin of the deceased author, there being no will.”




If the work falls under one of the following categories, the copyright proprietor—or owner—of the work can claim the renewal copyright.



    - 
Posthumous works. 
A posthumous work is a work first published after the author’s death where no copyright assignment or other contract for exploitation occurred during the author’s life-time. On the renewal application, the copyright proprietor on the last day of the original term can claim the renewal copyright as “proprietor of copyright in a posthumous work.”

    - 
Periodical, cyclopedic, or other composite works.
 The copyright proprietor on the last day of the original term can claim the renewal copyright on the application as “proprietor of copyright in a composite work."

    - 
Work copyrighted by an employer for whom such work was made for hire.
 The copyright propri-etor on the last day of the original term can claim the renewal copyright on the application as “proprietor of copyright in a work made for hire.”

    - 
Work copyrighted by a corporate body otherwise than an assignee or licensee of the individual author.
 The copyright proprietor on the last day of the original term can claim the renewal copyright on the application as “proprietor of copyright in a work copyrighted by a corpo-rate body otherwise than as assignee or licensee of the individual author.” NOTE: This type of claim is rare.




In completing a renewal application, identify the initial vested owner of the renewal copyright and state the basis for claiming the renewal copyright from one of the lists above. When the initial vested owner is dead or defunct, also identify the party that currently owns the renewal copyright and state how that party acquired the renewal copyright from the initial vested owner. For more information about vested ownership, see 
chapter 2100, section 2115.5, of the Compendium.


## Filing a Renewal Registration with the Copyright Office


The Copyright Office will accept a renewal application for works that secured copyright between January 1, 1964, and December 31, 1977, at any time during the renewal term. To register a renewal claim, you must submit a complete application Form RE and filing fee, and you may need to submit a copy of the work. You may also need to file Form RE/Addendum, in addition to Form RE, depend-ing on whether the work was registered for the original term.

2





NOTE:The Copyright Office charges a nonrefundable filing fee for Form RE and an additional nonre-fundable fee for Form RE/Addendum. Fees are subject to change. For current registration fees, see Copyright Office Fees (Article 4).


If you do not know whether a work was registered for the original term, you can search the physical records at the Copyright Office, consult the Catalog of Copyright Entries, or request a paid search by the Office’s staff. For more information about Office records and copyright searches, see Obtaining Access to and Copies of Copyright Records and Deposits (Article 6), How to Investigate the Copyright Status of a Work (Article 22), and The Copyright Card Catalog and the Online Files of the Copyright Office (Article 23).

__ 
Works Registered for the Original Term 



If a work was registered with the Copyright Office for the original term, or was first published as part of a larger registered work, complete and submit Form RE along with the filing fee. For assis-tance in completing Form RE, see chapter 2100, section 2115, of the Compendium. You do not need to submit a copy of the work with the application and filing fee if the renewal claim is based on the original registration record for the work itself or for the larger work in which it was first published. However, the Office may request material from the work if it is needed to facilitate the examination of the renewal claim.

__ 
Published Works Not Registered for the Original Term



If a work was not registered for the original term, or if the renewal claim is for a published work that initially secured copyright protection by being registered as an unpublished work, complete and submit Form RE and Form RE/Addendum along with the filing fee for each form. For assistance in completing Form RE and Form RE/Addendum, see chapter 2100, sections 2115 and 2116, of the Com-pendium. In addition, submit a complete copy or phonorecord of the best edition of the work as first published or approved identifying material. For more information, see Mandatory Deposit of Copies or Phonorecords for the Library of Congress (Article 7D) and Best Edition of Published Copyrighted Works for the Collections of the Library of Congress (Article 7B).


The Office may accept alternative deposits or identifying material when only archival copies exist; when the work was originally published in a periodical, a collection, or a larger work; or when the work being registered is an eligible component embodied in a published motion picture. For prac-tices and procedures related to alternative deposits and identifying material, see chapter 2100, sec-tions 2116.5(B) through (D), of the Compendium.


The Office will use the information on Form RE/Addendum to determine if the copyright was properly secured when the work was first published or if copyright protection has been lost because of a subsequent failure to comply with the requirements of the law. The Office will use the deposit material to facilitate its examination of the renewal claim.

__ 
Derivative Works



The copyright in a derivative work—a new version of a preexisting work—covers only the additions, changes, or other new material appearing for the first time in the new version. Examples of deriva-tive works include a work reissued with new material, such as a translation, musical arrangement, dramatization, or compilation. The copyright in the derivative work is independent of the copyright in any version that was previously published or registered. For renewal purposes, the person who created the original version is not the author of the derivative work, unless that person also created or cocreated the derivative work.

__ 
Contributions to Periodicals or Other Composite Works



You can submit a separate renewal application for a contribution to a larger work even if the contri-bution was initially registered as part of the larger work. This option applies to contributions first published in periodicals, encyclopedic works, composite works, and eligible published collections, including contributions that were created as works made for hire. However, when a contribution or separate work was first published with its own separate copyright notice, you must register it separately for the renewal term. For more information, see 
chapter 2100
, sections 2126 and 2127, of the Compendium.

## Notice of Renewal of Copyright


The Office is often asked whether the copyright notice appearing on a work should reflect the date that the copyright was originally secured or the date that the copyright was renewed. The copyright law is silent on this point. Using the copyright notice that appeared on the work when it was first published may be appropriate, but it may also be helpful to indicate that the copyright has been renewed, as in © 1975 Robert Wyse. Copyright renewed 2003 by Renita Holmes Wyse.



1. This article is intended as a limited introduction to renewal registration of works that first secured federal copyright protection between January 1, 1964, and December 31, 1977. Works that secured copyright protection on or after January 1, 1978, are granted one term of copyright protec-tion that generally lasts the life of an author plus seventy years. The authoritative source for U.S. copyright law is the Copyright Act, codified in Title 17 of the United States Code. Copyright Office regulations are codified in Title 37 of the Code of Federal Regulations. Copyright Office practices and procedures are summarized in the third edition of the Compendium of U.S. Copyright Office Practices, cited as the Compendium. The copyright law, regulations, and the Compendium are available on the Copyright Office website at www.copyright.gov.
↩





2. At this time, Form RE and Form RE/Addendum are available only as paper application forms. Elec-tronic registration is not available for renewal registrations.
↩


