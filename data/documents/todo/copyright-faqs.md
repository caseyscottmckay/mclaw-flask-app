Copyright FAQs|United States|Intellectual Property and Technology|Copyright|copyright|Copyright is a form of protection grounded in the U.S. Constitution and granted by law for original works of authorship fixed in a tangible medium of expression. Copyright covers both published and unpublished works.

Copyright FAQs

# Copyright in General


What is copyright?


Copyright is a form of protection grounded in the U.S. Constitution and granted by law for original works of authorship fixed in a tangible medium of expression. Copyright covers both published and unpublished works.


What does copyright protect?

Copyright, a form of intellectual property law, protects original works of authorship including literary, dramatic, musical, and artistic works, such as poetry, novels, movies, songs, computer software, and architecture. Copyright does not protect facts, ideas, systems, or methods of operation, although it may protect the way these things are expressed. See Circular 1, Copyright Basics, section "What Works Are Protected."

How is a copyright different from a patent or a trademark?


Copyright protects original works of authorship, while a patent protects inventions or discoveries. Ideas and discoveries are not protected by the copyright law, although the way in which they are expressed may be. A trademark protects words, phrases, symbols, or designs identifying the source of the goods or services of one party and distinguishing them from those of others.


When is my work protected?


Your work is under copyright protection the moment it is created and fixed in a tangible form that it is perceptible either directly or with the aid of a machine or device.


Do I have to register with your office to be protected?

No. In general, registration is voluntary. Copyright exists from the moment the work is created. You will have to register, however, if you wish to bring a lawsuit for infringement of a U.S. work. See Circular 1, Copyright Basics, section “Copyright Registration.”

Why should I register my work if copyright protection is automatic?


Registration is recommended for a number of reasons. Many choose to register their works because they wish to have the facts of their copyright on the public record and have a certificate of registration. Registered works may be eligible for statutory damages and attorney's fees in successful litigation. Finally, if registration occurs within five years of publication, it is considered prima facie evidence in a court of law. See Circular 1, Copyright Basics, section “Copyright Registration” and Circular 38b, Highlights of Copyright Amendments Contained in the Uruguay Round Agreements Act (URAA), on non-U.S. works.


I’ve heard about a “poor man’s copyright.” What is it?


The practice of sending a copy of your own work to yourself is sometimes called a “poor man’s copyright.” There is no provision in the copyright law regarding any such type of protection, and it is not a substitute for registration.


Is my copyright good in other countries?


The United States has copyright relations with most countries throughout the world, and as a result of these agreements, we honor each other's citizens' copyrights. However, the United States does not have such copyright relationships with every country. For a listing of countries and the nature of their copyright relations with the United States, see Circular 38a, International Copyright Relations of the United States.

# What does copyright protect?


What does copyright protect?

Copyright, a form of intellectual property law, protects original works of authorship including literary, dramatic, musical, and artistic works, such as poetry, novels, movies, songs, computer software, and architecture. Copyright does not protect facts, ideas, systems, or methods of operation, although it may protect the way these things are expressed. See Circular 1, Copyright Basics, section "What Works Are Protected."

Can I copyright my website?


The original authorship appearing on a website may be protected by copyright. This includes writings, artwork, photographs, and other forms of authorship protected by copyright. Procedures for registering the contents of a website may be found in Circular 66, Copyright Registration of Websites and Website Content.


Can I copyright my domain name?


Copyright law does not protect domain names. The Internet Corporation for Assigned Names and Numbers (ICANN), a nonprofit organization that has assumed the responsibility for domain name system management, administers the assigning of domain names through accredited registers.


How do I protect my recipe?


A mere listing of ingredients is not protected under copyright law. However, where a recipe or formula is accompanied by substantial literary expression in the form of an explanation or directions, or when there is a collection of recipes as in a cookbook, there may be a basis for copyright protection. Note that if you have secret ingredients to a recipe that you do not wish to be revealed, you should not submit your recipe for registration, because applications and deposit copies are public records. See Circular 33, Works Not Protected by Copyright.


Can I copyright the name of my band?


No. Names are not protected by copyright law. Some names may be protected under trademark law. Contact the U.S. Patent & Trademark Office, TrademarkAssistanceCenter@uspto.gov or see Circular 34 "Copyright Protection Not Available for Names, Titles, or Short Phrases".


How do I copyright a name, title, slogan, or logo?


Copyright does not protect names, titles, slogans, or short phrases. In some cases, these things may be protected as trademarks. Contact the U.S. Patent & Trademark Office, TrademarkAssistanceCenter@uspto.gov or see Circular 33, for further information. However, copyright protection may be available for logo artwork that contains sufficient authorship. In some circumstances, an artistic logo may also be protected as a trademark.


How do I protect my idea?


Copyright does not protect ideas, concepts, systems, or methods of doing something. You may express your ideas in writing or drawings and claim copyright in your description, but be aware that copyright will not protect the idea itself as revealed in your written or artistic work.


Does my work have to be published to be protected?


Publication is not necessary for copyright protection.


Can I register a diary I found in my grandmother's attic?

You can register copyright in the diary within a certain duration only if you own the rights to the work, for example, by will or by inheritance. Copyright is the right of the author of the work or the author's heirs or assignees, not of the one who only owns or possesses the physical work itself. See Circular 1, Copyright Basics, section “Who Can Claim Copyright.”

How do I protect my sighting of Elvis?


Copyright law does not protect sightings. However, copyright law will protect your photo (or other depiction) of your sighting of Elvis. File your claim to copyright online by means of the electronic Copyright Office (eCO). Pay the fee online and attach a copy of your photo. For more information on registering a copyright, see SL-35. No one can lawfully use your photo of your sighting, although someone else may file his own photo of his sighting. Copyright law protects the original photograph, not the subject of the photograph.


Does copyright protect architecture?

Yes. Architectural works became subject to copyright protection on December 1, 1990. The copyright law defines “architectural work” as “the design of a building embodied in any tangible medium of expression, including a building, architectural plans, or drawings.” Copyright protection extends to any architectural work created on or after December 1, 1990. Also, any architectural works that were unconstructed and embodied in unpublished plans or drawings on that date and were constructed by December 31, 2002, are eligible for protection. Architectural designs embodied in buildings constructed prior to December 1, 1990, are not eligible for copyright protection. See Circular 41, Copyright Claims in Architectural Works

Can I get a star named after me and claim copyright to it?


No. There is a lot of misunderstanding about this. Names are not protected by copyright. Publishers of works such as a star registry may register a claim to copyright in the text of the volume [or book] containing the names the registry has assigned to stars, and perhaps the compilation of data; but such a registration would not extend protection to any of the individual star names appearing therein. Copyright registration of such a volume of star names does not confer any official or governmental status on any of the star names included in the volume. For further information on copyright protection and names, see Circular 33, Works Not Protected by Copyright.

# Who Can Register?


Can foreigners register their works in the United States?

Any work that is protected by U.S. copyright law can be registered. This includes many works of foreign origin. All works that are unpublished, regardless of the nationality of the author, are protected in the United States. Works that are first published in the United States or in a country with which we have a copyright treaty or that are created by a citizen or domiciliary of a country with which we have a copyright treaty are also protected and may therefore be registered with the U.S. Copyright Office. See Circular 38a, International Copyright Relations of the United States, for the status of specific countries, and Circular 38b, Copyright Restoration Under the URAA

Can a minor claim copyright?


Minors may claim copyright, and the Copyright Office issues registrations to minors, but state laws may regulate the business dealings involving copyrights owned by minors. For information on relevant state laws, consult an attorney.


Can I register a diary I found in my grandmother's attic?

You can register copyright in the diary only if you own the rights to the work, for example, by will or by inheritance. Copyright is the right of the author of the work or the author's heirs or assignees, not of the one who only owns or possesses the physical work itself. See Circular 1, Copyright Basics, section “Who Can Claim Copyright.”
# Registering a Work


How do I register my copyright?


To register a work, submit a completed application form, and a nonreturnable copy or copies of the work to be registered. See Circular 1, Copyright Basics, section “Registration Procedures., and Circular 4, Copyright Office Fees”.


Where can I get application forms?


See SL-35.


Can I file online?


Yes. We offer online registration through our electronic Copyright Office (eCO). See SL-35.


What is the registration fee?


See Circular 4, Copyright Fees.


Do you take credit cards?


If you file your application online using eCO eService, you may pay by credit card. Credit cards are not accepted for registration through the mail, but may be used for registrations that are filed in person in the Copyright Office. There are other services for which the Copyright Office will accept a credit card payment. For more information see Circular 4, Copyright Fees.


Do I have to send in my work? Do I get it back?


You must send the required copy or copies of the work to be registered. Your copies will not be returned. If you register online using eCO eService, you may attach an electronic copy of your deposit. However, even if you register online, if the Library of Congress requires a hard-copy deposit of your work, you must send what the Library defines as the “best edition” of your work. For further information, see Circular 7b, Best Edition of Published Copyrighted Works for the Collection of the Library of Congress, and Circular 7d, Mandatory Deposit of Copies or Phonorecords for the Library of Congress. Upon their deposit in the Copyright Office, under sections 407 and 408 of the copyright law, all copies and identifying material, including those deposited in connection with claims that have been refused registration, are the property of the U.S. government.


Will my deposit be damaged by security measures in place on Capitol Hill?

To avoid damage to your hard-copy deposit caused by necessary security measures, package the following items in boxes rather than envelopes for mailing to the Copyright Office: electronic media such as audiocassettes, videocassettes, CDs, and DVDs; microform; photographs; slick advertisements, color photocopies, and other print items

May I register more than one work on the same application?


Generally, you may only register one work per application but there are several exceptions to this rule. Each exception has strict eligibility requirements. If you do not meet or comply with these requirements the Office may contact you, which will delay the registration decision, or the Office may refuse registration. See Circular 34, Multiple Works, for specific information on the exceptions for registering multiple works with one application.


Do I have to use my real name on the form? Can I use a stage name or a pen name?


There is no legal requirement that the author be identified by his or her real name on the application form. For further information, see FL 101, Pseudonyms. If filing under a fictitious name, check the “Pseudonymous” box when giving information about the authors.


Will my personal information be available to the public?


Yes. Please be aware that when you register your claim to a copyright in a work with the U.S. Copyright Office, you are making a public record. All the information you provide on your copyright registration is available to the public and will be available on the Internet.


How long does the registration process take, and when will I receive my certificate?

The time the Copyright Office requires to process an application varies, depending on the number of applications the Office is receiving and clearing at the time of submission and the extent of questions associated with the application. Current Processing Times

Can I submit my manuscript on a computer disk?


No. Floppy disks and other removal media such as Zip disks, except for CD-ROMs, are not acceptable. Therefore, the Copyright Office still generally requires a printed copy or audio recording of the work for deposit. However, if you register online using eCO eService, you may attach an electronic copy of your deposit. However, even if you register online, if the Library of Congress requires a hard-copy deposit of your work, you must send what the Library defines as the "best edition" of your work. For further information, see Circular 7b, Best Edition of Published Copyrighted Works for the Collection of the Library of Congress, and Circular 7d, Mandatory Deposit of Copies or Phonorecords for the Library of Congress.


Can I submit a CD-ROM of my work?


Yes. The deposit requirement consists of the best edition of the CD-ROM package of any work, including the accompanying operating software, instruction manual, and a printed version, if included in the package.


Does my work have to be published to be protected?


Publication is not necessary for copyright protection.


How much do I have to change in my own work to make a new claim of copyright?


You may make a new claim in your work if the changes are substantial and creative, something more than just editorial changes or minor changes. This would qualify as a new derivative work. For instance, simply making spelling corrections throughout a work does not warrant a new registration, but adding an additional chapter would. See Circular 14, Copyright Registration in Derivative Works and Compilations, for further information.


Do you have special mailing requirements?


If you register online, you may attach an electronic copy of your deposit unless a hard-copy deposit is required under the "Best Edition" requirements of the Library of Congress. See Circular 7b. If you file using a paper application, our only requirement is that all three elements—the application, the copy or copies of the work, and the filing fee—be sent in the same package. Please limit any individual box to 20 pounds. Many people send their material to us by certified mail, with a return receipt request, but this is not required.

# Privacy: Copyright Public Records


Can I see my copyright registration records?


Yes. The Copyright Office is required by law to maintain records of copyright registrations and to make them available for public inspection. Once a registration is completed and a claim has been cataloged, it becomes part of the public record. Individuals have always been able to come to the Copyright Office to inspect its public records. Individuals may also request copies of registration records from the Copyright Office’s Records Research and Certification Section. Information from registration records dating from January 1, 1978, is available on the Copyright Office’s website.


Will my registration records help provide contact information for someone interested in using my work?


Records of copyright registrations and documents that are recorded in relationship to them can be used by the public to identify the author(s) and copyright owner(s) of a work. The public record may also provide information about an agent of the owner who can be contacted to license the registered work or to request permission to use it.


Can I remove information that I don't want publicized?


Generally, no. When you register a claim to copyright in a work with the Copyright Office, you create a public record of your claim to copyright. Some information you provide on your copyright registration will be available to the public and on the internet. You may wish to consider whether you want to include a business address or phone number instead of personal contact information, or any other optional detail you consider to be sensitive on your application. Under certain circumstances, authors and claimants, or their authorized representatives, may request the replacement or removal of certain personally identifiable information that is requested by the Office and collected on a registration application, such as a home addresses or personal phone numbers, from the Office’s internet-accessible public catalog, though such information will be retained in the Office’s offline records as required by law. Extraneous personally identifiable information, such as driver’s license numbers, social security numbers, banking information, and credit card information, will be removed on the Office’s own volition or upon request by authors, claimants, or their authorized representatives.

# Preregistration


What is preregistration? What works can be preregistered?

Preregistration which became available in 2005 pursuant to the provisions of the artists' rights and Theft Prevention Act, is a procedure in the Copyright Office for certain classes of works that the Register of Copyrights has determined have a history of pre-release infringement. Preregistration serves as a place-holder for limited purposes, mainly where a copyright owner needs to sue for infringement while a work is still being prepared for commercial release. Preregistration is not a substitute for registration, and its use is only appropriate in certain circumstances. A work submitted for preregistration must meet three conditions: (1)the work must be unpublished; (2) the work must be in the process of being prepared for commercial distribution in either physical or digital format, e.g., film copies, CDs, computer programs to be sold online, and the applicant must have a reasonable expectation of this commercial distribution; (3) the work must fall within the following classes of works determined by the Register of Copyrights to have had a history of infringement prior to authorized commercial distribution. The works determined to be eligible under this requirement are: motion pictures, sound recordings, musical compositions, literary works being prepared for publication in book form, computer programs(which may include videogames), advertising or marketing photographs

What classes of works are eligible for preregistration?

Motion pictures, Sound recordings, Musical compositions, Literary works being prepared for publication in book form, Computer programs (which may include videogames), Advertising or marketing photographs

Is preregistration a substitute for registration?


No. Preregistration is not a form of registration but is simply an indication of an intent to register a work once the work has been completed and/or published. When the work has been completed, it may be registered as an unpublished work, and when it has been published, it may be registered as a published work. Preregistration of a work offers certain advantages to a copyright owner pursuant to 17 U.S.C. 408(f), 411 and 412. However, preregistration of a work does not constitute prima facie evidence of the validity of the copyright or of the facts stated in the application for preregistration or in the preregistration record. The fact that a work has been preregistered does not create any presumption that the Copyright Office will register the work upon submission of an application for registration. A person who has preregistered a work must register the work within one month after the copyright owner becomes aware of infringement and no later than three months after first publication. If full registration is not made within the prescribed time period, a court must dismiss an action for copyright infringement that occurred before or within the first two months after first publication.


Will I need to make a regular registration after my work is completed?


Preregistration is not a form of registration but is simply an indication of an intent to register a work once the work has been completed and/or published. The law requires that if you have preregistered a work, you are required to register the work within one month after the copyright owner becomes aware of infringement and no later than three months after first publication. If full registration is not made within the prescribed time period, a court must dismiss an action for copyright infringement that occurred before or within the first two months after first publication. See 17 U.S.C. 408(f) and 411, as amended; also 37 C.F.R. 202.16, as added.


When should I register my work if I have already preregistered it?


To preserve the legal benefits of preregistration, a person who has preregistered a work is required, to preserve the legal benefits of preregistration, to register the work within one month after the copyright owner becomes aware of infringement and no later than three months after first publication. If full registration is not made within the prescribed time period, a court must dismiss an action for copyright infringement that occurred before or within the first two months after first publication. See U.S.C. 17 408(f) and 411, as amended; also 37 C.F.R. 202.16, as added.


How do I preregister?


You must apply online; no paper application form is available. Only an application and fee are required; a copy or phonorecord of the work itself, or any finished part thereof, should not be submitted. Instead, the applicant must give as full a description of the work as possible in the online application.


What is the effective date of my preregistration?


The effective date is the day on which the completed application and fee for an eligible work have been received in the Copyright Office.

### Preregistration Application and Notification


How do I complete a preregistration application?


The preregistration application is only available online. We recommend that you read the detailed information by clicking here, including the screen-by-screen instructions, before beginning your online application. Much of this information is also provided on the individual application screens. To begin the preregistration process, go to the Preregister Your Work page and click on the Start the Preregistration Process button at the bottom of the page.


You ask for a description in the preregistration application. What should it include?


Your description should be sufficient to reasonably identify the work but should consist of no more than 2,000 characters (approximately 330 words). It need not be detailed and need not include confidential information. This description will be made part of the online public record. (See specific help about the description).


Will I receive a certificate for my preregistration?


No. When the Copyright Office completes your preregistration, we will send you an official notification email containing the information from your application, and the preregistration number and date. This same information will also appear in the Copyright Office permanent online catalog record of the preregistration. A certified copy of the official notification may be obtained from the Certifications and Documents Section of the Copyright Office.

### Preregistration Application and Notification


What is the fee for preregistration?


See Circular 4 - Copyright Office Fees.


What methods of payment are accepted for preregistration?


You may pay the nonrefundable filing fee for your submission(s) with a credit card, by ACH, or by debiting your existing Copyright Office Deposit Account.


What does ACH (payment) mean?


ACH is an acronym for The Automated Clearing House Network. If you choose this option of payment, you may have money transferred electronically from your personal or corporate bank account to make your payment to the Copyright Office. If you choose this method, you will need your bank’s routing number and your bank account number. These usually appear on your check; the routing number is sometimes the one appearing at the bottom left on your check, with your check account number appearing to the right of the routing number. You may give a check number on the online payment screen, but it is not required.

# Which Form Should I Use?


Which form should I use?


Online registration through the electronic Copyright Office (eCO) is the preferred way to register basic claims. Paper versions of Form TX (literary works); Form VA (visual arts works); Form PA (performing arts); Form SR (sound recordings) are available on the Copyright Office website. Staff will send them to you by postal mail upon request. Remember that online registration through eCO can be used for these types of applications.


How do I copyright my business name? Which form do I use?


Names, titles, short phrases, and slogans are not copyrightable. You may have protection under the federal trademark laws. Contact the U.S. Patent & Trademark Office, 800-786-9199, for more information. TrademarkAssistanceCenter@uspto.gov or See Circular 33 Works Not Protected by Copyright .


Which form do I use to register a computer software application I am creating?


See Circular 61, Copyright Registration of Computer Programs for information on registering a computer software application.


I have several copyrights on file with the Copyright Office and have moved since submitting those applications. Is there a form to submit to change the address on my applications?


Although there is no statutory requirement to notify the Copyright Office of a change of address, you may wish to have our records reflect such information. There are several ways to do this: 1) You may file a document listing all your registered works by title and registration number, along with your new address. Your current address will then be included in our records and be available to those who search the documents file. However, the original registration records are not changed. See Circular 12, Recordation of Transfers and Other Documents, for more information. 2) You may file a supplementary registration Form CA to amend a completed registration to indicate the new address. If you have multiple registrations, filing a Form CA only on the most recent registration will effectively get your new address on record. See Circular 8, Supplementary Registration, for more information. 3) Finally, filing a registration for a new work in the future will also effectively get your new address on record. For more information on fees, please refer to Circular 4.

# I've Submitted My Application, Fee, and Copy of My Work to the Copyright Office. Now What?


How can I know when my submission for registration is received by the Copyright Office?


If you apply for copyright registration online, you will receive email confirmations of completed steps in the application process. Otherwise, the Copyright Office does not provide a confirmation of receipt. Currently, if you use a commercial carrier (such as Federal Express, Airborne Express, DHL Worldwide Express, or United Parcel Service), that company may be able to provide an acknowledgment of receipt by the Copyright Office. Due to the mail disruption, an acknowledgment of receipt for mail sent via the U.S. Postal Service, e.g., certified, registered and overnight delivery, may take several weeks or longer to receive. Claims to copyright may also be hand-delivered to the Copyright Office. See About the Office for hours and location.


How long does the registration process take, and when will I receive my certificate?

The time the Copyright Office requires to process an application varies, depending on the number of applications the Office is receiving and clearing at the time of submission and the extent of questions associated with the application. Current Processing Times

I’ve been getting solicitation letters from publishers. Is the Copyright Office selling my personal information?


The Copyright Office does not sell personal information. Copyright Office records, however, are public records, which means anyone may come to our office and inspect them. Occasionally organizations such as music publishers or book publishers send a representative to the Copyright Office to compile lists of names and addresses of those authors who have most recently registered their works. Their purpose, undoubtedly, is to solicit new business. This practice is not a violation of the law.
