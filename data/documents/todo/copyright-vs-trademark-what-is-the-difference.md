Copyright vs. Trademark: What is the Difference?|United States|Intellectual Property and Technology|Copyright, Trademark|copyright, trademark|A good familiarity with the different kinds of intellectual property protection allows anyone to separate which aspects trademark protects and which aspects copyright protects.

The shining painted pupils of the fiberglass princess are fixed on you. As the swing squeaks and your daughter squeals, the familiar princess statue haunts you. Then you see it: she's a faded likeness of Disney's Snow White. She's standing next to an even less recognizable Cinderella. As you gaze around the park, you notice other almost-recognizable characters: a sun-bleached Barney the dinosaur here, a giant Bugs Bunny head there.


The creepiness is entirely lost on your daughter. Whoever owns this park has to be violating some kind of intellectual property rights, but which ones?


To answer this question, it is essential to understand the differences between trademark vs. copyright protection. Both forms of intellectual property protection protect creative works, but they do so from different directions. Copyright and trademark overlap, and learning to unwind those overlapping rights is a great way to explain what each one protects.

## Copyright: the Expression of an Idea


Disney does not own the stories of Snow White or Cinderella. The company could not register a copyright that would prevent anyone from using the underlying story. However, Disney does own the copyright to the animated films it made featuring those characters. What, exactly, then does the copyright to those films protect?


Copyright registration protects the particular creative expression of an idea. The films' producers made a series of choices. They wrote a script, animated sequences, and, importantly, designed the now iconic characters.


Disney can stop someone from copying their film outright, yet copyright goes even further. Disney can prevent others from copying the components of the film as well: the script, the scenes, and the characters.


How far the copyright protection can go depends on the expressiveness of the work. In Snow White's case, Disney made particular creative choices that made these characters iconic: each of the Seven Dwarfs has his own personality; Snow White has her particular hairstyle and blue and yellow dress. Those choices are unique to Disney's version of the story and are part of what's protected by the copyright.


Among the most creative and expressive elements of the film is the character of Snow White that Disney created. So, Disney does not rely only on the copyright to protect her.

## Trademark: an Investment in Goodwill


Over the years, these iconic princesses have become deeply associated with Disney: Their likenesses adorn toys and clothes. Actresses become these princesses in Disney's theme parks. Customers have come to associate these characters with the Disney name. The characters are ambassadors for the company's products.


So, when Disney had to decide between copyright or trademark protection, they went with both. In addition to the copyright, Disney chose to register a trademark—to identify and distinguish Disney as the owner of the goods. Over time, customers have come to associate Disney with these characters well beyond the animated films they came from. This association is a form of goodwill.


Disney sells particular kinds of products that come with certain consumer expectations. Disney sells princess toys to children. Disney princess-branded products provide family-oriented entertainment. They use Snow White, Cinderella, and other princesses to create specific kinds of magical, glamorous Disney princess products.


That business did not create itself. Disney leveraged its copyrighted characters to create a brand that its customers associate with. Now, when using that familiar Snow White on a product, customers know that it is from Disney. Moreover, they know what kind of Disney product it is. Disney, through its trademark registration, can prevent others from using its characters in an attempt to co-opt the goodwill that Disney has worked so hard to create.


And What About Infringement?


To infringe a trademark or copyright, the infringer must trespass on what the intellectual property protects. The infringer must take a particular expression (like Snow White as a character from a film) or co-opt the goodwill built up by the company (Snow White as part of the Disney princess brand).


For the fiberglass statue in the park, the infringement is a bit of both. The playground is taking the decisions that Disney made when designing the characters. Even if the execution is less than perfect, reproducing the look of Disney's character uses a critical part of the animated film without permission. Moreover, the way the playground is using the characters is closely aligned to how Disney uses its princesses.


The test to see if it is too close is likely confusing. A jury would have to determine if the playground's use of Snow White might cause users to think that the playground is in some way associated with Disney. The fiberglass princesses may be a long way from Disneyland, but it may be reasonable to think that the maker of the statue or the owner of the park would have licensed rights from Disney in order to use its characters and trademarks.

## Know the Differences


The use of these characters illustrates how intellectual property rights can overlap. Trademark and copyright can protect the same thing but in very different ways. A good familiarity with the different kinds of intellectual property protection allows anyone to separate which aspects trademark protects and which aspects copyright protects. It also illustrates how careful everyone needs to be with the creative content of others.


UpLaw can help you register a copyright or register a trademark. The process for each begins by completing a simple questionnaire. We'll work with you to assemble your application and file it with the appropriate government agency.
