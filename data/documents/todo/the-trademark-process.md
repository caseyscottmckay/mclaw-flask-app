The Trademark Process|United States|Intellectual Property and Technology|Trademark|trademark process, registration|An overview of a trademark application and maintenance process.

The Trademark Process

# Step 1

## Is a trademark application right for you?


Trademarks, patents, copyrights, domain names, and business name registrations all differ, so it is important to learn whether a trademark is appropriate for you.


A trademark typically protects brand names and logos used on goods and services. A patent protects an invention. A copyright protects an original artistic or literary work. For example, if you invent a new kind of vacuum cleaner, you would apply for a patent to protect the invention itself. You would apply to register a trademark to protect the brand name of the vacuum cleaner. And you might register a copyright for the TV commercial that you use to market the product.


A domain name is part of a web address that links to the internet protocol address (IP address) of a particular website. For example, in the web address "https://www.uspto.gov," the domain name is "uspto.gov." You register your domain name with an accredited domain name registrar, not through the USPTO. A domain name and a trademark differ. A trademark identifies goods or services as being from a particular source. Use of a domain name only as part of a web address does not qualify as source-indicating trademark use, though other prominent use apart from the web address may qualify as trademark use. Registration of a domain name with a domain name registrar does not give you any trademark rights. For example, even if you register a certain domain name with a domain name registrar, you could later be required to surrender if it infringes someone else's trademark rights.


Similarly, use of a business name does not necessarily qualify as trademark use, though other use of a business name as the source of goods or services might qualify it as both a business name and a trademark. Many states and local jurisdictions register business names, either as part of obtaining a certificate to do business or as an assumed name filing. For example, in a state where you will be doing business, you might file documents (typically with a state corporation or state division of corporations) to form a business entity, such as a corporation or limited liability company.


For more information about whether a trademark is right for you, please watch the animated video titled “Basic Facts: Trademarks, Patents, and Copyrights,” part of the Basic Facts About Trademarks video series.

# Step 2

## Get ready to apply

### Selecting a Mark

### Once you determine that the type of protection you need is, in fact, trademark protection, then selecting a mark is the very first step in the overall application/registration process. This must be done with thought and care, because not every mark is registrable with the USPTO. Nor is every mark legally protectable, that is, some marks may not be capable of serving as the basis for a legal claim by the owner seeking to stop others from using a similar mark on related goods or services. Businesses and individuals new to trademarks and the application/registration process often choose a mark for their product or service that may be difficult or even impossible to register and/or protect for various reasons. Before filing a trademark/service mark application, you should consider:

- Whether the mark you want to register is registrable, and

- How difficult it will be to protect your mark based on the strength of the mark selected.


Note in this regard that the USPTO only registers marks. You, as the mark owner, are solely responsible for enforcement.


For more information about selecting a protectable trademark, please watch the animated video titled “Basic Facts: Selecting a Mark,” part of the Basic Facts About Trademarks video series.


For an overview of the most important issues to be aware of before filing a trademark application, please watch the news broadcast-style video titled “Before You File” (video #2 in the Trademark Information Network (TMIN) series).

### Mark Format


You must identify your mark format: a standard character mark, a stylized/design mark, or a sound mark.  For more information about choosing a mark format, please watch the news broadcast-style video titled “Drawing Issues” (video #5 in the TMIN series).

### Identification of Goods and/or Services


It is critical you identify clearly the precise goods and/or services to which the mark will apply.  For more information about properly identifying your goods and services, please watch the news broadcast-style video titled “Goods and Services” (video #6 in the TMIN series).

### Searching


Always search the USPTO database to determine whether anyone is already claiming trademark rights in wording/design that is similar and used on related goods/services through a federal registration.  For more information about conducting a clearance search, please watch the news broadcast-style video titled “Searching” (video #3 in the TMIN series).

### Filing Basis


Before filing an application, you must know what your "basis" for filing is.  For more information about selecting the proper filing basis, please watch the news broadcast-style video titled “Filing Basis Information” (video #7 in the TMIN series).

### Trademark Attorney

### Do I have to use one?


Yes, if you are foreign-domiciled. You must be represented at the USPTO by an attorney who is licensed to practice law in the United States.


No, if you are domiciled in the United States or its territories. Although you are not required to have an attorney, we strongly encourage you to hire a U.S.-licensed attorney who specializes in trademark law to guide you through the application process.


Click for more information about hiring an attorney.


For more information about getting help with you application, including information about the benefits of hiring a private attorney to assist with the process, please watch the animated video titled "Basic Facts: How Do I Get Help With My Application?" part of the Basic Facts About Trademarks video series.

# Step 3

## Prepare and submit your application

### Trademark Application


File the application online through the Trademark Electronic Application System (TEAS). View trademark fee information.


NOTE: The application fee is a processing fee. Not all applications result in registrations. Your fee will not be refunded, even if ultimately no registration issues. All information you submit to the USPTO at any point in the application and/or registration process will become public record, including your name, phone number, e-mail address, and street address.

### Monitoring Application Status


Throughout the entire process, periodically monitor the progress of your application through the Trademark Status and Document Retrieval (TSDR) system. It is important to check the status of your application every 3-4 months after the initial filing of the application, because otherwise you may miss a filing deadline. See checking status for more information about this.

### Applicant Address


It is critical that, as necessary, you update your correspondence address (including your email address if you have authorized the USPTO to communicate with your via email) and update your owner address. Changing the owner's address will not change the correspondence address, and vice versa.


For an overview of the most important issues to be aware of after filing a trademark application, watch the news broadcast-style video titled “After You File” (video #9 in the TMIN series).

# Step 4

## Work with the assigned USPTO examining attorney

### USPTO Reviews Application


After the USPTO determines that you have met the minimum filing requirements, an application serial number is assigned and the application is forwarded to an examining attorney. This may take a number of months. The examining attorney reviews the application to determine whether it complies with all applicable rules and statutes, and includes all required fees. Filing fees will not be refunded, even if the application is later refused registration on legal grounds. A complete review includes a search for conflicting marks and an examination of the written application, the drawing, and any specimen.

### USPTO Issues Letter (Office Action)


If the examining attorney determines that a mark should not be registered, the examining attorney will issue a letter (Office action) to you explaining any substantive reasons for refusal, and any technical or procedural deficiencies in the application. If only minor corrections are required, the examining attorney may contact you by telephone or email (if the applicant has authorized communication by email).

### Applicant Timely Responds to Letter


If the examining attorney sends you an Office action, you, or your attorney, if you have one, must submit a response to the Office action that is received by the USPTO within six (6) months of the issue date of the Office action, or the application will be declared abandoned.


For more information about how to respond to an Office action, please watch the news broadcast-style video titled “Response to Office Action” (video #14 in the TMIN series).


For more information about how to revive an abandoned application, please watch the news broadcast-style video titled “Petitions” (video #11 in the TMIN series).

# Step 5

## Receive approval/denial of your application

### USPTO Publishes Mark


If the examining attorney raises no objections to registration, or if you overcome all objections, the examining attorney will approve the mark for publication in the "Official Gazette," a weekly publication of the USPTO. The USPTO will send you a notice of publication stating the date of publication. After the mark is published in the "Official Gazette," any party who believes it may be damaged by registration of the mark has thirty (30) days from the publication date to file either an opposition to registration or a request to extend the time to oppose. An opposition is similar to a proceeding in a federal court, but is held before the Trademark Trial and Appeal Board (TTAB), an administrative tribunal within the USPTO. If no opposition is filed or if the opposition is unsuccessful, the application enters the next stage of the registration process.  It can take three to four months from the time the notice of publication is sent before the applicant will receive official notice of the next status of the application.  During this time, you should continue to monitor the status of your application through the TSDR system as explained above in Step 3.


For more information about the TTAB, please watch the news broadcast-style video titled “TTAB” (video #13 in the TMIN series).

### Registration Certificate Issues for Applications Based on Use


If the mark is based on use in commerce, a foreign registration, or an extension of protection of an international registration to the United States under Section 66(a), and no party files an opposition or request to extend the time to oppose, the USPTO will register the mark and send the owner a certificate of registration. After the mark registers, the owner of the mark must file specific maintenance documents to keep the registration live.

### Notice of Allowance Issues for Applications Based on an Intent to Use the Mark


If the mark is published based upon the applicant's bona fide intention to use the mark in commerce and no party files either an opposition or request to extend the time to oppose, the USPTO will issue a notice of allowance about eight (8) weeks after the date the mark was published. The applicant then has six (6) months from the date of the notice of allowance to either:

### Use the mark in commerce and submit a statement of use (SOU); or


Request a six (6) month extension of time to file a statement of use (extension request).


A notice of allowance is a written notification from the USPTO that a specific mark has survived the opposition period following publication in the Official Gazette, and has consequently been allowed; it does not mean that the mark has registered yet. Receiving a notice of allowance is another step on the way to registration. Notices of allowance are only issued for applications that have a filing basis of intent to use a mark in commerce under Trademark Act Section 1(b).

### Applicant Files Timely Statement of Use or Extension Request


The applicant has six (6) months from the mailing date of the notice of allowance in which to either file a statement of use (SOU) or file an extension request. More than one extension request may be filed, but a limit exists on the total number of extension requests permitted and the timeframe that they must be submitted within. Please review the additional information for the SOU use and extension request processes.

>> File Statement of Use 
> File Extension Request <<

For more information about submitting a statement of use, please watch the news broadcast-style video titled “Statement of Use” (video #10 in the TMIN series).

### Applicant Does Not File Timely Statement of Use or Extension Request


If the applicant does not file a statement of use or extension request within six (6) months from the date the notice of allowance issued, the application is abandoned (no longer pending/under consideration for approval). To continue the application process, the applicant must file a petition to revive the application within two (2) months of the abandonment date.


For more information about how to revive an abandoned application, please watch the news broadcast-style video titled “Petitions” (video #11 in the TMIN series).

### USPTO Reviews Statement of Use


A statement of use (SOU) must meet minimum filing requirements before an examining attorney fully reviews it. If the SOU does meet the minimum filing requirements, then the examining attorney reviews it to determine whether it is acceptable to permit registration. Submission of an SOU does not guarantee registration. You may not withdraw the SOU and the filing fee(s) will not be refunded, even if the SOU/application is later refused registration on legal grounds. If no refusals or additional requirements are identified, the examining attorney approves the SOU.


If refusals or requirements must still be satisfied, the examining attorney issues you a letter (Office action) stating the refusals/requirements. This is the same process that occurs prior to publication of the mark if the examining attorney determines that legal requirements must be met. The process and timeframes remain the same, except that if issues are ultimately resolved and the statement of use is approved, the USPTO issues a registration within approximately two (2) months. If all issues are not resolved, the application will abandon.

### Applicant’s Response Fails to Overcome All Objections


If your response does not overcome all objections, the examining attorney will issue a final refusal Office action. If you disagree with the final refusal, you may, for an additional fee, appeal the decision to the TTAB.

# Step 6

## Maintain your registration

### Registration Certificate Issues


Within approximately two (2) months after the SOU is approved, the USPTO issues a registration. To keep the registration "live," the registrant must file specific maintenance documents. Failure to make these required filings will result in cancellation and/or expiration of the registration.  If your registration is cancelled or expired, your only option is to file a brand new application and begin the entire process again from the very beginning.  The fact that your mark was previously registered does not guarantee registration when you submit a new application.


For more information about specific registration maintenance documents that must be filed, please watch the news broadcast-style video titled “Post-Registration Issues” (video #12 in the TMIN series).

### Monitoring Registration Status


Even if your mark registers, you should monitor the status of your registration on an annual basis through the Trademark Status and Document Retrieval (TSDR) system. It is particularly important to check the status of your registration after you make any of the filings required to keep your registration alive including between the fifth and sixth year after the registration date and between the ninth and tenth year after the registration date.

### Registrant Address


It is critical that, as necessary, you update your correspondence address (and, where appropriate, your email address) and update your owner address. Please be advised that changing the owner’s address will not change the correspondence address, and vice versa.

### Protecting Your Rights


You are responsible for enforcing your rights if you receive a registration, because the USPTO does not "police" the use of marks. While the USPTO attempts to ensure that no other party receives a federal registration for an identical or similar mark for or as applied to related goods/services, the owner of a registration is responsible for bringing any legal action to stop a party from using an infringing mark.  If a right holder suspects that a registered mark is being infringed upon or may be subject to future infringement, the registered mark can be recorded with U.S. Customs and Border Protection through its e-Recordation application.
