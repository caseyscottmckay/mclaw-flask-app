import os
from dotenv import load_dotenv
import socket
basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    PROFILE =  os.environ.get('PROFILE')

    def get_profile(self=None):
        host = socket.gethostname()
        ip = socket.gethostbyname(host)
        print(host+ip)
        if host =='laptop-l' or host == 'laptop-d':
            print('profile: dev; ip: ' + ip +'; host: '+ host)
            return 'dev'
        elif host == 'ubuntu-8gb-ash-1':
            print('profile: prod; ip: ' + ip +'; host: '+ host)
            return 'prod'
    PROFILE = get_profile()
    ELASTICSEARCH_URL ='http://localhost:9200' or os.environ.get('ELASTICSEARCH_URL_DEV')
    if PROFILE == 'dev':
        STRIPE_PUBLISHABLE_KEY = os.environ.get('STRIPE_TEST_PUBLISHABLE_KEY')
        STRIPE_SECRET_KEY = os.environ.get('STRIPE_TEST_SECRET_KEY')
    elif PROFILE == 'prod':
        STRIPE_PUBLISHABLE_KEY = os.environ.get('STRIPE_LIVE_PUBLISHABLE_KEY')
        STRIPE_SECRET_KEY =os.environ.get('STRIPE_LIVE_SECRET_KEY')
        ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL_PROD')
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-nesdefsddssver-guess'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URI') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    #SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    LOG_TO_STDOUT = os.environ.get('LOG_TO_STDOUT')
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    ADMINS = ['info@mclaw.io']
    ORDERS_EMAIL = ['orders@mclaw.io']
    LANGUAGES = ['en', 'es']
    MS_TRANSLATOR_KEY = os.environ.get('MS_TRANSLATOR_KEY')
    REDIS_URL = os.environ.get('REDIS_URL') or 'redis://'
    POSTS_PER_PAGE = 25
    PUBLIC_DOWNLOAD_FOLDER = os.environ.get('PUBLIC_DOWNLOAD_FOLDER')
    DOCX_TEMPLATE_FOLDER = os.environ.get('DOCX_TEMPLATE_FOLDER')
    RESOURCES = os.environ.get('RESOURCES')
