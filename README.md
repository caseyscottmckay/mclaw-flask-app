

**Set up Environment**
`virtualenv venv`
`pip --no-cache-dir install -r requirements.txt`


**Set up postgreSQL Database**
`sudo -u postgres psql`
`CREATE USER mclawdev WITH PASSWORD 'mclawdevpassword';`
`ALTER ROLE mclawdev SET client_encoding TO 'utf8';`
`ALTER ROLE mclawdev SET default_transaction_isolation TO 'read committed';`
`ALTER ROLE mclawdev SET timezone TO 'UTC';`
`GRANT ALL PRIVILEGES ON DATABASE mclaw_backend TO mclawdev;`

`flask db init`
`flask db migrate`
`flask db upgrade`






##API

###Endpoints



### Crawlers

## Crawlers

`/api/crawlers/articles`